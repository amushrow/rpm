#ifdef BACON
#include "StaticGame.h"
#include "MenuState.h"

char* controllerMenuItems[] = {"KEYBOARD", "STEER LEFT", "STEER RIGHT", "THROTTLE", "BRAKE", "GEAR UP", "GEAR DOWN", "GAMEPAD",
	"PEDAL AXES", "STEERING", "BRAKE", "THROTTLE", "BRAKE/THROTTLE", "GEAR UP", "GEAR DOWN", "STEERING SENSITIVITY",
	"BRAKE SENSITIVITY","THROTTLE SENSITIVITY"};

enum ControllerSetupItems
{
	KB_KEYBOARD,
	KB_STEERLEFT,
	KB_STEERRIGHT,
	KB_THROTTLE,
	KB_BRAKE,
	KB_GEARUP,
	KB_GEARDOWN,
	JOY_GAMEPAD,
	JOY_COMBINED,
	JOY_STEERING,
	JOY_BRAKE,
	JOY_THROTTLE,
	JOY_BRAKETHROTTLE,
	JOY_GEARUP,
	JOY_GEARDOWN,
	JOY_STEERSENS,
	JOY_BRAKESENS,
	JOY_THROTTLESENS,
	MENU_END
};

void MenuState::handleControllerMenu(float timeStep)
{
	if(keyPressed(GB_BACK))
	{
		storeControllerMappings(Game::Keyboard);
		if(Game::Joystick)
			storeControllerMappings(Game::Joystick);

		CreateDirectory("Data", NULL);
		SaveButtonMapData(&Game::StoredButtonMaps[0], Game::StoredButtonMaps.Length(), "Data\\ButtonMaps.bin");

		m_curItem = 0;
	}
	if(keyPressed(GB_ACTION))
	{
		GameButtons button = GB_END;
		switch(m_curItem)
		{
		case KB_KEYBOARD:
			break;
		case KB_STEERLEFT:
			button = GB_STEER_LEFT;
			break;
		case KB_STEERRIGHT:
			button = GB_STEER_RIGHT;
			break;
		case KB_THROTTLE:
			button = GB_THROTTLE;
			break;
		case KB_BRAKE:
			button = GB_BRAKE;
			break;
		case KB_GEARUP:
			button = GB_GEARUP;
			break;
		case KB_GEARDOWN:
			button = GB_GEARDOWN;
			break;
		case JOY_STEERING:
			button = GB_STEERING;
			break;
		case JOY_BRAKE:
			button = GB_BRAKE;
			break;
		case JOY_THROTTLE:
			button = GB_THROTTLE;
			break;
		case JOY_BRAKETHROTTLE:
			button = GB_COMBINED_BRAKE_THROTTLE;
			break;
		case JOY_GEARUP:
			button = GB_GEARUP;
			break;
		case JOY_GEARDOWN:
			button = GB_GEARDOWN;
			break;
		}

		if(button != GB_END)
		{
			m_keyChangeData.button = button;
			if(m_curItem < JOY_GAMEPAD)
			{
				DArray<SKInput::Key> keys = Game::Keyboard->GetKeyAssignments(button);
				if(keys.Length() > 0)
					Game::Keyboard->UnassignKey(keys[0], button);
				m_keyChangeData.device = Game::Keyboard;
				Game::Keyboard->AddKeyPressedEvent(keyDown, this);
			}
			else
			{
				for(int i=0; i<8; i++)
				{
					m_analogueMove[i] = 0.f;
				}
				DArray<SKInput::Key> keys = Game::Joystick->GetKeyAssignments(button);
				if(keys.Length() > 0)
					Game::Joystick->UnassignKey(keys[0], button);
				m_keyChangeData.device = Game::Joystick;
				Game::Joystick->AddKeyPressedEvent(keyDown, this);
			}

			m_assigningKey = true;
		}
	}
			
	if(keyPressed(GB_MENU_RIGHT))
	{
		switch(m_curItem)
		{
		case JOY_GAMEPAD:
			{
				int len = m_gameControllers.Length();
				if(m_controllerId <  len -1)
				{
					m_controllerId++;
					if(Game::Joystick)
					{
						storeControllerMappings(Game::Joystick);
						delete Game::Joystick;
						Game::Joystick = Game::InputDevices->CreateJoystick(m_gameControllers[m_controllerId]);
						loadControllerMappings(Game::Joystick);
					}
				}
			}
			break;

		case JOY_COMBINED:
			m_combinedAxis = !m_combinedAxis;
			break;
		}
	}
	if(keyPressed(GB_MENU_RIGHT))
	{
		switch(m_curItem)
		{
		case JOY_STEERSENS:
			{
				m_steerSens += 0.25f * timeStep;
				m_steerSens = min(1.f, m_steerSens);
			}
			break;

		case JOY_BRAKESENS:
			{
				m_brakeSens += 0.25f * timeStep;
				m_brakeSens = min(1.f, m_brakeSens);
			}
			break;

		case JOY_THROTTLESENS:
			{
				m_throttleSens += 0.25f * timeStep;
				m_throttleSens = min(1.f, m_throttleSens);
			}
			break;
		}
	}

	if(keyPressed(GB_MENU_LEFT))
	{
		switch(m_curItem)
		{
		case JOY_GAMEPAD:
			{
				if(m_controllerId >  0)
				{
					m_controllerId--;
					if(Game::Joystick)
					{
						storeControllerMappings(Game::Joystick);
						delete Game::Joystick;
						Game::Joystick = Game::InputDevices->CreateJoystick(m_gameControllers[m_controllerId]);
						loadControllerMappings(Game::Joystick);
					}
				}
			}
			break;

		case JOY_COMBINED:
			m_combinedAxis = !m_combinedAxis;
			break;
		}
	}

	if(keyPressed(GB_MENU_LEFT))
	{
		switch(m_curItem)
		{
		case JOY_STEERSENS:
			{
				m_steerSens -= 0.25f * timeStep;
				m_steerSens = max(0.f, m_steerSens);
			}
			break;

		case JOY_BRAKESENS:
			{
				m_brakeSens -= 0.25f * timeStep;
				m_brakeSens = max(0.f, m_brakeSens);
			}
			break;

		case JOY_THROTTLESENS:
			{
				m_throttleSens -= 0.25f * timeStep;
				m_throttleSens = max(0.f, m_throttleSens);
			}
			break;
		}
	}

	if(keyPressed( GB_MENU_DOWN ))
	{
		m_curItem++;
		if(m_combinedAxis)
		{
			if(m_curItem == JOY_BRAKE)
				m_curItem = JOY_BRAKETHROTTLE;
		}
		else
		{
			if(m_curItem == JOY_BRAKETHROTTLE)
				m_curItem = JOY_GEARUP;
		}
	}
	if(keyPressed( GB_MENU_UP ))
	{
		m_curItem--;
		if(m_combinedAxis)
		{
			if(m_curItem == JOY_THROTTLE)
				m_curItem = JOY_STEERING;
		}
		else
		{
			if(m_curItem == JOY_BRAKETHROTTLE)
				m_curItem = JOY_THROTTLE;
		}
	}
	if(Game::Joystick)
		m_curItem = max(KB_STEERLEFT, min(MENU_END-1, m_curItem));
	else
		m_curItem = max(KB_STEERLEFT, min(JOY_GAMEPAD-1, m_curItem));

}

void MenuState::drawControllerMenu()
{
	float scale = m_screenResolution.y / 720.f;
	float xPos = (m_screenResolution.x * 0.5f) - (166 * scale);
	float yPos = 232 * scale;
	SKColour col(0.55f, 0.55f, 0.55f);

	SKMath::SKVector start(xPos, yPos + 22*scale, 0);
	start.x = floor(start.x + 0.5f) - 0.5f;
	start.y = floor(start.y + 0.5f);
	SKMath::SKVector end(start);
	end.x += floor( (494 * scale) + 0.5f );

	col.Set(0.4f, 0.4f, 0.4f);
	Game::StandardFont->Draw("CONTROLLER SETUP", 20 * scale, xPos, yPos, TOP_LEFT, col);
			
	col = 0.55f;
	xPos = (m_screenResolution.x * 0.5f) + (20 * scale);
	yPos = 275 * scale;

	char buff[64];
	for(int i=0; i<JOY_GAMEPAD; i++)
	{
		float textPos = xPos;
		float space = 20*scale;
		float vspace = 0.f;
		GameButtons button = GB_END;
		switch(i)
		{
		case KB_KEYBOARD:
			textPos = (m_screenResolution.x * 0.5f) - (84 * scale);
			vspace = 8*scale;
			break;
		case KB_STEERLEFT:
			button = GB_STEER_LEFT;
			break;
		case KB_STEERRIGHT:
			button = GB_STEER_RIGHT;
			break;
		case KB_THROTTLE:
			button = GB_THROTTLE;
			break;
		case KB_BRAKE:
			button = GB_BRAKE;
			break;
		case KB_GEARUP:
			button = GB_GEARUP;
			break;
		case KB_GEARDOWN:
			button = GB_GEARDOWN;
			break;
		}

		if(button == GB_END)
		{
			sprintf_s(buff, 32, "");
		}
		else
		{
			DArray<SKInput::Key> keys = Game::Keyboard->GetKeyAssignments(button);
			if(keys.Length() > 0)
				sprintf_s(buff, 64, "%s", SKInput::GetButtonName((SKInput::Button)keys[0]));
			else if(m_keyChangeData.button == button && m_keyChangeData.device == Game::Keyboard)
				sprintf_s(buff, 64, "--");
			else
				sprintf_s(buff, 64, "Not Set");
		}

		col.Set(0.55f, 0.55f, 0.55f, 1.f);
		Game::StandardFont->Draw(controllerMenuItems[i], 16*scale, textPos, yPos, TOP_RIGHT, col);
		if(m_onSubStage && m_curItem == i)
			col.Set(0.9f, 0.2f, 0.2f, 1.f);
		else
			col.Set(0.55f, 0.55f, 0.55f, 0.5f);
		Game::StandardFont->Draw(buff, 16*scale, textPos + space, yPos, TOP_LEFT, col);
		yPos += 16*scale;
		yPos += vspace;
	}
	yPos += 24*scale;

	if(Game::Joystick)
	{
		for(int i=JOY_GAMEPAD; i<MENU_END; i++)
		{
			float textPos = xPos;
			float space = 20*scale;
			float vspace = 0.f;
			GameButtons button = GB_END;
			switch(i)
			{
			case JOY_GAMEPAD:
				textPos = (m_screenResolution.x * 0.5f) - (91 * scale);
				vspace = 8*scale;
				break;
			case JOY_COMBINED:
				break;
			case JOY_STEERING:
				button = GB_STEERING;
				break;
			case JOY_BRAKE:
				if(m_combinedAxis)
					continue;
				else
					button = GB_BRAKE;
				break;
			case JOY_THROTTLE:
				if(m_combinedAxis)
					continue;
				else
					button = GB_THROTTLE;
				break;
			case JOY_BRAKETHROTTLE:
				if(!m_combinedAxis)
					continue;
				else
					button = GB_COMBINED_BRAKE_THROTTLE;
				break;
			case JOY_GEARUP:
				button = GB_GEARUP;
				break;
			case JOY_GEARDOWN:
				button = GB_GEARDOWN;
				break;
			case JOY_STEERSENS:
				break;
			case JOY_BRAKESENS:
				break;
			case JOY_THROTTLESENS:
				break;
			}

			if(button == GB_END)
			{
				switch(i)
				{
				case JOY_GAMEPAD:
					sprintf_s(buff, 64, "%s", m_gameControllers[m_controllerId].Name);
					break;
				case JOY_COMBINED:
					if(m_combinedAxis)
						sprintf_s(buff, 64, "Combined");
					else
						sprintf_s(buff, 64, "Separate");
					break;
				case JOY_STEERSENS:
					sprintf_s(buff, 64, "%.3f", m_steerSens);
					break;
				case JOY_BRAKESENS:
					sprintf_s(buff, 64, "%.3f", m_brakeSens);
					break;
				case JOY_THROTTLESENS:
					sprintf_s(buff, 64, "%.3f", m_throttleSens);
					break;
				}
			}
			else
			{
				DArray<SKInput::Key> keys = Game::Joystick->GetKeyAssignments(button);
				if(keys.Length() > 0)
					sprintf_s(buff, 64, "%s", SKInput::GetButtonName((SKInput::Button)keys[0]));
				else if(m_keyChangeData.button == button && m_keyChangeData.device == Game::Joystick)
					sprintf_s(buff, 64, "--");
				else
					sprintf_s(buff, 64, "Not Set");

			}

			col.Set(0.55f, 0.55f, 0.55f, 1.f);
			Game::StandardFont->Draw(controllerMenuItems[i], 16*scale, textPos, yPos, TOP_RIGHT, col);
			if(m_onSubStage && m_curItem == i)
				col.Set(0.9f, 0.2f, 0.2f, 1.f);
			else
				col.Set(0.55f, 0.55f, 0.55f, 0.5f);
			Game::StandardFont->Draw(buff, 16*scale, textPos + space, yPos, TOP_LEFT, col);
					
			yPos += 16*scale;
			yPos += vspace;
		}
	}
			

	col = 0.4f;
	for(int i=0; i<2; i++)
	{
		Game::VertexManager->RenderLine(&start, &end, &col);
		start.y += 1;
		end.y += 1;
	}
}

void MenuState::storeControllerMappings(SKInput::SKInputDevice* inputDev)
{
	bool found = false;
	ButtonMapData* data;
	
	const DWORD EMPTY = 0xFFFFFFFF;
	for(unsigned i=0; i<Game::StoredButtonMaps.Length(); i++)
	{
		if(Game::StoredButtonMaps[i].DeviceID == inputDev->GetDeviceID())
		{
			found = true;
			data = &Game::StoredButtonMaps[i];
		}
	}

	if(!found)
	{
		ButtonMapData newData;
		newData.DeviceID = inputDev->GetDeviceID();
		int i = Game::StoredButtonMaps.Add(newData);
		data = &Game::StoredButtonMaps[i];
	}
	
	data->CombinedAxes = m_combinedAxis;
	data->SteerSens = m_steerSens;
	data->BrakeSens = m_brakeSens;
	data->BrakeDir = m_brakeDir;
	data->ThrottleDir = m_throttleDir;
	data->ThrottleSens = m_throttleSens;
	data->Brake = EMPTY;
	data->CombinedThrottleBrake = EMPTY;
	data->GearDown = EMPTY;
	data->GearUp = EMPTY;
	data->Steering = EMPTY;
	data->SteerLeft = EMPTY;
	data->SteerRight = EMPTY;
	data->Throttle = EMPTY;
				
	DArray<SKInput::Key> keys;
	keys = inputDev->GetKeyAssignments(GB_COMBINED_BRAKE_THROTTLE);
	if(keys.Length() > 0)
		data->CombinedThrottleBrake = keys[0];
				
	keys = inputDev->GetKeyAssignments(GB_THROTTLE);
	if(keys.Length() > 0)
		data->Throttle = keys[0];

	keys = inputDev->GetKeyAssignments(GB_BRAKE);
	if(keys.Length() > 0)
		data->Brake = keys[0];
				
	keys = inputDev->GetKeyAssignments(GB_STEERING);
	if(keys.Length() > 0)
		data->Steering = keys[0];

	keys = inputDev->GetKeyAssignments(GB_GEARUP);
	if(keys.Length() > 0)
		data->GearUp = keys[0];
	
	keys = inputDev->GetKeyAssignments(GB_GEARDOWN);
	if(keys.Length() > 0)
		data->GearDown = keys[0];

	keys = inputDev->GetKeyAssignments(GB_STEER_LEFT);
	if(keys.Length() > 0)
		data->SteerLeft = keys[0];

	keys = inputDev->GetKeyAssignments(GB_STEER_RIGHT);
	if(keys.Length() > 0)
		data->SteerRight = keys[0];
}

void MenuState::loadControllerMappings(SKInput::SKInputDevice* inputDev)
{
	bool found = false;
	ButtonMapData* data;
	
	const DWORD EMPTY = 0xFFFFFFFF;
	for(unsigned i=0; i<Game::StoredButtonMaps.Length(); i++)
	{
		if(Game::StoredButtonMaps[i].DeviceID == inputDev->GetDeviceID())
		{
			found = true;
			data = &Game::StoredButtonMaps[i];
		}
	}

	if(found)
	{
		if(inputDev != Game::Keyboard)
		{
			m_throttleSens = data->ThrottleSens;
			m_brakeSens = data->BrakeSens;
			m_steerSens = data->SteerSens;
			m_brakeDir = data->BrakeDir;
			m_throttleDir = data->ThrottleDir;
			m_combinedAxis = data->CombinedAxes;
		}

		DArray<SKInput::Key> keys;
		keys = inputDev->GetKeyAssignments(GB_COMBINED_BRAKE_THROTTLE);
		if(keys.Length() > 0)
			inputDev->UnassignKey(keys[0], GB_COMBINED_BRAKE_THROTTLE);
		if(data->CombinedThrottleBrake != EMPTY)
			inputDev->AssignKey(data->CombinedThrottleBrake, GB_COMBINED_BRAKE_THROTTLE);
				
		keys = inputDev->GetKeyAssignments(GB_THROTTLE);
		if(keys.Length() > 0)
			inputDev->UnassignKey(keys[0], GB_THROTTLE);
		if(data->Throttle != EMPTY)
			inputDev->AssignKey(data->Throttle, GB_THROTTLE);

		keys = inputDev->GetKeyAssignments(GB_BRAKE);
		if(keys.Length() > 0)
			inputDev->UnassignKey(keys[0], GB_BRAKE);
		if(data->Brake != EMPTY)
			inputDev->AssignKey(data->Brake, GB_BRAKE);
				
		keys = inputDev->GetKeyAssignments(GB_STEERING);
		if(keys.Length() > 0)
			inputDev->UnassignKey(keys[0], GB_STEERING);
		if(data->Steering != EMPTY)
			inputDev->AssignKey(data->Steering, GB_STEERING);

		keys = inputDev->GetKeyAssignments(GB_GEARUP);
		if(keys.Length() > 0)
			inputDev->UnassignKey(keys[0], GB_GEARUP);
		if(data->GearUp != EMPTY)
			inputDev->AssignKey(data->GearUp, GB_GEARUP);
	
		keys = inputDev->GetKeyAssignments(GB_GEARDOWN);
		if(keys.Length() > 0)
			inputDev->UnassignKey(keys[0], GB_GEARDOWN);
		if(data->GearDown != EMPTY)
			inputDev->AssignKey(data->GearDown, GB_GEARDOWN);

		keys = inputDev->GetKeyAssignments(GB_STEER_LEFT);
		if(keys.Length() > 0)
			inputDev->UnassignKey(keys[0], GB_STEER_LEFT);
		if(data->SteerLeft != EMPTY)
			inputDev->AssignKey(data->SteerLeft, GB_STEER_LEFT);

		keys = inputDev->GetKeyAssignments(GB_STEER_RIGHT);
		if(keys.Length() > 0)
			inputDev->UnassignKey(keys[0], GB_STEER_RIGHT);
		if(data->SteerRight != EMPTY)
			inputDev->AssignKey(data->SteerRight, GB_STEER_RIGHT);
	}
	else
	{
		m_throttleSens = 1.f;
		m_brakeSens = 1.f;
		m_steerSens = 1.f;
		m_brakeDir = 1.f;
		m_throttleDir = 1.f;
		m_combinedAxis = false;
	}
}
#endif