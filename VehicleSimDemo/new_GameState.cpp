#include "new_RPMDemo.h"
#include "CustomDataLib.h"
#include "Camera.h"
#include "new_CameraTarget.h"
#include "new_States.h"
#include "new_GameState.h"
#include "new_ppflags.h"
#include "new_PressStartState.h"

#include "GameButtons.h"

using namespace TopStates;
using namespace GameStates;
using namespace DataLib;
using namespace SKMath;

WORD GameState::m_fullscreenIndices[6] = {0,1,2,2,3,0};

GameState::GameState() : m_tyreData(4, 1), ReplayBuffer(1024, 1024)
{
	m_scene = NULL;
	m_vehicle = NULL;
	m_trackScene = NULL;
	m_sceneGraph = NULL;
	GameCamera = NULL;
	GameCamTarget = NULL;
	
	DepthRender = NULL;
	EdgeDetect = NULL;
	ToonBlinnPhong = NULL;
	PerlinNoise = NULL;
	ShadowMap = NULL;
	GaussianBlur = NULL;

	ShadowMapHi = NULL;
	ShadowMapMed = NULL;
	ShadowMapLo = NULL;
	ShadowDepthBuffer = NULL;
	ShadowRenderTarget = NULL;
	NormalTex = NULL;
	LineDrawingRenderTarget = NULL;
	NormalRenderTarget = NULL;
	DepthBuffer = NULL;
	Ping = NULL;
	Pong = NULL;
	EngineSound = NULL;

	m_usePrerendered = false;
	m_createPrerendered = false;
	m_active = false;
	m_initialised = false;
	m_enterSubState = false;
	DWORD m_postProcessFlags = 0;

	for(int i=0; i<5; i++) m_vehicleScene[i] = NULL;
	for(int i=0; i<5; i++) m_vehicleRigidBody[i] = NULL;
	for(int i=0; i<5; i++) m_ghostVehicleScene[i] = NULL;
}

//-- Warning: Runs in renderer thread
void GameState::loadResources(LPVOID Args)
{
	GameState* self = reinterpret_cast<GameState*>(Args);
	RPMDemo* Game = RPMDemo::GetInstance();

	self->loadGraphics();
	self->loadPhysics();

	//-- Set up the Joystick
	if(Game->Joystick)
	{
		for(unsigned i=0; i<Game->StoredButtonMaps.Length(); i++)
		{
			if(Game->StoredButtonMaps[i].DeviceID == Game->Joystick->GetDeviceID())
			{
				self->m_controllerIndex = i;
				break;
			}
		}

		self->m_vehicle->SetBrakeSensitivity( Game->StoredButtonMaps[self->m_controllerIndex].BrakeSens );
		self->m_vehicle->SetThrottleSensitivity( Game->StoredButtonMaps[self->m_controllerIndex].ThrottleSens );
		self->m_vehicle->SetSteerSensitivity( Game->StoredButtonMaps[self->m_controllerIndex].SteerSens );
	}
	//--

	//Simulate a couple of seconds so that everything is settled when we first see the car
	self->m_vehicle->SetBraking(1.f);
	Game->PhysicsWorld->Update(2.0f);

	//Put the camera in the right place
	const SKRigidBody::State* state = Game->PhysicsWorld->GetBodyState(self->m_vehicleRigidBody[VS_ELISE_BODY]);
	self->GameCamTarget->Pos = state->Position;
	self->GameCamTarget->Rot = state->Orientation;
	self->GameCamTarget->Mat = state->Transformations;
	self->GameCamera->SetStage(0);
	self->GameCamera->Update(0.f);
	self->GameCamera->SetStage(1);

	SKMath::SKMatrix<float> worldMat;
	const real* bodyMat;
	//Get car position
	for(int i=0; i<5; i++)
	{
		state = Game->PhysicsWorld->GetBodyState(self->m_vehicleRigidBody[i]);

		bodyMat = &state->Transformations._11;
		for(int j=0; j<16; j++)
			worldMat[j] = (float)bodyMat[j];

		self->m_vehicleScene[i]->SetWorldMatrix(worldMat);	
		self->m_vehicleScene[i]->CalcMatrices();
	}

	state = Game->PhysicsWorld->GetBodyState(self->m_vehicleRigidBody[VS_ELISE_BODY]);
	bodyMat = &state->Transformations._11;
	for(int j=0; j<16; j++)
		worldMat[j] = (float)bodyMat[j];
	self->m_vehicleScene[VS_ELISE_SHADOW]->SetWorldMatrix(worldMat);	
	self->m_vehicleScene[VS_ELISE_SHADOW]->CalcMatrices();
	self->m_vehicleScene[VS_ELISE_SHELL]->SetWorldMatrix(worldMat);	
	self->m_vehicleScene[VS_ELISE_SHELL]->CalcMatrices();

	Game->Renderer->CreateEffectFromFile("Resources\\Shaders\\NormalDepth.fx", &self->DepthRender);
	Game->Renderer->CreateEffectFromFile("Resources\\Shaders\\EdgeDetect.fx", &self->EdgeDetect);
	Game->Renderer->CreateEffectFromFile("Resources\\Shaders\\ToonBlinnPhong.fx", &self->ToonBlinnPhong);
	Game->Renderer->CreateEffectFromFile("Resources\\Shaders\\Perlin.fx", &self->PerlinNoise);
	Game->Renderer->CreateEffectFromFile("Resources\\Shaders\\ShadowMap.fx", &self->ShadowMap);
	Game->Renderer->CreateEffectFromFile("Resources\\Shaders\\GaussianBlur.fx", &self->GaussianBlur);
	Game->Renderer->CreateEffectFromFile("Resources\\Shaders\\HighPass.fx", &self->HighPassFilter);
	Game->Renderer->CreateEffectFromFile("Resources\\Shaders\\BloomCombine.fx", &self->BloomCombine);
	Game->Renderer->CreateEffectFromFile("Resources\\Shaders\\Desaturate.fx", &self->Desaturate);
	
	self->DepthRenderParams = self->DepthRender->CreateUserParamsDecleration();
	self->EdgeDetectrParams = self->EdgeDetect->CreateUserParamsDecleration();
	self->ToonPhongParams = self->ToonBlinnPhong->CreateUserParamsDecleration();
	self->PerlinParams = self->PerlinNoise->CreateUserParamsDecleration();
	self->ShadowParams = self->ShadowMap->CreateUserParamsDecleration();
	self->BlurParams = self->GaussianBlur->CreateUserParamsDecleration();
	self->HighPassParams = self->HighPassFilter->CreateUserParamsDecleration();
	self->BloomParams = self->BloomCombine->CreateUserParamsDecleration();
	self->DesaturateParams = self->Desaturate->CreateUserParamsDecleration();

	self->m_globalAlpha = self->ToonBlinnPhong->GetParameterByName("GlobalAlpha");
	self->m_lgtCamPos = self->ToonBlinnPhong->GetParameterByName("CamPos");
	self->m_lgtLightViewProjHi = self->ToonBlinnPhong->GetParameterByName("LightViewProjHi");
	self->m_lgtLightViewProjMed = self->ToonBlinnPhong->GetParameterByName("LightViewProjMed");
	self->m_lgtLightViewProjLo = self->ToonBlinnPhong->GetParameterByName("LightViewProjLo");
	self->m_lgtTexShadowHi = self->ToonBlinnPhong->GetParameterByName("TexShadowHi");
	self->m_lgtTexShadowMed = self->ToonBlinnPhong->GetParameterByName("TexShadowMed");
	self->m_lgtTexShadowLo = self->ToonBlinnPhong->GetParameterByName("TexShadowLo");
	self->m_edgScreenResolution = self->EdgeDetect->GetParameterByName("screenResolution");
	self->m_edgNormalsTex = self->EdgeDetect->GetParameterByName("normalTexture");

	for(int i=0; i<5; i++)
	{
		self->m_ghostVehicleScene[i] = self->m_vehicleScene[i]->Clone();
	}
	
	self->InitialiseGraphicsResources();

	self->m_scene = new Screen(renderScene, 10, self);
	self->m_enterSubState = true;
	self->m_active = true;
}

void GameState::unloadResources(LPVOID Args)
{

}
//--

void GameState::Enter()
{
	RPMDemo* Game = RPMDemo::GetInstance();

	GameCamera = new Camera();
	GameCamTarget = new CamTarget();
	GameCamera->SetTarget( GameCamTarget );
	GameCamera->SetStage(0);
	GameCamera->SetSpeed(3);

	m_camOffsets[0].Set(0, 1.5f, -5);
	m_camOffsets[1].Set(0,1.1f,0.4f);
	m_camOffsets[2].Set(0,0.3f,1.5f);
	m_camLookOffsets[0].Set(0,0.3f,0.5f);
	m_camLookOffsets[1].Set(0, 0.3f, 3);

	GameCamera->SetPosOffset(m_camOffsets[0]);
	GameCamera->SetLookOffset(m_camLookOffsets[0]);

	POINT Res;
	Game->Renderer->GetResolution(&Res);

	memset(m_fullscreenQuad, 0, sizeof(Vertex)*4);
	m_fullscreenQuad[0].x = -0.5f;
	m_fullscreenQuad[0].y = -0.5f;
	m_fullscreenQuad[1].x = Res.x-0.5f;		m_fullscreenQuad[1].tu = 1;
	m_fullscreenQuad[1].y = -0.5f;
	m_fullscreenQuad[2].x = Res.x-0.5f;		m_fullscreenQuad[2].tu = 1;
	m_fullscreenQuad[2].y = Res.y-0.5f;		m_fullscreenQuad[2].tv = 1;
	m_fullscreenQuad[3].x = -0.5f;
	m_fullscreenQuad[3].y = Res.y-0.5f;		m_fullscreenQuad[3].tv = 1;

	m_material = new SKRenderer::Material("WhiteMat");
	m_material->Ambient = 1.f;
	m_material->Diffuse = 1.f;
	m_material->Specular = 0.f;
	m_material->Emissive = 0.f;
	m_material->SpecularExponent = 0.f;

	m_clippingTimer = 0;

	m_doLowShadow = true;
	m_doMedShadow = true;
	m_renderLines = true;
	m_renderCar = true;
	m_renderScene = false;

	PosClassify = BACK;
	CurrentLapTime = 0;
	LapCounter = 1;
	FastestLapTime = 0;
	LapStartTime = 0;
	TimeLap = false;

	EngineSound = new EngineSounds();

	//-- Load some sounds
	DataLib::IFile* file = Game->DataMan->GetFiles().Find("Sounds\\tyres_skid.wav");
	int len = (int)file->GetOriginalLength();
	BYTE* buffer = new BYTE[len];
	DataLib::IStream* stream = Game->DataMan->GetFileStream(file->GetUID());
	int bytesRead = stream->Read(buffer, 0, len);

	m_tyreSkid = Game->AudioDevice->LoadAudio(buffer, bytesRead);

	delete[] buffer;
	stream->Release();

	for(int i=0; i<4; i++)
	{
		TyreSkidSound[i] = m_tyreSkid->CreateInstance();
		TyreSkidSound[i]->SetNumLoops(-1);
	}
	//--

	AddState(S_PRESS_START, new PressStart(this), true);
	AddState(S_GAME_MAIN, new GameSubStates::GameLogic(this));
	AddState(S_GAME_PAUSE, new GameSubStates::GamePause(this));
	AddTransition(S_PRESS_START, S_GAME_MAIN, "StartGame");
	AddTransition(S_GAME_MAIN, S_GAME_PAUSE, "PauseGame");
	AddTransition(S_GAME_PAUSE, S_GAME_MAIN, "ResumeGame");

	Game->Invoke(loadResources, this);
}

void GameState::loadGraphics()
{
	RPMDemo* Game = RPMDemo::GetInstance();
	SKScene* eliseMain = NULL;
	SKScene* wheel = NULL;
	
	loadModel("Elise.x", "Elise", &eliseMain);
	loadModel("Elise_Shadow.x", "Elise_Shadow",  &m_vehicleScene[VS_ELISE_SHADOW]);
	loadModel("Elise_Shell.x", "Elise_Shell", &m_vehicleScene[VS_ELISE_SHELL]);
	loadModel("Donington.x", "Donington", &m_trackScene);
	loadModel("skybox.x", "SkyBox", &SkyBox);

	m_trackScene->CalcMatrices();
	eliseMain->CalcMatrices();
	m_vehicleScene[VS_WHEEL_FL] = eliseMain->FindChild("Wheel_FL");
	m_vehicleScene[VS_WHEEL_FR] = eliseMain->FindChild("Wheel_FR");
	m_vehicleScene[VS_WHEEL_RL] = eliseMain->FindChild("Wheel_RL");
	m_vehicleScene[VS_WHEEL_RR] = eliseMain->FindChild("Wheel_RR");

	//Calculate extentes of each compenent of the track
	SKMath::SKVector<float> vcMin, vcMax;
	m_trackScene->GetExtents(vcMin, vcMax);
	
	m_sceneGraph = new SKSceneGraph();

	SKMath::SKMatrix<float> rotatedMat;
	for(int i=0; i<4; i++)
	{
		if(m_vehicleScene[i])
		{
			m_vehicleScene[i]->GetFinalMatrix(&rotatedMat);
			m_vehicleScene[i]->SetLocalMatrix(rotatedMat);
			m_vehicleScene[i]->Detach();
			m_sceneGraph->AddScene( m_vehicleScene[i] );
		}
	}
	m_sceneGraph->AddScene(eliseMain);
	m_sceneGraph->AddScene(m_trackScene);
	m_vehicleScene[VS_ELISE_BODY] = eliseMain;

	Game->Renderer->SetAmbientLight(0.5f,0.5f,0.5f);
}

void GameState::InitialiseGraphicsResources()
{
	if(!m_initialised)
	{
		RPMDemo* Game = RPMDemo::GetInstance();
		int shadowMapSize = 256;
		if(Game->ShadowResolutionLevel == 1)
			shadowMapSize = 512;
		else if(Game->ShadowResolutionLevel == 2)
			shadowMapSize = 1024;

		SKViewport port = { 0, 0, shadowMapSize, shadowMapSize };
		Game->Renderer->InitialiseStage(0, &port, 2);

		POINT res;
		Game->Renderer->GetResolution(&res);

		MultisampleQuality ms = MS_NONE;
		if(Game->FilteringQuality == 1)
			ms = MSAA_2X;
		else if(Game->FilteringQuality == 2)
			ms = MSAA_4X;

		//-- Shadows
		Game->Renderer->CreateTexture(shadowMapSize, shadowMapSize, 1, true, PF_G16R16, &ShadowMapHi);
		Game->Renderer->CreateTexture(shadowMapSize, shadowMapSize, 1, true, PF_G16R16, &ShadowMapMed);
		Game->Renderer->CreateTexture(shadowMapSize, shadowMapSize, 1, true, PF_G16R16, &ShadowMapLo);
	
		Game->Renderer->CreateDepthStencil(shadowMapSize, shadowMapSize, ms, DF_24S, &ShadowDepthBuffer);
		Game->Renderer->CreateRenderTarget(shadowMapSize, shadowMapSize, ms, PF_G16R16, &ShadowRenderTarget);
		//--

		//-- Everything else
		Game->Renderer->CreateTexture(res.x, res.y, 1, true, PF_A8R8G8B8, &NormalTex);
		Game->Renderer->CreateTexture(res.x, res.y, 1, true, PF_A8R8G8B8, &LineDrawingRenderTarget);
		Game->Renderer->CreateRenderTarget(res.x, res.y, ms, PF_A8R8G8B8, &NormalRenderTarget);
		Game->Renderer->CreateDepthStencil(res.x, res.y, ms, DF_24S, &DepthBuffer);
		Game->Renderer->CreateTexture(res.x, res.y, 1, true, PF_X8R8G8B8, &Ping);
		Game->Renderer->CreateTexture(res.x, res.y, 1, true, PF_X8R8G8B8, &Pong);
		//--

		Pointf displaySize = { (float)Game->VideoSettings.DisplayWidth, (float)Game->VideoSettings.DisplayHeight };
		EdgeDetect->SetConst(m_edgScreenResolution, &displaySize, 8);
		ToonBlinnPhong->SetTexture(m_lgtTexShadowHi, ShadowMapHi);
		ToonBlinnPhong->SetTexture(m_lgtTexShadowMed, ShadowMapMed);
		ToonBlinnPhong->SetTexture(m_lgtTexShadowLo, ShadowMapLo);
		EdgeDetect->SetTexture(m_edgNormalsTex, NormalTex);

		m_fullscreenQuad[1].x = Game->VideoSettings.DisplayWidth-0.5f;
		m_fullscreenQuad[2].x = Game->VideoSettings.DisplayWidth-0.5f;
		m_fullscreenQuad[2].y = Game->VideoSettings.DisplayHeight-0.5f;
		m_fullscreenQuad[3].y = Game->VideoSettings.DisplayHeight-0.5f;

		if(m_usePrerendered)
			m_createPrerendered = true;

		m_initialised = true;
	}
}

void GameState::ClearGraphicsResources()
{
	m_initialised = false;
}

void GameState::loadPhysics()
{
	RPMDemo* Game = RPMDemo::GetInstance();
	Game->PhysicsWorld->Initialise(100, 25);

	//--Load Up Elise Data
	IDataObjectCollection& objects =  Game->DataMan->GetDataObjects();
	IDataType *lotusElise, *startPos, *finishLine;
	int len = objects.Count();
	for(int i=0; i<len; i++)
	{
		int id = objects[i]->GetID();
		switch(id)
		{
		case 1: lotusElise = objects[i]; break;
		case 4: startPos = objects[i]; break;
		case 5: finishLine = objects[i]; break;
		}
	}
	 
	if(lotusElise == NULL || startPos == NULL || finishLine == NULL)
	{
		this->GetParent()->FireEvent("QuitGame");
		return;
	}

	DataLibExporter::VehicleDescription eliseDesc;
	DataLibExporter::CustomLoader::LoadVehicleDescription(lotusElise, &eliseDesc);

	DataLibExporter::PosDir startDesc, finishDesc;
	DataLibExporter::CustomLoader::LoadPosDir(startPos, &startDesc);
	DataLibExporter::CustomLoader::LoadPosDir(finishLine, &finishDesc);
	
	SKMath::SKQuaternion<real> ori; ori.MakeFromEuler(finishDesc.Pitch, finishDesc.Yaw, finishDesc.Roll);
	SKMath::SKVector<real> dir = ori.Rotate(SKMath::SKVector<real>::Forward);
	dir.Normalise();
	m_finishPos = SKMath::SKVector<real>(finishDesc.Position.X, finishDesc.Position.Y, finishDesc.Position.Z);
	m_finishLine.Set(dir, m_finishPos);
	m_lapDir = dir;
	//Wow, I bet that was easier than you thought it'd be. The new data tool rocks eh? With its fancy exporter an all that jazz
	//--

	//--Set World Parameters
	Game->PhysicsWorld->FixedFrequency(150);
	Game->PhysicsWorld->Initialise(100, 300);
	//--

	SKMatrix<real> tensor, localMat;
	SKVector<real> extents;
	SKVector<real> extentsSq;
	SKVector<real> vcMin, vcMax, pos;
	SKVector<real> centerOfMass = SKVector<real>(eliseDesc.Centre_of_Mass.X, eliseDesc.Centre_of_Mass.Y, eliseDesc.Centre_of_Mass.Z);
	tensor.Identity();

	//-- Track Mesh
	DataLib::IFile* file = Game->DataMan->GetFiles().Find("Donington_phy.obj");
	len = (int)file->GetOriginalLength();
	BYTE* buffer = new BYTE[len];
	DataLib::IStream* stream = Game->DataMan->GetFileStream(file->GetUID());
	int bytesRead = stream->Read(buffer, 0, len);

	SKRigidBody** meshBodies;
	int numBods = Game->PhysicsWorld->LoadPhysicsMesh(buffer, bytesRead, &meshBodies);
	delete[] buffer;
	stream->Release();

	m_trackMesh.Reserve(numBods);
	for(int i=0; i<numBods; i++)
	{
		meshBodies[i]->SetInverseMass(0);
		meshBodies[i]->SetFriction(1);
		meshBodies[i]->SetDynamicFriction(0.8f);
		m_trackMesh.Add(meshBodies[i]);
	}
	
	delete[] meshBodies;
	//--

	//-- Elise Body
	extents.Set(eliseDesc.Model.HalfExtents.X, eliseDesc.Model.HalfExtents.Y, eliseDesc.Model.HalfExtents.Z);
	extentsSq.Set(extents.x * extents.x, extents.y * extents.y, extents.z * extents.z);

	m_vehicleRigidBody[VS_ELISE_BODY] = Game->PhysicsWorld->CreateBody(SKPhysics::BOX, MAT_DEFAULT, extents);
	m_vehicleRigidBody[VS_ELISE_BODY]->SetRestitution( 0.3f );
	m_vehicleRigidBody[VS_ELISE_BODY]->SetFriction( 0.3f );
	m_vehicleRigidBody[VS_ELISE_BODY]->SetDynamicFriction( 0.4f );
	m_vehicleRigidBody[VS_ELISE_BODY]->SetCanSleep( false );
	m_vehicleRigidBody[VS_ELISE_BODY]->SetCOM(centerOfMass);

	//TODO: Pull data from GDT
	tensor._11 = 1150;
	tensor._22 = 1200;
	tensor._33 = 300;

	m_vehicleRigidBody[VS_ELISE_BODY]->SetMass( eliseDesc.Mass );
	m_vehicleRigidBody[VS_ELISE_BODY]->SetInertiaTensor( tensor );
	m_vehicle = Game->PhysicsWorld->CreateVehicle(m_vehicleRigidBody[VS_ELISE_BODY]);
	//--

	//-- Set up the wheels
	setupWheel(VS_WHEEL_FL, eliseDesc.FrontLeft, centerOfMass, SKPhysics::WF_STEER);
	setupWheel(VS_WHEEL_FR, eliseDesc.FrontRight, centerOfMass, SKPhysics::WF_STEER);
	setupWheel(VS_WHEEL_RL, eliseDesc.RearLeft, centerOfMass, SKPhysics::WF_DRIVE);
	setupWheel(VS_WHEEL_RR, eliseDesc.RearRight, centerOfMass, SKPhysics::WF_DRIVE);
	//--
	
	real* bb = new real[4];
	bb[0] = eliseDesc.BrakeBalance * 0.5f;
	bb[1] = eliseDesc.BrakeBalance * 0.5f;
	bb[2] = (1.f-eliseDesc.BrakeBalance) * 0.5f;
	bb[3] = (1.f-eliseDesc.BrakeBalance) * 0.5f;
	m_vehicle->SetBrakeBalance(bb, 4);
	delete[] bb;

	m_vehicle->SetBrakeStrength(eliseDesc.BrakeForce);
	m_vehicle->SetEngineEfficiency(eliseDesc.EngineEfficiency);
	m_vehicle->SetMaxSteeringAngle(eliseDesc.MaxSteeringAngle);
	m_vehicle->SetAckermanValue(eliseDesc.AckermanValue);
	m_vehicleRigidBody[VS_ELISE_BODY]->SetDragCoefficient(eliseDesc.DragCoefficient);
	
	if(eliseDesc.Gears.CanReverse)
		m_vehicle->SetGearRatio(G_REVERSE, eliseDesc.Gears.Reverse);
	else
		m_vehicle->SetGearRatio(G_REVERSE, 0.f);

	m_vehicle->SetGearRatio(G_FIRST, eliseDesc.Gears.First);
	m_vehicle->SetGearRatio(G_SECOND, eliseDesc.Gears.Second);
	m_vehicle->SetGearRatio(G_THIRD, eliseDesc.Gears.Third);
	m_vehicle->SetGearRatio(G_FOURTH, eliseDesc.Gears.Fourth);
	m_vehicle->SetGearRatio(G_FIFTH, eliseDesc.Gears.Fifth);
	m_vehicle->SetGearRatio(G_SIXTH, eliseDesc.Gears.Sixth);
	m_vehicle->SetGearRatio(G_FINALDRIVE, eliseDesc.Gears.FinalDrive);
	m_vehicle->SetTopGear((Gear)eliseDesc.Gears.NumGears);
	m_vehicle->SetEngineTorqueCurve(*reinterpret_cast<const SKMath::CubicSpline<real>*>(eliseDesc.TorqueCurve));
	m_vehicle->AddDifferential(WHEEL_RL,WHEEL_RR, DIFF_OPEN);

	ori.MakeFromEuler(startDesc.Pitch, startDesc.Yaw, startDesc.Roll);
	m_vehicle->SetOrientation(ori);
	m_vehStartPos = SKMath::SKVector<real>(startDesc.Position.X, startDesc.Position.Y, startDesc.Position.Z);
	m_vehicle->SetPosition(m_vehStartPos);
	m_vehStartDir = ori;

	TyreData fricData;
	fricData.LoadSensitivity = reinterpret_cast<const SKMath::CubicSpline<real>*>(eliseDesc.Friction.TyreLoadSensitivity);
	fricData.LonAccCurve = reinterpret_cast<const SKMath::CubicSpline<real>*>(eliseDesc.Friction.SlipRatioAcc);
	fricData.LonDecCurve = reinterpret_cast<const SKMath::CubicSpline<real>*>(eliseDesc.Friction.SlipRatioDec);
	fricData.SlipAngle = reinterpret_cast<const SKMath::CubicSpline<real>*>(eliseDesc.Friction.SlipAngle);
	fricData.SlipVelocity = reinterpret_cast<const SKMath::CubicSpline<real>*>(eliseDesc.Friction.SlipVelocityDropOff);
	fricData.MaxFriction = eliseDesc.Friction.MaxFriction;

	//-- Store the wheel data
	fricData.WheelRadius = eliseDesc.FrontLeft.Diameter * 0.5f;
	m_tyreData.Add(fricData);
	fricData.WheelRadius = eliseDesc.FrontRight.Diameter * 0.5f;
	m_tyreData.Add(fricData);
	fricData.WheelRadius = eliseDesc.RearLeft.Diameter * 0.5f;
	m_tyreData.Add(fricData);
	fricData.WheelRadius = eliseDesc.RearRight.Diameter * 0.5f;
	m_tyreData.Add(fricData);
	//--

	Game->PhysicsWorld->AddFrictionFunc(MAT_TYRE, MAT_ROAD, tyreFriction, this);
}

void GameState::setupWheel(VehicleScenes WheelID, const DataLibExporter::WheelData& WheelData, const SKVector<real>& bodyCOM, SKPhysics::WheelFlags Flags)
{
	RPMDemo* Game = RPMDemo::GetInstance();
	SKVector<real> extents(WheelData.Diameter*0.5f, WheelData.Width*0.5f, 0);
	SKVector<real> extentsSq(extents.x * extents.x, extents.y * extents.y, 0);

	SKMatrix<real> inertiaTensor;
	inertiaTensor._11 = 0.5f * WheelData.Mass * extentsSq.x;
	inertiaTensor._22 = (0.083f * WheelData.Mass * extentsSq.y) + (0.25f * WheelData.Mass * extentsSq.x);
	inertiaTensor._33 = (0.083f * WheelData.Mass * extentsSq.y) + (0.25f * WheelData.Mass * extentsSq.x);
	inertiaTensor._44 = 1;

	m_vehicleRigidBody[WheelID] = Game->PhysicsWorld->CreateBody(SKPhysics::SPHERE_SLICE, MAT_TYRE, extents, SKMath::SKVector<real>::Right);
	m_vehicleRigidBody[WheelID]->SetMass(WheelData.Mass);
	m_vehicleRigidBody[WheelID]->SetInertiaTensor(inertiaTensor);
	
	SKMatrix<float> localMat;
	SKVector<real> wheelPosition;
	m_vehicleScene[WheelID]->GetLocalMatrix(&localMat);
	wheelPosition.Set(localMat._row4.x, localMat._row4.y, localMat._row4.z);
	wheelPosition.y -= WheelData.RideHeightAdjustment;
	localMat._row4.Set(0,0,0,1);
	m_vehicleScene[WheelID]->SetLocalMatrix(localMat);
	
	SKVector<real> attachOffset = SKMath::SKVector<real>(WheelData.AttachPoint.X, WheelData.AttachPoint.Y, WheelData.AttachPoint.Z);
	m_vehicleRigidBody[WheelID]->SetPositionWithoutCom(wheelPosition+attachOffset);
	m_vehicleRigidBody[WheelID]->SetRestitution(0.0f);
	m_vehicleRigidBody[WheelID]->SetAngularDamping(1.f);
	m_vehicleRigidBody[WheelID]->SetLinearDamping(1.f);
	m_vehicleRigidBody[WheelID]->SetCanSleep(false);

	SKVector<real> deltaArm(WheelData.LCA_Length, 0, 0);
	SKVector<real> deltaSpring(0, 0.2f+WheelData.RideHeightAdjustment, 0);
	wheelPosition -= bodyCOM;
	m_vehicle->SetWheel(	(VehicleWheel)WheelID, 
							Flags,
							m_vehicleRigidBody[WheelID],
							attachOffset,
							wheelPosition+deltaArm+attachOffset,
							wheelPosition+deltaSpring,
							SKMath::SKVector<real>::Right,
							SKMath::SKVector<real>::Forward);

	m_vehicle->SetArmLength((VehicleWheel)WheelID, SKMath::FAbs(WheelData.LCA_Length));
	m_vehicle->SetSuspensionLength((VehicleWheel)WheelID, WheelData.SuspensionRestLen, WheelData.MaxSuspensionLen, WheelData.MinSuspensionLen);
	m_vehicle->SetSpringStiffness((VehicleWheel)WheelID, WheelData.SuspensionStrength);
	m_vehicle->SetSpringDamping((VehicleWheel)WheelID, WheelData.SuspensionDamping);
	m_vehicle->SetCastor((VehicleWheel)WheelID, WheelData.Castor);
	
}

void GameState::loadModel(LPCSTR FileName, LPCSTR InternalName, SKScene** out_scene)
{
	RPMDemo* Game = RPMDemo::GetInstance();
	DataLib::IFile* file = Game->DataMan->GetFiles().Find(FileName);
	int len = (int)file->GetOriginalLength();
	BYTE* buffer = new BYTE[len];
	DataLib::IStream* stream = Game->DataMan->GetFileStream(file->GetUID());
	int bytesRead = stream->Read(buffer, 0, len);

	if( FAILED(Game->Renderer->LoadModelHierarchy(buffer, bytesRead, InternalName, out_scene, NULL)))
	{
		this->GetParent()->FireEvent("QuitGame");
		return;
	}

	delete[] buffer;
	stream->Release();
}

void GameState::Leave()
{
	Machine::Leave();
	RPMDemo* Game = RPMDemo::GetInstance();
	Game->Renderer->DestroyModelHierarchy(&m_trackScene);
	Game->Renderer->DestroyEffect(&DepthRender);
	Game->Renderer->DestroyEffect(&EdgeDetect);
	Game->Renderer->DestroyEffect(&ToonBlinnPhong);
	Game->Renderer->DestroyEffect(&PerlinNoise);
	Game->Renderer->DestroyEffect(&ShadowMap);
	Game->Renderer->DestroyEffect(&GaussianBlur);

	Game->RemoveScreen(m_scene);
	delete m_scene;
	delete EngineSound;

	for(int i=0; i<4; i++)
	{
		TyreSkidSound[i]->Release();
		TyreSkidSound[i] = NULL;
	}
	m_tyreSkid->Release();
	m_tyreSkid = NULL;
}

void GameState::Run()
{
	RPMDemo* Game = RPMDemo::GetInstance();

	if(m_enterSubState)
	{
		Machine::Enter();
		m_enterSubState = false;
	}
	else if(m_active)
	{
		m_clippingTimer += Game->TimeStep;

		if(m_clippingTimer > 0.25f)
		{
			m_doMedShadow = true;
			m_doLowShadow = true;
			m_clippingTimer = 0;
			SKMath::SKMatrix<float> projMat, viewProjMat;
			SKMath::SKVector<float> from, to, up;
			GameCamera->GetDeviceLookat(&from, &to, &up);
			Game->Renderer->CalcPerspProjMatrix(0.82f, (float)Game->VideoSettings.DisplayHeight/Game->VideoSettings.DisplayWidth, 2, 500, &projMat);
			Game->Renderer->CalcViewMatrix(from, to, up, &viewProjMat);
			viewProjMat *= projMat;

			m_culledTrack.Clear();
		
			SceneList children = m_trackScene->GetChildren()[0]->GetChildren();
			int len = children.Length();
			SKMath::SKVector<float> vcMin, vcMax, point[8];
		
			for(int i=0; i<len; i++)
			{
				children[i]->GetExtents(vcMin, vcMax);
			
				point[0].Set(vcMin.x, vcMin.y, vcMin.z);
				point[1].Set(vcMin.x, vcMax.y, vcMax.z);
				point[2].Set(vcMin.x, vcMin.y, vcMax.z);
				point[3].Set(vcMin.x, vcMax.y, vcMin.z);
				point[4].Set(vcMax.x, vcMin.y, vcMin.z);
				point[5].Set(vcMax.x, vcMax.y, vcMin.z);
				point[6].Set(vcMax.x, vcMin.y, vcMax.z);
				point[7].Set(vcMax.x, vcMax.y, vcMax.z);

				int x = 0, y = 0 , z = 0;
				for(int j=0; j<8; j++)
					point[j] *= viewProjMat;
			
				for(int j=0; j<8; j++)
				{
					if(point[j].x > 1) x += 1;
					if(point[j].x < -1) x -= 1;
					if(point[j].y > 1) y += 1;
					if(point[j].y < -1) y -= 1;
					if(point[j].z > 1) z += 1;
					if(point[j].z < 0) z -= 1;
				}

				bool excluded = (x == 8 || x == -8) | (y == 8 || y == -8) | (z == 8 || z == -8);
				if(!excluded)
				{
					m_culledTrack.Add(children[i]);
				}
			}
		}

		Machine::Run();
	}
}

void GameState::renderScene(SKRenderer::SKRenderDevice* renderDevice, void* user_data)
{
	static SKMath::SKVector<float> posOffset(0.22808579f, 0.76028591f, -0.60822874f);
	SKMath::SKVector<float> pos, lookat, up, offset;
	GameState* self = reinterpret_cast<GameState*>(user_data);
	RPMDemo* Game = RPMDemo::GetInstance();
	
	if(self->m_createPrerendered || !self->m_usePrerendered)
	{
		self->GameCamera->GetDeviceLookat(&pos, &lookat, &up);
		renderDevice->EnableLighting(true);
		renderDevice->UseShaders(true);
	
		//-- Render Depth information [Shadow Map]
		renderDevice->SetEffect(self->ShadowMap, 0, &self->ShadowParams);
		renderDevice->SetRenderTarget(0, self->ShadowRenderTarget, self->ShadowDepthBuffer);
		renderDevice->SetClearColour(1,1,1,1);
		renderDevice->SetMode(EMD_ORTHOGONAL, 2);
			
		SKScene* eliseBody = self->GetVehicleScene(VS_ELISE_SHADOW);
		SKScene* wheel_fl = self->GetVehicleScene(VS_WHEEL_FL);
		SKScene* wheel_fr = self->GetVehicleScene(VS_WHEEL_FR);
		SKScene* wheel_rl = self->GetVehicleScene(VS_WHEEL_RL);
		SKScene* wheel_rr = self->GetVehicleScene(VS_WHEEL_RR);

		// Low Res
		if(self->m_doLowShadow)
		{
			self->m_doLowShadow = false;
			offset.Set(0,0,65);
			lookat = self->GameCamera->GetPos() + self->GameCamera->GetOrientation().Rotate(offset);
			lookat.y -= 2;
			pos = lookat + posOffset*47;
			renderDevice->SetClippingPlanes(2.5f, 95);
			renderDevice->SetViewLookAt(pos, lookat, up);
			renderDevice->SetOrthoScale(100, 2);

			renderDevice->Clear(true, true, false);
			renderDevice->BeginEffect();
			renderDevice->StartEffectPass(0);
			for(UINT i=0; i<self->m_culledTrack.Length(); i++)
				renderDevice->Render(self->m_culledTrack[i]);
			
			renderDevice->EndEffectPass();
			renderDevice->EndEffect();

			renderDevice->CopySurface(self->ShadowRenderTarget, self->ShadowMapLo);
			renderDevice->SetEffect(self->ToonBlinnPhong, 0, &self->ToonPhongParams);
			renderDevice->SetShaderConst(self->m_lgtLightViewProjLo, ED_SK_VIEWPROJ);
			renderDevice->SetEffect(self->ShadowMap, 0, &self->ShadowParams);
		}

		// Med Res
		if(self->m_doMedShadow)
		{
			self->m_doMedShadow = false;
			offset.Set(0,0,20);
			lookat = self->GameCamera->GetPos() + self->GameCamera->GetOrientation().Rotate(offset);
			lookat.y -= 2;
			pos = lookat + posOffset*20;
			renderDevice->SetClippingPlanes(2.5f, 46);
			renderDevice->SetViewLookAt(pos, lookat, up);
			renderDevice->SetOrthoScale(30, 2);

			renderDevice->Clear(true, true, false);
			renderDevice->BeginEffect();
			renderDevice->StartEffectPass(0);
			for(UINT i=0; i<self->m_culledTrack.Length(); i++)
				renderDevice->Render(self->m_culledTrack[i]);

			renderDevice->EndEffectPass();
			renderDevice->EndEffect();

			renderDevice->CopySurface(self->ShadowRenderTarget, self->ShadowMapMed);
			renderDevice->SetEffect(self->ToonBlinnPhong, 0, &self->ToonPhongParams);
			renderDevice->SetShaderConst(self->m_lgtLightViewProjMed, ED_SK_VIEWPROJ);
			renderDevice->SetEffect(self->ShadowMap, 0, &self->ShadowParams);
		}
		
		// Hight Res
		offset.Set(0,0,4);
		lookat = self->GameCamera->GetPos() + self->GameCamera->GetOrientation().Rotate(offset);
		lookat.y -= 2;
		pos = lookat + posOffset*10;
		renderDevice->SetClippingPlanes(2.5f, 13);
		renderDevice->SetViewLookAt(pos, lookat, up);
		renderDevice->SetOrthoScale(5, 2);

		renderDevice->Clear(true, true, false);
		renderDevice->BeginEffect();
		renderDevice->StartEffectPass(0);
		for(UINT i=0; i<self->m_culledTrack.Length(); i++)
				renderDevice->Render(self->m_culledTrack[i]);
		self->renderScene(eliseBody, renderDevice);
		self->renderScene(wheel_fl, renderDevice);
		self->renderScene(wheel_fr, renderDevice);
		self->renderScene(wheel_rl, renderDevice);
		self->renderScene(wheel_rr, renderDevice);
		renderDevice->EndEffectPass();
		renderDevice->EndEffect();

		renderDevice->CopySurface(self->ShadowRenderTarget, self->ShadowMapHi);
		renderDevice->SetEffect(self->ToonBlinnPhong, 0, &self->ToonPhongParams);
		renderDevice->SetShaderConst(self->m_lgtLightViewProjHi, ED_SK_VIEWPROJ);
		//--

		eliseBody = self->GetVehicleScene(VS_ELISE_BODY);
		//-- Render Scene
		float speed = (float)self->m_vehicleRigidBody[VS_ELISE_BODY]->GetVelocity().GetSqrLength();
		float FOV = min(1, max(0, (speed-324)/2401));
		FOV = 0.82f + 0.15f*FOV;
		renderDevice->InitialiseStage(FOV, NULL, 0);
		renderDevice->RestoreRenderTarget();
		renderDevice->RestoreDepthStencil();

		renderDevice->SetClippingPlanes(1, 300);
		renderDevice->SetMode(EMD_PERSPECTIVE, 0);
		self->GameCamera->GetDeviceLookat(&pos, &lookat, &up);
		renderDevice->SetViewLookAt(pos, lookat, up);

		renderDevice->EnableLighting(false);
		renderDevice->UseShaders(false);
		renderDevice->SetDepthBufferMode(RS_DEPTH_NONE);
		renderDevice->SetTextureStage(0, RS_TEX_CLAMP);
		SKMath::SKMatrix<float> mat; mat.Identity(); mat._row4 = pos;
		self->SkyBox->SetWorldMatrix(mat);
		self->SkyBox->CalcMatrices();
		self->renderScene(self->SkyBox, renderDevice);
		renderDevice->SetDepthBufferMode(RS_DEPTH_READWRITE);
		renderDevice->SetTextureStage(0, RS_TEX_WRAP);

		renderDevice->EnableLighting(true);
		renderDevice->UseShaders(true);

		renderDevice->SetWorldTransform(NULL);

		float alpha = 1;
		renderDevice->SetShaderConst(self->m_globalAlpha, &alpha, 4);
		renderDevice->SetShaderConst(self->m_lgtCamPos, &pos, 16);
	
		renderDevice->BeginEffect();
		renderDevice->StartEffectPass(0);
		for(UINT i=0; i<self->m_culledTrack.Length(); i++)
			renderDevice->Render(self->m_culledTrack[i]);
		if(self->m_renderCar)
		{
			self->renderScene(eliseBody, renderDevice);
			self->renderScene(wheel_fl, renderDevice);
			self->renderScene(wheel_fr, renderDevice);
			self->renderScene(wheel_rl, renderDevice);
			self->renderScene(wheel_rr, renderDevice);
		}
		renderDevice->EndEffectPass();
		renderDevice->EndEffect();

		if(self->FastestLapTime > 0)
		{
			//-- Ghost Car
			alpha = 0.2f;
			renderDevice->SetShaderConst(self->m_globalAlpha, &alpha, 4);
			renderDevice->BeginEffect();
			renderDevice->StartEffectPass(0);
			self->renderScene(self->m_ghostVehicleScene[VS_ELISE_BODY], renderDevice);
			self->renderScene(self->m_ghostVehicleScene[VS_WHEEL_FL], renderDevice);
			self->renderScene(self->m_ghostVehicleScene[VS_WHEEL_FR], renderDevice);
			self->renderScene(self->m_ghostVehicleScene[VS_WHEEL_RL], renderDevice);
			self->renderScene(self->m_ghostVehicleScene[VS_WHEEL_RR], renderDevice);
			renderDevice->EndEffectPass();
			renderDevice->EndEffect();
			//--
		}
		//--

		//-- Render Depth information [Edge Detection]
		if(self->m_renderLines && self->m_renderCar)
		{
			eliseBody = self->GetVehicleScene(VS_ELISE_SHELL);
			renderDevice->SetEffect(self->DepthRender, 0, &self->DepthRenderParams);
			renderDevice->SetRenderTarget(0, self->NormalRenderTarget, self->DepthBuffer);
			renderDevice->SetClearColour(0,0,0,0);
			renderDevice->Clear(true, true, false);
			renderDevice->BeginEffect();
			renderDevice->StartEffectPass(0);

			self->renderScene(eliseBody, renderDevice);
			self->renderScene(wheel_fl, renderDevice);
			self->renderScene(wheel_fr, renderDevice);
			self->renderScene(wheel_rl, renderDevice);
			self->renderScene(wheel_rr, renderDevice);

			renderDevice->EndEffectPass();
			renderDevice->EndEffect();
			renderDevice->CopySurface(self->NormalRenderTarget, self->NormalTex);
			//--

			//-- Render Line Drawing
			renderDevice->RestoreRenderTarget();
			renderDevice->RestoreDepthStencil();
			renderDevice->SetDepthBufferMode(RS_DEPTH_NONE);
			if(Game->FilteringQuality > 0)
				renderDevice->SetEffect(self->EdgeDetect, 0, &self->EdgeDetectrParams);
			else
				renderDevice->SetEffect(self->EdgeDetect, 1, &self->EdgeDetectrParams);
			renderDevice->SetMode(EMD_TWOD, 1);
			renderDevice->SetWorldTransform(NULL);
			renderDevice->SetRenderTarget(0, self->LineDrawingRenderTarget, NULL);
			renderDevice->SetClearColour(0,0,0,0);
			renderDevice->Clear(true, false, false);
	
			renderDevice->BeginEffect();
			renderDevice->StartEffectPass(0);
			renderDevice->Render(VID_UU, self->m_fullscreenQuad, 4, self->m_fullscreenIndices, 6, self->m_material);
			renderDevice->EndEffectPass();
			renderDevice->EndEffect();
			renderDevice->SetDepthBufferMode(RS_DEPTH_READWRITE);
			//--
		}
		else
		{
			renderDevice->SetMode(EMD_TWOD, 1);
			renderDevice->SetWorldTransform(NULL);
		}

		renderDevice->RestoreRenderTarget();
		renderDevice->RestoreDepthStencil();
		renderDevice->CopySurface((RenderTarget*)NULL, self->Ping);
		renderDevice->SetRenderTarget(0, self->Pong, NULL);
		renderDevice->SetEffect(self->HighPassFilter, 0, &self->HighPassParams);
		self->m_material->DiffuseTexture = self->Ping;
		renderDevice->BeginEffect();
		renderDevice->StartEffectPass(0);
		renderDevice->Render(VID_UU, self->m_fullscreenQuad, 4, self->m_fullscreenIndices, 6, self->m_material);
		renderDevice->EndEffectPass();
		renderDevice->EndEffect();

		renderDevice->SetEffect(self->GaussianBlur, 2, &self->BlurParams);
		renderDevice->BeginEffect();
		renderDevice->StartEffectPass(0);
		renderDevice->SetRenderTarget(0, self->Ping, NULL);
		self->m_material->DiffuseTexture = self->Pong;
		renderDevice->Render(VID_UU, self->m_fullscreenQuad, 4, self->m_fullscreenIndices, 6, self->m_material);
		renderDevice->EndEffectPass();
		renderDevice->StartEffectPass(1);
		renderDevice->SetRenderTarget(0, self->Pong, NULL);
		self->m_material->DiffuseTexture = self->Ping;
		renderDevice->Render(VID_UU, self->m_fullscreenQuad, 4, self->m_fullscreenIndices, 6, self->m_material);
		renderDevice->EndEffectPass();
		renderDevice->EndEffect();

	
		renderDevice->RestoreRenderTarget();
		renderDevice->RestoreDepthStencil();
		renderDevice->UseShaders(false);
		renderDevice->SetMode(EMD_TWOD, 1);
	
		renderDevice->UseAdditiveBlending(true);
		self->m_material->DiffuseTexture = self->Pong;
		renderDevice->Render(VID_UU, self->m_fullscreenQuad, 4, self->m_fullscreenIndices, 6, self->m_material);
		renderDevice->UseAdditiveBlending(false);

		if(self->m_renderLines)
		{
			self->m_material->DiffuseTexture = self->LineDrawingRenderTarget;
			renderDevice->Render(VID_UU, self->m_fullscreenQuad, 4, self->m_fullscreenIndices, 6, self->m_material);
		}
	
		renderDevice->SetClearColour(1,1,1);

		if(self->m_createPrerendered)
		{
			renderDevice->CopySurface((RenderTarget*)0, self->Pong);

			if(self->m_postProcessFlags & PP_BLUR)
			{
				renderDevice->UseShaders(true);
				renderDevice->SetEffect(self->GaussianBlur, 2, &self->BlurParams);
				renderDevice->BeginEffect();
				renderDevice->StartEffectPass(0);
				renderDevice->SetRenderTarget(0, self->Ping, NULL);
				self->m_material->DiffuseTexture = self->Pong;
				renderDevice->Render(VID_UU, self->m_fullscreenQuad, 4, self->m_fullscreenIndices, 6, self->m_material);
				renderDevice->EndEffectPass();
				renderDevice->StartEffectPass(1);
				renderDevice->SetRenderTarget(0, self->Pong, NULL);
				self->m_material->DiffuseTexture = self->Ping;
				renderDevice->Render(VID_UU, self->m_fullscreenQuad, 4, self->m_fullscreenIndices, 6, self->m_material);
				renderDevice->EndEffectPass();
				renderDevice->EndEffect();
			}

			if(self->m_postProcessFlags & PP_DESATURATE)
			{
				renderDevice->UseShaders(true);
				renderDevice->SetEffect(self->Desaturate, 0, &self->DesaturateParams);
				renderDevice->BeginEffect();
				renderDevice->StartEffectPass(0);
				renderDevice->SetRenderTarget(0, self->Ping, NULL);
				self->m_material->DiffuseTexture = self->Pong;
				renderDevice->Render(VID_UU, self->m_fullscreenQuad, 4, self->m_fullscreenIndices, 6, self->m_material);
				renderDevice->EndEffectPass();
				renderDevice->EndEffect();

				SKRenderer::Texture* tmp = self->Ping;
				self->Ping = self->Pong;
				self->Pong = tmp;
			}

			renderDevice->RestoreRenderTarget();
			renderDevice->RestoreDepthStencil();

			self->m_createPrerendered = false;
		}
	}
	else
	{
		renderDevice->UseShaders(false);
		renderDevice->SetMode(EMD_TWOD, 1);
		self->m_material->DiffuseTexture = self->Pong;
		renderDevice->Render(VID_UU, self->m_fullscreenQuad, 4, self->m_fullscreenIndices, 6, self->m_material);
	}
}

void GameState::renderScene(SKScene* scene, SKRenderDevice* device)
{
	if(scene->IsVisible())
	{
		SceneList children = scene->GetChildren();
		int len = children.Length();
		for(int i=0; i<len; i++)
		{
			renderScene(children[i], device);
		}

		const ModelList* models = scene->GetModels();
		len = models->Length();
		if(len > 0)
		{
			SKMath::SKMatrix<float> mat = scene->GetFinalMatrix();
			device->SetWorldTransform(&mat);
			for(int i=0; i<len; i++)
				device->Render(models->At(i));
		}
	}
}

void GameState::FreezeFrame(bool freeze, DWORD PP)
{
	m_postProcessFlags = PP;
	m_usePrerendered = freeze;
	m_createPrerendered = freeze;
}

void GameState::SetCameraPos(int index)
{
	SKScene* badges = m_vehicleScene[VS_ELISE_BODY]->FindChild("Badges");
	switch(index)
	{
	case 0:
		GameCamera->SetPosOffset(m_camOffsets[0]);
		GameCamera->SetLookOffset(m_camLookOffsets[0]);
		GameCamera->SetStage(1);
		m_renderCar = true;
		m_renderLines = true;
		badges->SetVisible(true, false);
		break;

	case 1:
		GameCamera->SetPosOffset(m_camOffsets[1]);
		GameCamera->SetLookOffset(m_camLookOffsets[1]);
		GameCamera->SetStage(0);
		m_renderCar = true;
		m_renderLines = true;
		badges->SetVisible(false, false);
		break;

	case 2:
		GameCamera->SetPosOffset(m_camOffsets[2]);
		GameCamera->SetLookOffset(m_camLookOffsets[1]);
		GameCamera->SetStage(0);
		m_renderCar = false;
		m_renderLines = false;
		badges->SetVisible(false, false);
		break;
	}
}

void GameState::Restart()
{
	PosClassify = BACK;
	CurrentLapTime = 0;
	LapCounter = 1;
	PreviousLapTime = 0;
	TimeLap = false;

	SKMath::SKVector<real> nowt(0,0,0);
	for(int i=0; i<5; i++)
	{
		m_vehicleRigidBody[i]->SetVelocity(nowt);
		m_vehicleRigidBody[i]->SetAngularVelocity(nowt);
	}

	m_vehicle->SetOrientation(m_vehStartDir);
	m_vehicle->SetPosition(m_vehStartPos);
	m_vehicle->SetGear(G_NEUTRAL);
}

real GameState::tyreFriction(FrictionFuncData& data, void* user_data)
{
	GameState* self = reinterpret_cast<GameState*>(user_data);
	TyreData* tyreData = NULL;
	for(int i=0; i<4; i++)
	{
		if(data.BodyA == self->m_vehicleRigidBody[i])
			tyreData = &self->m_tyreData[i];
	}
	
	real SlipRatio;
	real SlipAngle;
	real TyreLoad = min(data.NormalForce, 30000);
	data.ContactVelocity.x = 0;
	real SlipVelocity = data.ContactVelocity.GetLength();
	
	//--Calculate Required Variables
	SKMath::SKQuaternion<real> rot = data.BodyA->GetOrientation();
	SKMath::SKVector<real> angVel = data.BodyA->GetAngularVelocity();

	SKMath::SKVector<real> wheelAxis = rot.Rotate(SKMath::SKVector<real>::Right);
	SKMath::SKVector<real> wheelForwards = wheelAxis % data.ContactNormal;
	if(angVel * wheelAxis > 0)
		wheelForwards = -wheelForwards;
	wheelForwards.Normalise();

	SKMath::SKVector<real> vWS = data.BodyA->GetVelocity() - data.BodyB->GetVelocity();
	real wheelSpeed = vWS.GetLength();
	wheelForwards *= data.ToContact;
	real slipVel = SlipVelocity;
	if(SKMath::FAbs(slipVel) < EPSILON)
		SlipRatio = 0;
	else if(SKMath::FAbs(wheelSpeed) > EPSILON)
	{
		SlipRatio = (slipVel / wheelSpeed);
		SlipRatio = max(0.f, min(25, SlipRatio));
	}
	else
		SlipRatio = 1.f;


	if(SlipVelocity > 0.1f)
	{
		SlipAngle = data.ContactVelocity.AngleWith(wheelForwards);
		if(SlipAngle > PI_2)
			SlipAngle = PI - SlipAngle;
	}
	else
		SlipAngle = 0.f;
	//--

	//--Find the coefficients by evaluating the curves
	real coefA = tyreData->LoadSensitivity->Evaluate(data.NormalForce);
	real coefB = tyreData->SlipAngle->Evaluate(SlipAngle);
	real coefC = tyreData->SlipVelocity->Evaluate(SlipVelocity);
	real coefD = 0.f;
	if(data.ContactVelocity * wheelForwards < 0)
		coefD = tyreData->LonAccCurve->Evaluate(SlipRatio);
	else
		coefD = tyreData->LonDecCurve->Evaluate(SlipRatio);
	//--

	coefA = max(0, min(1, coefA));
	coefB = max(0, min(1, coefB));
	coefC = max(0, min(1, coefC));
	coefD = max(0, min(1, coefD));
	
	return (tyreData->MaxFriction * coefA * coefB * coefC * coefD);
}

void GameState::RenderScene(bool val)
{
	RPMDemo* Game = RPMDemo::GetInstance();
	if(val && !m_renderScene)
	{
		m_renderScene = true;
		Game->AddScreen(m_scene);
	}
	else if(!val)
	{
		m_renderScene = false;
		Game->RemoveScreen(m_scene);
	}
}