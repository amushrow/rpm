#pragma once
#include "SKRenderDevice.h"
#include "SKPhysics.h"
#include "SKInput.h"
#include "new_FiniteStateMachine.h"
#include "CustomDataLib.h"
#include "new_GameState.h"

class Screen;
class Camera;
class CamTarget;
class MenuController;

namespace TopStates
{
	enum States
	{
		S_INTRO,
		S_GAME,
		S_QUIT,
		S_FORCE_DWORD = 0x7FFFFFFF
	};

	class IntroState : public FiniteStateMachine::State
	{
	public:
		IntroState();
		virtual ~IntroState();
	
	protected:
		virtual void Enter();
		virtual void Run();
		virtual void Leave();

	private:
		static void render(SKRenderer::SKRenderDevice* renderDevice, void* user_data);
		static void loadResources(LPVOID Args);
		static void unloadResources(LPVOID Args);
		
		enum SubState
		{
			IS_FADE_IN,
			IS_WAIT,
			IS_FADE_OUT,
			IS_LEAVE
		};
		LVertex					m_verts[4];
		WORD					m_indices[6];
		Screen*					m_screen;
		SKRenderer::Material*	m_material;
		SKRenderer::Texture*	m_logoTexture;
		SubState				m_subState;
		double					m_timer;
		bool					m_resourcesLoaded;
	};
}

namespace GameStates
{
	enum States
	{
		S_PRESS_START,
		S_INTRO_ANIM,
		S_GAME_MAIN,
		S_GAME_PAUSE,
		S_FORCE_DWORD = 0x7FFFFFFF
	};

	class GameMain : public FiniteStateMachine::Machine
	{
	public:
		GameMain(TopStates::GameState* gameDataState);

	protected:
		virtual void Enter() {}
		virtual void Run();
		virtual void Leave() {}

	};

	namespace GameSubStates
	{
		enum States
		{
			S_LOGIC,
			S_PAUSE,
			S_FORCE_DWORD = 0x7FFFFFFF
		};

		class GameLogic : public FiniteStateMachine::Machine
		{
		public:
			GameLogic(TopStates::GameState* gameDataState);

		protected:
			virtual void Enter();
			virtual void Run();
			virtual void Leave();

		private:
			void		updateStates();
			void		updateInput();
			void		updateSound();
			void		updateLapPositions();
			static void renderHud(SKRenderer::SKRenderDevice* renderDevice, void* user_data);
			static void loadResources(LPVOID Args);
			static void wheelImpulseCallback(void* sender, SKPhysics::SKRigidBody::ImpulseEventArgs* args, LPVOID user_data);
			static void contactCallback(void* sender, SKPhysics::SKRigidBody::ContactEventArgs* args, LPVOID user_data);
			static void physicsUpdated(LPVOID sender, SKPhysics::SKPhysicsWorld::UpdateEventArgs* e, LPVOID data);
			static WORD m_indices[6];

			float					m_avgContactVel[4][5];
			Vertex					m_tachQuad[4];
			Vertex					m_needleQuad[4];
			float					m_ffbAvgs[24];
			float					m_rpmAvgs[5];
			real					m_wheelContactVel[4];
			double					m_keyboardSteer;
			double					m_keyFrameTimer;
			double					m_timer;
			real					m_ffb;
			float					m_speed;
			float					m_throttle;
			float					m_brake;
			float					m_steer;
			float					m_rpm;
			int						m_camIndex;
			SKRenderer::Material*	m_material;
			SKRenderer::Texture*	m_tach;	
			SKRenderer::Texture*	m_needle;
			TopStates::GameState*	GameData;
			Screen*					m_hudOverlay;
			char*					m_gear;
			bool					m_pauseGame;
		};
		
		//Pause Menu
		class GamePause : public FiniteStateMachine::State
		{
		public:
			GamePause(TopStates::GameState* gameDataState);

		protected:
			virtual void Enter();
			virtual void Run();
			virtual void Leave();

		private:
			struct Resolution
			{
				DWORD Width;
				DWORD Height;
				DWORD Frequency;
			};
			static void renderMenus(SKRenderer::SKRenderDevice* renderDevice, void* user_data);
			static void mainMenuCallback(UINT ID, DWORD Input, void* data);
			static void displayMenuCallback(UINT ID, DWORD Input, void* data);
			static void keyboardMenuCallback(UINT ID, DWORD Input, void* data);
			static void joyMenuCallback(UINT ID, DWORD Input, void* data);
			static void keyDown(void* sender, SKInput::KeyEventArgs* e, void* user_data);
			static void joyDown(void* sender, SKInput::KeyEventArgs* e, void* user_data);

			void drawMainMenu		(SKRenderer::SKRenderDevice* device);
			void drawDisplayMenu	(SKRenderer::SKRenderDevice* device);
			void drawkeyboardMenu	(SKRenderer::SKRenderDevice* device);
			void drawJoyMenu		(SKRenderer::SKRenderDevice* device);

			SKRenderer::MSQualityList		m_availableAA;
			DArray<Resolution>				m_resolutions;
			SKInput::ControllerEnum			m_controllers;
			float							m_analogueMove[8];
			TopStates::GameState*			GameData;
			Screen*							m_scene;
			MenuController*					m_mainMenu;
			MenuController*					m_displayMenu;
			MenuController*					m_keyboardMenu;
			MenuController*					m_joyMenu;
			MenuController*					m_activeMenu;
			SKRenderer::MultisampleQuality	m_msType;
			DWORD							m_editButton;
			int								m_selectedResolution;
			int								m_shadowLevel;
			int								m_filterQuality;
			bool							m_windowed;
			bool							m_vsync;
		};
	};
}