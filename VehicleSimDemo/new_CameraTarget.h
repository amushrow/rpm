#pragma once
#include "SKEngine.h"

//-- Wrapper to contain extrapolated RigidBody positions
class CamTarget : public SKEngine::GameObject
{
public:
	SKMath::SKVector<real> Pos;
	SKMath::SKQuaternion<real> Rot;
	SKMath::SKMatrix<real> Mat;

	virtual real						GetRadius() { return 1; }
	virtual SKMath::SKVector<real>		GetPosition() { return Pos; }
	virtual SKMath::SKQuaternion<real>	GetOrientation() {return Rot; }
	virtual SKMath::SKMatrix<real>		GetTransformation() { return Mat; }
	virtual int							GetType() { return 32; }
};
//--