#pragma once

#include <SKRenderer.h>

enum TextAlign
{
	TOP_LEFT,
	TOP_MIDDLE,
	MIDDLE,
	TOP_RIGHT
};

class Font
{
public:
	~Font();

	static HRESULT CreateFont(std::string fontName, Font** out_font);

	void	Draw(SKRenderer::SKRenderDevice* device, const char* text, float size, float x, float y, TextAlign alignment = TOP_LEFT, SKColour col = SKColour(0,0,0));
	void	Draw(SKRenderer::SKRenderDevice* device, const char* text, float size, float x, float y, TextAlign alignment, SKColour col1, SKColour col2);

private:
	Font(SKRenderer::Texture* tex, SKRenderer::Material* mat, const char* data);
	float					m_uvWidth[256];
	float					m_charWidth[256];
	float					m_charSize;
	float					m_fullSize;
	SKRenderer::Material*	m_material;
	SKRenderer::Texture*	m_fontTexture;
	DArray<LVertex>			m_verts;
	DArray<WORD>			m_indices;
	DArray<float>			m_lineWidths;
	DArray<int>				m_lineEndVert;
};