#pragma once

#include <State.h>
#include <SKRenderer.h>
#include <SKModels.h>
#include <SKPhysics.h>
#include <SKDefines.h>

class Camera;

class CamTarget : public SKEngine::GameObject
{
public:
	SKMath::SKVector Pos;
	SKMath::SKQuaternion Rot;
	SKMath::SKMatrix Mat;

	virtual float					GetRadius() { return 1; }
	virtual SKMath::SKVector		GetPosition() { return Pos; }
	virtual SKMath::SKQuaternion	GetOrientation() {return Rot; }
	virtual SKMath::SKMatrix		GetTransformation() { return Mat; }
	virtual int						GetType() { return 32; }
};

struct ModelConMatrix
{
	SKRenderer::SKModel*	Model;
	SKMath::SKMatrix		Matrix;
};

typedef DArray<ModelConMatrix> RenderList;

class GameState : public State
{
public:
	GameState(std::string name, long ID);
	~GameState();

	virtual StateResult	EnterState	();
	virtual StateResult Update		(float timeStep);
	virtual StateResult LeaveState	();

	virtual void		PreDraw		(SKMath::SKMatrixStack& worldTransform, float timeStep) {}
	virtual void		Render		(SKMath::SKMatrixStack& worldTransform, float timeStep);
	virtual void		Draw		(SKMath::SKMatrixStack& worldTransform, float timeStep);

	virtual void		OnPause		() {}
	virtual void		OnResume	() {}
	virtual void		OnChildDeath(State* child) {}
	virtual void		OnChildAdded(State* newChild) {}

private:
	enum CarParts
	{
		WHEEL_FL = 0,
		WHEEL_FR,
		WHEEL_RL,
		WHEEL_RR,
		ELISE_BODY,
		GROUND
	};

	void	setupWheel(CarParts wheel);
	bool	loadGraphics();
	bool	loadPhysics();
	void	addModelsToRenderList(RenderList* target, SKRenderer::SKScene* scene);
	static void impulseCallback(void* sender, SKPhysics::SKRigidBody::ImpulseEventArgs* e, void* user_data);

	SKPhysics::SKVehicle*	m_vehicle;
	SKPhysics::SKRigidBody*	m_bodies[6];
	SKRenderer::SKScene*	m_scenes[5];
	SKLight					m_light;
	Camera*					m_camera;
	SKMath::SKVector		m_camOffsets[4];
	SKMath::SKVector		m_camLookOffsets[2];
	SKMath::SKVector		m_wheelForces[4];
	
	RenderList*				m_renderLists[3];
	bool					m_buffUseShader[3];
	SKRenderer::SKSceneGraph*		m_sceneGraph;

	DArray<SKPhysics::SKRigidBody*> m_boxes;
	DArray<SKPhysics::SKRigidBody*> m_trackMesh;
	DArray<SKRenderer::SKScene*>	m_boxScenes;
	SKRenderer::SKScene*			m_origBoxScene;
	SKRenderer::SKScene*			m_trackScene;

	struct
	{
		SKMath::SKVector Pos;
		SKMath::SKVector LookAt;
		SKMath::SKVector Up;
	} m_ViewLookAt[3];

	unsigned				m_controllerIndex;
	bool					m_initialised;
	bool					m_quiting;
	float					m_timer;
	float					m_cachedRPM;
	float					m_cachedSpeed;
	CamTarget				m_camTarget;
	
	DArray<float>			m_lapTimes;
	float					m_lapTimer;
	int						m_lapCounter;
	SKMath::SKPlane			m_finishLine;
	SKMath::SKVector		m_finishPos;
	SKMath::SKVector		m_lapDir;
	int						m_posClassify;
	bool					m_timing;
	bool					m_useShaders;


	bool m_do60;
	float m_60time;
	
	bool					m_ready;
};