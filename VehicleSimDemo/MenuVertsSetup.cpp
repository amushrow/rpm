#ifdef BACON
#include "MenuState.h"

#define FORECOL 1.f
#define BACKCOL 0.7f
#define LINECOL 0.7f

void MenuState::setupVerts()
{
	WORD* indis = new WORD[6];
	indis[0] = 0;
	indis[1] = 1;
	indis[2] = 2;
	indis[3] = 2;
	indis[4] = 3;
	indis[5] = 0;

	Vertex* verts = new Vertex[24];
	float uvLogoBottom = 0.625f;
	float scale = m_screenResolution.y / 720.f;
	float width = 608.f * scale;
	float height = 380.f * scale;
	float mid = m_screenResolution.x * 0.5f;
	float yPos = (m_screenResolution.y - height) * 0.5f;
	float xPos = (m_screenResolution.x - width) * 0.5f;
	yPos = floor( yPos + 0.5f );
	xPos = floor( xPos + 0.5f );

	//-- Set up verts
	//Logo
	verts[0].x = xPos;
	verts[0].y = yPos;
	verts[0].z = 0.f;
	verts[0].tu = 0.f;
	verts[0].tv = 0.f;

	verts[1].x = m_screenResolution.x - xPos;
	verts[1].y = yPos;
	verts[1].z = 0.f;
	verts[1].tu = 1.f;
	verts[1].tv = 0.f;

	verts[2].x = m_screenResolution.x - xPos;
	verts[2].y = m_screenResolution.y - yPos;
	verts[2].z = 0.f;
	verts[2].tu = 1.f;
	verts[2].tv = uvLogoBottom;

	verts[3].x = xPos;
	verts[3].y = m_screenResolution.y - yPos;
	verts[3].z = 0.f;
	verts[3].tu = 0.f;
	verts[3].tv = uvLogoBottom;

	//Title
	yPos = 20.f * scale;
	height = 160.f * scale;
	yPos = floor( yPos + 0.5f );
	height = floor( height + 0.5f );

	verts[4].x = xPos;
	verts[4].y = yPos;
	verts[4].z = 0.f;
	verts[4].tu = 0.f;
	verts[4].tv = 0.75f;

	verts[5].x = m_screenResolution.x - xPos;
	verts[5].y = yPos;
	verts[5].z = 0.f;
	verts[5].tu = 1.f;
	verts[5].tv = 0.75f;

	verts[6].x = m_screenResolution.x - xPos;
	verts[6].y = yPos + height;
	verts[6].z = 0.f;
	verts[6].tu = 1.f;
	verts[6].tv = 1.f;

	verts[7].x = xPos;
	verts[7].y = yPos + height;
	verts[7].z = 0.f;
	verts[7].tu = 0.f;
	verts[7].tv = 1.f;

	for(int k=0; k<4; k++)
	{
		Pointf topLeft;
		Pointf size;
		int startvert = 8 + (k*4);

		switch(k)
		{
		case 0:
			{
				topLeft.x = mid - (338 * scale);
				topLeft.y = 224 * scale;
				size.x = 150.f * scale;
				size.y = 50.f * scale;
			}
			break;

		case 1:
			{
				topLeft.x = mid - (338 * scale);
				topLeft.y = 286 * scale;
				size.x = 150.f * scale;
				size.y = 50.f * scale;
			}
			break;

		case 2:
			{
				topLeft.x = mid - (338 * scale);
				topLeft.y = 348 * scale;
				size.x = 150.f * scale;
				size.y = 50.f * scale;
			}
			break;

		case 3:
			{
				topLeft.x = mid - (338 * scale);
				topLeft.y = 410 * scale;
				size.x = 150.f * scale;
				size.y = 50.f * scale;
			}
			break;
		}

		topLeft.x = floor(topLeft.x + 0.5f);
		topLeft.y = floor(topLeft.y + 0.5f);
		size.x = floor(size.x + 0.5f);
		size.y = floor(size.y + 0.5f);

		verts[startvert].x = topLeft.x;
		verts[startvert].y = topLeft.y;
		verts[startvert].z = 0.f;
		verts[startvert].tu = 0.f;
		verts[startvert].tv = 0.f;

		verts[startvert+1].x = topLeft.x + size.x;
		verts[startvert+1].y = topLeft.y;
		verts[startvert+1].z = 0.f;
		verts[startvert+1].tu = 0.9375f;
		verts[startvert+1].tv = 0.f;
		
		verts[startvert+2].x = topLeft.x + size.x;
		verts[startvert+2].y = topLeft.y + size.y;
		verts[startvert+2].z = 0.f;
		verts[startvert+2].tu = 0.9375f;
		verts[startvert+2].tv = 0.3125f;
		
		verts[startvert+3].x = topLeft.x;
		verts[startvert+3].y = topLeft.y + size.y;
		verts[startvert+3].z = 0.f;
		verts[startvert+3].tu = 0.f;
		verts[startvert+3].tv = 0.3125f;

		m_indexBuffer[MENU_BUTTON_START + k].Length = 6;
		m_indexBuffer[MENU_BUTTON_START + k].Indices = new WORD[6];
		for(int i=0; i<6; i++)
			m_indexBuffer[MENU_BUTTON_START + k].Indices[i] = indis[i]+startvert;
			
	}

	m_texturedQuads.Length = 24;
	m_texturedQuads.Vertices = verts;
	m_indexBuffer[LOGO].Length = 6;
	m_indexBuffer[LOGO].Indices = new WORD[6];
	for(int i=0; i<6; i++)
		m_indexBuffer[LOGO].Indices[i] = indis[i];
	m_indexBuffer[TITLE].Length = 6;
	m_indexBuffer[TITLE].Indices = new WORD[6];
	for(int i=0; i<6; i++)
		m_indexBuffer[TITLE].Indices[i] = indis[i]+4;
	//--

	LVertex* lit_vert = new LVertex[56];
	yPos = 50.f * scale;
	yPos = floor( yPos + 0.5f );
	float top = 120.f * scale;
	top = floor( top + 0.5f );
	SKColour col(BACKCOL);
	lit_vert[0].x = 0.f;
	lit_vert[0].y =  top;
	lit_vert[0].z = 0.f;
	lit_vert[0].Colour = col.GetUintCol();
	
	lit_vert[1].x = m_screenResolution.x;
	lit_vert[1].y = top;
	lit_vert[1].z = 0.f;
	lit_vert[1].Colour = col.GetUintCol();

	col = FORECOL;
	lit_vert[2].x = m_screenResolution.x;
	lit_vert[2].y = m_screenResolution.y - yPos;
	lit_vert[2].z = 0.f;
	lit_vert[2].Colour = col.GetUintCol();

	lit_vert[3].x = 0.f;
	lit_vert[3].y = m_screenResolution.y - yPos;
	lit_vert[3].z = 0.f;
	lit_vert[3].Colour = col.GetUintCol();

	yPos = scale * 50.f;
	yPos = floor(yPos + 0.5f);
	lit_vert[4].x = 0.f;
	lit_vert[4].y = 0.f;
	lit_vert[4].z = 10.f;
	lit_vert[4].Colour = 0xFF000000;

	lit_vert[5].x = m_screenResolution.x;
	lit_vert[5].y = 0.f;
	lit_vert[5].z = 10.f;
	lit_vert[5].Colour = 0xFF000000;

	lit_vert[6].x = m_screenResolution.x;
	lit_vert[6].y = yPos;
	lit_vert[6].z = 10.f;
	lit_vert[6].Colour = 0xFF000000;
	
	lit_vert[7].x = 0.f;
	lit_vert[7].y = yPos;
	lit_vert[7].z = 10.f;
	lit_vert[7].Colour = 0xFF000000;

	lit_vert[8].x = 0.f;
	lit_vert[8].y = m_screenResolution.y - yPos;
	lit_vert[8].z = 10.f;
	lit_vert[8].Colour = 0xFF000000;

	lit_vert[9].x = m_screenResolution.x;
	lit_vert[9].y = m_screenResolution.y - yPos;
	lit_vert[9].z = 10.f;
	lit_vert[9].Colour = 0xFF000000;

	lit_vert[10].x = m_screenResolution.x;
	lit_vert[10].y = m_screenResolution.y;
	lit_vert[10].z = 10.f;
	lit_vert[10].Colour = 0xFF000000;
	
	lit_vert[11].x = 0.f;
	lit_vert[11].y = m_screenResolution.y;
	lit_vert[11].z = 10.f;
	lit_vert[11].Colour = 0xFF000000;

	float lineWidth = 3.f;
	UINT colour = 0x7FFFFFFF;

	m_indexBuffer[OUTLINES].Length = 126;
	m_indexBuffer[OUTLINES].Indices = new WORD[126];
	Pointf topLeft;
	Pointf size;

	topLeft.x = mid - (176 * scale);
	topLeft.y = 224 * scale;
	size.x = 514.f * scale;
	size.y = 420.f * scale;
	topLeft.x = floor(topLeft.x + 0.5f);
	topLeft.y = floor(topLeft.y + 0.5f);
	size.x = floor(size.x + 0.5f);
	size.y = floor(size.y + 0.5f);

	lit_vert[12].x = topLeft.x;
	lit_vert[12].y = topLeft.y;
	lit_vert[12].z = 0.f;
	lit_vert[12].Colour = colour;

	lit_vert[13].x = topLeft.x + size.x;
	lit_vert[13].y = topLeft.y;
	lit_vert[13].z = 0.f;
	lit_vert[13].Colour = colour;

	lit_vert[14].x = topLeft.x + size.x;
	lit_vert[14].y = topLeft.y + size.y;
	lit_vert[14].z = 0.f;
	lit_vert[14].Colour = colour;

	lit_vert[15].x = topLeft.x;
	lit_vert[15].y = topLeft.y + size.y;
	lit_vert[15].z = 0.f;
	lit_vert[15].Colour = colour;

	m_indexBuffer[OUTLINES].Indices[0] = 12;
	m_indexBuffer[OUTLINES].Indices[1] = 13;
	m_indexBuffer[OUTLINES].Indices[2] = 14;
	m_indexBuffer[OUTLINES].Indices[3] = 14;
	m_indexBuffer[OUTLINES].Indices[4] = 15;
	m_indexBuffer[OUTLINES].Indices[5] = 12;

	colour = 0xFF000000;

	for(int k=0; k<5; k++)
	{
		int startvert = 16 + (k*8);
		switch(k)
		{
		case 0:
			{
				topLeft.x = mid - (338 * scale);
				topLeft.y = 224 * scale;
				size.x = 150.f * scale;
				size.y = 50.f * scale;
			}
			break;

		case 1:
			{
				topLeft.x = mid - (338 * scale);
				topLeft.y = 286 * scale;
				size.x = 150.f * scale;
				size.y = 50.f * scale;
			}
			break;

		case 2:
			{
				topLeft.x = mid - (338 * scale);
				topLeft.y = 348 * scale;
				size.x = 150.f * scale;
				size.y = 50.f * scale;
			}
			break;

		case 3:
			{
				topLeft.x = mid - (338 * scale);
				topLeft.y = 410 * scale;
				size.x = 150.f * scale;
				size.y = 50.f * scale;
			}
			break;

		case 4:
			{
				topLeft.x = mid - (176 * scale);
				topLeft.y = 224 * scale;
				size.x = 514.f * scale;
				size.y = 420.f * scale;
			}
			break;
		}

		topLeft.x = floor(topLeft.x + 0.5f);
		topLeft.y = floor(topLeft.y + 0.5f);
		size.x = floor(size.x + 0.5f);
		size.y = floor(size.y + 0.5f);

		lit_vert[startvert].x = topLeft.x;
		lit_vert[startvert].y = topLeft.y;
		lit_vert[startvert].z = 0.f;
		lit_vert[startvert].Colour = colour;

		lit_vert[startvert+1].x = topLeft.x + size.x;
		lit_vert[startvert+1].y = topLeft.y;
		lit_vert[startvert+1].z = 0.f;
		lit_vert[startvert+1].Colour = colour;

		lit_vert[startvert+2].x = topLeft.x + size.x;
		lit_vert[startvert+2].y = topLeft.y + size.y;
		lit_vert[startvert+2].z = 0.f;
		lit_vert[startvert+2].Colour = colour;

		lit_vert[startvert+3].x = topLeft.x;
		lit_vert[startvert+3].y = topLeft.y + size.y;
		lit_vert[startvert+3].z = 0.f;
		lit_vert[startvert+3].Colour = colour;

		lit_vert[startvert+4].x = topLeft.x + lineWidth;
		lit_vert[startvert+4].y = topLeft.y + lineWidth;
		lit_vert[startvert+4].z = 0.f;
		lit_vert[startvert+4].Colour = colour;

		lit_vert[startvert+5].x = topLeft.x + size.x - lineWidth;
		lit_vert[startvert+5].y = topLeft.y + lineWidth;
		lit_vert[startvert+5].z = 0.f;
		lit_vert[startvert+5].Colour = colour;

		lit_vert[startvert+6].x = topLeft.x + size.x - lineWidth;
		lit_vert[startvert+6].y = topLeft.y + size.y - lineWidth;
		lit_vert[startvert+6].z = 0.f;
		lit_vert[startvert+6].Colour = colour;

		lit_vert[startvert+7].x = topLeft.x + lineWidth;
		lit_vert[startvert+7].y = topLeft.y + size.y - lineWidth;
		lit_vert[startvert+7].z = 0.f;
		lit_vert[startvert+7].Colour = colour;

		int fi = 6 + (k*24);
		m_indexBuffer[OUTLINES].Indices[fi++] = startvert;
		m_indexBuffer[OUTLINES].Indices[fi++] = startvert+1;
		m_indexBuffer[OUTLINES].Indices[fi++] = startvert+5;
		m_indexBuffer[OUTLINES].Indices[fi++] = startvert+5;
		m_indexBuffer[OUTLINES].Indices[fi++] = startvert+4;
		m_indexBuffer[OUTLINES].Indices[fi++] = startvert;

		m_indexBuffer[OUTLINES].Indices[fi++] = startvert+1;
		m_indexBuffer[OUTLINES].Indices[fi++] = startvert+2;
		m_indexBuffer[OUTLINES].Indices[fi++] = startvert+6;
		m_indexBuffer[OUTLINES].Indices[fi++] = startvert+6;
		m_indexBuffer[OUTLINES].Indices[fi++] = startvert+5;
		m_indexBuffer[OUTLINES].Indices[fi++] = startvert+1;

		m_indexBuffer[OUTLINES].Indices[fi++] = startvert+6;
		m_indexBuffer[OUTLINES].Indices[fi++] = startvert+2;
		m_indexBuffer[OUTLINES].Indices[fi++] = startvert+3;
		m_indexBuffer[OUTLINES].Indices[fi++] = startvert+3;
		m_indexBuffer[OUTLINES].Indices[fi++] = startvert+7;
		m_indexBuffer[OUTLINES].Indices[fi++] = startvert+6;

		m_indexBuffer[OUTLINES].Indices[fi++] = startvert+3;
		m_indexBuffer[OUTLINES].Indices[fi++] = startvert;
		m_indexBuffer[OUTLINES].Indices[fi++] = startvert+4;
		m_indexBuffer[OUTLINES].Indices[fi++] = startvert+4;
		m_indexBuffer[OUTLINES].Indices[fi++] = startvert+7;
		m_indexBuffer[OUTLINES].Indices[fi] = startvert+3;
	}

	m_screenShapes.Length = 56;
	m_screenShapes.Vertices = lit_vert;
	m_indexBuffer[BG_GRADIENT].Length = 6;
	m_indexBuffer[BG_GRADIENT].Indices = new WORD[6];
	for(int i=0; i<6; i++)
		m_indexBuffer[BG_GRADIENT].Indices[i] = indis[i];
	m_indexBuffer[LETTER_BOX].Length = 12;
	m_indexBuffer[LETTER_BOX].Indices = new WORD[12];
	for(int i=0; i<6; i++)
		m_indexBuffer[LETTER_BOX].Indices[i] = indis[i]+4;
	for(int i=0; i<6; i++)
		m_indexBuffer[LETTER_BOX].Indices[i+6] = indis[i]+8;

	delete indis;
}
#endif