#ifdef BACON
#include "StaticGame.h"
//#include "GameState.h"
#include "GameButtons.h"
#include "MenuState.h"
#include "FadeManager.h"
#include "TopState.h"
#include <fstream>
#include <Clock.h>
#include "..\CppCustomDataTypes\CubicSpline.h"

volatile bool RunRenderThread = true;

void loadGameDataResource(void* sender, SKRenderer::LoadResourceEventArgs* e, void* user_data)
{
	DataLib::IFile* file = Game::DataMan->GetFiles().Find(e->ResourceName);
	if(file != NULL)
	{
		int len = (int)file->GetOriginalLength();
		BYTE* buffer = new BYTE[len];
		DataLib::IStream* stream = Game::DataMan->GetFileStream(file->GetUID());
		int bytesRead = stream->Read(buffer, 0, len);
		stream->Release();

		e->Handled = true;
		e->Data = buffer;
		e->DataLen = bytesRead;
		e->DataOwner = true;
	}
}

TopState::TopState(std::string name, long ID, HINSTANCE hInst, HWND hWnd) : State(name, ID)
{
	m_hWnd = hWnd;
	m_hInst = hInst;

	m_inputObj = NULL;
	m_physicsObj = NULL;
	m_renderObj = NULL;
	m_haveLock = false;
}

TopState::~TopState()
{

}

StateResult TopState::EnterState()
{
	GUID tmpGuid;
	CLSIDFromString(L"{F4D8AE27-C914-4EC3-8D25-F341AC24D5B0}", &tmpGuid);

	m_renderObj = new SKRendererObject(m_hInst);
	m_inputObj = new SKInputObject(m_hInst, m_hWnd);
	m_physicsObj = new SKPhysicsObject();

#ifdef _DEBUG
	m_renderObj->CreateDevice("Modules\\SKD3D_d.dll");
	m_inputObj->CreateDevice("Modules\\SKDirectInput_d.dll");
	m_physicsObj->CreateWorld("Modules\\SKPhysics_d.dll");
	Game::DataLibDll = LoadLibrary("Modules\\CppDataLib_d.dll");
#else
	m_renderObj->CreateDevice("Modules\\SKD3D.dll");
	m_inputObj->CreateDevice("Modules\\SKDirectInput.dll");
	m_physicsObj->CreateWorld("Modules\\SKPhysics.dll");
	Game::DataLibDll = LoadLibrary("Modules\\CppDataLib.dll");
#endif

	CREATEDATAMANAGER createDataMan = (CREATEDATAMANAGER)GetProcAddress(Game::DataLibDll, "CreateDataManager");
	Game::DataMan = createDataMan();
	SplineContainer* cubicSplineContainer = new SplineContainer();
	Game::DataMan->GetTypes().Add(cubicSplineContainer);
	//_asm{int 3}
	Game::DataMan->LoadFromFile("Resources\\GameData.skd");

	Game::Renderer = m_renderObj->GetDevice();

	std::ifstream settings_file("Data\\RenderSettings.bin", std::ios::binary | std::ios::in);
	if(settings_file.is_open())
	{
		settings_file.read((char*)&Game::VideoSettings, sizeof(RenderSettings));
		settings_file.close();
	}
	else
	{
		Game::VideoSettings.RefreshRate = 60;
		Game::VideoSettings.Windowed = true;
		Game::VideoSettings.NumBackBuffers = 1;
		Game::VideoSettings.DepthStencilFormat = DF_24S;
		Game::VideoSettings.EnableMultisampling = true;
		Game::VideoSettings.FSAAQuality = SKRenderer::MS_NONE;
		Game::VideoSettings.EnableVSync = false;
		Game::VideoSettings.DisplayWidth = 800;
		Game::VideoSettings.DisplayHeight = 600;
		Game::VideoSettings.BackBufferFormat = SKRenderer::PF_A8R8G8B8;
		Game::VideoSettings.LODBias = -0.225f;
	}
	
	RECT area;
	GetClientRect(m_hWnd, &area);
	Game::BorderSize.x = 640 - area.right;
	Game::BorderSize.y = 480 - area.bottom;

	if(Game::VideoSettings.Windowed)
	{
		SetWindowLong(m_hWnd, GWL_STYLE, WS_VISIBLE | WS_CLIPSIBLINGS | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX);
		SetWindowPos(m_hWnd, NULL, 0, 0, Game::VideoSettings.DisplayWidth + Game::BorderSize.x, 
			Game::VideoSettings.DisplayHeight + Game::BorderSize.y, SWP_NOZORDER);
		ShowCursor( TRUE );
	}
	else
	{
		SetWindowLong(m_hWnd, GWL_STYLE, WS_POPUP);
		SetWindowPos(m_hWnd, NULL, 0, 0, Game::VideoSettings.DisplayWidth, Game::VideoSettings.DisplayHeight, SWP_NOZORDER);
		ShowCursor( FALSE );
	}

	bool modified = false;
	RenderSettings updatedSettings;
	Game::Renderer->ValidateSettings(m_hWnd, Game::VideoSettings, modified, &updatedSettings);
	if(modified)
		memcpy(&Game::VideoSettings, &updatedSettings, sizeof(RenderSettings));

	Game::Renderer->Initialise(m_hWnd, Game::VideoSettings);
	Game::Renderer->SetClippingPlanes(0.1f, 1000.f);
	Game::Renderer->InitialiseStage(1.047f, NULL, 0);
	Game::Renderer->SetMode(EMD_PERSPECTIVE, 0);
	Game::Renderer->SetWorldTransform(NULL);
	Game::Renderer->SetClearColour(0,0,0);
	Game::Renderer->UseTextures(true);
	Game::Renderer->SetTextureStage(0, RS_TEX_MODULATE);
	Game::Renderer->SetBackfaceCulling(RS_CULL_CCW);
	Game::Renderer->InitialiseStage(1.f, NULL, 1);
	Game::Renderer->SetMode(EMD_TWOD, 1);

	Game::Renderer->OnLoadResource.Add(loadGameDataResource, NULL);
	
	Game::InputDevices = m_inputObj->GetDevice();
	Game::Keyboard = Game::InputDevices->CreateKeyboard();
	Game::Keyboard->Acquire();

	SKInput::ControllerEnum controllers = Game::InputDevices->GetJoysticks();
	if(controllers.Length() > 0)
	{
		Game::Joystick = Game::InputDevices->CreateJoystick(controllers[0]);
		Game::Joystick->Acquire();
	}

	//Permanent unchangable assignments
	Game::Keyboard->AssignKey(SKInput::SK_ESCAPE, GB_ESCAPE);
	Game::Keyboard->AssignKey(SKInput::SK_RETURN, GB_ACTION);
	Game::Keyboard->AssignKey(SKInput::SK_ESCAPE, GB_BACK);
	Game::Keyboard->AssignKey(SKInput::SK_BACK, GB_BACK);
	Game::Keyboard->AssignKey(SKInput::SK_UP, GB_MENU_UP);
	Game::Keyboard->AssignKey(SKInput::SK_DOWN, GB_MENU_DOWN);
	Game::Keyboard->AssignKey(SKInput::SK_LEFT, GB_MENU_LEFT);
	Game::Keyboard->AssignKey(SKInput::SK_RIGHT, GB_MENU_RIGHT);
	Game::Keyboard->AssignKey(SKInput::SK_1, GB_CAM1);
	Game::Keyboard->AssignKey(SKInput::SK_2, GB_CAM2);
	Game::Keyboard->AssignKey(SKInput::SK_3, GB_CAM3);
	Game::Keyboard->AssignKey(SKInput::SK_4, GB_CAM4);
	Game::Keyboard->AssignKey(SKInput::SK_F7, GB_TOGGLESHADERS);

	if(Game::Joystick)
	{
		Game::Joystick->AssignKey(SKInput::SK_BUTTON1, GB_ACTION);
		Game::Joystick->AssignKey(SKInput::SK_BUTTON3, GB_BACK);
		Game::Joystick->AssignKey(SKInput::SK_BUTTON4, GB_ESCAPE);
		Game::Joystick->AssignKey(SKInput::SK_POV1_UP, GB_MENU_UP);
		Game::Joystick->AssignKey(SKInput::SK_POV1_DOWN, GB_MENU_DOWN);
		Game::Joystick->AssignKey(SKInput::SK_POV1_LEFT, GB_MENU_LEFT);
		Game::Joystick->AssignKey(SKInput::SK_POV1_RIGHT, GB_MENU_RIGHT);
	}

	ButtonMapData* buttonData;
	int Len = LoadButtonMapData("Data\\ButtonMaps.bin", &buttonData);
	Game::StoredButtonMaps.Clear();
	if(Len > 0)
	{
		for(int i=0; i<Len; i++)
		{
			Game::StoredButtonMaps.Add(buttonData[i]);
		}
		delete buttonData;
	}
	else
	{
		//Default keyboard controls
		ButtonMapData data;
		data.DeviceID = Game::Keyboard->GetDeviceID();
		data.Brake = SKInput::SK_DOWN;
		data.GearUp = SKInput::SK_RIGHT;
		data.GearDown = SKInput::SK_LEFT;
		data.Throttle = SKInput::SK_UP;
		data.SteerLeft = SKInput::SK_A;
		data.SteerRight = SKInput::SK_D;
		data.BrakeSens = 1.f;
		data.SteerSens = 1.f;
		data.ThrottleSens = 1.f;
		Game::StoredButtonMaps.Add(data);

		if(Game::Joystick)
		{
			ButtonMapData data;
			data.DeviceID = Game::Joystick->GetDeviceID();
			data.Brake = 0xFFFFFFFF;
			data.GearUp = 0xFFFFFFFF;
			data.GearDown = 0xFFFFFFFF;
			data.Throttle = 0xFFFFFFFF;
			data.SteerLeft = 0xFFFFFFFF;
			data.SteerRight = 0xFFFFFFFF;
			data.BrakeSens = 1.f;
			data.SteerSens = 1.f;
			data.ThrottleSens = 1.f;
			Game::StoredButtonMaps.Add(data);
		}
	}

	Game::PhysicsWorld = m_physicsObj->GetWorld();
	Game::VertexManager = Game::Renderer->GetVertexCacheManager();
	Game::StandardFont = new Font("LT95");
	Game::TitleFont = new Font("Wevli");
	Game::ModelMan = new ModelManager(Game::Renderer);
	Game::Window = m_hWnd;

	m_gameState = NULL; //new GameState("GameState", 3);
	m_menuState = new MenuState("MainMenu", 2);
	AddChildState(m_menuState);

	m_addGameState = false;
	m_addMenuState = false;
	m_running = true;

	//Pre create fade manager
	FadeManager::Instance();

	DWORD_PTR ProcessAffinityMask;
	DWORD_PTR SystemAffinityMask;

	if( GetProcessAffinityMask( GetCurrentProcess(), &ProcessAffinityMask, &SystemAffinityMask ) )
	{
		ULONG ProcessorCount = 0;
		for( DWORD_PTR ProcessorBit = 1; ProcessorBit > 0; ProcessorBit <<= 1 )
		{
			if( ProcessAffinityMask & ProcessorBit )
				ProcessorCount++;
		}

#ifdef _DEBUG
		ProcessorCount = 0;
#else
		ProcessorCount--; //We're using one right now!
#endif

		switch(ProcessorCount)
		{
		case 0:
			break;

		case 1:
			Game::NumPhysicsThreads = 1;
			break;

		case 2:
			Game::NumPhysicsThreads = 2;
			break;

		case 3:
			Game::NumPhysicsThreads = 2;
			Game::NumRendererThreads = 1;
			break;

		default:
			Game::NumRendererThreads = 1;
			ProcessorCount--;
			Game::NumPhysicsThreads = ProcessorCount;
			break;
		}
	}

	//Create rendering thread
	Game::RendererLock = CreateSemaphore(0, 1, 1, "SKRendererSem");
	Game::BufferLock = CreateSemaphore(0, 1, 1, "SKBufferSem");

	m_multiThreaded = Game::NumRendererThreads > 0;
	if(m_multiThreaded)
		m_renderThread = CreateThread(NULL, 0, TopState::t_render, this, 0, NULL);

	return RES_CONTINUE;
}

StateResult TopState::LeaveState()
{
	//We for the renderer to finish before deleting it
	WaitForSingleObject(m_renderThread, INFINITE);

	Game::ModelMan->ForceUnloadAll();

	if(m_renderObj)
	{
		m_renderObj->Release();
		delete m_renderObj;
	}

	if(m_inputObj)
	{
		if(Game::Keyboard)
			Game::InputDevices->ReleaseDevice(&Game::Keyboard);
		if(Game::Joystick)
			Game::InputDevices->ReleaseDevice(&Game::Joystick);

		m_inputObj->Release();
		delete m_inputObj;
	}
	
	if(m_physicsObj)
	{
		m_physicsObj->Release();
		delete m_physicsObj;
	}

	RELEASEDATAMANAGER releaseDataMan = (RELEASEDATAMANAGER)GetProcAddress(Game::DataLibDll, "ReleaseDataManager");
	releaseDataMan(&Game::DataMan);

	if(Game::StandardFont)
		delete Game::StandardFont;

	if(Game::ModelMan)
		delete Game::ModelMan;

	if(Game::TitleFont)
		delete Game::TitleFont;

	FadeManager::DestroyInstance();

	return RES_CONTINUE;
}

StateResult TopState::Update(float timeStep)
{
	if(m_haveLock)
	{
		ReleaseSemaphore(Game::RendererLock, 1, NULL);
		m_haveLock = false;
	}

	//if(timeStep > 0.025f)
	//	timeStep = 0.025f;

	if( !m_running )
		return RES_LEAVE;

	//--Main Thread
	if(Game::Keyboard->LostDevice())
		Game::Keyboard->Acquire();

	bool keyboardLost = Game::Keyboard->LostDevice();
	//if(m_gameState && keyboardLost)
	//	m_gameState->PauseState();
	
	if(!Game::Renderer->IsRunning())
	{
		//if(m_gameState)
		//	m_gameState->PauseState();
		if(m_menuState)
			m_menuState->PauseState();
	}
	else
	{
		//if(m_gameState && !keyboardLost)
		//	m_gameState->ResumeState();
		if(m_menuState)
			m_menuState->ResumeState();
	}

	Game::Keyboard->Poll();
	if(Game::Joystick != NULL)
	{
		if(Game::Joystick->LostDevice())
			Game::Joystick->Acquire();
		Game::Joystick->Poll();
	}

	State::Update(timeStep);
	if(m_addGameState)
	{
		//m_gameState = new GameState("GameState", 3);
		//AddChildState( m_gameState );
		m_addGameState = false;
	}
	if(m_addMenuState)
	{
		m_menuState = new MenuState("MainMenu", 2, true);
		AddChildState(m_menuState); 
		m_addMenuState = false;
	}
	//--
	
	//--Rendering Thread
	if(!m_multiThreaded)
	{
		if(Game::Renderer->IsRunning())
		{
			Game::RenderBufferID = Game::NextRenderBufferID;
			
			FadeManager::Instance()->Update(timeStep);
			Game::Renderer->BeginRendering(true, true, true);
			SKMatrixStack matStack; matStack.Clear();
			PreDraw(matStack, timeStep);
			Render(matStack, timeStep);
			Draw(matStack, timeStep);
			Game::VertexManager->Flush();
			Game::Renderer->EndRendering();		
		}
	}
	//--

	return RES_CONTINUE;
}

static float fps = 0;
static float fpsCounter = 0;
static float fpsTimer = 0;
static float timer = 0;
static Clock graphicsClock;

DWORD TopState::t_render(PVOID args)
{
	TopState* self = reinterpret_cast<TopState*>(args);
	while(RunRenderThread)
	{
		WaitForSingleObject(Game::BufferLock, INFINITE);
		Game::RenderBufferID = Game::NextRenderBufferID;
		ReleaseSemaphore(Game::BufferLock, 1, NULL);

		WaitForSingleObject(Game::RendererLock, INFINITE);
		if(Game::Renderer->IsRunning())
		{
			float timeStep = (float)graphicsClock.GetSeconds_Reset();
			if(fpsTimer < 1.f)	{
				fpsTimer += timeStep;
				fpsCounter += 1.f;
			} else	{
				fps = fpsCounter;
				fpsCounter = 0.f;
				fpsTimer = 0.f;
			}
			timer += timeStep;

			FadeManager::Instance()->Update(timeStep);
			Game::Renderer->BeginRendering(true, true, true);
			SKMatrixStack matStack; matStack.Clear();
			self->PreDraw(matStack, timeStep);
			self->Render(matStack, timeStep);
			self->Draw(matStack, timeStep);
			Game::VertexManager->Flush();
			Game::Renderer->EndRendering();
		}

		ReleaseSemaphore(Game::RendererLock, 1, NULL);
	}

	return 0;
}

void TopState::PreDraw(SKMath::SKMatrixStack &worldMatrix, float timeStep)
{
	Game::Renderer->UseShaders(false);
	Game::Renderer->EnableLighting(false);
	Game::Renderer->SetMode(EMD_TWOD, 1);
	Game::Renderer->SetDepthBufferMode(RS_DEPTH_NONE);

	for(unsigned int i=0; i<m_children.size(); i++)
	{
		worldMatrix.Clear();
		worldMatrix.Translate(-.5f, -.5f, 0);
		m_children[i]->PreDraw(worldMatrix, timeStep);
	}

	Game::Renderer->EnableLighting(true);
	Game::Renderer->SetDepthBufferMode(RS_DEPTH_READWRITE);
}

void TopState::Render(SKMatrixStack &worldMatrix, float timeStep)
{
	

	Game::Renderer->SetMode(EMD_PERSPECTIVE, 0);
	Game::Renderer->SetWorldTransform(NULL);
	for(unsigned int i=0; i<m_children.size(); i++)
	{
		m_children[i]->Render(worldMatrix, timeStep);
	}
}

void TopState::Draw(SKMath::SKMatrixStack &worldMatrix, float timeStep)
{
	Game::Renderer->UseShaders(false);
	Game::Renderer->EnableLighting(false);
	Game::Renderer->SetMode(EMD_TWOD, 1);

	for(unsigned int i=0; i<m_children.size(); i++)
	{
		worldMatrix.Clear();
		worldMatrix.Translate(-.5f, -.5f, 0);
		m_children[i]->Draw(worldMatrix, timeStep);
	}

	if(m_gameState != NULL)
	{
		char buffer[128];
		sprintf_s(buffer, 128, "    Graphics FPS: %.f\nRender Threads: %d\nPhysics Threads: %d", fps, Game::NumRendererThreads, Game::NumPhysicsThreads);
		Game::TitleFont->Draw(buffer, 16, (float)(Game::VideoSettings.DisplayWidth-175), 2, TOP_LEFT, SKColour(1,1,1));
	}

	FadeManager::Instance()->Draw();
	Game::Renderer->EnableLighting(true);
}

void TopState::OnChildDeath(State* child)
{
	if(!m_haveLock)
	{
		WaitForSingleObject(Game::RendererLock, INFINITE);
		m_haveLock = true;
	}

	if(child == m_menuState)
	{
		if(m_menuState->GetReturnCode() == 0)
		{
			m_addGameState = true;
		}
		else
		{
			m_running = false;
			RunRenderThread = false;
		}

		m_menuState = NULL;
	}
	//else if(child == m_gameState)
	//{
	//	m_gameState = NULL;
	//	m_addMenuState = true;
	//}
}

#endif