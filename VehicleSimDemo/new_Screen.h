#pragma once
#include "SKRenderer.h"

typedef void (*RenderFunction)(SKRenderer::SKRenderDevice* sender, void* user_data);

class Screen
{
public:
	Screen(RenderFunction func, int zIndex, void* user_data = NULL);
	~Screen() {}

	int		GetZIndex() { return m_zindex; }
	void	Render(SKRenderer::SKRenderDevice* renderDevice);

private:
	int				m_zindex;
	RenderFunction	m_renderFunc;
	void*			m_userData;
};

class ScreenCollection
{
public:
	ScreenCollection();
	~ScreenCollection() {}

	void	AddScreen		(Screen* screen);
	void	RemoveScreen	(Screen* screen);
	void	Render			(SKRenderer::SKRenderDevice* renderDevice);

private:
	Screen**	m_screens;
	UINT		m_numScreens;
	UINT		m_maxScreens;
};