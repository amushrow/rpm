#ifdef BACON
#include "StaticGame.h"
#include "FadeManager.h"

FadeManager* FadeManager::m_inst = NULL;

FadeManager::FadeManager()
{
	LVertex* verts = new LVertex[4];
	POINT res; Game::Renderer->GetResolution(&res);
	Pointf screenRes; 
	screenRes.x = (float)res.x;
	screenRes.y = (float)res.y;
	screenRes.x -= 0.5f;
	screenRes.y -= 0.5f;

	//-- Set up verts
	verts[0].x = -0.5f;
	verts[0].y = -0.5f;
	verts[0].z = 0.f;

	verts[1].x = screenRes.x;
	verts[1].y = -0.5f;
	verts[1].z = 0.f;

	verts[2].x = screenRes.x;
	verts[2].y = screenRes.y;
	verts[2].z = 0.f;

	verts[3].x = -0.5f;
	verts[3].y = screenRes.y;
	verts[3].z = 0.f;
	//--

	WORD* indis = new WORD[6];
	indis[0] = 0;
	indis[1] = 1;
	indis[2] = 2;
	indis[3] = 2;
	indis[4] = 3;
	indis[5] = 0;

	m_quad.Length = 4;
	m_quad.Vertices = verts;
	m_quadIndis.Length = 6;
	m_quadIndis.Indices = indis;

	SKRenderer::SKMaterial mat;
	mat.Alpha = true;
	mat.Ambient.Set(0,0,0);
	mat.Diffuse.Set(0,0,0);
	mat.Specular.Set(0,0,0);
	mat.Emissive.Set(0,0,0);
	mat.Shininess = 0.f;
	WaitForSingleObject(Game::RendererLock, INFINITE);
	m_fadeMatID = Game::Renderer->GetMaterialManager()->AddMaterial(&mat);
	ReleaseSemaphore(Game::RendererLock, 1, NULL);

	m_fadeAlpha = 0.f;
	m_currentState = FADE_OFF;
}

void FadeManager::RebuildVerts()
{
	POINT res; Game::Renderer->GetResolution(&res);
	Pointf screenRes; 
	screenRes.x = (float)res.x;
	screenRes.y = (float)res.y;
	screenRes.x -= 0.5f;
	screenRes.y -= 0.5f;

	//-- Set up verts
	m_quad.Vertices[0].x = -0.5f;
	m_quad.Vertices[0].y = -0.5f;
	m_quad.Vertices[0].z = 0.f;

	m_quad.Vertices[1].x = screenRes.x;
	m_quad.Vertices[1].y = -0.5f;
	m_quad.Vertices[1].z = 0.f;

	m_quad.Vertices[2].x = screenRes.x;
	m_quad.Vertices[2].y = screenRes.y;
	m_quad.Vertices[2].z = 0.f;

	m_quad.Vertices[3].x = -0.5f;
	m_quad.Vertices[3].y = screenRes.y;
	m_quad.Vertices[3].z = 0.f;
	//--
}
FadeManager::~FadeManager()
{
	m_inst = NULL;
	delete[] m_quad.Vertices;
	delete[] m_quadIndis.Indices;
}

FadeManager* FadeManager::Instance()
{
	if(m_inst == NULL)
		m_inst = new FadeManager();

	return m_inst;
}

void FadeManager::AddStateChangedHandler( void (*func)(void* sender, FadeEventArgs* args, void* user_data), void* user_data)
{
	m_stateChangedEvent.Add(func, user_data);
}

bool FadeManager::FadeOut(SKColour fadeCol, float duration)
{
	if(m_currentState == FADE_OFF)
	{
		m_fadeCol = fadeCol;
		m_fadeDuration = duration;
		m_timer = 0.f;
		m_currentState = FADING_OUT;
		setQuadColour();

		return true;
	}

	return false;
}

bool FadeManager::FadeIn(float duration)
{
	if(m_currentState == FADE_ON)
	{
		m_fadeDuration = duration;
		m_timer = duration;
		m_currentState = FADING_IN;

		return true;
	}

	return false;
}

void FadeManager::Update(float timeStep)
{
	FadeState newState = m_currentState;
	switch( m_currentState )
	{
	case FADING_OUT:
		{
			//Increase alpha
			m_timer += timeStep;
			if(m_timer < m_fadeDuration)
			{
				m_fadeAlpha = m_timer / m_fadeDuration;
			}
			else
			{
				m_fadeAlpha = 1.f;
				newState = FADE_ON;
			}

			setQuadColour();
		}
		break;

	case FADING_IN:
		{
			//Decrease alpha
			m_timer -= timeStep;
			if(m_timer > 0.f)
			{
				m_fadeAlpha = m_timer / m_fadeDuration;
			}
			else
			{
				m_fadeAlpha = 0.f;
				newState = FADE_OFF;
			}

			setQuadColour();
		}
		break;

	case FADE_ON:
	case FADE_OFF:
		break;
	}

	if(newState != m_currentState)
	{
		FadeEventArgs e(newState);
		m_stateChangedEvent(this, &e);

		m_currentState = newState;
	}
}

void FadeManager::ForceAlpha(SKColour col, float alpha)
{
	alpha = max(0.f, min(1.f, alpha));
	m_fadeCol = col;
	m_fadeAlpha = alpha;
	setQuadColour();

	FadeState newState;
	if(alpha > 0.f)
		newState = FADE_ON;
	else
		newState = FADE_OFF;

	if(newState != m_currentState)
	{
		FadeEventArgs e(newState);
		m_stateChangedEvent(this, &e);

		m_currentState = newState;
	}
}

void FadeManager::setQuadColour()
{
	float alpha = m_fadeCol.A * m_fadeAlpha;
	UINT col = m_fadeCol.GetUintCol();
	BYTE* alphaChannel = (BYTE*)&col+3;
	*alphaChannel = (BYTE)(alpha * 255.f);
	for(int i=0; i<4; i++)
	{
		m_quad.Vertices[i].Colour = col;
	}
}

void FadeManager::Draw()
{
	if(m_currentState == FADE_OFF)
		return;

	Game::Renderer->SetWorldTransform(NULL);
	Game::VertexManager->Render(m_quad, m_quadIndis, m_fadeMatID);
}
#endif