#include "new_RPMDemo.h"
#include "GameButtons.h"
#include "new_Menu.h"

MenuController::MenuController(MenuController* parentMenu) : m_menuItems(4, 2)
{
	m_parent = parentMenu;
	m_selectedItem = 0xFFFFFFFF;
	m_blockInput = false;
}

MenuController::~MenuController()
{
	UINT Len = m_menuItems.Length();
	for(UINT i=0; i<Len; i++)
		delete m_menuItems[i];
}

void MenuController::AddItem(UINT ID, MenuItemCallback Callback, void* data)
{
	MenuItem* newItem = new MenuItem(ID, Callback, data);
	m_menuItems.Add(newItem);

	if(m_selectedItem > m_menuItems.Length())
		m_selectedItem = 0;
}

void MenuController::HandleInput()
{
	if(m_blockInput || m_menuItems.Length() == 0)
		return;

	RPMDemo* Game = RPMDemo::GetInstance();

	if(Game->Keyboard->KeyPressed(GB_MENU_DOWN)) {
		m_selectedItem++;
		if(m_selectedItem >= m_menuItems.Length())
			m_selectedItem = 0;
	} else if(Game->Keyboard->KeyPressed(GB_MENU_UP)) {
		if(m_selectedItem > 0)
			m_selectedItem--;
		else
			m_selectedItem = m_menuItems.Length()-1;
	} else if(Game->Joystick) {
		if(Game->Joystick->KeyPressed(GB_MENU_DOWN)) {
			m_selectedItem++;
			if(m_selectedItem >= m_menuItems.Length())
				m_selectedItem = 0;
		} else if(Game->Joystick->KeyPressed(GB_MENU_UP)) {
			if(m_selectedItem > 0)
				m_selectedItem--;
			else
				m_selectedItem = m_menuItems.Length()-1;
		}
	}

	if(Game->Keyboard->KeyPressed(GB_ESCAPE)) {
		m_menuItems[m_selectedItem]->DoCallback(GB_ESCAPE);
	}
	if(Game->Keyboard->KeyPressed(GB_ACTION)) {
		m_menuItems[m_selectedItem]->DoCallback(GB_ACTION);
	}
	if(Game->Keyboard->KeyPressed(GB_PAUSE)) {
		m_menuItems[m_selectedItem]->DoCallback(GB_PAUSE);
	}
	if(Game->Keyboard->KeyPressed(GB_BACK)) {
		m_menuItems[m_selectedItem]->DoCallback(GB_BACK);
	}
	if(Game->Keyboard->KeyPressed(GB_MENU_LEFT)) {
		m_menuItems[m_selectedItem]->DoCallback(GB_MENU_LEFT);
	}
	if(Game->Keyboard->KeyPressed(GB_MENU_RIGHT)) {
		m_menuItems[m_selectedItem]->DoCallback(GB_MENU_RIGHT);
	}

	if(Game->Joystick)
	{
		if(Game->Joystick->KeyPressed(GB_ESCAPE)) {
			m_menuItems[m_selectedItem]->DoCallback(GB_ESCAPE);
		}
		if(Game->Joystick->KeyPressed(GB_ACTION)) {
			m_menuItems[m_selectedItem]->DoCallback(GB_ACTION);
		}
		if(Game->Joystick->KeyPressed(GB_PAUSE)) {
			m_menuItems[m_selectedItem]->DoCallback(GB_PAUSE);
		}
		if(Game->Joystick->KeyPressed(GB_BACK)) {
			m_menuItems[m_selectedItem]->DoCallback(GB_BACK);
		}
		if(Game->Joystick->KeyPressed(GB_MENU_LEFT)) {
			m_menuItems[m_selectedItem]->DoCallback(GB_MENU_LEFT);
		}
		if(Game->Joystick->KeyPressed(GB_MENU_RIGHT)) {
			m_menuItems[m_selectedItem]->DoCallback(GB_MENU_RIGHT);
		}
	}
}
