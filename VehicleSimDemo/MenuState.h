#pragma once

#include <State.h>
#include <SKRenderer.h>
#include <SKInput.h>
#include "GameButtons.h"


struct KeyChange
{
	SKInput::SKInputDevice* device;
	GameButtons button;
};

class MenuState : public State
{
public:
	MenuState(std::string name, long ID, bool quickStart = false);
	~MenuState();

	virtual StateResult	EnterState	();
	virtual StateResult Update		(float timeStep);
	virtual StateResult LeaveState	();

	virtual void		PreDraw		(SKMath::SKMatrixStack& worldTransform, float timeStep);
	virtual void		Render		(SKMath::SKMatrixStack& worldTransform, float timeStep);
	virtual void		Draw		(SKMath::SKMatrixStack& worldTransform, float timeStep);

	virtual void		OnPause		() {}
	virtual void		OnResume	() {}
	virtual void		OnChildDeath(State* child) {}
	virtual void		OnChildAdded(State* newChild) {}

	int					GetReturnCode()	{ return m_returnCode; }

private:
	bool	keyPressed(SKInput::Key key);
	void	setupVerts();
	void	handleSubMenus(float timeStep);
	void	handleControllerMenu(float timeStep);

	void	drawSubMenus();
	void	drawControllerMenu();
	void	drawVideoMenu();

	void	storeControllerMappings(SKInput::SKInputDevice* inputDev);
	void	loadControllerMappings(SKInput::SKInputDevice* inputDev);
	static void keyDown(void* sender, SKInput::KeyEventArgs* e, void* user_data);

	enum MenuStage
	{
		INIT_PAUSE = 0,
		SHOW_LOGO,
		SHOW_START,
		EXIT_MENU
	};

	enum MenuShapes
	{
		LOGO = 0,
		TITLE,
		BG_GRADIENT,
		LETTER_BOX,
		OUTLINES,
		MENU_BUTTON_START,
		MENU_BUTTON_CONTROLS,
		MENU_BUTTTON_VIDEO,
		MENU_BUTTON_QUIT,
		MENU_SHAPES_END
	};

	Pointf								m_screenResolution;
	float								m_timer;
	double								m_rotAngle;
	int									m_mainMenuSelection;

	int									m_returnCode;
	MenuStage							m_stage;
	bool								m_onSubStage;
	bool								m_quickStart;
	bool								m_setbackcol;

	//Sub menu data
	bool					m_combinedAxis;
	int						m_curItem;
	bool					m_assigningKey;
	KeyChange				m_keyChangeData;
	SKInput::ControllerEnum	m_gameControllers;
	int						m_controllerId;
	float					m_throttleSens;
	float					m_brakeSens;
	float					m_steerSens;
	float					m_combinedSens;
	float					m_throttleDir;
	float					m_brakeDir;
	float					m_analogueMove[8];

	struct Resolution
	{
		DWORD Width;
		DWORD Height;
		DWORD Frequency;
	};
	DArray<Resolution> m_resolutions;
	SKRenderer::RenderSettings	m_renderSettings;
	SKRenderer::MultisampleQuality m_maxAA;
	unsigned m_selectedResolution;
};