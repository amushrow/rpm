#include "new_RPMDemo.h"
#include "EngineSoundController.h"

void GetFileData(std::string fileName, BYTE** outData, DWORD* out_len)
{
	*outData = NULL;
	*out_len = 0;
	RPMDemo* Game = RPMDemo::GetInstance();
	DataLib::IFile* file = Game->DataMan->GetFiles().Find(fileName);
	if(file != NULL)
	{
		int len = (int)file->GetOriginalLength();
		BYTE* buffer = new BYTE[len];
		DataLib::IStream* stream = Game->DataMan->GetFileStream(file->GetUID());
		int bytesRead = stream->Read(buffer, 0, len);
		stream->Release();

		*outData = buffer;
		*out_len = len;
	}	
}

EngineSounds::EngineSounds()
{
	RPMDemo* Game = RPMDemo::GetInstance();

	//-- Load up sounds
	BYTE* data;
	DWORD len;

	GetFileData("Sounds\\Engine\\idle.wav", &data, &len);
	m_engineClipsOff[0] = Game->AudioDevice->LoadAudio(data, len);
	delete[] data;
	GetFileData("Sounds\\Engine\\offverylow.wav", &data, &len);
	m_engineClipsOff[1] = Game->AudioDevice->LoadAudio(data, len);
	delete[] data;
	GetFileData("Sounds\\Engine\\offlow.wav", &data, &len);
	m_engineClipsOff[2] = Game->AudioDevice->LoadAudio(data, len);
	delete[] data;
	GetFileData("Sounds\\Engine\\offmid.wav", &data, &len);
	m_engineClipsOff[3] = Game->AudioDevice->LoadAudio(data, len);
	delete[] data;
	GetFileData("Sounds\\Engine\\offhigh.wav", &data, &len);
	m_engineClipsOff[4] = Game->AudioDevice->LoadAudio(data, len);
	delete[] data;

	GetFileData("Sounds\\Engine\\onidle.wav", &data, &len);
	m_engineClipsOn[0] = Game->AudioDevice->LoadAudio(data, len);
	delete[] data;
	GetFileData("Sounds\\Engine\\onverylow.wav", &data, &len);
	m_engineClipsOn[1] = Game->AudioDevice->LoadAudio(data, len);
	delete[] data;
	GetFileData("Sounds\\Engine\\onlow.wav", &data, &len);
	m_engineClipsOn[2] = Game->AudioDevice->LoadAudio(data, len);
	delete[] data;
	GetFileData("Sounds\\Engine\\onmid.wav", &data, &len);
	m_engineClipsOn[3] = Game->AudioDevice->LoadAudio(data, len);
	delete[] data;
	GetFileData("Sounds\\Engine\\onhigh.wav", &data, &len);
	m_engineClipsOn[4] = Game->AudioDevice->LoadAudio(data, len);
	delete[] data;
	//--

	for(int i=0; i<5; i++)
	{
		m_engineInstsOff[i] = m_engineClipsOff[i]->CreateInstance();
		m_engineInstsOn[i] = m_engineClipsOn[i]->CreateInstance();

		m_engineInstsOff[i]->SetNumLoops(-1);
		m_engineInstsOn[i]->SetNumLoops(-1);
	}

	m_offData[0].Set(0,		2000,	-1,		0.5f);
	m_offData[1].Set(1100,	3000,	-1,		0.5f);
	m_offData[2].Set(2400,	4400,	-0.5f,	0.2f);
	m_offData[3].Set(3800,	6700,	-0.1f,	0.5f);
	m_offData[4].Set(6000,	9000,	 0,		0.8f);
	m_onData[0].Set(0,		1500,	-1.0f,	0.0f);
	m_onData[1].Set(1100,	3000,	-0.8f,	0.0f);
	m_onData[2].Set(2400,	4600,	-0.4f,	0.0f);
	m_onData[3].Set(3600,	6700,	-0.3f,	0.4f);
	m_onData[4].Set(6000,	9000,	 0.0f,	0.8f);

	m_throttle = 0;
	m_rpm = 0;
	m_playing = false;
}

EngineSounds::~EngineSounds()
{
	for(int i=0; i<5; i++)
	{
		m_engineInstsOff[i]->Release();
		m_engineClipsOff[i]->Release();
		m_engineInstsOn[i]->Release();
		m_engineClipsOn[i]->Release();
	}
}

void EngineSounds::SetThrottle(float throttle)
{
	m_throttle = throttle;
}

void EngineSounds::SetRPM(float rpm)
{
	m_rpm = rpm;
}

void EngineSounds::Play()
{
	if(!m_playing)
	{
		m_playing = true;
		Update();
	}
}

void EngineSounds::Stop()
{
	if(m_playing)
	{
		m_playing = false;
		for(int i=0; i<5; i++)
		{
			m_engineInstsOff[i]->Stop();
			m_engineInstsOn[i]->Stop();
		}
	}
}

void EngineSounds::Update()
{
	if(!m_playing)
		return;

	float onVolume = 0.f;
	float offVolume = 1.f;
	if(m_throttle > 0.5f)
	{
		onVolume = (m_throttle-0.5f)*2;
		if(onVolume > 0.5f)
			offVolume = 1.5f - onVolume;
	}

	onVolume *= 0.25f;
	offVolume *= 0.25f;

	//-- Off Sound
	for(int i=0; i<5; i++)
	{
		float RPM = m_rpm - m_offData[i].LowRPM;
		float Range = m_offData[i].HighRPM - m_offData[i].LowRPM;
		if (RPM > 0 && RPM < Range)
		{
			float vol = 1;
			if (RPM < Range * 0.2f)
				vol = RPM / (Range*0.2f);
			if (RPM > Range - (Range * 0.2f))
				vol = (Range - RPM) / (Range*0.2f);

			float pitchRange = m_offData[i].HighPitch - m_offData[i].LowPitch;
			float weight = RPM / Range;
			float pitch = m_offData[i].LowPitch + (weight*pitchRange);
			m_engineInstsOff[i]->SetPitch(pitch);
			m_engineInstsOff[i]->SetVolume(vol*offVolume);
			m_engineInstsOff[i]->Play();
		}
		else
			m_engineInstsOff[i]->Stop();
	}
	//--

	//-- On Sound
	for(int i=0; i<5; i++)
	{
		float RPM = m_rpm - m_onData[i].LowRPM;
		float Range = m_onData[i].HighRPM - m_onData[i].LowRPM;
		if (RPM > 0 && RPM < Range)
		{
			float vol = 1;
			if (RPM < Range * 0.2f)
				vol = RPM / (Range*0.2f);
			if (RPM > Range - (Range * 0.2f))
				vol = (Range - RPM) / (Range*0.2f);

			float pitchRange = m_onData[i].HighPitch - m_onData[i].LowPitch;
			float weight = RPM / Range;
			float pitch = m_onData[i].LowPitch + (weight*pitchRange);
			m_engineInstsOn[i]->SetPitch(pitch);
			m_engineInstsOn[i]->SetVolume(vol*onVolume);
			m_engineInstsOn[i]->Play();
		}
		else
			m_engineInstsOn[i]->Stop();
	}
	//--
}