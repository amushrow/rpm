#include "new_Screen.h"

ScreenCollection::ScreenCollection()
{
	m_screens = new Screen*[16];
	m_numScreens = 0;
	m_maxScreens = 16;
};

void ScreenCollection::AddScreen(Screen* screen)
{
	//-- Add more space
	if(m_numScreens >= m_maxScreens)
	{
		m_maxScreens += 16;
		Screen** newArray = new Screen*[m_maxScreens];
		memcpy(newArray, m_screens, sizeof(Screen*)*m_numScreens);
		delete[] m_screens;

		m_screens = newArray;
	}
	//--

	int index = screen->GetZIndex();
	bool inserted = false;
	for(UINT i=0; i<m_numScreens; i++)
	{
		if(m_screens[i]->GetZIndex() > index)
		{
			//Insert screen here
			for(UINT j=m_numScreens; j > i; j--)
				m_screens[j] = m_screens[j-1];
			m_screens[i] = screen;
			inserted = true;
			break;
		}
	}

	//Add at the end
	if(!inserted)
		m_screens[m_numScreens] = screen;

	m_numScreens++;
}

void ScreenCollection::RemoveScreen(Screen* screen)
{
	for(UINT i=0; i<m_numScreens; i++)
	{
		if(m_screens[i] == screen)
		{
			//Push back all of the other screens
			for(UINT j=i; j < m_numScreens-1; j++)
			{
				m_screens[j] = m_screens[j+1];
			}
			m_numScreens--;
		}
	}
}

void ScreenCollection::Render(SKRenderer::SKRenderDevice* renderDevice)
{
	for(UINT i=0; i<m_numScreens; i++)
	{
		m_screens[i]->Render(renderDevice);
	}
}

Screen::Screen(RenderFunction func, int zIndex, void* user_data)
{
	m_renderFunc = func;
	m_zindex = zIndex;
	m_userData = user_data;
}

void Screen::Render(SKRenderer::SKRenderDevice* renderDevice)
{
	m_renderFunc(renderDevice, m_userData);
}