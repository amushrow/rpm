#pragma once

enum PPFlags
{
	PP_BLUR = 0x01,
	PP_DESATURATE = 0x02
};