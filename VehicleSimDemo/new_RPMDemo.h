#pragma once

#include "SKRenderDevice.h"
#include "SKPhysics.h"
#include "SKAudio.h"
#include "SKInput.h"
#include "ButtonMapData.h"
#include "DataLib.h"
#include "Queue.h"
#include "new_FiniteStateMachine.h"
#include "new_Screen.h"
#include "Clock.h"
using namespace SKRenderer;
using namespace SKPhysics;
using namespace SKInput;
using namespace SKAudio;

#define MAX_BUFFERED_FRAMES 2
typedef void (*InvokeFunc)(LPVOID args);
namespace TopStates
{
	class GameState;
}

struct InvokeFuncData
{
	InvokeFunc	Func;
	LPVOID		Args;
	__int64		FrameID;
};
struct CommandRecorderData
{
	SKRenderer::SKCommandRecorder*	Buffer;
	__int64							FrameID;
};

class RPMDemo
{
public:
	static RPMDemo*	GetInstance		();
	static void		DestroyInstance	();

	void Initialise				(HINSTANCE hInst, HWND hWnd);
	bool Invoke					(InvokeFunc func, LPVOID args);
	bool Run					();
	bool IsRunning				() { return m_running; }
	bool IsInitialised			() { return m_initialised; }
	void AddScreen				(Screen* screen);
	void RemoveScreen			(Screen* screen);
	void StopGame				() { m_running = false; }
	void DisplaySettingsChanged	();
	void SaveJoypadConfig		();
	void LoadJoypadConfig		();
	int  GetFPS					() { return m_curFps; }

	//-- Exposed Devices
	SKRenderDevice*			Renderer;
	VertexCacheManager*		VertexManager;
	SKPhysicsWorld*			PhysicsWorld;
	SKInputDevice*			Keyboard;
	SKInputDevice*			Joystick;
	SKInputDeviceMaster*	InputDevices;
	SKAudioDevice*			AudioDevice;
	//--
	
	//-- Global Resources
	UINT					TitleFont;
	UINT					StandardFont;
	UINT					BoldFont;
	UINT					MonospaceFont;
	UINT					MonospaceMedFont;
	UINT					MonospaceSmallFont;
	DArray<ButtonMapData>	StoredButtonMaps;
	UINT					JoypadMapIndex;
	DataLib::IDataManager*	DataMan;
	RenderSettings			VideoSettings;
	UINT					ShadowResolutionLevel;
	UINT					FilteringQuality;
	//--

	// Game Step
	double					TimeStep;


private:
	RPMDemo();
	~RPMDemo();

	static DWORD WINAPI t_logic(PVOID args);
	static void			loadGameDataResource(void* sender, SKRenderer::LoadResourceEventArgs* e, void* user_data);

	Clock									m_gameTimer;
	Clock									m_graphicsTimer;
	t_Queue<CommandRecorderData>			m_renderBufferQueue;
	t_Queue<SKRenderer::SKCommandRecorder*> m_freeBufferQueue;
	t_Queue<InvokeFuncData>					m_queuedFuncs;
	DArray<InvokeFuncData>					m_localQueuedFuncs;
	ScreenCollection						m_screens;
	POINT									m_borderSize;
	__int64									m_frameID;
	double									m_fpsTimer;
	HANDLE									m_logicThread;
	DWORD									m_mainThreadID;
	HWND									m_hWnd;
	HMODULE									m_dataLibDll;
	SKRenderer::SKCommandRecorder*			m_commandBuffer;
	SKRendererObject*						m_renderObj;
	SKInputObject*							m_inputObj;
	SKPhysicsObject*						m_physicsObj;
	SKAudioObject*							m_audioObject;
	FiniteStateMachine::Machine*			m_topLevelFSM;	
	TopStates::GameState*					m_gameState;

	int										m_fpsCount;
	int										m_curFps;
	bool									m_running;
	bool									m_initialised;
	bool									m_reinitialiseGraphics;
	bool									m_multithreaded;

	static RPMDemo*							m_instance;
};