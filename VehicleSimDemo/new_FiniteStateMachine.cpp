#include "new_FiniteStateMachine.h"
using namespace FiniteStateMachine;

Machine::Machine() : m_states(0, 2), m_transitions(0, 2)
{
	m_currentState = NULL;
	m_currentStateID = 0xFFFFFFFF;
	m_parentMachine = NULL;
}

Machine::~Machine()
{
	UINT len = m_transitions.Length();
	for(UINT i=0; i<len; i++)
		delete[] m_transitions[i].EventName;
}

void Machine::Enter()
{
	if(m_currentState)
		m_currentState->Enter();
}

void Machine::Run()
{
	//-- Handle queued events
	bool EventHandled = false;
	for(UINT i=0; i < m_eventQueue.Length(); i++)
	{
		char* eventName = m_eventQueue[i];

		UINT len = m_transitions.Length();
		for(UINT i=0; i<len; i++)
		{
			Transition& trans = m_transitions[i];
			if(trans.CurrentState == m_currentStateID && strcmp(eventName, trans.EventName)==0)
			{
				m_currentState->Leave();
				m_currentState = trans.NextClassPtr;
				m_currentStateID = trans.NextState;
				m_currentState->Enter();
				EventHandled = true;
				break;
			}
		}

		if(!EventHandled && m_currentState != NULL)
			m_currentState->FireEvent(eventName);

		delete[] eventName;
	}
	m_eventQueue.Clear();
	//--

	if(m_currentState != NULL && !EventHandled)
		m_currentState->Run();
}

void Machine::Leave()
{
	if(m_currentState)
		m_currentState->Leave();
}

void Machine::FireEvent(char* eventName)
{
	//Add the event to the queue
	int len = strlen(eventName) + 1;
	char* cpy_eventName = new char[len];
	strcpy_s(cpy_eventName, len, eventName);
	m_eventQueue.Add(cpy_eventName);
}

void Machine::AddState(DWORD StateID, State* newState, bool startState)
{
	if(newState == NULL)
		return;

	UINT len = m_states.Length();
	for(UINT i=0; i<len; i++)
		if(m_states[i].StateID == StateID || m_states[i].ClassPtr == newState)
			return;

	IDState state = { newState, StateID };
	m_states.Add(state);
	newState->m_parentMachine = this;

	if(startState)
	{
		if(m_currentState) m_currentState->Leave();
		m_currentState = newState;
		m_currentStateID = StateID;
	}
}

void Machine::AddTransition(DWORD State, DWORD NextState, const char* EventName)
{
	FiniteStateMachine::State* statePtr = NULL;
	
	UINT len = m_states.Length();
	for(UINT i=0; i<len; i++)
	{
		if(m_states[i].StateID == NextState)
		{
			statePtr = m_states[i].ClassPtr;
			break;
		}
	}

	if(statePtr != NULL)
	{
		len = strlen(EventName) + 1;
		char* ev = new char[len];
		strcpy_s(ev, len, EventName);

		Transition trans = {State, NextState, statePtr, ev};
		m_transitions.Add(trans);
	}
}

void Machine::SetCurrentState(DWORD StateID)
{
	UINT len = m_states.Length();
	for(UINT i=0; i<len; i++) {
		if(m_states[i].StateID == StateID && m_states[i].ClassPtr != m_currentState)
		{
			if(m_currentState != NULL)
				m_currentState->Leave();
			m_currentState = m_states[i].ClassPtr;
			m_currentStateID = StateID;
			m_currentState->Enter();
			break;
		}
	}
}

DWORD Machine::GetCurrentState()
{
	return m_currentStateID;
}