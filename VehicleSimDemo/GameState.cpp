#ifdef BACON
#include "StaticGame.h"
#include "GameButtons.h"
#include <Camera.h>
#include <CustomObject.h>
#include <CustomObjectData.h>
#include "FadeManager.h"
#include "GameState.h"
#include <Clock.h>
#include "CustomDataLib.h"

using namespace SKPhysics;
using namespace SKRenderer;
using namespace SKMath;
using namespace DataLib;

SKMath::SKMatrix lightProj;
SKMath::SKMatrix lightView;

void setLightMat(void* sender, SKRenderDevice::MatrixChangedEventArgs* e, void* user_data)
{
	//SKMath::SKMatrix final = e->Mat * lightView * lightProj;
	//Game::Renderer->SetShaderConst("LightWorldViewProj", &final, 64);
}

GameState::GameState(std::string name, long ID) : State(name, ID)
{
	m_initialised = false;
	m_useShaders = true;
}

GameState::~GameState()
{

}

StateResult GameState::EnterState()
{
	m_do60 = false;
	m_60time = 0;

	WaitForSingleObject(Game::RendererLock, INFINITE);
	m_sceneGraph = new SKSceneGraph();

	if(!loadGraphics())
		return RES_LEAVE;

	if(!loadPhysics())
		return RES_LEAVE;

	ReleaseSemaphore(Game::RendererLock, 1, NULL);

	unsigned len = m_boxes.Length();
	for(unsigned i=0; i<len; i++)
		m_sceneGraph->AddScene( m_boxScenes[i]);

	m_camera = new Camera(Game::Renderer);
	m_camera->SetTarget( &m_camTarget );
	m_camera->SetStage(0);
	m_camera->SetSpeed(10.f);

	m_camOffsets[0].Set(0, 1.5f, -4.f);
	m_camOffsets[1].Set(-0.25f,0.4f,0.25f);
	m_camOffsets[2].Set(1.4f,0.5f,-2.5f);
	m_camOffsets[3].Set(-3.5f,0.3f,0.5f);
	m_camLookOffsets[0].Set(0,0.3f,0.5f);
	m_camLookOffsets[1].Set(-0.25f, 0.5f, 1.25f);

	m_camera->SetPosOffset(m_camOffsets[0]);
	m_camera->SetLookOffset(m_camLookOffsets[0]);
	
	for(int i=0; i<3; i++)
	{
		m_renderLists[i] = new RenderList(256, 16);
	}

	if(Game::Joystick)
	{
		for(unsigned i=0; i<Game::StoredButtonMaps.Length(); i++)
		{
			if(Game::StoredButtonMaps[i].DeviceID == Game::Joystick->GetDeviceID())
			{
				m_controllerIndex = i;
				break;
			}
		}

		m_vehicle->SetBrakeSensitivity( Game::StoredButtonMaps[m_controllerIndex].BrakeSens );
		m_vehicle->SetThrottleSensitivity( Game::StoredButtonMaps[m_controllerIndex].ThrottleSens );
		m_vehicle->SetSteerSensitivity( Game::StoredButtonMaps[m_controllerIndex].SteerSens );
	}

	//Simulate a couple of seconds so that everything is settled when we first see the car
	m_vehicle->SetBraking(1.f);
	Game::PhysicsWorld->Update(2.f);

	//Make sure the camera is happy too
	const SKRigidBody::State* state = Game::PhysicsWorld->GetBodyState(m_bodies[ELISE_BODY]);
	m_camTarget.Pos = state->Position;
	m_camTarget.Rot = state->Orientation;
	m_camTarget.Mat = state->Transformations;
	m_camera->SetStage(0);
	m_camera->Update(0.f);
	m_camera->SetStage(1);

	FadeManager::Instance()->FadeIn(0.75f);
	m_timer = 0.f;
	m_quiting = false;
	m_ready = false;
	m_timing = false;
	m_lapTimer = 0;
	m_lapCounter = 1;
	m_posClassify = BACK;
	m_initialised = true;

	return RES_CONTINUE;
}

StateResult GameState::LeaveState()
{
	if(m_initialised)
	{
		m_initialised = false;

		WaitForSingleObject(Game::RendererLock, INFINITE);
		Game::PhysicsWorld->DestroyVehicle(m_vehicle);
		for(int i=0; i<5; i++)
		{
			m_sceneGraph->RemoveScene(m_scenes[i]);
			Game::PhysicsWorld->DestroyBody(m_bodies[i]);
		}
		int box_len = m_boxes.Length();
		for(int i=0; i<box_len; i++)
		{
			m_sceneGraph->RemoveScene(m_boxScenes[i]);
			Game::PhysicsWorld->DestroyBody(m_boxes[i]);
		}

		int numBods = m_trackMesh.Length();
		for(int i=0; i<numBods; i++)
		{
			Game::PhysicsWorld->DestroyBody(m_trackMesh[i]);
		}
		m_sceneGraph->RemoveScene(m_trackScene);

		//Since we Detached the wheels, they are now our responsibility to delete
		delete m_scenes[WHEEL_FL];
		delete m_scenes[WHEEL_FR];
		delete m_scenes[WHEEL_RL];
		delete m_scenes[WHEEL_RR];

		ReleaseSemaphore(Game::RendererLock, 1, NULL);

		delete m_camera;
		delete m_sceneGraph;

		for(int i=0; i<3; i++)
		{
			delete m_renderLists[i];
		}
	}

	return RES_CONTINUE;
}

static float fps = 0;
static float fpsCounter = 0;
static float fpsTimer = 0;
static float timer = 0;

StateResult GameState::Update(float timeStep)
{
	for(int i=0; i<4; i++)
	{
		m_wheelForces[i].Set(0,0,0);
	}

	Game::PhysicsWorld->Update(timeStep);
	
	//-- Check lap position
	SKMath::SKVector vehiclePos = m_vehicle->GetPosition();
	if((vehiclePos - m_finishPos).GetSqrLength() < 225)
	{
		int classify = m_finishLine.Classify(vehiclePos);
		if(classify != m_posClassify)
		{
			float dir = m_lapDir * m_bodies[ELISE_BODY]->GetVelocity();
			if(classify==FRONT && dir > EPSILON)
				m_lapCounter--;
			else if(classify==BACK && dir < -EPSILON)
				m_lapCounter++;
		}
		m_posClassify = classify;
	}

	if(m_timing)
		m_lapTimer += timeStep;
	
	if(m_lapCounter == 0)
	{
		if(m_timing)
			m_lapTimes.Add(m_lapTimer);

		m_lapTimer = 0;
		m_timing = true;
		m_lapCounter = 1;
	}
	//--

	const SKRigidBody::State* state;
	for(int i=0; i<5; i++)
	{
		state = Game::PhysicsWorld->GetBodyState(m_bodies[i]);//m_bodies[i]->GetInterpolatedState(frameWeight);
		m_scenes[i]->SetWorldMatrix(state->Transformations);	

		if(i == ELISE_BODY)
		{
			m_camTarget.Pos = state->Position;
			m_camTarget.Rot = state->Orientation;
			m_camTarget.Mat = state->Transformations;
		}
	}
	
	unsigned len = m_boxes.Length();
	for(unsigned i=0; i<len; i++)
	{
		state = Game::PhysicsWorld->GetBodyState(m_boxes[i]);
		m_boxScenes[i]->SetWorldMatrix(state->Transformations);
	}

	m_camera->Update(timeStep);
	m_sceneGraph->QueueSetCameraPos(m_camera->GetPos());

	if(fpsTimer < 1.f)
	{
		fpsTimer += timeStep;
		fpsCounter += 1.f;
	}
	else
	{
		fps = fpsCounter;
		fpsCounter = 0.f;
		fpsTimer = 0.f;
	}
	m_timer += timeStep;

	if( Game::Keyboard->KeyPressed(GB_ESCAPE) && !m_quiting)
	{
		FadeManager::Instance()->FadeOut(SKColour(0,0,0), 0.25f);
		m_quiting = true;
		m_timer = 0.f;
	}
	if(m_quiting)
	{
		if(m_timer > 0.25f)
			return RES_LEAVE;
	}
	else if(m_timer > 0.05f)
	{
		m_timer = 0.f;
		m_cachedSpeed = m_bodies[ELISE_BODY]->GetVelocity().GetLength() * 2.2369f;
		m_cachedRPM = m_vehicle->GetRPM();
	}

	
	if(m_do60 && m_cachedSpeed > 0.01f)
	{
		m_60time += timeStep;
		if(m_cachedSpeed >= 60)
			m_do60 = false;
	}

	float acceleration = 0.f;
	float braking = 0.f;
	float steering = 0.f;
	bool useKb = false;

	if( Game::Keyboard->KeyPressed(GB_CAM1) )
	{
		m_camera->SetPosOffset(m_camOffsets[0]);
		m_camera->SetLookOffset(m_camLookOffsets[0]);
		m_camera->SetStage(1);
	}
	if( Game::Keyboard->KeyPressed(GB_CAM2) )
	{
		m_camera->SetPosOffset(m_camOffsets[1]);
		m_camera->SetLookOffset(m_camLookOffsets[1]);
		m_camera->SetStage(0);
	}
	if( Game::Keyboard->KeyPressed(GB_CAM3) )
	{
		m_camera->SetPosOffset(m_camOffsets[2]);
		m_camera->SetLookOffset(m_camLookOffsets[0]);
		m_camera->SetStage(0);
	}

	if( Game::Keyboard->KeyPressed(GB_CAM4) )
	{
		m_do60 = true;
		m_60time = 0;
	}

	//-- Keyboard Input
	if( Game::Keyboard->KeyDown(GB_THROTTLE) )
	{
		acceleration = 1.f;
		useKb = true;
	}
	
	if( Game::Keyboard->KeyDown(GB_BRAKE) )
	{
		braking = 1.f;
		useKb = true;
	}

	if( Game::Keyboard->KeyDown(GB_STEER_LEFT) )
	{
		steering = -1.f;
		useKb = true;
	}
	else if( Game::Keyboard->KeyDown(GB_STEER_RIGHT) )
	{
		steering = 1.f;
		useKb = true;
	}

	if( Game::Keyboard->KeyPressed(GB_GEARUP) )
		m_vehicle->NextGear();

	if( Game::Keyboard->KeyPressed(GB_GEARDOWN) )
		m_vehicle->PrevGear();

	if( Game::Keyboard->KeyPressed(GB_TOGGLESHADERS) )
		m_useShaders = !m_useShaders;
	//--

	//-- Joypad Input
	if(Game::Joystick && !useKb)
	{
		if( Game::Joystick->KeyPressed(GB_GEARUP) )
			m_vehicle->NextGear();

		if( Game::Joystick->KeyPressed(GB_GEARDOWN) )
			m_vehicle->PrevGear();

		if(Game::StoredButtonMaps[m_controllerIndex].CombinedAxes)
		{
			float val = Game::Joystick->GetKeyValue(GB_COMBINED_BRAKE_THROTTLE);
			val *= Game::StoredButtonMaps[m_controllerIndex].ThrottleDir;
			acceleration = max(0.f, val);
			braking = max(0.f, -val);
		}
		else
		{
			acceleration = Game::Joystick->GetKeyValue(GB_THROTTLE);
			acceleration *= Game::StoredButtonMaps[m_controllerIndex].ThrottleDir;
			acceleration = (acceleration + 1.f) / 2.f;

			braking = Game::Joystick->GetKeyValue(GB_BRAKE);
			braking *= Game::StoredButtonMaps[m_controllerIndex].BrakeDir;
			braking = (braking + 1.f) / 2.f;
		}

		steering = Game::Joystick->GetKeyValue(GB_STEERING);
	}
	//--

	//--Pseudo handbrake
	if(m_cachedSpeed < 0.1f && (braking+acceleration) < EPSILON)
		braking = 1.f;
	//--

	m_vehicle->SetAcceleration(acceleration);
	m_vehicle->SetBraking(braking);
	m_vehicle->SetSteering(steering);

	m_camera->GetDeviceLookat(&m_ViewLookAt[Game::WorkingBufferID].Pos,
		&m_ViewLookAt[Game::WorkingBufferID].LookAt,
		&m_ViewLookAt[Game::WorkingBufferID].Up);

	m_sceneGraph->Update();

	m_renderLists[Game::WorkingBufferID]->Clear();
	const SceneList* scenes = m_sceneGraph->GetScenes();
	len = scenes->Length();
	for(unsigned i=0; i<len; i++)
	{
		if(scenes->At(i)->IsVisible())
			addModelsToRenderList(m_renderLists[Game::WorkingBufferID], scenes->At(i));
	}
	m_buffUseShader[Game::WorkingBufferID] = m_useShaders;

	WaitForSingleObject(Game::BufferLock, INFINITE);
	Game::NextRenderBufferID = Game::WorkingBufferID;
	for(int i=0; i<3; i++)
	{
		if(i != Game::NextRenderBufferID &&	i != Game::RenderBufferID)
			Game::WorkingBufferID = i;
	}
	ReleaseSemaphore(Game::BufferLock, 1, NULL);

	return RES_CONTINUE;
}

void GameState::addModelsToRenderList(RenderList* list, SKScene* scene)
{
	const SKRenderer::ModelList* models = scene->GetModels();
	unsigned len = models->Length();
	ModelConMatrix newmodel;
	scene->GetFinalMatrix(&newmodel.Matrix);
	for(unsigned i=0; i<len; i++)	{
		newmodel.Model = models->At(i);
		list->Add(newmodel);
	}

	SKRenderer::SceneList children = scene->GetChildren();
	len = children.Length();
	for(unsigned i=0; i<len; i++)	{
		if(children[i]->IsVisible())
			addModelsToRenderList(list, children[i]);
	}
}

void GameState::Render(SKMatrixStack& stack, float timeStep)
{
	if(!m_initialised)
		return;

	if(GetState() < S_LEAVING)
	{
		Game::Renderer->UseShaders(m_buffUseShader[Game::WorkingBufferID]);
		UINT numPasses;
		if(m_buffUseShader[Game::WorkingBufferID])
			Game::Renderer->BeginEffect(&numPasses, VID_UU);
		else
			numPasses = 1;

		for(UINT i=0; i<numPasses; i++)
		{
			if(m_buffUseShader[Game::WorkingBufferID]) Game::Renderer->StartEffectPass(i);

			Game::Renderer->SetShaderConst("CamPos", &m_ViewLookAt[Game::RenderBufferID].Pos, 16);
			Game::Renderer->CommitShaderChanges();

			SKMath::SKVector from = m_ViewLookAt[Game::RenderBufferID].LookAt - m_light.Direction;
			Game::Renderer->SetViewLookAt(m_ViewLookAt[Game::RenderBufferID].Pos,
				m_ViewLookAt[Game::RenderBufferID].LookAt,
				m_ViewLookAt[Game::RenderBufferID].Up);

			SKColour col(1,0,0);
			SKMath::SKVector fwd = m_bodies[ELISE_BODY]->GetOrientation().Rotate(SKMath::SKVector::Forward);
			fwd.y = 0;
			fwd.Normalise();
			SKMath::SKVector up = SKMath::SKVector::Up;
			SKMath::SKVector right = fwd % up;
			right.Normalise();

			RenderList* curList = m_renderLists[ Game::RenderBufferID ];
			unsigned len = curList->Length();
			//-- Render non-alpha components of the screen
			Game::Renderer->SetBackfaceCulling(RS_CULL_CCW);
			Game::VertexManager->SetVertexSortOrder(RS_SORT_NONE);
			for(unsigned cake=0; cake<len; cake++)
			{
				const ModelConMatrix& m = curList->At(cake);
				SKRenderer::MaterialPointer mat = Game::Renderer->GetMaterialManager()->GetMaterial(m.Model->GetMaterialID());
				if(!mat->Alpha)
					Game::VertexManager->Render(m.Model, m.Matrix);
			}
			//--
			Game::VertexManager->ForcedFlush();
			//-- Render alpha components
			Game::Renderer->SetBackfaceCulling(RS_CULL_NONE);
			Game::VertexManager->SetVertexSortOrder(RS_BACK_TO_FRONT);
			for(unsigned cake=0; cake<len; cake++)
			{
				const ModelConMatrix& m = curList->At(cake);
				SKRenderer::MaterialPointer mat = Game::Renderer->GetMaterialManager()->GetMaterial(m.Model->GetMaterialID());
				if(mat->Alpha)
					Game::VertexManager->Render(m.Model, m.Matrix);
			}
			//--

			if(m_buffUseShader[Game::WorkingBufferID]) Game::Renderer->EndEffectPass();
		}

		if(m_buffUseShader[Game::WorkingBufferID]) Game::Renderer->EndEffect();
	}
}

void GameState::Draw(SKMath::SKMatrixStack& stack, float timeStep)
{
	if(!m_initialised)
		return;

	if(GetState() < S_LEAVING)
	{
		Game::Renderer->SetWorldTransform(NULL);
		SKColour textCol(1,1,1);
		char buff[128];
		sprintf_s(buff, 128, "Logic FPS: %.2f\nSpeed: %.3f mph\nGear: %s\nRPM: %.0f\n0-60: %.3f", fps, m_cachedSpeed, m_vehicle->GetGearName(), m_cachedRPM, m_60time);
		Game::TitleFont->Draw(buff, 16, 5, 2, TOP_LEFT, textCol);

		float yPos = 96;
		for(unsigned i=0; i<m_lapTimes.Length(); i++)
		{
			float fTime = m_lapTimes[i];
			int iTime = (int)fTime;
			int mins = iTime / 60;
			int secs = iTime % 60;
			int msecs = (int)((fTime - iTime) * 1000);

			sprintf_s(buff, 128, "%02d: %02d:%02d:%03d", i, mins, secs, msecs);
			Game::TitleFont->Draw(buff, 16, 5, yPos, TOP_LEFT, textCol);
			yPos += 16;
		}

		int iTime = (int)m_lapTimer;
		int mins = iTime / 60;
		int secs = iTime % 60;
		int msecs = (int)((m_lapTimer - iTime) * 1000);
		sprintf_s(buff, 128, "%02d:%02d:%03d", mins, secs, msecs);
		Game::StandardFont->Draw(buff, 24, Game::VideoSettings.DisplayWidth*0.5f, 10, TOP_MIDDLE, textCol);
		
		sprintf_s(buff, 128, "Press 1-3 to change the camera angle.");
		yPos = (float)(Game::VideoSettings.DisplayHeight - 18);
		Game::TitleFont->Draw(buff, 16, 5, yPos, TOP_LEFT, textCol);
	}

	if(GetState() == S_PAUSED)
	{
		float scale = (float)Game::VideoSettings.DisplayHeight / 720.f;
		float x = (float)(Game::VideoSettings.DisplayWidth / 2);
		float y = (float)(Game::VideoSettings.DisplayHeight / 2);
		Game::StandardFont->Draw("PAUSED", 48*scale, x, y, MIDDLE, SKColour(1,1,1));
	}
}

bool GameState::loadGraphics()
{
	SKScene* eliseMain = NULL;
	SKScene* wheel = NULL;
	
	DataLib::IFile* file = Game::DataMan->GetFiles().Find("Elise.x");
	int len = (int)file->GetOriginalLength();
	BYTE* buffer = new BYTE[len];
	DataLib::IStream* stream = Game::DataMan->GetFileStream(file->GetUID());
	int bytesRead = stream->Read(buffer, 0, len);

	if( FAILED(Game::ModelMan->LoadModel(buffer, bytesRead, "Elise", true)))
		return false;

	delete[] buffer;
	stream->Release();

	file = Game::DataMan->GetFiles().Find("Donington.x");
	len = (int)file->GetOriginalLength();
	buffer = new BYTE[len];
	stream = Game::DataMan->GetFileStream(file->GetUID());
	bytesRead = stream->Read(buffer, 0, len);
	if( FAILED(Game::ModelMan->LoadModel(buffer, bytesRead, "Donington", true)))
		return false;

	delete[] buffer;
	stream->Release();
	
	m_trackScene = Game::ModelMan->GetModel("Donington");
	eliseMain = Game::ModelMan->GetModel("Elise");

	m_scenes[WHEEL_FL] = eliseMain->FindChild("TireFL");
	m_scenes[WHEEL_FR] = eliseMain->FindChild("TireFR");
	m_scenes[WHEEL_RL] = eliseMain->FindChild("TireRL");
	m_scenes[WHEEL_RR] = eliseMain->FindChild("TireRR");

	for(int i=0; i<4; i++)
	{
		if(m_scenes[i])
		{
			m_scenes[i]->Detach();
			m_sceneGraph->AddScene( m_scenes[i] );
		}
	}
	m_sceneGraph->AddScene(eliseMain);
	m_sceneGraph->AddScene(m_trackScene);
	m_scenes[ELISE_BODY] = eliseMain;

	m_light.Ambient = 0.3f;
	m_light.Diffuse.Set(0.7f, 0.68f, 0.63f);
	m_light.Specular.Set(0.7f, 0.68f, 0.63f);
	m_light.Type = LGT_DIRECTIONAL;
	m_light.Direction.Set(-0.5f, -1, 0);
	m_light.Direction.Normalise();

	Game::Renderer->SetShaderConst("LightAmbient", &m_light.Ambient, 16);
	Game::Renderer->SetShaderConst("LightColour", &m_light.Diffuse, 16);
	Game::Renderer->SetShaderConst("LightDir", &m_light.Direction, 12);

	float fnear = -35.f;
	float ffar = 40.f;
	lightProj._11 = 2.0f/128.f;
	lightProj._22 = 2.0f/128.f;
	lightProj._33 = 1.0f/(ffar - fnear);
	
	lightProj._41 = 0;
	lightProj._42 = 0;
	lightProj._43 = fnear/(fnear - ffar);
	lightProj._44 = 1.f;

	
	SKMath::SKVector Pos(117,7,-103);

	float d = SKMath::SKVector::Up * m_light.Direction;
	SKMath::SKVector tmp = m_light.Direction * d;
	SKMath::SKVector Up = SKMath::SKVector::Up - tmp;
	float L = Up.Normalise();
	if(L < EPSILON)
	{
		SKMath::SKVector Y(0,0,1);
		tmp = m_light.Direction * m_light.Direction.z;
		Up = Y - tmp;
		L = Up.Normalise();
	}
	SKMath::SKVector Right = Up % m_light.Direction;

	lightView._14 = lightView._24 = lightView._34 = 0.0f;
	lightView._44 = 1.0f;

	lightView._11 = Right.x;
	lightView._21 = Right.y;
	lightView._31 = Right.z;
	lightView._41 = -(Right*Pos);

	lightView._12 = Up.x;
	lightView._22 = Up.y;
	lightView._32 = Up.z;
	lightView._42 = -(Up*Pos);

	lightView._13 = m_light.Direction.x;
	lightView._23 = m_light.Direction.y;
	lightView._33 = m_light.Direction.z;
	lightView._43 = -(m_light.Direction*Pos);
	
	Game::Renderer->AddWorldMatChangedEvent(setLightMat, NULL);

	Game::Renderer->SetAmbientLight(0,0,0);
	Game::Renderer->SetLight(&m_light, 0);
	Game::Renderer->EnableSpecular(true);

	file = Game::DataMan->GetFiles().Find("Box.x");
	len = (int)file->GetOriginalLength();
	buffer = new BYTE[len];
	stream = Game::DataMan->GetFileStream(file->GetUID());
	bytesRead = stream->Read(buffer, 0, len);

	Game::ModelMan->LoadModel(buffer, bytesRead, "Box", true);
	delete[] buffer;
	stream->Release();

	m_origBoxScene = Game::ModelMan->GetModel("Box");

	return true;
}

bool GameState::loadPhysics()
{
	//--Load Up Elise Data
	IDataObjectCollection& objects =  Game::DataMan->GetDataObjects();
	IDataType *lotusElise, *startPos, *finishLine;
	int len = objects.Count();
	for(int i=0; i<len; i++)
	{
		int id = objects[i]->GetID();
		switch(id)
		{
		case 1: lotusElise = objects[i]; break;
		case 4: startPos = objects[i]; break;
		case 5: finishLine = objects[i]; break;
		}
	}
	 
	if(lotusElise == NULL || startPos == NULL || finishLine == NULL)
		return false;

	DataLibExporter::VehicleDescription eliseDesc;
	DataLibExporter::CustomLoader::LoadVehicleDescription(lotusElise, &eliseDesc);

	DataLibExporter::PosDir startDesc, finishDesc;
	DataLibExporter::CustomLoader::LoadPosDir(startPos, &startDesc);
	DataLibExporter::CustomLoader::LoadPosDir(finishLine, &finishDesc);
	
	SKMath::SKQuaternion ori; ori.MakeFromEuler(finishDesc.Pitch, finishDesc.Yaw, finishDesc.Roll);
	SKMath::SKVector dir = ori.Rotate(SKMath::SKVector::Forward);
	dir.Normalise();
	m_finishPos = SKMath::SKVector(&finishDesc.Position.X);
	m_finishLine.Set(dir, m_finishPos);
	m_lapDir = dir;
	//Wow, I bet that was easier than you thought it'd be. The new data tool rocks eh? With its fancy exporter an all that jazz
	//--

	//--Set World Parameters
	// TODO: something
	Game::PhysicsWorld->FixedFrequency(120);
	Game::PhysicsWorld->SetNumThreads(Game::NumPhysicsThreads);
	Game::PhysicsWorld->Initialise(100, 300);
	//--

	//-- Ground Plane
	//m_bodies[GROUND] = Game::PhysicsWorld->CreateBody(SKPhysics::PLANE, SKVector::Up);
	//m_bodies[GROUND]->SetInverseMass(0);
	//m_bodies[GROUND]->SetFriction(1.f);
	//--

	SKMatrix tensor, localMat;
	SKVector extents;
	SKVector extentsSq;
	SKVector vcMin, vcMax, pos;
	SKVector centerOfMass = SKVector(&eliseDesc.Centre_of_Mass.X);
	SKRigidBody* tempBody;
	float mass;
	tensor.Identity();

	//-- Track Mesh
	DataLib::IFile* file = Game::DataMan->GetFiles().Find("Donington_phy.obj");
	len = (int)file->GetOriginalLength();
	BYTE* buffer = new BYTE[len];
	DataLib::IStream* stream = Game::DataMan->GetFileStream(file->GetUID());
	int bytesRead = stream->Read(buffer, 0, len);

	SKRigidBody** meshBodies;
	int numBods = Game::PhysicsWorld->LoadPhysicsMesh(buffer, bytesRead, &meshBodies);
	delete[] buffer;
	stream->Release();

	m_trackMesh.Reserve(numBods);
	for(int i=0; i<numBods; i++)
	{
		meshBodies[i]->SetInverseMass(0);
		m_trackMesh.Add(meshBodies[i]);
	}
	
	delete[] meshBodies;
	//--

	//-- Elise Body
	// TODO: Don't hard-code scene names
	SKScene* boundingBox = m_scenes[ELISE_BODY]->FindChild("Body_bb");
	boundingBox->SetVisible(false, true);

	// TODO: Add this kind of data to the GameData file
	boundingBox->GetExtents(vcMin, vcMax);
	extents.Interpolate(vcMin, vcMax, 0.5f);
	extents -= vcMin;
	extentsSq.Set(extents.x * extents.x, extents.y * extents.y, extents.z * extents.z);
	
	tempBody = Game::PhysicsWorld->CreateBody(SKPhysics::BOX, extents, SKVector::Up);
	m_bodies[ELISE_BODY] = tempBody;
	tempBody->SetRestitution( 0.3f );
	tempBody->SetFriction( 0.3f );
	tempBody->SetDynamicFriction( 0.4f );
	tempBody->SetCanSleep( false );
	tempBody->SetCOM(centerOfMass);
	tensor._11 = 0.3f * eliseDesc.Mass * (extentsSq.y + extentsSq.z);
	tensor._22 = 0.3f * eliseDesc.Mass * (extentsSq.x + extentsSq.z);
	tensor._33 = 0.3f * eliseDesc.Mass * (extentsSq.x + extentsSq.y);
	m_bodies[ELISE_BODY]->SetMass( eliseDesc.Mass );
	m_bodies[ELISE_BODY]->SetInertiaTensor( tensor );
	
	boundingBox->GetLocalMatrix( &localMat);
	pos = localMat._row4;
	localMat._row4.Set(0,0,0);
	
	m_scenes[ELISE_BODY]->SetLocalMatrix(localMat);
	tempBody->SetPositionWithoutCom( pos );

	m_vehicle = Game::PhysicsWorld->CreateVehicle(m_bodies[ELISE_BODY], 4);
	//--

	//-- Set up the wheels
	SKVector deltaSpring(0, 0.3f, 0);
	SKVector deltaArm(0, 0, 0);
	SKVector carPos = m_bodies[ELISE_BODY]->GetPosition();

	extents.y = 0.1f;

	//--Front Left
	extents.x = eliseDesc.FrontLeft.Diameter*0.5f;
	extentsSq.Set(extents.x * extents.x, extents.y * extents.y, extents.z * extents.z);

	tensor._11 = 0.5f * eliseDesc.FrontLeft.Mass * extentsSq.x;
	tensor._22 = (0.083f * eliseDesc.FrontLeft.Mass * extentsSq.y) + (0.25f * eliseDesc.FrontLeft.Mass * extentsSq.x);
	tensor._33 = (0.083f * eliseDesc.FrontLeft.Mass * extentsSq.y) + (0.25f * eliseDesc.FrontLeft.Mass * extentsSq.x);

	tempBody = Game::PhysicsWorld->CreateBody(SKPhysics::SPHERE_SLICE, extents, SKMath::SKVector::Right);
	tempBody->SetMass(eliseDesc.FrontLeft.Mass);
	tempBody->SetInertiaTensor(tensor);
		
	m_scenes[WHEEL_FL]->GetLocalMatrix(&localMat);
	pos = localMat._row4;
	localMat._row4.Set(0,0,0,1);
	m_scenes[WHEEL_FL]->SetLocalMatrix(localMat);
		
	tempBody->SetPositionWithoutCom(pos);
	tempBody->SetRestitution(0.0f);
	tempBody->SetAngularDamping(1.f);
	tempBody->SetLinearDamping(1.f);
	tempBody->SetCanSleep(false);
	m_bodies[WHEEL_FL] = tempBody;

	pos.Set(-0.734f, -0.2667f, 1.1093f);
	deltaArm.Set(0.4f, 0, 0);
	pos -= centerOfMass;
	m_vehicle->SetWheel(WHEEL_FL, SKPhysics::SKVehicle::WF_STEER, tempBody, pos+deltaArm, pos+deltaSpring, SKMath::SKVector::Right, SKMath::SKVector::Forward);
	m_vehicle->SetArmLength(WHEEL_FL, 0.4f);
	//--

	//--Front Right
	extents.x = eliseDesc.FrontRight.Diameter*0.5f;
	extentsSq.Set(extents.x * extents.x, extents.y * extents.y, extents.z * extents.z);

	tensor._11 = 0.5f * eliseDesc.FrontRight.Mass * extentsSq.x;
	tensor._22 = (0.083f * eliseDesc.FrontRight.Mass * extentsSq.y) + (0.25f * eliseDesc.FrontRight.Mass * extentsSq.x);
	tensor._33 = (0.083f * eliseDesc.FrontRight.Mass * extentsSq.y) + (0.25f * eliseDesc.FrontRight.Mass * extentsSq.x);

	tempBody = Game::PhysicsWorld->CreateBody(SKPhysics::SPHERE_SLICE, extents, SKMath::SKVector::Right);
	tempBody->SetMass(eliseDesc.FrontRight.Mass);
	tempBody->SetInertiaTensor(tensor);
		
	m_scenes[WHEEL_FR]->GetLocalMatrix(&localMat);
	pos = localMat._row4;
	localMat._row4.Set(0,0,0,1);
	m_scenes[WHEEL_FR]->SetLocalMatrix(localMat);
		
	tempBody->SetPositionWithoutCom(pos);
	tempBody->SetRestitution(0.0f);
	tempBody->SetAngularDamping(1.f);
	tempBody->SetLinearDamping(1.f);
	tempBody->SetCanSleep(false);
	m_bodies[WHEEL_FR] = tempBody;

	pos.Set(0.734f, -0.2667f, 1.1093f);
	deltaArm.Set(-0.4f, 0, 0);
	pos -= centerOfMass;
	m_vehicle->SetWheel(WHEEL_FR, SKPhysics::SKVehicle::WF_STEER, tempBody, pos+deltaArm, pos+deltaSpring, SKMath::SKVector::Right, SKMath::SKVector::Forward);
	m_vehicle->SetArmLength(WHEEL_FR, 0.4f);
	//--

	//--Rear Left
	extents.x = eliseDesc.RearLeft.Diameter*0.5f;
	extentsSq.Set(extents.x * extents.x, extents.y * extents.y, extents.z * extents.z);

	tensor._11 = 0.5f * eliseDesc.RearLeft.Mass * extentsSq.x;
	tensor._22 = (0.083f * eliseDesc.RearLeft.Mass * extentsSq.y) + (0.25f * eliseDesc.RearLeft.Mass * extentsSq.x);
	tensor._33 = (0.083f * eliseDesc.RearLeft.Mass * extentsSq.y) + (0.25f * eliseDesc.RearLeft.Mass * extentsSq.x);

	tempBody = Game::PhysicsWorld->CreateBody(SKPhysics::SPHERE_SLICE, extents, SKMath::SKVector::Right);
	tempBody->SetMass(eliseDesc.RearLeft.Mass);
	tempBody->SetInertiaTensor(tensor);
		
	m_scenes[WHEEL_RL]->GetLocalMatrix(&localMat);
	pos = localMat._row4;
	localMat._row4.Set(0,0,0,1);
	m_scenes[WHEEL_RL]->SetLocalMatrix(localMat);
		
	tempBody->SetPositionWithoutCom(pos);
	tempBody->SetRestitution(0.0f);
	tempBody->SetAngularDamping(1.f);
	tempBody->SetLinearDamping(1.f);
	tempBody->SetCanSleep(false);
	m_bodies[WHEEL_RL] = tempBody;

	pos.Set(-0.758f, -0.2667f, -1.1925f);
	deltaArm.Set(0.4f, 0, 0);
	pos -= centerOfMass;
	m_vehicle->SetWheel(WHEEL_RL, SKPhysics::SKVehicle::WF_DRIVE, tempBody, pos+deltaArm, pos+deltaSpring, SKMath::SKVector::Right, SKMath::SKVector::Forward);
	m_vehicle->SetArmLength(WHEEL_RL, 0.4f);
	//--

	//--Rear Right
	extents.x = eliseDesc.RearRight.Diameter*0.5f;
	extentsSq.Set(extents.x * extents.x, extents.y * extents.y, extents.z * extents.z);

	tensor._11 = 0.5f * eliseDesc.RearRight.Mass * extentsSq.x;
	tensor._22 = (0.083f * eliseDesc.RearRight.Mass * extentsSq.y) + (0.25f * eliseDesc.RearRight.Mass * extentsSq.x);
	tensor._33 = (0.083f * eliseDesc.RearRight.Mass * extentsSq.y) + (0.25f * eliseDesc.RearRight.Mass * extentsSq.x);

	tempBody = Game::PhysicsWorld->CreateBody(SKPhysics::SPHERE_SLICE, extents, SKMath::SKVector::Right);
	tempBody->SetMass(eliseDesc.RearRight.Mass);
	tempBody->SetInertiaTensor(tensor);
		
	m_scenes[WHEEL_RR]->GetLocalMatrix(&localMat);
	pos = localMat._row4;
	localMat._row4.Set(0,0,0,1);
	m_scenes[WHEEL_RR]->SetLocalMatrix(localMat);
		
	tempBody->SetPositionWithoutCom(pos);
	tempBody->SetRestitution(0.0f);
	tempBody->SetAngularDamping(1.f);
	tempBody->SetLinearDamping(1.f);
	tempBody->SetCanSleep(false);
	m_bodies[WHEEL_RR] = tempBody;

	pos.Set(0.758f, -0.2667f, -1.1925f);
	deltaArm.Set(-0.4f, 0, 0);
	pos -= centerOfMass;
	m_vehicle->SetWheel(WHEEL_RR, SKPhysics::SKVehicle::WF_DRIVE, tempBody, pos+deltaArm, pos+deltaSpring, SKMath::SKVector::Right, SKMath::SKVector::Forward);
	m_vehicle->SetArmLength(WHEEL_RR, 0.4f);
	//--

	float* bb = new float[4];
	bb[0] = eliseDesc.BrakeBalance * 0.5f;
	bb[1] = eliseDesc.BrakeBalance * 0.5f;
	bb[2] = (1.f-eliseDesc.BrakeBalance) * 0.5f;
	bb[3] = (1.f-eliseDesc.BrakeBalance) * 0.5f;
	m_vehicle->SetBrakeBalance(bb, 4);
	delete[] bb;

	m_vehicle->SetBrakeStrength(eliseDesc.BrakeForce);
	m_vehicle->SetEngineEfficiency(eliseDesc.EngineEfficiency);
	m_vehicle->SetMaxSteeringAngle(eliseDesc.MaxSteeringAngle);
	m_vehicle->SetAckermanValue(eliseDesc.AckermanValue);
	m_bodies[ELISE_BODY]->SetDragCoefficient(eliseDesc.DragCoefficient);
	m_vehicle->SetSuspensionLength(WHEEL_FL, eliseDesc.FrontLeft.SuspensionRestLen, eliseDesc.FrontLeft.MaxSuspensionLen, eliseDesc.FrontLeft.MinSuspensionLen);
	m_vehicle->SetSpringStiffness(WHEEL_FL, eliseDesc.FrontLeft.SuspensionStrength);
	m_vehicle->SetSpringDamping(WHEEL_FL, eliseDesc.FrontLeft.SuspensionDamping);
	m_vehicle->SetCastor(WHEEL_FL, eliseDesc.FrontLeft.Castor);
	m_vehicle->SetSuspensionLength(WHEEL_FR, eliseDesc.FrontRight.SuspensionRestLen, eliseDesc.FrontRight.MaxSuspensionLen, eliseDesc.FrontRight.MinSuspensionLen);
	m_vehicle->SetSpringStiffness(WHEEL_FR, eliseDesc.FrontRight.SuspensionStrength);
	m_vehicle->SetSpringDamping(WHEEL_FR, eliseDesc.FrontRight.SuspensionDamping);
	m_vehicle->SetCastor(WHEEL_FR, eliseDesc.FrontRight.Castor);
	m_vehicle->SetSuspensionLength(WHEEL_RL, eliseDesc.RearLeft.SuspensionRestLen, eliseDesc.RearLeft.MaxSuspensionLen, eliseDesc.RearLeft.MinSuspensionLen);
	m_vehicle->SetSpringStiffness(WHEEL_RL, eliseDesc.RearLeft.SuspensionStrength);
	m_vehicle->SetSpringDamping(WHEEL_RL, eliseDesc.RearLeft.SuspensionDamping);
	m_vehicle->SetCastor(WHEEL_RL, eliseDesc.RearLeft.Castor);
	m_vehicle->SetSuspensionLength(WHEEL_RR, eliseDesc.RearRight.SuspensionRestLen, eliseDesc.RearRight.MaxSuspensionLen, eliseDesc.RearRight.MinSuspensionLen);
	m_vehicle->SetSpringStiffness(WHEEL_RR, eliseDesc.RearRight.SuspensionStrength);
	m_vehicle->SetSpringDamping(WHEEL_RR, eliseDesc.RearRight.SuspensionDamping);
	m_vehicle->SetCastor(WHEEL_RR, eliseDesc.RearRight.Castor);

	if(eliseDesc.Gears.CanReverse)
		m_vehicle->SetGearRatio(SKVehicle::G_REVERSE, eliseDesc.Gears.Reverse);
	else
		m_vehicle->SetGearRatio(SKVehicle::G_REVERSE, 0.f);

	m_vehicle->SetGearRatio(SKVehicle::G_FIRST, eliseDesc.Gears.First);
	m_vehicle->SetGearRatio(SKVehicle::G_SECOND, eliseDesc.Gears.Second);
	m_vehicle->SetGearRatio(SKVehicle::G_THIRD, eliseDesc.Gears.Third);
	m_vehicle->SetGearRatio(SKVehicle::G_FOURTH, eliseDesc.Gears.Fourth);
	m_vehicle->SetGearRatio(SKVehicle::G_FIFTH, eliseDesc.Gears.Fifth);
	m_vehicle->SetGearRatio(SKVehicle::G_SIXTH, eliseDesc.Gears.Sixth);
	m_vehicle->SetGearRatio(SKVehicle::G_FINALDRIVE, eliseDesc.Gears.FinalDrive);
	m_vehicle->SetTopGear((SKVehicle::Gear)eliseDesc.Gears.NumGears);
	m_vehicle->SetEngineTorqueCurve(*reinterpret_cast<const SKMath::CubicSpline*>(eliseDesc.TorqueCurve));
	m_vehicle->AddDifferential(2,3, SKVehicle::DIFF_OPEN);

	ori.MakeFromEuler(startDesc.Pitch, startDesc.Yaw, startDesc.Roll);
	m_vehicle->SetOrientation(ori);
	m_vehicle->SetPosition(&startDesc.Position.X);

	SKVehicle::TyreData fricData;
	fricData.LoadSensitivity = reinterpret_cast<const SKMath::CubicSpline*>(eliseDesc.Friction.TyreLoadSensitivity);
	fricData.LonAccCurve = reinterpret_cast<const SKMath::CubicSpline*>(eliseDesc.Friction.SlipRatioAcc);
	fricData.LonDecCurve = reinterpret_cast<const SKMath::CubicSpline*>(eliseDesc.Friction.SlipRatioDec);
	fricData.SlipAngle = reinterpret_cast<const SKMath::CubicSpline*>(eliseDesc.Friction.SlipAngle);
	fricData.SlipVelocity = reinterpret_cast<const SKMath::CubicSpline*>(eliseDesc.Friction.SlipVelocityDropOff);
	fricData.MaxFriction = eliseDesc.Friction.MaxFriction;

	fricData.WheelRadius = eliseDesc.FrontLeft.Diameter * 0.5f;
	m_vehicle->SetTyreData(WHEEL_FL, fricData);
	fricData.WheelRadius = eliseDesc.FrontRight.Diameter * 0.5f;
	m_vehicle->SetTyreData(WHEEL_FR, fricData);
	fricData.WheelRadius = eliseDesc.RearLeft.Diameter * 0.5f;
	m_vehicle->SetTyreData(WHEEL_RL, fricData);
	fricData.WheelRadius = eliseDesc.RearRight.Diameter * 0.5f;
	m_vehicle->SetTyreData(WHEEL_RR, fricData);
	//--

	for(int i=0; i<4; i++)
	{
		m_bodies[i]->AddImpulseAppliedCallback(GameState::impulseCallback, this);
	}


	SKRigidBody* myBod;
	tensor.Identity();
	extents.Set(0.5f,0.5f,0.5f);
	extentsSq.Set(extents.x * extents.x, extents.y * extents.y, extents.z * extents.z);
	mass = 3.f;
	tensor._11 = 0.3f * mass * (extentsSq.y + extentsSq.z);
	tensor._22 = 0.3f * mass * (extentsSq.x + extentsSq.z);
	tensor._33 = 0.3f * mass * (extentsSq.x + extentsSq.y);

	//BLOCK TOWER PYRAMID
	SKMath::SKVector x(-24.05f, 0.51f, 35);
	SKMath::SKVector y;
	SKMath::SKVector deltaX(0.65f, 1.2f, 0);
	SKMath::SKVector deltaY(1.3f, 0.0f, 0);
	for(int arse=0; arse<0; arse++)
	{
		for (int i = 0; i < 4; i++)
		{
			y = x;
			for (int j = i; j < 4; j++)
			{
				myBod = Game::PhysicsWorld->CreateBody(SKPhysics::BOX, extents);
				myBod->SetMass(mass);
				myBod->SetInertiaTensor(tensor);
				myBod->SetPosition(y);
				myBod->SetRestitution(0.0f);
				myBod->SetAngularDamping(.95f);
				myBod->SetLinearDamping(.95f);
				m_boxes.Add(myBod);
				m_boxScenes.Add(m_origBoxScene->Clone());

				y += deltaY;
			}

			x += deltaX;
		}

		y.Set(0,0,0);
		x.y = 0.51f;
		x.x += 3.9f;
	}
	return true;
}

void GameState::impulseCallback(void* sender, SKPhysics::SKRigidBody::ImpulseEventArgs* e, void* user_data)
{
	GameState* self = (GameState*)user_data;
	int index = -1;
	for(int i=0; i<4; i++)
	{
		if(sender == self->m_bodies[i])
		{
			index = i;
			break;
		}
	}

	if(index != -1)
	{
		self->m_wheelForces[index] += e->Force;
	}
}
#endif