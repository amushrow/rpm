#pragma once
#include "SKAudio.h"

using namespace SKAudio;

struct ClipRangeData
{
	float LowRPM;
	float HighRPM;
	float LowPitch;
	float HighPitch;

	void Set(float a, float b, float c, float d)
	{
		LowRPM = a;
		HighRPM = b;
		LowPitch = c;
		HighPitch = d;
	}
};

class EngineSounds
{
public:
	EngineSounds();
	~EngineSounds();

	void	SetRPM(float RPM);
	void	SetThrottle(float Throttle);
	void	Play();
	void	Stop();
	void	Update();

private:
	
	SKAudioClip* m_engineClipsOn[5];
	SKAudioClip* m_engineClipsOff[5];
	SKAudioClipInstance* m_engineInstsOn[5];
	SKAudioClipInstance* m_engineInstsOff[5];

	ClipRangeData m_offData[5];
	ClipRangeData m_onData[5];

	float m_throttle;
	float m_rpm;
	bool m_playing;
};