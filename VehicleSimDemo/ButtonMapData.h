#pragma once

#include <SKInput.h>
#include <fstream>

struct ButtonMapData
{
	GUID DeviceID;
	DWORD Steering;
	DWORD SteerLeft;
	DWORD SteerRight;
	DWORD Throttle;
	DWORD Brake;
	DWORD CombinedThrottleBrake;
	DWORD GearUp;
	DWORD GearDown;
	DWORD Back;
	DWORD Select;
	DWORD Pause;
	DWORD Cam;
	bool CombinedAxes;
	float BrakeDir;
	float ThrottleDir;
	float ThrottleSens;
	float BrakeSens;
	float SteerSens;
	float SteerLeftDir;
	float SteerRightDir;
};

static void SaveButtonMapData(ButtonMapData* data, int Len, char* path)
{
	std::ofstream myFile(path, std::ios::out | std::ios::binary);
	myFile.write((char*)&Len, sizeof(int));
	myFile.write((char*)data, sizeof(ButtonMapData) * Len);
	myFile.close();
}

static int LoadButtonMapData(char* path, ButtonMapData** data_out)
{
	int Len = 0;
	std::ifstream myFile(path, std::ios::in | std::ios::binary);
	if(myFile.is_open())
	{
		char buffer[32];
		myFile.read((char*)buffer, sizeof(int));

		Len = *(int*)buffer;
		*data_out = new ButtonMapData[Len];
		myFile.read((char*)*data_out, sizeof(ButtonMapData) * Len);
		myFile.close();
	}
	return Len;
}