#pragma once
#include <State.h>
#include <SKRenderer.h>
#include <SKPhysics.h>
#include <SKInput.h>

using namespace SKRenderer;
using namespace SKMath;
using namespace SKInput;
using namespace SKPhysics;

class GameState;
class PausedState;
class MenuState;
class FadeManager;

class TopState: public State
{
public:
	TopState(std::string name, long ID, HINSTANCE hInst, HWND hWnd);
	~TopState();

	virtual StateResult	EnterState	();
	virtual StateResult Update		(float timeStep);
	virtual StateResult LeaveState	();

	virtual void		PreDraw		(SKMath::SKMatrixStack& worldMatrix, float timeStep);
	virtual void		Render		(SKMath::SKMatrixStack& worldTransform, float timeStep);
	virtual void		Draw		(SKMath::SKMatrixStack& worldTransform, float timeStep);

	virtual void		OnPause		() {}
	virtual void		OnResume	() {}
	virtual void		OnChildDeath(State* child);
	virtual void		OnChildAdded(State* newChild) {}

	void				SetState	(StateEnum state) { setState(state); }

private:
	static DWORD WINAPI t_render(PVOID args);
	HANDLE				m_renderThread;
	HINSTANCE			m_hInst;
	HWND				m_hWnd;

	SKRendererObject*	m_renderObj;
	SKInputObject*		m_inputObj;
	SKPhysicsObject*	m_physicsObj;
	GameState*			m_gameState;
	PausedState*		m_pausedState;
	MenuState*			m_menuState;

	SKRenderer::EffectID	m_basicEffect;

	bool				m_haveLock;
	bool				m_addGameState;
	bool				m_addMenuState;
	bool				m_running;
	bool				m_multiThreaded;
};