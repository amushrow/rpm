#ifdef BACON
#include "StaticGame.h"
#include "MenuState.h"
#include "GameButtons.h"
#include "FadeManager.h"
#include "ButtonMapData.h"
#include <fstream>

char g_releaseInfo[] = "SKEngine Vehicle Simulation Demo \n \
					    \n \
						 Welcome to my physics demo where you can drive around\n \
						 a track that looks suspiciously like Donington in a\n \
						 car that bears a striking resemblance to the Lotus\n \
						 Elise.\n \
						 \n \
						 Remember that this is a work in progress so if you\n \
						 find any bugs or have any suggestions then please get\n \
						 in touch.\n \
						 \n \
						 \n \
						 Not Yet Implemented: \n \
						  � Anti-Roll bar \n \
						  � Handbrake \n \
						  � Wheel Toe \n \
						  � Downforce and Lift \n \
						  � Force Feedback \n \
						  � TCS \n \
						  � ABS \n \
						  � ESP";

#define FORECOL 1.f
#define BACKCOL 0.7f
#define LINECOL 0.7f

char* videoMenuItems[] = {"RESOLUTION", "FULLSCREEN", "V-SYNC", "ANTI-ALIASING", "[APPLY]"};

enum VideoSetupItems
{
	VID_RESOLUTION,
	VID_FULLSCREEN,
	VID_VSYNC,
	VID_AA,
	VID_APPLY,
	VID_END
};

bool MenuState::keyPressed(SKInput::Key key)
{
	bool result = Game::Keyboard->KeyPressed(key);
	if(Game::Joystick)
		result |= Game::Joystick->KeyPressed(key);

	return result;
}

void MenuState::keyDown(void* sender, SKInput::KeyEventArgs* e, void* user_data)
{
	MenuState* menuState = (MenuState*)user_data;
	if(!e->IsAnalogue)
	{
		menuState->m_keyChangeData.device->RemoveKeyPressedEvent(keyDown);
		menuState->m_keyChangeData.device->Poll();
		menuState->m_assigningKey = false;
		
		if(e->DeviceKey != SKInput::SK_ESCAPE)
			menuState->m_keyChangeData.device->AssignKey(e->DeviceKey, menuState->m_keyChangeData.button);

		menuState->m_keyChangeData.device = NULL;
		menuState->m_keyChangeData.button = GB_END;
	}
	else
	{
		int index = e->DeviceKey - SKInput::SK_X_AXISPOS;
		menuState->m_analogueMove[index] += e->AnalogueMovement;
		if(SKMath::FAbs( menuState->m_analogueMove[index] ) > 0.5f)
		{
			menuState->m_keyChangeData.device->RemoveKeyPressedEvent(keyDown);
			menuState->m_keyChangeData.device->Poll();
			menuState->m_assigningKey = false;
			menuState->m_keyChangeData.device->AssignKey(e->DeviceKey, menuState->m_keyChangeData.button);
			
			if(menuState->m_keyChangeData.button == GB_THROTTLE || menuState->m_keyChangeData.button == GB_COMBINED_BRAKE_THROTTLE)
				menuState->m_throttleDir = menuState->m_analogueMove[index] > 0 ? 1.f : -1.f;
			else if(menuState->m_keyChangeData.button == GB_BRAKE)
				menuState->m_brakeDir = menuState->m_analogueMove[index] > 0 ? 1.f : -1.f;

			menuState->m_keyChangeData.device = NULL;
			menuState->m_keyChangeData.button = GB_END;
		}
	}
}

MenuState::MenuState(std::string name, long ID, bool quickStart) : m_resolutions(15, 5), State(name, ID)
{
	m_materialID = new SKRenderer::MaterialID[4];
	m_quickStart = quickStart;
	m_setbackcol = false;
}

MenuState::~MenuState()
{
	
}

StateResult MenuState::EnterState()
{
	POINT res;
	Game::Renderer->GetResolution(&res);
	m_screenResolution.x = (float)res.x;
	m_screenResolution.y = (float)res.y;
	m_indexBuffer = new SKRenderer::IndexBuffer[MENU_SHAPES_END];
	
	setupVerts();

	//-- Set up logo material
	WaitForSingleObject(Game::RendererLock, INFINITE);

	SKRenderer::TextureID tex = Game::Renderer->GetTextureManager()->AddTexture("logo.tga");
	SKRenderer::SKMaterial mat;
	mat.Alpha = true;
	mat.Ambient.Set(1,1,1);
	mat.Diffuse.Set(1,1,1);
	mat.Specular.Set(0,0,0);
	mat.Emissive.Set(0,0,0);
	mat.Shininess = 0.f;
	mat.Textures.Texture[0] = tex;

	m_materialID[0] = Game::Renderer->GetMaterialManager()->AddMaterial(&mat);

	mat.Textures.Texture[0] = SKRenderer::INACTIVE;
	m_materialID[1] = Game::Renderer->GetMaterialManager()->AddMaterial(&mat);
	
	tex = Game::Renderer->GetTextureManager()->AddTexture("button.png");
	mat.Textures.Texture[0] = tex;
	m_materialID[2] = Game::Renderer->GetMaterialManager()->AddMaterial(&mat);
	
	mat.Ambient.A = 0.5f;
	mat.Diffuse.A = 0.5f;
	m_materialID[3] = Game::Renderer->GetMaterialManager()->AddMaterial(&mat);
	//--

	Game::Renderer->SetAmbientLight(1,1,1);
	m_maxAA = Game::Renderer->GetMaxMultisampleQuality();
	m_renderSettings = Game::VideoSettings;
	m_rotAngle = PI_2;
	Game::Renderer->SetClearColour(0,0,0);

	ReleaseSemaphore(Game::RendererLock, 1, NULL);
	
	m_mainMenuSelection = 0;
	m_returnCode = 0;
	m_onSubStage = false;

	m_curItem = 0;
	m_combinedAxis = false;
	m_assigningKey = false;

	m_gameControllers = Game::InputDevices->GetJoysticks();
	m_controllerId = 0;

	m_throttleSens = 1.f;
	m_brakeSens = 1.f;
	m_steerSens = 1.f;
	m_brakeDir = 1.f;
	m_throttleDir = 1.f;

	loadControllerMappings(Game::Keyboard);
	if(Game::Joystick)
		loadControllerMappings(Game::Joystick);

	m_selectedResolution = 0;
	DEVMODE out_mode;
	int index = 0;
	while(EnumDisplaySettings(NULL, index, &out_mode))
	{
		bool found = false;
		for(unsigned i=0; i<m_resolutions.Length(); i++)
		{
			if(m_resolutions[i].Width == out_mode.dmPelsWidth &&
				m_resolutions[i].Height == out_mode.dmPelsHeight)
			{
				found = true;
				if(m_resolutions[i].Frequency < out_mode.dmDisplayFrequency)
					m_resolutions[i].Frequency = out_mode.dmDisplayFrequency;
			}
		}

		if(!found)
		{
			Resolution r = { out_mode.dmPelsWidth, out_mode.dmPelsHeight, out_mode.dmDisplayFrequency};
			m_resolutions.Add(r);
		}

		index++;
	}

	for(unsigned i=0; i<m_resolutions.Length(); i++)
	{
		if(m_resolutions[i].Width == Game::VideoSettings.DisplayWidth &&
			m_resolutions[i].Height == Game::VideoSettings.DisplayHeight)
		{
			m_selectedResolution = i;
			break;
		}
	}


	m_timer = 0.f;
	if(m_quickStart)
	{
		m_stage = SHOW_START;
		FadeManager::Instance()->ForceAlpha(SKColour(0,0,0), 1.f);
	}
	else
	{
		m_stage = INIT_PAUSE;
	}

	return RES_CONTINUE;
}

StateResult MenuState::Update(float timeStep)
{
	float scale = m_screenResolution.y / 720.f;
	
	StateResult res = RES_CONTINUE;
	switch(m_stage)
	{
	case INIT_PAUSE:
		{
			m_timer += timeStep;
			if(m_timer > 0.5f)
			{
				m_stage = (MenuStage)(m_stage+1);
				m_timer = 0.f;
				FadeManager::Instance()->ForceAlpha(SKColour(0,0,0), 1.f);
			}
		}
		break;
	case SHOW_LOGO:
		{
			FadeManager* fade = FadeManager::Instance();
			switch(fade->GetState())
			{
			case FADE_ON:
				{
					if(m_timer == 0.f)
						fade->FadeIn(1.5f);
					else
					{
						m_timer = 0.f;
						m_stage = (MenuStage)(m_stage+1);
					}
				}
				break;
			
			case FADE_OFF:
				{
					m_timer += timeStep;
					if(m_timer > 2.f)
					{
						fade->FadeOut(SKColour(0,0,0), 1.5f);
					}
				}
				break;
			}
		}
		break;

	case SHOW_START:
		{
			m_rotAngle -= (timeStep * PIx2) / 60.f;
			if(m_rotAngle < 0)
				m_rotAngle += PIx2;

			m_timer += timeStep;
			
			FadeManager* fade = FadeManager::Instance();
			if(m_timer > 0.5f && fade->GetState() == FADE_ON)
			{
				SKColour col(1,1,1);
				fade->ForceAlpha(col, 1.f);
				fade->FadeIn(0.5f);
				m_setbackcol = true;
			}
			
			if(m_timer > 0.5f)
			{
				if(m_onSubStage)
				{
					handleSubMenus(timeStep);
				}
				else
				{
					if( keyPressed(GB_MENU_UP) )
						m_mainMenuSelection--;

					if( keyPressed(GB_MENU_DOWN) )
						m_mainMenuSelection++;

					m_mainMenuSelection = max(0, min(3, m_mainMenuSelection));

					if( keyPressed(GB_ESCAPE) )
					{
						res = RES_LEAVE;
						m_returnCode = -1;
					}

					if( keyPressed(GB_ACTION) )
					{
						switch(m_mainMenuSelection)
						{
						case 0:
							{
								m_timer = 0.f;
								FadeManager::Instance()->FadeOut(SKColour(0.f), 0.5f);
								m_stage = (MenuStage)(m_stage+1);
							}
							break;

						case 1:
						case 2:
							m_onSubStage = true;
							break;

						case 3:
							res = RES_LEAVE;
							m_returnCode = -1;
							break;
						}	
					}
				}
			}
		}
		break;

	case EXIT_MENU:
		m_timer += timeStep;
		if(m_timer > 0.5f)
		{
			m_timer = 0.f;
			res = RES_LEAVE;
		}
		break;

	}
	return res;
}

void MenuState::handleSubMenus(float timeStep)
{
	//Handle input
	if(m_assigningKey)
	{
		if(keyPressed(GB_ESCAPE))
		{
			m_assigningKey = false;
			m_keyChangeData.device = NULL;
			m_keyChangeData.button = GB_END;
			Game::Keyboard->RemoveKeyPressedEvent(keyDown);
			if(Game::Joystick)
				Game::Joystick->RemoveKeyPressedEvent(keyDown);
		}
	}
	else
	{
		if(keyPressed(GB_BACK))
		{
			m_onSubStage = false;
		}

		switch(m_mainMenuSelection)
		{
		case 0:
			break;
		case 1:
			handleControllerMenu(timeStep);
			break;
		case 2:
			//Video Settings
			if(keyPressed(GB_BACK))
			{
				m_renderSettings = Game::VideoSettings;
				CreateDirectory("Data", NULL);
				std::ofstream settings_file("Data\\RenderSettings.bin", std::ios::out | std::ios::binary);
				if(settings_file.is_open())
				{
					settings_file.write((char*)&Game::VideoSettings, sizeof(SKRenderer::RenderSettings));
					settings_file.close();
				}

				m_curItem = 0;
			}
			else if(keyPressed(GB_MENU_UP))
			{
				if(m_curItem > 0)
					m_curItem--;
			}
			else if(keyPressed(GB_MENU_DOWN))
			{
				if(m_curItem < VID_APPLY)
					m_curItem++;
			}

			switch(m_curItem)
			{
			case VID_RESOLUTION:
				if(keyPressed(GB_MENU_RIGHT))
				{
					if(m_selectedResolution < m_resolutions.Length()-1)
					{
						m_selectedResolution++;
						m_renderSettings.DisplayWidth = m_resolutions[m_selectedResolution].Width;
						m_renderSettings.DisplayHeight = m_resolutions[m_selectedResolution].Height;
						m_renderSettings.RefreshRate = m_resolutions[m_selectedResolution].Frequency;
					}
				}
				else if(keyPressed(GB_MENU_LEFT))
				{
					if(m_selectedResolution > 0)
					{
						m_selectedResolution--;
						m_renderSettings.DisplayWidth = m_resolutions[m_selectedResolution].Width;
						m_renderSettings.DisplayHeight = m_resolutions[m_selectedResolution].Height;
						m_renderSettings.RefreshRate = m_resolutions[m_selectedResolution].Frequency;
					}
				}
				break;
			case VID_AA:
				if(keyPressed(GB_MENU_RIGHT))
				{
					if(m_renderSettings.FSAAQuality < m_maxAA)
						m_renderSettings.FSAAQuality = (SKRenderer::MultisampleQuality)(m_renderSettings.FSAAQuality + 1);
					m_renderSettings.EnableMultisampling = true;
				}
				else if(keyPressed(GB_MENU_LEFT))
				{
					if(m_renderSettings.FSAAQuality > SKRenderer::MS_NONE)
						m_renderSettings.FSAAQuality = (SKRenderer::MultisampleQuality)(m_renderSettings.FSAAQuality - 1);
					if(m_renderSettings.FSAAQuality == SKRenderer::MS_NONE)
						m_renderSettings.EnableMultisampling = false;
				}
				break;
			case VID_VSYNC:
				if(keyPressed(GB_MENU_RIGHT) || keyPressed(GB_MENU_LEFT))
				{
					m_renderSettings.EnableVSync = !m_renderSettings.EnableVSync;
				}
				break;
			case VID_FULLSCREEN:
				if(keyPressed(GB_MENU_RIGHT) || keyPressed(GB_MENU_LEFT))
				{
					m_renderSettings.Windowed = ! m_renderSettings.Windowed;
				}
				break;
			case VID_APPLY:
				if(keyPressed(GB_ACTION))
				{
					WaitForSingleObject(Game::RendererLock, INFINITE);
					Game::VideoSettings.RefreshRate = m_renderSettings.RefreshRate;					
					if(m_renderSettings != Game::VideoSettings)
					{
						HRESULT hr;
						hr = Game::Renderer->Reset(&m_renderSettings);
						if(SUCCEEDED(hr))
							Game::VideoSettings = m_renderSettings;
						else
						{
							m_renderSettings = Game::VideoSettings;
							Game::Renderer->Reset(&m_renderSettings);
						}

						if(m_renderSettings.Windowed)
						{
							SetWindowLong(Game::Window, GWL_STYLE, WS_VISIBLE | WS_CLIPSIBLINGS | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX);
							SetWindowPos(Game::Window, NULL, 0, 0, Game::VideoSettings.DisplayWidth + Game::BorderSize.x, 
								Game::VideoSettings.DisplayHeight + Game::BorderSize.y, SWP_NOZORDER);
							ShowCursor( TRUE );
						}
						else
						{
							SetWindowLong(Game::Window, GWL_STYLE,  WS_VISIBLE | WS_CLIPSIBLINGS | WS_POPUP);
							SetWindowPos(Game::Window, NULL, 0, 0, Game::VideoSettings.DisplayWidth, Game::VideoSettings.DisplayHeight, SWP_NOZORDER);
							ShowCursor( FALSE );
						}
						
						Game::Renderer->InitStage(1.135f, NULL, 0);
						Game::Renderer->InitStage(1.f, NULL, 1);

						POINT res;
						Game::Renderer->GetResolution(&res);
						m_screenResolution.x = (float)res.x;
						m_screenResolution.y = (float)res.y;
						m_maxAA = Game::Renderer->GetMaxMultisampleQuality();
						delete[] m_texturedQuads.Vertices;
						delete[] m_screenShapes.Vertices;
						for(int i=0; i<MENU_SHAPES_END; i++)
							delete[] m_indexBuffer[i].Indices;
						setupVerts();
						FadeManager::Instance()->RebuildVerts();
					}

					ReleaseSemaphore(Game::RendererLock, 1, NULL);
				}
			}
			break;
		}
	}
}

StateResult MenuState::LeaveState()
{
	delete[] m_texturedQuads.Vertices;
	delete[] m_screenShapes.Vertices;

	for(int i=0; i<MENU_SHAPES_END; i++)
		delete[] m_indexBuffer[i].Indices;
	delete[] m_indexBuffer;
	delete[] m_materialID;

	return RES_CONTINUE;
}

void MenuState::PreDraw(SKMath::SKMatrixStack& worldTransform, float timeStep)
{
	if(GetState() > S_ENTERING)
	{
		if(m_stage >= SHOW_START && GetState() < S_LEAVING)
		{
			Game::Renderer->SetWorldTransform(&worldTransform.GetTop());
			Game::VertexManager->Render(m_screenShapes, m_indexBuffer[BG_GRADIENT], 0);
		}

		if(m_setbackcol)
		{
			Game::Renderer->SetClearColour(BACKCOL, BACKCOL, BACKCOL);
			m_setbackcol = false;
		}
	}
}

void MenuState::Render(SKMath::SKMatrixStack& worldTransform, float timeStep)
{
	if(GetState() > S_ENTERING)
	{
		if(m_stage >= SHOW_START && GetState() < S_LEAVING)
		{
			SKMath::SKVector camPos(0, 4.f, 4.5f);
			SKMath::SKVector target(0, 1.5f, -0.5f);

			SKMath::SKQuaternion quat(SKMath::SKVector(0,1,0), (float)m_rotAngle);
			camPos = quat.Rotate(camPos);
			target = quat.Rotate(target);
			Game::Renderer->SetViewLookAt(camPos, target, SKMath::SKVector::Up);

			float halfgridSize = 30;
			float gridSeparation = 2.5f;

			SKMath::SKVector start, end;
			float realGridSize = halfgridSize * gridSeparation;
			start.Set(-realGridSize, 0, 0);
			end.Set(realGridSize,0,0);
			SKColour col(LINECOL);

			for(float z= -realGridSize; z<=realGridSize; z+=gridSeparation) {
				start.z = z;
				end.z = z;
				Game::VertexManager->RenderLine(&start, &end, &col);
			}
			start.z = -realGridSize;
			end.z = realGridSize;
			for(float x= -realGridSize; x<=realGridSize; x+=gridSeparation) {
				start.x = x;
				end.x = x;
				Game::VertexManager->RenderLine(&start, &end, &col);
			}
		}
	}
}

void MenuState::Draw(SKMath::SKMatrixStack& worldTransform, float timeStep)
{
	if(GetState() > S_ENTERING)
	{
		if(GetState() < S_LEAVING)
		{
			float scale = (m_screenResolution.y / 720.f);

			if(m_stage == SHOW_LOGO)
			{
				Game::Renderer->SetWorldTransform(&worldTransform.GetTop());
				Game::VertexManager->Render(m_texturedQuads, m_indexBuffer[LOGO], m_materialID[0]);
			}
			else if(m_stage >= SHOW_START)
			{
				//-- Draw persistant title screen stuff
				Game::Renderer->SetWorldTransform(&worldTransform.GetTop());
				Game::VertexManager->Render(m_screenShapes, m_indexBuffer[LETTER_BOX], m_materialID[1]);
				Game::VertexManager->ForcedFlush();
				Game::VertexManager->Render(m_texturedQuads, m_indexBuffer[TITLE], m_materialID[0]);

				Game::Renderer->EnableLighting(true);
				for(int i=0; i<4; i++)
				{
					if(m_mainMenuSelection == i)
						Game::VertexManager->Render(m_texturedQuads, m_indexBuffer[MENU_BUTTON_START+i], m_materialID[2]);
					else
						Game::VertexManager->Render(m_texturedQuads, m_indexBuffer[MENU_BUTTON_START+i], m_materialID[3]);
				}
				Game::Renderer->EnableLighting(false);

				Game::VertexManager->Render(m_screenShapes, m_indexBuffer[OUTLINES], m_materialID[1]);

				Game::Renderer->SetWorldTransform(NULL);
							
				float xPos = m_screenResolution.x * 0.5f;
				float yPos = scale * 175.f;
				SKColour col = 0.f;
				Game::TitleFont->Draw("Vehicle Simulation Demo", 32*scale, xPos, yPos, TOP_MIDDLE, col);
		
				col = 1.f;
				yPos = scale * 695.f;
				Game::TitleFont->Draw("--Powered by SKEngine--\nSKEngine � 2008-2011, RPM � 2010-2011 Anthony Mushrow", 16*scale, xPos, yPos, MIDDLE, col);		
				//--

				static char* items[] = {"START", "CONTROLS", "VIDEO", "QUIT"};

				col.Set(0.4f, 0.4f, 0.4f);
				float textHeight = 26*scale;
				xPos -= (263 * scale);
				yPos = 249 * scale;

				for(int i=0; i<4; i++)
				{
					if(m_mainMenuSelection != i)
						col.Set(0.4f, 0.4f, 0.4f, 0.5f);
					else if(m_mainMenuSelection == i && m_onSubStage)
						col.Set(0.4f, 0.4f, 0.4f, 1.f);
					else
						col.Set(0.9f, 0.2f, 0.2f, 1.f);

					Game::StandardFont->Draw(items[i], textHeight, xPos, yPos, MIDDLE, col);
					yPos += 62 * scale;
				}
				drawSubMenus();
			}
		}
	}
}

void MenuState::drawSubMenus()
{
	//Middle: mid+81 x 434
	//Top Left: mid-166 x 234
	float scale = m_screenResolution.y / 720.f;
	float xPos = (m_screenResolution.x * 0.5f) - (166 * scale);
	float yPos = 232 * scale;

	SKColour col(0.55f, 0.55f, 0.55f);
	switch(m_mainMenuSelection)
	{
	case 0:
		Game::StandardFont->Draw(g_releaseInfo, 18*scale, xPos, yPos, TOP_LEFT, col);
		break;
	case 1:
		drawControllerMenu();
		break;
	case 2:
		drawVideoMenu();
		//Game::StandardFont->Draw("Not here yet", 18*scale, xPos, yPos, TOP_LEFT, col);
		break;
	case 3:
		{
			xPos = (m_screenResolution.x * 0.5f) + (81 * scale);
			yPos = 434 * scale;
			Game::StandardFont->Draw("Bye Bye", 18*scale, xPos, yPos, MIDDLE, col);
		}
		break;
	}
}



void MenuState::drawVideoMenu()
{
	float scale = m_screenResolution.y / 720.f;
	float xPos = (m_screenResolution.x * 0.5f) - (166 * scale);
	float yPos = 232 * scale;
	SKColour col(0.55f, 0.55f, 0.55f);

	SKMath::SKVector start(xPos, yPos + 22*scale, 0);
	start.x = floor(start.x + 0.5f) - 0.5f;
	start.y = floor(start.y + 0.5f);
	SKMath::SKVector end(start);
	end.x += floor( (494 * scale) + 0.5f );

	col.Set(0.4f, 0.4f, 0.4f);
	Game::StandardFont->Draw("VIDEO SETTINGS", 20 * scale, xPos, yPos, TOP_LEFT, col);

	xPos = (m_screenResolution.x * 0.5f) + (20 * scale);
	yPos = 275 * scale;
	float textPos = xPos;
	float space = 20*scale;
	char buff[64] = "Empty";
	
	for(int i=0; i<VID_END; i++)
	{
		col.Set(0.55f, 0.55f, 0.55f, 1.f);
		switch(i)
		{
		case VID_RESOLUTION:
			sprintf_s(buff, 64, "%ux%u", m_renderSettings.DisplayWidth, m_renderSettings.DisplayHeight);
			break;
		case VID_AA:
			switch(m_renderSettings.FSAAQuality)
			{
			case SKRenderer::MS_NONE:
				sprintf_s(buff, 64, "None");
				break;
			case SKRenderer::MS_2_SAMPLES:
				sprintf_s(buff, 64, "2x");
				break;
			case SKRenderer::MS_4_SAMPLES:
				sprintf_s(buff, 64, "4x");
				break;
			case SKRenderer::MS_8_SAMPLES:
				sprintf_s(buff, 64, "8x");
				break;
			case SKRenderer::MS_16_SAMPLES:
				sprintf_s(buff, 64, "16x");
				break;
			}
			break;

		case VID_FULLSCREEN:
			if(m_renderSettings.Windowed)
				sprintf_s(buff, 64, "No");
			else
				sprintf_s(buff, 64, "Yes");
			break;

		case VID_VSYNC:
			if(m_renderSettings.EnableVSync)
				sprintf_s(buff, 64, "Enabled");
			else
				sprintf_s(buff, 64, "Disabled");
			break;

		case VID_APPLY:
			sprintf_s(buff, 64, "[APPLY]");
			col.A = 0.f;
			break;

		default:
			sprintf_s(buff, 64, "");
			break;
		}
		Game::StandardFont->Draw(videoMenuItems[i], 16*scale, textPos, yPos, TOP_RIGHT, col);
		if(m_onSubStage && m_curItem == i)
			col.Set(0.9f, 0.2f, 0.2f, 1.f);
		else
			col.Set(0.55f, 0.55f, 0.55f, 0.5f);
		Game::StandardFont->Draw(buff, 16*scale, textPos + space, yPos, TOP_LEFT, col);
		yPos += 16*scale;
	}

	col = 0.4f;
	for(int i=0; i<2; i++)
	{
		Game::VertexManager->RenderLine(&start, &end, &col);
		start.y += 1;
		end.y += 1;
	}
}
#endif