#include "GameButtons.h"
#include "SKDefines.h"
#include "new_RPMDemo.h"
#include "new_Menu.h"
#include "new_ppflags.h"
#include "new_MenuHandlers.h"
#include "new_States.h"

using namespace TopStates;
using namespace GameStates;
using namespace GameStates::GameSubStates;
static DWORD mbl[] = {GB_THROTTLE, GB_BRAKE, GB_STEER_LEFT, GB_STEER_RIGHT, GB_GEARUP, GB_GEARDOWN, GB_BACK, GB_ACTION, GB_PAUSE};

GamePause::GamePause(TopStates::GameState* gameDataState)
{
	this->GameData = gameDataState;
}

void GamePause::Enter()
{
	RPMDemo* Game = RPMDemo::GetInstance();

	m_controllers = Game->InputDevices->GetJoysticks();

	m_mainMenu = new MenuController();
	m_displayMenu = new MenuController(m_mainMenu);
	m_keyboardMenu = new MenuController(m_mainMenu);
	m_joyMenu = new MenuController(m_mainMenu);

	m_mainMenu->AddItem(MI_RESUME, mainMenuCallback, this);
	m_mainMenu->AddItem(MI_RESTART, mainMenuCallback, this);
	m_mainMenu->AddItem(MI_DISPLAY, mainMenuCallback, this);
	m_mainMenu->AddItem(MI_KEYBOARD, mainMenuCallback, this);
	if(m_controllers.Length() > 0)
		m_mainMenu->AddItem(MI_JOY, mainMenuCallback, this);
	m_mainMenu->AddItem(MI_QUIT, mainMenuCallback, this);

	m_displayMenu->AddItem(MI_RESOLUTION, displayMenuCallback, this);
	m_displayMenu->AddItem(MI_WINDOWED, displayMenuCallback, this);
	m_displayMenu->AddItem(MI_VSYNC, displayMenuCallback, this);
	m_displayMenu->AddItem(MI_TEXRES, displayMenuCallback, this);
	m_displayMenu->AddItem(MI_FILTERING, displayMenuCallback, this);
	m_displayMenu->AddItem(MI_AA, displayMenuCallback, this);
	m_displayMenu->AddItem(MI_APPLY, displayMenuCallback, this);
	m_displayMenu->AddItem(MI_BACK, displayMenuCallback, this);

	m_keyboardMenu->AddItem(MI_BTN_ACC, keyboardMenuCallback, this);
	m_keyboardMenu->AddItem(MI_BTN_BRAKE, keyboardMenuCallback, this);
	m_keyboardMenu->AddItem(MI_BTN_TLEFT, keyboardMenuCallback, this);
	m_keyboardMenu->AddItem(MI_BTN_TRIGHT, keyboardMenuCallback, this);
	m_keyboardMenu->AddItem(MI_BTN_GUP, keyboardMenuCallback, this);
	m_keyboardMenu->AddItem(MI_BTN_GDOWN, keyboardMenuCallback, this);
	m_keyboardMenu->AddItem(MI_BTN_BACK, keyboardMenuCallback, this);
	m_keyboardMenu->AddItem(MI_BTN_ACTION, keyboardMenuCallback, this);
	m_keyboardMenu->AddItem(MI_BTN_PAUSE, keyboardMenuCallback, this);
	m_keyboardMenu->AddItem(MI_BTN_CAM, keyboardMenuCallback, this);
	m_keyboardMenu->AddItem(MI_BACK, keyboardMenuCallback, this);

	m_joyMenu->AddItem(MI_CONTROLLER, joyMenuCallback, this);
	m_joyMenu->AddItem(MI_BTN_ACC, joyMenuCallback, this);
	m_joyMenu->AddItem(MI_BTN_BRAKE, joyMenuCallback, this);
	m_joyMenu->AddItem(MI_BTN_TLEFT, joyMenuCallback, this);
	m_joyMenu->AddItem(MI_BTN_TRIGHT, joyMenuCallback, this);
	m_joyMenu->AddItem(MI_BTN_GUP, joyMenuCallback, this);
	m_joyMenu->AddItem(MI_BTN_GDOWN, joyMenuCallback, this);
	m_joyMenu->AddItem(MI_BTN_BACK, joyMenuCallback, this);
	m_joyMenu->AddItem(MI_BTN_ACTION, joyMenuCallback, this);
	m_joyMenu->AddItem(MI_BTN_PAUSE, joyMenuCallback, this);
	m_joyMenu->AddItem(MI_BTN_CAM, joyMenuCallback, this);
	m_joyMenu->AddItem(MI_THROTTLESENS, joyMenuCallback, this);
	m_joyMenu->AddItem(MI_BRAKESENS, joyMenuCallback, this);
	m_joyMenu->AddItem(MI_STEERSENS, joyMenuCallback, this);
	m_joyMenu->AddItem(MI_BACK, joyMenuCallback, this);

	m_activeMenu = m_mainMenu;
	m_editButton = 0xFFFFFFFF;

	DEVMODE out_mode;
	int index = 0;
	while(EnumDisplaySettings(NULL, index, &out_mode))
	{
		bool found = false;
		for(unsigned i=0; i<m_resolutions.Length(); i++)
		{
			if(m_resolutions[i].Width == out_mode.dmPelsWidth &&
				m_resolutions[i].Height == out_mode.dmPelsHeight)
			{
				found = true;
				if(m_resolutions[i].Frequency < out_mode.dmDisplayFrequency)
					m_resolutions[i].Frequency = out_mode.dmDisplayFrequency;
			}
		}

		if(!found)
		{
			Resolution r = { out_mode.dmPelsWidth, out_mode.dmPelsHeight, out_mode.dmDisplayFrequency};
			if(out_mode.dmPelsWidth == Game->VideoSettings.DisplayWidth &&
				out_mode.dmPelsHeight == Game->VideoSettings.DisplayHeight)
			{
				m_selectedResolution = m_resolutions.Length();
			}
			m_resolutions.Add(r);
		}

		index++;
	}
	m_availableAA = Game->Renderer->GetAvailableAASettings();

	m_windowed = Game->VideoSettings.Windowed;
	m_vsync = Game->VideoSettings.EnableVSync;
	m_shadowLevel = Game->ShadowResolutionLevel;
	m_filterQuality = Game->FilteringQuality;
	m_msType = Game->VideoSettings.FSAAQuality;

	GameData->FreezeFrame(true, PP_BLUR | PP_DESATURATE);
	m_scene = new Screen(renderMenus, 100, this);
	Game->AddScreen(m_scene);
}

void GamePause::Leave()
{
	RPMDemo* Game = RPMDemo::GetInstance();
	Game->RemoveScreen(m_scene);
	GameData->FreezeFrame(false);
	delete m_scene;
	delete m_mainMenu;
	delete m_displayMenu;
	delete m_keyboardMenu;
}

void GamePause::Run()
{
	RPMDemo* Game = RPMDemo::GetInstance();	
	m_activeMenu->HandleInput();
}

void GamePause::renderMenus(SKRenderer::SKRenderDevice* renderDevice, void* user_data)
{
	GamePause* self = reinterpret_cast<GamePause*>(user_data);

	if(self->m_activeMenu == self->m_mainMenu)
	{
		self->drawMainMenu(renderDevice);
	}
	else if(self->m_activeMenu == self->m_displayMenu)
	{
		self->drawDisplayMenu(renderDevice);
	}
	else if(self->m_activeMenu == self->m_keyboardMenu)
	{
		self->drawkeyboardMenu(renderDevice);
	}
	else if(self->m_activeMenu == self->m_joyMenu)
	{
		self->drawJoyMenu(renderDevice);
	}
}

void GamePause::keyDown(void* sender, KeyEventArgs* e, void* data)
{
	GamePause* self = reinterpret_cast<GamePause*>(data);
	RPMDemo* Game = RPMDemo::GetInstance();

	switch(self->m_editButton)
	{
	case GB_BRAKE:
		Game->StoredButtonMaps[0].Brake = e->DeviceKey;
		Game->StoredButtonMaps[0].BrakeDir = 1.f;
		break;

	case GB_THROTTLE:
		Game->StoredButtonMaps[0].Throttle = e->DeviceKey;
		Game->StoredButtonMaps[0].ThrottleDir = 1.f;
		break;

	case GB_STEER_LEFT:
		Game->StoredButtonMaps[0].SteerLeft = e->DeviceKey;
		Game->StoredButtonMaps[0].SteerLeftDir = 1.f;
		break;

	case GB_STEER_RIGHT:
		Game->StoredButtonMaps[0].SteerRight = e->DeviceKey;
		Game->StoredButtonMaps[0].SteerRightDir = 1.f;
		break;

	case GB_ACTION:
		Game->StoredButtonMaps[0].Select = e->DeviceKey;
		break;

	case GB_PAUSE:
		Game->StoredButtonMaps[0].Pause = e->DeviceKey;
		break;

	case GB_BACK:
		Game->StoredButtonMaps[0].Back = e->DeviceKey;
		break;

	case GB_GEARDOWN:
		Game->StoredButtonMaps[0].GearDown = e->DeviceKey;
		break;

	case GB_GEARUP:
		Game->StoredButtonMaps[0].GearUp = e->DeviceKey;
		break;

	case GB_CAM:
		Game->StoredButtonMaps[0].Cam = e->DeviceKey;
		break;
	}

	Game->Keyboard->RemoveKeyPressedEvent(keyDown);
	Game->Keyboard->AssignKey(e->DeviceKey, self->m_editButton);
	self->m_editButton = 0xFFFFFFFF;
	Game->Keyboard->Poll();

	self->m_activeMenu->BlockInput(false);
}

void GamePause::joyDown(void* sender, KeyEventArgs* e, void* data)
{
	GamePause* self = reinterpret_cast<GamePause*>(data);
	RPMDemo* Game = RPMDemo::GetInstance();

	if(e->IsAnalogue)
	{
		int index = e->DeviceKey - SKInput::SK_X_AXISPOS;
		self->m_analogueMove[index] += e->AnalogueMovement;
		if(SKMath::FAbs( self->m_analogueMove[index] ) > 0.25f)
		{
			bool setKey = false;
			switch(self->m_editButton)
			{
			case GB_THROTTLE:
				Game->StoredButtonMaps[Game->JoypadMapIndex].Throttle = e->DeviceKey;
				Game->StoredButtonMaps[Game->JoypadMapIndex].ThrottleDir = self->m_analogueMove[index] > 0 ? 1.f : -1.f;
				if(Game->StoredButtonMaps[Game->JoypadMapIndex].Throttle == Game->StoredButtonMaps[Game->JoypadMapIndex].Brake
					|| Game->StoredButtonMaps[Game->JoypadMapIndex].Throttle == Game->StoredButtonMaps[Game->JoypadMapIndex].CombinedThrottleBrake)
				{
					Game->Joystick->UnassignKey(Game->StoredButtonMaps[Game->JoypadMapIndex].Throttle, GB_THROTTLE);
					Game->Joystick->UnassignKey(Game->StoredButtonMaps[Game->JoypadMapIndex].Brake, GB_BRAKE);
					Game->StoredButtonMaps[Game->JoypadMapIndex].Throttle = 0xFFFFFFFF;
					Game->StoredButtonMaps[Game->JoypadMapIndex].Brake = 0xFFFFFFFF;
					Game->StoredButtonMaps[Game->JoypadMapIndex].CombinedThrottleBrake = e->DeviceKey;
					self->m_editButton = GB_COMBINED_BRAKE_THROTTLE;
				}
				else
					Game->Joystick->UnassignKey(Game->StoredButtonMaps[Game->JoypadMapIndex].CombinedThrottleBrake, GB_COMBINED_BRAKE_THROTTLE);
				setKey = true;
				break;

			case GB_BRAKE:
				Game->StoredButtonMaps[Game->JoypadMapIndex].Brake = e->DeviceKey;
				Game->StoredButtonMaps[Game->JoypadMapIndex].BrakeDir = self->m_analogueMove[index] > 0 ? 1.f : -1.f;
				if(Game->StoredButtonMaps[Game->JoypadMapIndex].Throttle == Game->StoredButtonMaps[Game->JoypadMapIndex].Brake
					|| Game->StoredButtonMaps[Game->JoypadMapIndex].Brake == Game->StoredButtonMaps[Game->JoypadMapIndex].CombinedThrottleBrake)
				{
					Game->Joystick->UnassignKey(Game->StoredButtonMaps[Game->JoypadMapIndex].Throttle, GB_THROTTLE);
					Game->Joystick->UnassignKey(Game->StoredButtonMaps[Game->JoypadMapIndex].Brake, GB_BRAKE);
					Game->StoredButtonMaps[Game->JoypadMapIndex].Throttle = 0xFFFFFFFF;
					Game->StoredButtonMaps[Game->JoypadMapIndex].Brake = 0xFFFFFFFF;
					Game->StoredButtonMaps[Game->JoypadMapIndex].CombinedThrottleBrake = e->DeviceKey;
					self->m_editButton = GB_COMBINED_BRAKE_THROTTLE;
				}
				else
					Game->Joystick->UnassignKey(Game->StoredButtonMaps[Game->JoypadMapIndex].CombinedThrottleBrake, GB_COMBINED_BRAKE_THROTTLE);
				setKey = true;
				break;

			case GB_STEER_LEFT:
				Game->StoredButtonMaps[Game->JoypadMapIndex].SteerLeft = e->DeviceKey;
				Game->StoredButtonMaps[Game->JoypadMapIndex].SteerLeftDir = self->m_analogueMove[index] > 0 ? 1.f : -1.f;
				if(Game->StoredButtonMaps[Game->JoypadMapIndex].SteerLeft == Game->StoredButtonMaps[Game->JoypadMapIndex].SteerRight
					|| Game->StoredButtonMaps[Game->JoypadMapIndex].SteerLeft == Game->StoredButtonMaps[Game->JoypadMapIndex].Steering)
				{
					Game->Joystick->UnassignKey(Game->StoredButtonMaps[Game->JoypadMapIndex].SteerLeft, GB_STEER_LEFT);
					Game->Joystick->UnassignKey(Game->StoredButtonMaps[Game->JoypadMapIndex].SteerLeft, GB_STEER_RIGHT);
					Game->StoredButtonMaps[Game->JoypadMapIndex].SteerLeft = 0xFFFFFFFF;
					Game->StoredButtonMaps[Game->JoypadMapIndex].SteerRight = 0xFFFFFFFF;
					Game->StoredButtonMaps[Game->JoypadMapIndex].Steering = e->DeviceKey;
					self->m_editButton = GB_STEERING;
				}
				else
					Game->Joystick->UnassignKey(Game->StoredButtonMaps[Game->JoypadMapIndex].Steering, GB_STEERING);
				setKey = true;
				break;

			case GB_STEER_RIGHT:
				Game->StoredButtonMaps[Game->JoypadMapIndex].SteerRight = e->DeviceKey;
				Game->StoredButtonMaps[Game->JoypadMapIndex].SteerRightDir = self->m_analogueMove[index] > 0 ? 1.f : -1.f;
				if(Game->StoredButtonMaps[Game->JoypadMapIndex].SteerLeft == Game->StoredButtonMaps[Game->JoypadMapIndex].SteerRight
					|| Game->StoredButtonMaps[Game->JoypadMapIndex].SteerRight == Game->StoredButtonMaps[Game->JoypadMapIndex].Steering)
				{
					Game->Joystick->UnassignKey(Game->StoredButtonMaps[Game->JoypadMapIndex].SteerLeft, GB_STEER_LEFT);
					Game->Joystick->UnassignKey(Game->StoredButtonMaps[Game->JoypadMapIndex].SteerLeft, GB_STEER_RIGHT);
					Game->StoredButtonMaps[Game->JoypadMapIndex].SteerRight = 0xFFFFFFFF;
					Game->StoredButtonMaps[Game->JoypadMapIndex].SteerLeft = 0xFFFFFFFF;
					Game->StoredButtonMaps[Game->JoypadMapIndex].Steering = e->DeviceKey;
					self->m_editButton = GB_STEERING;
				}
				else
					Game->Joystick->UnassignKey(Game->StoredButtonMaps[Game->JoypadMapIndex].Steering, GB_STEERING);
				setKey = true;
				break;
			}

			if(setKey)
			{			
				Game->Joystick->RemoveKeyPressedEvent(joyDown);
				Game->Joystick->AssignKey(e->DeviceKey, self->m_editButton);
				self->m_editButton = 0xFFFFFFFF;
				self->m_activeMenu->BlockInput(false);
				Game->Joystick->Poll();
			}

		}
	}
	else
	{
		switch(self->m_editButton)
		{
		case GB_BRAKE:
			Game->StoredButtonMaps[Game->JoypadMapIndex].CombinedThrottleBrake = 0xFFFFFFFF;
			Game->StoredButtonMaps[Game->JoypadMapIndex].Brake = e->DeviceKey;
			Game->StoredButtonMaps[Game->JoypadMapIndex].BrakeDir = 1.f;
			break;

		case GB_THROTTLE:
			Game->StoredButtonMaps[Game->JoypadMapIndex].CombinedThrottleBrake = 0xFFFFFFFF;
			Game->StoredButtonMaps[Game->JoypadMapIndex].Throttle = e->DeviceKey;
			Game->StoredButtonMaps[Game->JoypadMapIndex].ThrottleDir = 1.f;
			break;

		case GB_STEER_LEFT:
			Game->StoredButtonMaps[Game->JoypadMapIndex].Steering = 0xFFFFFFFF;
			Game->StoredButtonMaps[Game->JoypadMapIndex].SteerLeft = e->DeviceKey;
			Game->StoredButtonMaps[Game->JoypadMapIndex].SteerLeftDir = 1.f;
			break;

		case GB_STEER_RIGHT:
			Game->StoredButtonMaps[Game->JoypadMapIndex].Steering = 0xFFFFFFFF;
			Game->StoredButtonMaps[Game->JoypadMapIndex].SteerRight = e->DeviceKey;
			Game->StoredButtonMaps[Game->JoypadMapIndex].SteerRightDir = 1.f;
			break;

		case GB_ACTION:
			Game->StoredButtonMaps[Game->JoypadMapIndex].Select = e->DeviceKey;
			break;

		case GB_PAUSE:
			Game->StoredButtonMaps[Game->JoypadMapIndex].Pause = e->DeviceKey;
			break;

		case GB_BACK:
			Game->StoredButtonMaps[Game->JoypadMapIndex].Back = e->DeviceKey;
			break;

		case GB_GEARDOWN:
			Game->StoredButtonMaps[Game->JoypadMapIndex].GearDown = e->DeviceKey;
			break;

		case GB_GEARUP:
			Game->StoredButtonMaps[Game->JoypadMapIndex].GearUp = e->DeviceKey;
			break;

		case GB_CAM:
			Game->StoredButtonMaps[Game->JoypadMapIndex].Cam = e->DeviceKey;
			break;
		}

		Game->Joystick->RemoveKeyPressedEvent(joyDown);
		Game->Joystick->AssignKey(e->DeviceKey, self->m_editButton);
		self->m_editButton = 0xFFFFFFFF;
		self->m_activeMenu->BlockInput(false);
		Game->Joystick->Poll();
	}
}