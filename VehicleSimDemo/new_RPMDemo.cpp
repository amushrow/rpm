#include "new_RPMDemo.h"
#include "new_States.h"
#include "new_GameState.h"
#include "GameButtons.h"
#include <fstream>
#include "..\CppCustomDataTypes\CubicSpline.h"

RPMDemo* RPMDemo::m_instance = NULL;

struct DisplaySettings
{
	SKRenderer::RenderSettings	VidoeSettings;
	UINT						ShadowResolutionLevel;
	UINT						FilteringQuality;
};

RPMDemo* RPMDemo::GetInstance()
{
	if(m_instance == NULL)
		m_instance = new RPMDemo();

	return m_instance;
}

void RPMDemo::DestroyInstance()
{
	if(m_instance != NULL)
	{
		delete m_instance;
		m_instance = NULL;
	}
}

RPMDemo::RPMDemo() : StoredButtonMaps(2, 2), m_renderBufferQueue(MAX_BUFFERED_FRAMES), m_freeBufferQueue(MAX_BUFFERED_FRAMES), m_queuedFuncs(32), m_localQueuedFuncs(8, 4)
{
	Renderer = NULL;
	VertexManager = NULL;
	PhysicsWorld = NULL;
	Keyboard = NULL;
	Joystick = NULL;
	InputDevices = NULL;
	AudioDevice = NULL;

	TitleFont = NULL;
	StandardFont = NULL;
	DataMan = NULL;
	
	m_running = true;
	m_initialised = false;
	m_reinitialiseGraphics = false;
}

void RPMDemo::Initialise(HINSTANCE hInst, HWND hWnd)
{
	if(m_initialised)
		return;

	m_frameID = 0;
	m_mainThreadID = GetCurrentThreadId();
	m_hWnd = hWnd;
	m_renderObj = new SKRendererObject(hInst);
	m_inputObj = new SKInputObject(hInst, hWnd);
	m_physicsObj = new SKPhysicsObject();
	m_audioObject = new SKAudioObject();

#ifdef _DEBUG
	m_renderObj->CreateDevice("Modules\\SKD3D_d.dll");
	m_inputObj->CreateDevice("Modules\\SKDirectInput_d.dll");
	m_audioObject->CreateAudioDevice("Modules\\SKAudio_d.dll");
	m_dataLibDll = LoadLibrary("Modules\\CppDataLib_d.dll");
#ifdef USE_DOUBLE_PRECISION
	m_physicsObj->CreateWorld("Modules\\SKPhysics_precise_d.dll");
#else
	m_physicsObj->CreateWorld("Modules\\SKPhysics_d.dll");
#endif
#else
	m_renderObj->CreateDevice("Modules\\SKD3D.dll");
	m_inputObj->CreateDevice("Modules\\SKDirectInput.dll");
	m_dataLibDll = LoadLibrary("Modules\\CppDataLib.dll");
	m_audioObject->CreateAudioDevice("Modules\\SKAudio.dll");
#ifdef USE_DOUBLE_PRECISION
	m_physicsObj->CreateWorld("Modules\\SKPhysics_precise.dll");
#else
	m_physicsObj->CreateWorld("Modules\\SKPhysics.dll");
#endif
#endif

	//-- Load Game Data
	CREATEDATAMANAGER createDataMan = (CREATEDATAMANAGER)GetProcAddress(m_dataLibDll, "CreateDataManager");
	this->DataMan = createDataMan();
	SplineContainer* cubicSplineContainer = new SplineContainer();
	this->DataMan->GetTypes().Add(cubicSplineContainer);
	this->DataMan->LoadFromFile("Resources\\GameData.skd");
	//--

	//-- Set Global Data
	this->Renderer = m_renderObj->GetDevice();
	this->InputDevices = m_inputObj->GetDevice();
	this->PhysicsWorld = m_physicsObj->GetWorld();
	this->AudioDevice = m_audioObject->GetAudioDevice();
	//--
	
	//-- Load Video Settings
	std::ifstream settings_file("Data\\RenderSettings.bin", std::ios::binary | std::ios::in);
	if(settings_file.is_open())
	{
		DisplaySettings DS;
		settings_file.read((char*)&DS, sizeof(DisplaySettings));
		settings_file.close();

		memcpy(&VideoSettings, &DS.VidoeSettings, sizeof(RenderSettings));
		ShadowResolutionLevel = DS.ShadowResolutionLevel;
		FilteringQuality = DS.FilteringQuality;
	}
	else
	{
		VideoSettings.RefreshRate = 60;
		VideoSettings.Windowed = true;
		VideoSettings.NumBackBuffers = 2;
		VideoSettings.DepthStencilFormat = DF_24S;
		VideoSettings.EnableMultisampling = true;
		VideoSettings.FSAAQuality = SKRenderer::MS_NONE;
		VideoSettings.EnableVSync = false;
		VideoSettings.DisplayWidth = 640;
		VideoSettings.DisplayHeight = 480;
		VideoSettings.BackBufferFormat = SKRenderer::PF_X8R8G8B8;
		VideoSettings.LODBias = -0.175f;

		ShadowResolutionLevel = 0;
		FilteringQuality = 0;
	}
	
	RECT area;
	GetClientRect(m_hWnd, &area);
	m_borderSize.x = 640 - area.right;
	m_borderSize.y = 480 - area.bottom;

	if(VideoSettings.Windowed)
	{
		SetWindowLong(m_hWnd, GWL_STYLE, WS_VISIBLE | WS_CLIPSIBLINGS | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX);
		SetWindowPos(m_hWnd, NULL, 0, 0, VideoSettings.DisplayWidth + m_borderSize.x, VideoSettings.DisplayHeight + m_borderSize.y, SWP_NOZORDER);
	}
	else
	{
		SetWindowLong(m_hWnd, GWL_STYLE, WS_POPUP);
		SetWindowPos(m_hWnd, NULL, 0, 0, VideoSettings.DisplayWidth, VideoSettings.DisplayHeight, SWP_NOZORDER);
		ShowCursor( FALSE );
	}

	bool modified = false;
	RenderSettings updatedSettings;
	this->Renderer->ValidateSettings(m_hWnd, VideoSettings, modified, &updatedSettings);
	if(modified)
		memcpy(&VideoSettings, &updatedSettings, sizeof(RenderSettings));


	this->Renderer->Initialise(m_hWnd, VideoSettings);
	this->Renderer->SetClippingPlanes(1, 300);
	this->Renderer->InitialiseStage(0.82f, NULL, 0);
	this->Renderer->SetMode(EMD_PERSPECTIVE, 0);
	this->Renderer->SetWorldTransform(NULL);
	this->Renderer->SetClearColour(0,0,0);
	this->Renderer->UseTextures(true);
	this->Renderer->SetTextureStage(0, RS_TEX_MODULATE);
	this->Renderer->SetBackfaceCulling(RS_CULL_CCW);
	this->Renderer->InitialiseStage(1.f, NULL, 1);
	this->Renderer->SetMode(EMD_TWOD, 1);
	this->Renderer->OnLoadResource.Add(loadGameDataResource, this);
	this->VertexManager = this->Renderer->GetVertexCacheManager();
	
	this->InputDevices = m_inputObj->GetDevice();
	this->Keyboard = this->InputDevices->CreateKeyboard();
	this->Keyboard->Acquire();

	SKInput::ControllerEnum controllers = this->InputDevices->GetJoysticks();
	if(controllers.Length() > 0)
	{
		this->Joystick = this->InputDevices->CreateJoystick(controllers[0]);
		this->Joystick->Acquire();
	}

	//-- Permanent unchangable assignments
	this->Keyboard->AssignKey(SKInput::SK_ESCAPE, GB_ESCAPE);
	this->Keyboard->AssignKey(SKInput::SK_UP, GB_MENU_UP);
	this->Keyboard->AssignKey(SKInput::SK_DOWN, GB_MENU_DOWN);
	this->Keyboard->AssignKey(SKInput::SK_LEFT, GB_MENU_LEFT);
	this->Keyboard->AssignKey(SKInput::SK_RIGHT, GB_MENU_RIGHT);
	this->Keyboard->AssignKey(SKInput::SK_F7, GB_TOGGLESHADERS);
	//--

	ButtonMapData* buttonData;
	int Len = LoadButtonMapData("Data\\ButtonMaps.bin", &buttonData);
	this->StoredButtonMaps.Clear();
	if(Len > 0)
	{
		for(int i=0; i<Len; i++)
		{
			this->StoredButtonMaps.Add(buttonData[i]);
		}
		delete[] buttonData;

		ButtonMapData& data = this->StoredButtonMaps[0];
		if(data.Brake != 0xFFFFFFFF)
			this->Keyboard->AssignKey(data.Brake, GB_BRAKE);
		if(data.Throttle != 0xFFFFFFFF)
			this->Keyboard->AssignKey(data.Throttle, GB_THROTTLE);
		if(data.SteerLeft != 0xFFFFFFFF)
			this->Keyboard->AssignKey(data.SteerLeft, GB_STEER_LEFT);
		if(data.SteerRight != 0xFFFFFFFF)
			this->Keyboard->AssignKey(data.SteerRight, GB_STEER_RIGHT);
		if(data.Back != 0xFFFFFFFF)
			this->Keyboard->AssignKey(data.Back, GB_BACK);
		if(data.GearDown != 0xFFFFFFFF)
			this->Keyboard->AssignKey(data.GearDown, GB_GEARDOWN);
		if(data.GearUp != 0xFFFFFFFF)
			this->Keyboard->AssignKey(data.GearUp, GB_GEARUP);
		if(data.Pause != 0xFFFFFFFF)
			this->Keyboard->AssignKey(data.Pause, GB_PAUSE);
		if(data.Select != 0xFFFFFFFF)
			this->Keyboard->AssignKey(data.Select, GB_ACTION);
		if(data.Cam != 0xFFFFFFFF)
			this->Keyboard->AssignKey(data.Cam, GB_CAM);
	}
	else
	{
		//Default keyboard controls
		ButtonMapData data;
		data.DeviceID = this->Keyboard->GetDeviceID();
		data.Brake = SKInput::SK_DOWN;
		data.GearUp = SKInput::SK_RIGHT;
		data.GearDown = SKInput::SK_LEFT;
		data.Throttle = SKInput::SK_UP;
		data.SteerLeft = SKInput::SK_A;
		data.SteerRight = SKInput::SK_D;
		data.Cam = SKInput::SK_V;
		data.Back = SKInput::SK_BACK;
		data.Pause = SKInput::SK_ESCAPE;
		data.Select = SKInput::SK_RETURN;
		data.BrakeSens = 1.f;
		data.SteerSens = 1.f;
		data.ThrottleSens = 1.f;
		this->StoredButtonMaps.Add(data);

		this->Keyboard->AssignKey(SKInput::SK_RETURN, GB_ACTION);
		this->Keyboard->AssignKey(SKInput::SK_ESCAPE, GB_PAUSE);
		this->Keyboard->AssignKey(SKInput::SK_BACK, GB_BACK);
		this->Keyboard->AssignKey(SKInput::SK_DOWN, GB_BRAKE);
		this->Keyboard->AssignKey(SKInput::SK_UP, GB_THROTTLE);
		this->Keyboard->AssignKey(SKInput::SK_RIGHT, GB_GEARUP);
		this->Keyboard->AssignKey(SKInput::SK_LEFT, GB_GEARDOWN);
		this->Keyboard->AssignKey(SKInput::SK_A, GB_STEER_LEFT);
		this->Keyboard->AssignKey(SKInput::SK_D, GB_STEER_RIGHT);
		this->Keyboard->AssignKey(SKInput::SK_V, GB_CAM);
	}

	LoadJoypadConfig();
	

	//-- Create Command Buffers
	SKCommandRecorder* comRec;
	for(int i=0; i<MAX_BUFFERED_FRAMES; i++)
	{
		this->Renderer->CreateCommandRecorder(&comRec, 65536);
		m_freeBufferQueue.Enqueue(comRec);
	}

	m_freeBufferQueue.Dequeue(&this->m_commandBuffer);
	this->m_commandBuffer->BeginRecording(RF_PACK_VERTS | RF_PACK_MATS);
	//--

	m_initialised = true;

	//-- Create FSM
	m_gameState = new TopStates::GameState();
	m_topLevelFSM = new FiniteStateMachine::Machine();
	m_topLevelFSM->AddState(TopStates::S_INTRO, new TopStates::IntroState(), true);
	m_topLevelFSM->AddState(TopStates::S_GAME, m_gameState);
	m_topLevelFSM->AddTransition(TopStates::S_INTRO, TopStates::S_GAME, "IntroCompleted");

	m_topLevelFSM->Enter();
	//--

	//Load fonts into memory
	DataLib::IFile* file = DataMan->GetFiles().Find("Wevli\\Medium.ttf");
	if(file != NULL)
	{
		int len = (int)file->GetOriginalLength();
		BYTE* buffer = new BYTE[len];
		DataLib::IStream* stream = DataMan->GetFileStream(file->GetUID());
		int bytesRead = stream->Read(buffer, 0, len);
		stream->Release();
		DWORD numFonts = 0;
		AddFontMemResourceEx(buffer, len, 0, &numFonts);
		delete[] buffer;
	}

	file = DataMan->GetFiles().Find("Wevli\\Bold.ttf");
	if(file != NULL)
	{
		int len = (int)file->GetOriginalLength();
		BYTE* buffer = new BYTE[len];
		DataLib::IStream* stream = DataMan->GetFileStream(file->GetUID());
		int bytesRead = stream->Read(buffer, 0, len);
		stream->Release();
		DWORD numFonts = 0;
		AddFontMemResourceEx(buffer, len, 0, &numFonts);
		delete[] buffer;
	}

	file = DataMan->GetFiles().Find("Wevli\\Numbers.ttf");
	if(file != NULL)
	{
		int len = (int)file->GetOriginalLength();
		BYTE* buffer = new BYTE[len];
		DataLib::IStream* stream = DataMan->GetFileStream(file->GetUID());
		int bytesRead = stream->Read(buffer, 0, len);
		stream->Release();
		DWORD numFonts = 0;
		AddFontMemResourceEx(buffer, len, 0, &numFonts);
		delete[] buffer;
	}

	//-- Load up the fonts
	float scale = this->VideoSettings.DisplayHeight / 720.f;
	this->Renderer->CreateFont("Wevli Medium", 400, false, false, false, (UINT)(26*scale + 0.5f), &this->TitleFont);
	this->Renderer->CreateFont("Wevli Bold", 400, false, false, false, (UINT)(26*scale + 0.5f), &this->BoldFont);
	this->Renderer->CreateFont("Wevli Medium", 400, false, false, false, (UINT)(16*scale + 0.5f), &this->StandardFont);
	this->Renderer->CreateFont("Wevli Numbers", 400, false, false, false, (UINT)(26*scale + 0.5f), &this->MonospaceFont);
	this->Renderer->CreateFont("Wevli Numbers", 400, false, false, false, (UINT)(20*scale + 0.5f), &this->MonospaceMedFont);
	this->Renderer->CreateFont("Wevli Numbers", 400, false, false, false, (UINT)(16*scale + 0.5f), &this->MonospaceSmallFont);
	//--

	m_gameTimer.Reset();
	m_graphicsTimer.Reset();
	m_fpsTimer = 0;
	m_fpsCount = 0;
	m_curFps = 0;
	this->TimeStep = 0;
	this->Renderer->UseShaders(false);

	DWORD_PTR ProcessAffinityMask;
	DWORD_PTR SystemAffinityMask;

	if( GetProcessAffinityMask( GetCurrentProcess(), &ProcessAffinityMask, &SystemAffinityMask ) )
	{
		ULONG ProcessorCount = 0;
		for( DWORD_PTR ProcessorBit = 1; ProcessorBit > 0; ProcessorBit <<= 1 )
		{
			if( ProcessAffinityMask & ProcessorBit )
				ProcessorCount++;
		}

		ProcessorCount--; //We're using one right now!
		m_multithreaded = ProcessorCount > 0;
				
		if(m_multithreaded)
		{
			m_logicThread = CreateThread(NULL, 0, t_logic, this, 0, NULL);
		}
	}
}

RPMDemo::~RPMDemo()
{
	//-- Save Settings
	CreateDirectory("Data", NULL);
	std::ofstream settings_file("Data\\RenderSettings.bin", std::ios::out | std::ios::binary);
	if(settings_file.is_open())
	{
		DisplaySettings DS;
		memcpy(&DS.VidoeSettings, &VideoSettings, sizeof(RenderSettings));
		DS.FilteringQuality = FilteringQuality;
		DS.ShadowResolutionLevel = ShadowResolutionLevel;
		settings_file.write((char*)&DS, sizeof(DisplaySettings));
		settings_file.close();
	}
	//--

	//-- Save ButtonMaps
	ButtonMapData* bmData = &StoredButtonMaps[0];
	SaveButtonMapData(bmData, StoredButtonMaps.Length(), "Data\\ButtonMaps.bin");
	//--

	if(m_initialised)
	{
		m_topLevelFSM->Leave();
		delete m_topLevelFSM;
	}

	if(Keyboard)
	{
		delete Keyboard;
		Keyboard = NULL;
	}
	if(Joystick)
	{
		delete Joystick;
		Joystick = NULL;
	}
	if(m_renderObj)
	{
		m_renderObj->Release();
		delete m_renderObj;
	}
	if(m_physicsObj)
	{
		m_physicsObj->Release();
		delete m_physicsObj;
	}
	if(m_inputObj)
	{
		m_inputObj->Release();
		delete m_inputObj;
	}
	if(m_audioObject)
	{
		m_audioObject->Release();
		delete m_audioObject;
	}
}

void RPMDemo::loadGameDataResource(void* sender, SKRenderer::LoadResourceEventArgs* e, void* user_data)
{
	RPMDemo* self = static_cast<RPMDemo*>(user_data);
	DataLib::IFile* file = self->DataMan->GetFiles().Find(e->ResourceName);
	if(file != NULL)
	{
		int len = (int)file->GetOriginalLength();
		BYTE* buffer = new BYTE[len];
		DataLib::IStream* stream = self->DataMan->GetFileStream(file->GetUID());
		int bytesRead = stream->Read(buffer, 0, len);
		stream->Release();

		e->Handled = true;
		e->Data = buffer;
		e->DataLen = bytesRead;
		e->DataOwner = true;
	}
	else
	{
		_asm { int 3 }
	}
}

void RPMDemo::AddScreen(Screen* screen)
{
	m_screens.AddScreen(screen);
}

void RPMDemo::RemoveScreen(Screen* screen)
{
	m_screens.RemoveScreen(screen);
}

bool RPMDemo::Run()
{
	//-- Logic Thread
	if(!m_multithreaded)
	{
		this->TimeStep = m_gameTimer.GetSeconds_Reset();
		this->TimeStep = min(0.05f, this->TimeStep);

		//-- Get Input
		if(this->Keyboard->LostDevice())
			this->Keyboard->Acquire();
		this->Keyboard->Poll();
	
		if(this->Joystick)
		{
			if(this->Joystick->LostDevice())
				this->Joystick->Acquire();
			this->Joystick->Poll();
		}
		//--

		AudioDevice->Update();
		m_topLevelFSM->Run();
	
		if(m_commandBuffer)
		{
			m_commandBuffer->BeginRecording(RF_PACK_VERTS | RF_PACK_MATS);
			m_screens.Render(m_commandBuffer);
			m_commandBuffer->EndRecording();

			SKCommandRecorder* tmpRec = m_commandBuffer;
			m_commandBuffer = NULL;
			m_frameID++;

			CommandRecorderData recData = { tmpRec, m_frameID };
			if(!m_renderBufferQueue.Enqueue(recData))
				m_commandBuffer = tmpRec;
		}
	
		if(m_commandBuffer == NULL)
			m_freeBufferQueue.Dequeue(&m_commandBuffer);
	}
	//--

	if(m_reinitialiseGraphics)
	{
		m_gameState->ClearGraphicsResources();
		this->Renderer->DestroyFont(this->TitleFont);
		this->Renderer->DestroyFont(this->BoldFont);
		this->Renderer->DestroyFont(this->StandardFont);
		this->Renderer->DestroyFont(this->MonospaceFont);
		this->Renderer->DestroyFont(this->MonospaceMedFont);
		this->Renderer->DestroyFont(this->MonospaceSmallFont);
	
		if(SUCCEEDED(this->Renderer->Reset(&this->VideoSettings)))
		{
			float scale = this->VideoSettings.DisplayHeight / 720.f;
			this->Renderer->CreateFont("Wevli Medium", 400, false, false, false, (UINT)(26*scale + 0.5f), &this->TitleFont);
			this->Renderer->CreateFont("Wevli Bold", 400, false, false, false, (UINT)(26*scale + 0.5f), &this->BoldFont);
			this->Renderer->CreateFont("Wevli Medium", 400, false, false, false, (UINT)(16*scale + 0.5f), &this->StandardFont);
			this->Renderer->CreateFont("Wevli Numbers", 400, false, false, false, (UINT)(26*scale + 0.5f), &this->MonospaceFont);
			this->Renderer->CreateFont("Wevli Numbers", 400, false, false, false, (UINT)(20*scale + 0.5f), &this->MonospaceMedFont);
			this->Renderer->CreateFont("Wevli Numbers", 400, false, false, false, (UINT)(16*scale + 0.5f), &this->MonospaceSmallFont);

			m_gameState->InitialiseGraphicsResources();

			if(VideoSettings.Windowed)
			{
				SetWindowLong(m_hWnd, GWL_STYLE, WS_VISIBLE | WS_CLIPSIBLINGS | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX);
				SetWindowPos(m_hWnd, NULL, 0, 0, VideoSettings.DisplayWidth + m_borderSize.x, VideoSettings.DisplayHeight + m_borderSize.y, SWP_NOZORDER);
				ShowCursor( TRUE );
			}
			else
			{
				SetWindowLong(m_hWnd, GWL_STYLE,  WS_VISIBLE | WS_CLIPSIBLINGS | WS_POPUP);
				SetWindowPos(m_hWnd, NULL, 0, 0, VideoSettings.DisplayWidth, VideoSettings.DisplayHeight, SWP_NOZORDER);
				ShowCursor( FALSE );
			}

			this->Renderer->InitialiseStage(0.82f, NULL, 0);
			this->Renderer->InitialiseStage(1.f, NULL, 1);

			m_reinitialiseGraphics = false;
		}
	}
	else if(this->Renderer->DeviceLost())
	{
		this->Renderer->Reset();
	}
	else if(this->Renderer->IsRunning())
	{
		//-- Move functions from threaded queue to a local buffer
		InvokeFuncData func;
		while(m_queuedFuncs.Dequeue(&func))
		{
			m_localQueuedFuncs.Add(func);
		}
		//--

		CommandRecorderData renderBuffer;
		if(m_renderBufferQueue.Dequeue(&renderBuffer))
		{
			this->Renderer->BeginRendering(true, true, true);
			renderBuffer.Buffer->Play();
			this->Renderer->EndRendering();

			m_freeBufferQueue.Enqueue(renderBuffer.Buffer);

			while(m_localQueuedFuncs.Length() > 0 && m_localQueuedFuncs[0].FrameID <= renderBuffer.FrameID)
			{
				m_localQueuedFuncs[0].Func(m_localQueuedFuncs[0].Args);
				m_localQueuedFuncs.RemoveAt_RO(0);
			}

			m_fpsCount++;
			m_fpsTimer += m_graphicsTimer.GetSeconds_Reset();
			if(m_fpsTimer > 1.f)
			{
				m_curFps = m_fpsCount;
				m_fpsCount = 0;
				m_fpsTimer = 0;
			}
		}
	}

	return true;
}

DWORD RPMDemo::t_logic(LPVOID args)
{
	RPMDemo* self = reinterpret_cast<RPMDemo*>(args);

	while(self->m_running)
	{
		self->TimeStep = self->m_gameTimer.GetSeconds_Reset();
		self->TimeStep = min(0.05f, self->TimeStep);

		//-- Get Input
		if(self->Keyboard->LostDevice())
			self->Keyboard->Acquire();
		self->Keyboard->Poll();
	
		if(self->Joystick)
		{
			if(self->Joystick->LostDevice())
				self->Joystick->Acquire();
			self->Joystick->Poll();
		}
		//--

		self->AudioDevice->Update();
		self->m_topLevelFSM->Run();
	
		if(self->m_commandBuffer)
		{
			self->m_commandBuffer->BeginRecording(RF_PACK_VERTS | RF_PACK_MATS);
			self->m_screens.Render(self->m_commandBuffer);
			self->m_commandBuffer->EndRecording();

			SKCommandRecorder* tmpRec = self->m_commandBuffer;
			self->m_commandBuffer = NULL;
			self->m_frameID++;
			CommandRecorderData recData = { tmpRec, self->m_frameID };
			if(!self->m_renderBufferQueue.Enqueue(recData))
				self->m_commandBuffer = tmpRec;
		}
	
		if(self->m_commandBuffer == NULL)
			self->m_freeBufferQueue.Dequeue(&self->m_commandBuffer);
	}

	return 0;
}

void RPMDemo::DisplaySettingsChanged()
{
	m_reinitialiseGraphics = true;
}

void RPMDemo::LoadJoypadConfig()
{
	if(this->Joystick)
	{
		this->Joystick->AssignKey(SKInput::SK_POV1_UP, GB_MENU_UP);
		this->Joystick->AssignKey(SKInput::SK_POV1_DOWN, GB_MENU_DOWN);
		this->Joystick->AssignKey(SKInput::SK_POV1_LEFT, GB_MENU_LEFT);
		this->Joystick->AssignKey(SKInput::SK_POV1_RIGHT, GB_MENU_RIGHT);

		int Len = this->StoredButtonMaps.Length();
		bool haveDevice = false;
		int dataIndex = 0;
		for(int i=0; i<Len; i++)
		{
			if(this->StoredButtonMaps[i].DeviceID == this->Joystick->GetDeviceID())
			{
				haveDevice = true;
				dataIndex = i;
				break;
			}
		}

		if(haveDevice)
		{
			JoypadMapIndex = dataIndex;
			ButtonMapData& data = this->StoredButtonMaps[dataIndex];
			
			if(data.CombinedThrottleBrake != 0xFFFFFFFF)
				this->Joystick->AssignKey(data.CombinedThrottleBrake, GB_COMBINED_BRAKE_THROTTLE);
			if(data.Brake != 0xFFFFFFFF)
				this->Joystick->AssignKey(data.Brake, GB_BRAKE);
			if(data.Throttle != 0xFFFFFFFF)
				this->Joystick->AssignKey(data.Throttle, GB_THROTTLE);
			if(data.Steering != 0xFFFFFFFF)
				this->Joystick->AssignKey(data.Steering, GB_STEERING);
			if(data.SteerLeft != 0xFFFFFFFF)
				this->Joystick->AssignKey(data.SteerLeft, GB_STEER_LEFT);
			if(data.SteerRight != 0xFFFFFFFF)
				this->Joystick->AssignKey(data.SteerRight, GB_STEER_RIGHT);
			if(data.Back != 0xFFFFFFFF)
				this->Joystick->AssignKey(data.Back, GB_BACK);
			if(data.GearDown != 0xFFFFFFFF)
				this->Joystick->AssignKey(data.GearDown, GB_GEARDOWN);
			if(data.GearUp != 0xFFFFFFFF)
				this->Joystick->AssignKey(data.GearUp, GB_GEARUP);
			if(data.Pause != 0xFFFFFFFF)
				this->Joystick->AssignKey(data.Pause, GB_PAUSE);
			if(data.Select != 0xFFFFFFFF)
				this->Joystick->AssignKey(data.Select, GB_ACTION);
			if(data.Cam != 0xFFFFFFFF)
				this->Joystick->AssignKey(data.Cam, GB_CAM);
		}
		else
		{
			ButtonMapData data;
			data.DeviceID = this->Joystick->GetDeviceID();
			data.Brake = 0xFFFFFFFF;
			data.GearUp = 0xFFFFFFFF;
			data.GearDown = 0xFFFFFFFF;
			data.Throttle = 0xFFFFFFFF;
			data.SteerLeft = 0xFFFFFFFF;
			data.SteerRight = 0xFFFFFFFF;
			data.Steering = 0xFFFFFFFF;
			data.CombinedThrottleBrake = 0xFFFFFFFF;
			data.Cam = 0xFFFFFFFF;
			data.BrakeDir = 1.f;
			data.SteerLeftDir = 1.f;
			data.SteerRightDir = 1.f;
			data.ThrottleDir = 1.f;
			data.BrakeSens = 1.f;
			data.SteerSens = 1.f;
			data.ThrottleSens = 1.f;
			data.Select = SKInput::SK_BUTTON1;
			data.Back = SKInput::SK_BUTTON2;
			data.Pause = SKInput::SK_BUTTON4;
			this->StoredButtonMaps.Add(data);

			this->Joystick->AssignKey(SKInput::SK_BUTTON1, GB_ACTION);
			this->Joystick->AssignKey(SKInput::SK_BUTTON2, GB_BACK);
			this->Joystick->AssignKey(SKInput::SK_BUTTON4, GB_PAUSE);

			JoypadMapIndex = this->StoredButtonMaps.Length()-1;
		}
	}
}

bool RPMDemo::Invoke(InvokeFunc func, LPVOID args)
{
	DWORD curThread = GetCurrentThreadId();
	bool success = true;
	if(curThread == m_mainThreadID)
		func(args);
	else
	{
		InvokeFuncData newFunc = { func, args };
		success = m_queuedFuncs.Enqueue(newFunc);
	}

	return success;
}