#ifdef BACON
#include "TopState.h"
#include "StaticGame.h"
#include <MainForm.h>
#include <Clock.h>

#include <windows.h>
#include <shlobj.h>
#include <tchar.h>
#include <stdio.h>

bool g_Running = true;


LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	HDC hdc;

	switch (message)
	{
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		g_Running = false;
		break;
	case WM_ACTIVATEAPP:
		if(Game::Renderer)
		{
			WaitForSingleObject(Game::RendererLock, INFINITE);
			Game::Renderer->Reset();
			ReleaseSemaphore(Game::RendererLock, 1, NULL);
		}
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}



int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLn, int nCmdShow)
{
	_CrtSetDbgFlag(_CrtSetDbgFlag(_CRTDBG_REPORT_FLAG)|_CRTDBG_LEAK_CHECK_DF);

	//_CrtSetBreakAlloc(133);
	MSG msg;
	HWND hWnd;

	MainForm* form = MainForm::CreateInstance();
	form->SetApplicationInstance(hInstance);
	form->SetWndProc( WndProc );
	hWnd = form->Show("VehicleSimDemo", "SKEngine - Elise Demo", 640, 480, WS_BORDER | WS_SYSMENU | WS_MINIMIZEBOX, NULL, NULL, NULL, NULL, true, 0XFF000000);


	if(!hWnd)
		return 0;

	Clock gameClock;
	float timeStep = 0.f;
	TopState mainState("TopState", 1, hInstance, hWnd);

	do
	{
		//-- Pass on messages
		while (PeekMessage(&msg, NULL, 0, 0, 0))
		{
			if(GetMessage(&msg, NULL, 0,0))
			{
				TranslateMessage(&msg);	
				DispatchMessage(&msg);
			}
		}
		//--

		timeStep = (float)gameClock.GetSeconds_Reset();

		StateResult res = RES_CONTINUE;
		switch(mainState.GetState())
		{
		case S_ENTERING:
			{
				res = mainState.EnterState();
				if(res == RES_CONTINUE)
					mainState.SetState(S_RUNNING);
			}
			break;

		case S_RUNNING:
			{
				res = mainState.Update(timeStep);
			}
			break;

		case S_LEAVING:
			{
				res = mainState.LeaveState();
				if(res == RES_CONTINUE)
				{
					mainState.SetState(S_DEAD);
				}
			}
			break;

		case S_DEAD:
			{
				//Break out of this loop
				g_Running = false;
				PostQuitMessage(0);
			}
			break;

		default:
			break;
		}

		if(res == RES_LEAVE)
			mainState.SetState(S_LEAVING);

	} while ( g_Running );

	MainForm::DestroyInstance();

	return (int) msg.wParam;
}
#endif