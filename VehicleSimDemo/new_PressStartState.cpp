#include "new_RPMDemo.h"
#include "Camera.h"
#include "new_CameraTarget.h"
#include "new_States.h"
#include "new_GameState.h"
#include "new_PressStartState.h"
#include "GameButtons.h"

using namespace TopStates;
using namespace GameStates;

WORD PressStart::m_fullscreenIndices[6] = {0,1,2,2,3,0};

PressStart::PressStart(TopStates::GameState* GameDataState)
{
	this->GameData = GameDataState;
}

void PressStart::loadResources(LPVOID Args)
{
	PressStart* self = reinterpret_cast<PressStart*>(Args);
	RPMDemo* Game = RPMDemo::GetInstance();
	if(Game && Game->IsInitialised())
	{
		Game->Renderer->CreateTexture("Logo.tga", &self->m_logoTexture);
	}

	POINT Res;
	Game->Renderer->GetResolution(&Res);
	self->m_material->DiffuseTexture = self->m_logoTexture;

	float height = (float)Res.y;
	float scale = height / 720.f;
	float width = 608.f * scale;
	height = 160.f * scale;
	float y = floor((20.f * scale) + 0.5f);
	float x = floor((Res.x - width)*0.5f + 0.5f);

	Vertex* verts = new Vertex[12];
	memset(verts, 0, sizeof(Vertex)*12);
	WORD indices[] = {
		0,1,2,2,3,0,					//Title
		4,5,6,6,7,4, 8,9,10,10,11,8		//Letterbox
	};

	//--Title
	verts[0].x = x - 0.5f;
	verts[0].y = y - 0.5f;
	verts[0].z = 0.f;
	verts[0].tu = 0.f;
	verts[0].tv = 0.75f;

	verts[1].x = (Res.x - x) - 0.5f;
	verts[1].y = y - 0.5f;
	verts[1].z = 0.f;
	verts[1].tu = 1.f;
	verts[1].tv = 0.75f;

	verts[2].x = (Res.x - x) - 0.5f;
	verts[2].y = (y + height) - 0.5f;
	verts[2].z = 0.f;
	verts[2].tu = 1.f;
	verts[2].tv = 1.f;

	verts[3].x = x - 0.5f;
	verts[3].y = (y + height) - 0.5f;
	verts[3].z = 0.f;
	verts[3].tu = 0.f;
	verts[3].tv = 1.f;
	//--

	//-- Letterbox
	height = floor(50*scale + 0.5f);
	width = (float)Res.x;
	y = (float)Res.y;
	verts[4].x = -0.5f;			verts[4].y = -0.5f;
	verts[5].x = width-0.5f;	verts[5].y = -0.5f;
	verts[6].x = width-0.5f;	verts[6].y = height-0.5f;
	verts[7].x = -0.5f;			verts[7].y = height-0.5f;
	verts[8].x = -0.5f;			verts[8].y = y-height-0.5f;
	verts[9].x = width-0.5f;	verts[9].y = y-height-0.5f;
	verts[10].x = width-0.5f;	verts[10].y = y-0.5f;
	verts[11].x = -0.5f;		verts[11].y = y-0.5f;
	//--
	
	Game->Renderer->CreateIndexBuffer(indices, 18, false, &self->m_indiBuff);
	Game->Renderer->CreateVertexBuffer(VID_UU, verts, 12, false, &self->m_vertBuff);

	self->m_lgtRenderDepth = self->GameData->ToonBlinnPhong->GetParameterByName("RenderDepth");

	self->m_title.Mat = self->m_material;
	self->m_title.NumIndis = 6;
	self->m_title.NumVerts = 4;
	self->m_title.StartIndex = 0;
	self->m_title.StartVertex = 0;
	self->m_title.IBuff = self->m_indiBuff;
	self->m_title.VBuff = self->m_vertBuff;
	
	self->m_letterBoxTop = self->m_title;
	self->m_letterBoxTop.Mat = self->m_blackMat;
	self->m_letterBoxTop.NumIndis = 6;
	self->m_letterBoxTop.NumVerts = 12;
	self->m_letterBoxTop.StartIndex = 6;
	self->m_letterBoxTop.StartVertex = 0;
	self->m_letterBoxBottom = self->m_letterBoxTop;
	self->m_letterBoxBottom.StartIndex = 12;

	self->m_initialised = true;
}

void PressStart::Enter()
{
	m_initialised = false;
	m_renderDistance = 0.25f;
	m_overlayVerticalOffset = 0;
	m_logoAlpha = 1;
	m_cameraRotationAmount = PI_2;
	m_delayTimer = 0;
	m_introFlashBrightness = 1;
	m_playAnimation = false;

	m_material = new SKRenderer::Material("LogoMaterial");
	m_material->Ambient = 1.f;
	m_material->Diffuse = 1.f;
	m_material->Specular = 1.f;
	m_material->Emissive = 0.f;
	m_material->SpecularExponent = 1.f;

	m_blackMat = new SKRenderer::Material("BlackMaterial");
	m_blackMat->Ambient = 0.f;
	m_blackMat->Diffuse = 0.f;
	m_blackMat->Specular = 0.f;
	m_blackMat->Emissive = 0.f;
	m_blackMat->SpecularExponent = 1.f;
	m_blackMat->DiffuseTexture = NULL;

	m_overlay = NULL;
	m_scene = NULL;

	RPMDemo* Game = RPMDemo::GetInstance();
	m_displaySize.x = (float)Game->VideoSettings.DisplayWidth;
	m_displaySize.y = (float)Game->VideoSettings.DisplayHeight;
	m_scale = m_displaySize.y / 720.f;

	memset(m_fullscreenQuad, 0, sizeof(Vertex)*4);
	m_fullscreenQuad[0].x = -0.5f;
	m_fullscreenQuad[0].y = -0.5f;
	m_fullscreenQuad[1].x = m_displaySize.x-0.5f;		m_fullscreenQuad[1].tu = 1;
	m_fullscreenQuad[1].y = -0.5f;
	m_fullscreenQuad[2].x = m_displaySize.x-0.5f;		m_fullscreenQuad[2].tu = 1;
	m_fullscreenQuad[2].y = m_displaySize.y-0.5f;		m_fullscreenQuad[2].tv = 1;
	m_fullscreenQuad[3].x = -0.5f;
	m_fullscreenQuad[3].y = m_displaySize.y-0.5f;		m_fullscreenQuad[3].tv = 1;

	//-- Position Camera
	SKMath::SKQuaternion<real> quat; quat.MakeFromEuler(0, (real)m_cameraRotationAmount, 0);
	const SKRigidBody::State* state = Game->PhysicsWorld->GetBodyState(GameData->GetVehicleBody(VS_ELISE_BODY));
	GameData->GameCamTarget->Rot = state->Orientation * quat;
	GameData->GameCamera->SetStage(0);
	GameData->GameCamera->Update(0);
	GameData->GameCamera->SetStage(1);
	//--

	Game->Invoke(loadResources, this);
}

void PressStart::Leave()
{
	RPMDemo* Game = RPMDemo::GetInstance();
	if(m_scene)
	{
		Game->RemoveScreen(m_scene);
		Game->RemoveScreen(m_overlay);

		delete m_scene;
		delete m_overlay;
	}
}

void PressStart::Run()
{
	if(!m_initialised)
		return;
	RPMDemo* Game = RPMDemo::GetInstance();
	
	if(m_scene == NULL)
	{
		m_overlay = new Screen(renderOverlay, 50, this);
		m_scene = new Screen(renderScene, 0, this);

		Game->AddScreen(m_scene);
		Game->AddScreen(m_overlay);
		GameData->RenderScene(true);
	}
	
	if(m_introFlashBrightness > 0)
	{
		m_introFlashBrightness -= (float)Game->TimeStep;
		if(m_introFlashBrightness < 0)
			m_introFlashBrightness = 0;
	}
	else
	{
		//Wait for keypress
		Game->Keyboard->AddKeyPressedEvent(keyDown, this);
		if(Game->Joystick)
			Game->Joystick->AddKeyPressedEvent(keyDown, this);
	}
	
	if(m_playAnimation)
	{
		GameData->GameCamera->SetStage(1);
		const SKRigidBody::State* state = Game->PhysicsWorld->GetBodyState(GameData->GetVehicleBody(VS_ELISE_BODY));
		double delta = Game->TimeStep;
		if(m_cameraRotationAmount > PI)
		{
			m_cameraRotationAmount += delta;
			if(m_cameraRotationAmount > PIx2) m_cameraRotationAmount = PIx2;
			SKMath::SKQuaternion<real> quat; quat.MakeFromEuler(0, (real)m_cameraRotationAmount, 0);
			GameData->GameCamTarget->Rot = state->Orientation * quat;
		}
		else
		{
			m_cameraRotationAmount -= delta;
			if(m_cameraRotationAmount < 0) m_cameraRotationAmount = 0;
			SKMath::SKQuaternion<real> quat; quat.MakeFromEuler(0, (real)m_cameraRotationAmount, 0);
			GameData->GameCamTarget->Rot = state->Orientation * quat;
		}

		m_delayTimer += Game->TimeStep;

		//-- Animation Variables
		if(m_delayTimer > 0.5f)
		{
			m_renderDistance += 0.25*Game->TimeStep;
			m_overlayVerticalOffset += (float)(25 * Game->TimeStep * m_scale);
		}
		
		if(m_delayTimer > 3.75f)
			m_logoAlpha -=  0.75f*Game->TimeStep;
		if(m_renderDistance > 1) m_renderDistance = 1.f;
		if(m_logoAlpha < 0)
		{
			m_logoAlpha = 0.f;
			this->GetParent()->FireEvent("StartGame");
		}
		//--
	}
	else
	{
		m_cameraRotationAmount += 0.3f*Game->TimeStep;
		if(m_cameraRotationAmount > PIx2)
			m_cameraRotationAmount -= PIx2;

		SKMath::SKQuaternion<real> quat; quat.MakeFromEuler(0, (real)m_cameraRotationAmount, 0);
		const SKRigidBody::State* state = Game->PhysicsWorld->GetBodyState(GameData->GetVehicleBody(VS_ELISE_BODY));
		GameData->GameCamTarget->Rot = state->Orientation * quat;
		GameData->GameCamTarget->Rot.Normalise();
	}
	
	GameData->GameCamera->Update((float)Game->TimeStep);
}

void PressStart::renderScene(SKRenderer::SKRenderDevice* renderDevice, void* user_data)
{
	PressStart* self = reinterpret_cast<PressStart*>(user_data);
	renderDevice->SetEffect(self->GameData->ToonBlinnPhong, 0, &self->GameData->ToonPhongParams);
	float dist = (float)self->m_renderDistance;
	renderDevice->SetShaderConst(self->m_lgtRenderDepth, &dist, 4);
}

void PressStart::renderOverlay(SKRenderer::SKRenderDevice* renderDevice, void* user_data)
{
	PressStart* self = reinterpret_cast<PressStart*>(user_data);

	renderDevice->EnableLighting(true);
	renderDevice->SetAmbientLight(1,1,1);
	renderDevice->SetClearColour(1,1,1);
	renderDevice->SetMode(EMD_TWOD, 1);
	renderDevice->SetShadeMode(RS_SHADE_SOLID, 0, NULL);
	self->m_material->DiffuseTexture = self->m_logoTexture;

	SKMath::SKMatrix<float> offset; offset.Identity();
	
	offset._42 = -self->m_overlayVerticalOffset;
	renderDevice->SetWorldTransform(&offset);
	renderDevice->Render(self->m_letterBoxTop);
	
	offset._42 = self->m_overlayVerticalOffset;
	renderDevice->SetWorldTransform(&offset);
	renderDevice->Render(self->m_letterBoxBottom);

	self->m_material->Diffuse.A = (float)min(self->m_logoAlpha, 1.f);
	renderDevice->SetWorldTransform(NULL);
	renderDevice->Render(self->m_title, self->m_material);
	self->m_material->Diffuse.A = 1;
	
	int xPos = (int)(self->m_displaySize.x * 0.5f);
	int yPos = (int)(self->m_scale * 175.f);
	RPMDemo* Game = RPMDemo::GetInstance();
	renderDevice->DrawString(Game->TitleFont, xPos, yPos, SK_TEXT_CENTER, 0,0,0, (UINT)(self->m_logoAlpha*255), "Vehicle Simulation Demo");
	
	yPos = (int)(self->m_scale * 695.f + self->m_overlayVerticalOffset);
	renderDevice->DrawString(Game->StandardFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_CENTER, 255,255,255,255, "--Powered by SKEngine--\nSKEngine � 2008-2011, RPM � 2010-2011 Anthony Mushrow");

	if(self->m_introFlashBrightness > 0)
	{
		renderDevice->SetDepthBufferMode(RS_DEPTH_NONE);
		self->m_material->DiffuseTexture = NULL;
		self->m_material->Diffuse.A = self->m_introFlashBrightness;
		renderDevice->Render(VID_UU, self->m_fullscreenQuad, 4, self->m_fullscreenIndices, 6, self->m_material);
		self->m_material->Diffuse.A = 1;
		renderDevice->SetDepthBufferMode(RS_DEPTH_READWRITE);
	}
}

void PressStart::keyDown(void* sender, SKInput::KeyEventArgs* e, void* user_data)
{
	RPMDemo* Game = RPMDemo::GetInstance();
	PressStart* self = reinterpret_cast<PressStart*>(user_data);
	if(!e->IsAnalogue)
	{
		Game->Keyboard->RemoveKeyPressedEvent(keyDown);
		if(Game->Joystick)
			Game->Joystick->RemoveKeyPressedEvent(keyDown);

		self->m_playAnimation = true;
	}
}