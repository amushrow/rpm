#pragma once
#include <Windows.h>
#include <DArray.h>

namespace FiniteStateMachine
{
	class Machine;
	class State
	{
	friend class Machine;
	public:
		State() { m_parentMachine = NULL; }
		virtual ~State() {}

		virtual void FireEvent(char* eventName) {}
		Machine*	GetParent() { return m_parentMachine; }

	protected:
		virtual void Enter()=0;
		virtual void Run()=0;
		virtual void Leave()=0;

	private:
		Machine* m_parentMachine;
	};

	class Machine : public State
	{
	public:
		Machine();
		virtual ~Machine();

		virtual void FireEvent(char* eventName);

		void		AddState(DWORD StateID, State* State, bool startState = false);
		void		AddTransition(DWORD State, DWORD NextState, const char* EventName);
		void		SetCurrentState(DWORD StateID);
		DWORD		GetCurrentState();

		virtual void Enter();
		virtual void Run();
		virtual void Leave();

	private:
		struct IDState
		{
			State*	ClassPtr;
			DWORD	StateID;
		};
		struct Transition
		{
			DWORD	CurrentState;
			DWORD	NextState;
			State*	NextClassPtr;
			char*	EventName;
		};
		DArray<IDState>		m_states;
		DArray<Transition>	m_transitions;
		State*				m_currentState;
		DWORD				m_currentStateID;
		Machine*			m_parentMachine;
		DArray<char*>		m_eventQueue;
	};

}