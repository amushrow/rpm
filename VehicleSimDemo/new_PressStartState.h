#pragma once
#include "SKRenderDevice.h"
#include "SKPhysics.h"
#include "new_FiniteStateMachine.h"

namespace TopStates
{
	class GameState;
}

namespace GameStates
{
	//Press Start Screen
	class PressStart : public FiniteStateMachine::State
	{
	public:
		PressStart(TopStates::GameState* gameDataState);

	protected:
		virtual void Enter();
		virtual void Run();
		virtual void Leave();

	private:
		static void renderOverlay(SKRenderer::SKRenderDevice* renderDevice, void* user_data);
		static void renderScene(SKRenderer::SKRenderDevice* renderDevice, void* user_data);
		static void keyDown(void* sender, SKInput::KeyEventArgs* e, void* user_data);
		static void loadResources(LPVOID Args);
		static void unloadResources(LPVOID Args);
		
		Vertex						m_fullscreenQuad[4];
		SKRenderer::RenderBuffer	m_title;
		SKRenderer::RenderBuffer	m_letterBoxTop;
		SKRenderer::RenderBuffer	m_letterBoxBottom;
		Pointf						m_displaySize;
		double						m_delayTimer;
		double						m_cameraRotationAmount;
		double						m_renderDistance;
		double						m_logoAlpha;
		float						m_overlayVerticalOffset;
		float						m_introFlashBrightness;
		float						m_scale;
		bool						m_initialised;
		bool						m_playAnimation;	
		SKRenderer::VertexBufferID	m_vertBuff;
		SKRenderer::IndexBufferID	m_indiBuff;

		TopStates::GameState*		GameData;
		Screen*						m_overlay;
		Screen*						m_scene;
		SKRenderer::Material*		m_material;
		SKRenderer::Material*		m_blackMat;
		SKRenderer::Texture*		m_logoTexture;
		LPVOID						m_lgtRenderDepth;

		static WORD					m_fullscreenIndices[6];
	};
}