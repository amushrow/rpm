#include "new_RPMDemo.h"
#include "Camera.h"
#include "new_CameraTarget.h"
#include "new_States.h"
#include "GameButtons.h"

using namespace TopStates;
using namespace GameStates;
using namespace GameStates::GameSubStates;

WORD GameLogic::m_indices[6] = {0,1,2,2,3,0};

GameLogic::GameLogic(TopStates::GameState* gameDataState)
{
	this->GameData = gameDataState;
}

void GameLogic::Enter()
{
	RPMDemo* Game = RPMDemo::GetInstance();
	m_hudOverlay = new Screen(renderHud, 100, this);

	m_material = new SKRenderer::Material("WhiteMat");
	m_material->Ambient = 1.f;
	m_material->Diffuse = 1.f;
	m_material->Specular = 0.f;
	m_material->Emissive = 0.f;
	m_material->SpecularExponent = 0.f;

	memset(m_tachQuad, 0, sizeof(Vertex)*4);
	m_tachQuad[0].x = -0.5f;
	m_tachQuad[0].y = -0.5f;
	m_tachQuad[1].x = 255.5f;		m_tachQuad[1].tu = 1;
	m_tachQuad[1].y = -0.5f;
	m_tachQuad[2].x = 255.5f;		m_tachQuad[2].tu = 1;
	m_tachQuad[2].y = 255.5f;		m_tachQuad[2].tv = 1;
	m_tachQuad[3].x = -0.5f;
	m_tachQuad[3].y = 255.5f;		m_tachQuad[3].tv = 1;

	memset(m_needleQuad, 0, sizeof(Vertex)*4);
	m_needleQuad[0].x = -0.5f;
	m_needleQuad[0].y = -0.5f;
	m_needleQuad[1].x = 13.5f;		m_needleQuad[1].tu = 0.4375f;
	m_needleQuad[1].y = -0.5f;
	m_needleQuad[2].x = 13.5f;		m_needleQuad[2].tu = 0.4375f;
	m_needleQuad[2].y = 104.5f;		m_needleQuad[2].tv = 0.8203f;
	m_needleQuad[3].x = -0.5f;
	m_needleQuad[3].y = 104.5f;		m_needleQuad[3].tv = 0.8203f;

	for(int i=0; i<5; i++) m_rpmAvgs[i] = 1000;
	for(int i=0; i<24; i++) m_ffbAvgs[i] = 0;

	for(int i=0; i<4; i++)
	{
		m_wheelContactVel[i] = 0;
		for(int j=0; j<5; j++)
			m_avgContactVel[i][j] = 0;
	}

	m_timer = 0;
	m_camIndex = 0;
	m_keyboardSteer = 0;
	m_keyFrameTimer = 0;

	GameData->EngineSound->Play();

	GameData->GetVehicleBody(VS_WHEEL_FL)->AddImpulseAppliedCallback(wheelImpulseCallback, this);
	GameData->GetVehicleBody(VS_WHEEL_FR)->AddImpulseAppliedCallback(wheelImpulseCallback, this);
	for(int i=0; i<4; i++)
		GameData->GetVehicleBody(i)->AddContactEvent(contactCallback, this);
	m_ffb = 0;

	Game->PhysicsWorld->AddWorldUpdatedEvent(physicsUpdated, this);
	Game->Invoke(loadResources, this);
}

void GameLogic::loadResources(LPVOID Args)
{
	GameLogic* self = reinterpret_cast<GameLogic*>(Args);
	RPMDemo* Game = RPMDemo::GetInstance();

	Game->Renderer->CreateTexture("tach.dds", &self->m_tach);
	Game->Renderer->CreateTexture("needle.dds", &self->m_needle);

	Game->AddScreen(self->m_hudOverlay);
}

void GameLogic::Run()
{
	RPMDemo* Game = RPMDemo::GetInstance();
	
	updateInput();

	//-- Update States
	Game->PhysicsWorld->Update(Game->TimeStep);
	m_timer += Game->TimeStep;
	if(GameData->FastestLapTime > 0)
		GameData->GhostCarController.Update(Game->TimeStep);
	if(GameData->TimeLap)
		GameData->CurrentLapTime += Game->TimeStep;

	if(m_timer > 0.016f)
	{
		//-- Calc average RPM
		m_timer = 0;
		for(int i=0; i<4; i++)
			m_rpmAvgs[i] = m_rpmAvgs[i+1];
		m_rpmAvgs[4] = (float)GameData->GetVehicle()->GetRPM();
			
		float avg = 0;
		for(int i=0; i<5; i++)
			avg += m_rpmAvgs[i];
		avg *= 0.2f;
		m_rpm = avg;

		updateSound();
	}
	//--

	updateStates();
	updateLapPositions();
	GameData->GameCamera->Update(Game->TimeStep);

	if(m_pauseGame)
		this->GetParent()->FireEvent("PauseGame");
}

void GameLogic::updateInput()
{
	RPMDemo* Game = RPMDemo::GetInstance();

	m_throttle = 0;
	m_steer = 0;
	m_brake = 0;
	m_pauseGame = false;
	int gearChange = 0;

	if(Game->Joystick)
	{
		if(Game->StoredButtonMaps[Game->JoypadMapIndex].Steering != 0xFFFFFFFF)
		{
			m_steer = Game->Joystick->GetKeyValue(GB_STEERING) * Game->StoredButtonMaps[Game->JoypadMapIndex].SteerRightDir; 
		}
		else
		{
			float val = Game->Joystick->GetKeyValue(GB_STEER_LEFT)*Game->StoredButtonMaps[Game->JoypadMapIndex].SteerLeftDir;
			m_steer = (val+1.f) * 0.5f;
			val = Game->Joystick->GetKeyValue(GB_STEER_RIGHT)*Game->StoredButtonMaps[Game->JoypadMapIndex].SteerRightDir;
			m_steer += (val+1.f) * -0.5f;
		}
		if(Game->StoredButtonMaps[Game->JoypadMapIndex].CombinedThrottleBrake != 0xFFFFFFFF)
		{
			float val = Game->Joystick->GetKeyValue(GB_COMBINED_BRAKE_THROTTLE);
			val *= Game->StoredButtonMaps[Game->JoypadMapIndex].ThrottleDir;
			m_throttle = max(0.f, val);
			m_brake = max(0.f, -val);
		}
		else
		{
			m_throttle = Game->Joystick->GetKeyValue(GB_THROTTLE)*Game->StoredButtonMaps[Game->JoypadMapIndex].ThrottleDir;
			m_throttle = (m_throttle+1.f) * 0.5f;
			m_brake = Game->Joystick->GetKeyValue(GB_BRAKE)*Game->StoredButtonMaps[Game->JoypadMapIndex].BrakeDir;
			m_brake = (m_brake+1.f) * 0.5f;
		}

		m_pauseGame = Game->Joystick->KeyPressed(GB_PAUSE);
		if(Game->Joystick->KeyPressed(GB_GEARDOWN))
			gearChange = -1;
		else if(Game->Joystick->KeyPressed(GB_GEARUP))
			gearChange = 1;

		if(Game->Joystick->KeyPressed(GB_CAM))
		{
			m_camIndex++;
			if(m_camIndex > 2)
				m_camIndex = 0;
			GameData->SetCameraPos(m_camIndex);
		}
	}

	float maxKeySteer = (160 - m_speed)/160;
	if(m_keyboardSteer > 0)
	{
		if(Game->Keyboard->KeyDown(GB_STEER_LEFT)) {
			m_keyboardSteer -= 2*Game->TimeStep;
			if(m_keyboardSteer < -1)
				m_keyboardSteer = -1;
			m_steer = (float)m_keyboardSteer * maxKeySteer;
		}
		else if(Game->Keyboard->KeyDown(GB_STEER_RIGHT)) {
			m_keyboardSteer += 2*Game->TimeStep;
			if(m_keyboardSteer > 1)
				m_keyboardSteer = 1;
			m_steer = (float)m_keyboardSteer * maxKeySteer;
		}
	}
	else
	{
		if(Game->Keyboard->KeyDown(GB_STEER_RIGHT))	{
			m_keyboardSteer += 2*Game->TimeStep;
			if(m_keyboardSteer > 1)
				m_keyboardSteer = 1;
			m_steer = (float)m_keyboardSteer * maxKeySteer;
		}
		else if(Game->Keyboard->KeyDown(GB_STEER_LEFT))	{
			m_keyboardSteer -= 2*Game->TimeStep;
			if(m_keyboardSteer < -1)
				m_keyboardSteer = -1;
			m_steer = (float)m_keyboardSteer * maxKeySteer;
		}
	}
	if(m_steer == 0)
	{
		m_keyboardSteer = 0;
	}

	if(Game->Keyboard->KeyPressed(GB_GEARUP))
		gearChange = 1;
	else if(Game->Keyboard->KeyPressed(GB_GEARDOWN))
		gearChange = -1;

	if(Game->Keyboard->KeyDown(GB_THROTTLE))
		m_throttle = 1.f;
	if(Game->Keyboard->KeyDown(GB_BRAKE))
		m_brake = 1.f;

	if(Game->Keyboard->KeyPressed(GB_PAUSE))
		m_pauseGame = true;

	if(Game->Keyboard->KeyPressed(GB_CAM))
	{
		m_camIndex++;
		if(m_camIndex > 2)
			m_camIndex = 0;
		GameData->SetCameraPos(m_camIndex);
	}

	if(m_speed < 2 && GameData->GetVehicle()->GetGear() == G_NEUTRAL)
		m_brake = 1.f;

	GameData->GetVehicle()->SetSteering(m_steer);
	GameData->GetVehicle()->SetBraking(m_brake);
	GameData->GetVehicle()->SetAcceleration(m_throttle);
	if(gearChange > 0)
		GameData->GetVehicle()->NextGear();
	else if(gearChange < 0)
		GameData->GetVehicle()->PrevGear();
}

void GameLogic::updateSound()
{
	// Engine noises
	GameData->EngineSound->SetRPM(m_rpm);
	GameData->EngineSound->SetThrottle(m_throttle);
	GameData->EngineSound->Update();

	// Wheel noises
	for(int i=0; i<4; i++)
	{
		for(int j=0; j<4; j++)
			m_avgContactVel[i][j] = m_avgContactVel[i][j+1];
		m_avgContactVel[i][4] = (float)m_wheelContactVel[i];
		m_wheelContactVel[i] = 0;

		float vel = 0;
		for(int j=0; j<5; j++)
			vel += m_avgContactVel[i][j];
		vel *= 0.2f;

		float vol = min((vel-0.2f) * 0.2f, 1.f);
		if(vol > 0)
		{
			GameData->TyreSkidSound[i]->SetVolume(vol);
			GameData->TyreSkidSound[i]->Play();
		}
		else
		{
			GameData->TyreSkidSound[i]->Stop();
		}
	}
}

void GameLogic::updateLapPositions()
{
	//-- Check lap position
	RPMDemo* Game = RPMDemo::GetInstance();
	const SKRigidBody::State* state = Game->PhysicsWorld->GetBodyState(GameData->GetVehicleBody(VS_ELISE_BODY));

	if((state->Position - GameData->GetFinishPos()).GetSqrLength() < 225)
	{
		int classify = GameData->GetFinishPlane().Classify(state->Position);
		if(classify != GameData->PosClassify)
		{
			float dir = (float)(GameData->GetLapDir() * state->Velocity);
			if(classify==FRONT && dir > EPSILON)
				GameData->LapCounter--;
			else if(classify==BACK && dir < -EPSILON)
				GameData->LapCounter++;
		}
		GameData->PosClassify = classify;
	}

	//-- Next lap
	if(GameData->LapCounter == 0)
	{
		if(GameData->TimeLap)
		{
			GameData->PreviousLapTime = GameData->CurrentLapTime;
			//-- New Fastest Lap
			if(GameData->CurrentLapTime < GameData->FastestLapTime || GameData->FastestLapTime == 0)
			{
				GameData->FastestLapTime = GameData->CurrentLapTime;
				GameData->FastestReplay = GameData->ReplayBuffer;
				GameData->GhostCarController.SetFrames(GameData->FastestReplay);
			}
			//--
		}

		GameData->ReplayBuffer.Clear();
		GameData->CurrentLapTime = 0;
		GameData->TimeLap = true;
		GameData->LapCounter = 1;
		GameData->GhostCarController.SetTime(0);
		GameData->LapStartTime = Game->PhysicsWorld->GetWorldTime();
	}
	//--
}

void GameLogic::updateStates()
{
	const SKRigidBody::State* state;
	SKMath::SKMatrix<float> worldMat;
	const real* bodyMat;
	RPMDemo* Game = RPMDemo::GetInstance();
	
	//-- Get Wheel States
	for(int i=0; i<4; i++)
	{
		state = Game->PhysicsWorld->GetBodyState(GameData->GetVehicleBody(i));
		SKScene* scene = GameData->GetVehicleScene(i);
		bodyMat = &state->Transformations._11;
		for(int j=0; j<16; j++)
			worldMat[j] = (float)bodyMat[j];
		scene->SetWorldMatrix(worldMat);	
		scene->CalcMatrices();
	}
	//--

	//-- Get Body State
	state = Game->PhysicsWorld->GetBodyState(GameData->GetVehicleBody(VS_ELISE_BODY));
	
	GameData->GameCamTarget->Pos = state->Position;
	GameData->GameCamTarget->Rot = state->Orientation;
	GameData->GameCamTarget->Mat = state->Transformations;
	
	//-- Position all of the car body models
	SKScene* scene = GameData->GetVehicleScene(VS_ELISE_BODY);
	bodyMat = &state->Transformations._11;
	for(int j=0; j<16; j++)
		worldMat[j] = (float)bodyMat[j];
	scene->SetWorldMatrix(worldMat);	
	scene->CalcMatrices();
	scene = GameData->GetVehicleScene(VS_ELISE_SHADOW);
	scene->SetWorldMatrix(worldMat);
	scene->CalcMatrices();
	scene = GameData->GetVehicleScene(VS_ELISE_SHELL);
	scene->SetWorldMatrix(worldMat);	
	scene->CalcMatrices();

	//-- Grab speed and gear to display on HUD
	m_speed = (float)state->Velocity.GetLength() * 2.23693629f;
	m_gear = GameData->GetVehicle()->GetGearName();

	//-- Update the ghost car position
	if(GameData->FastestLapTime > 0)
	{
		ReplayOutputFrame ghostTransform;
		GameData->GhostCarController.GetCurrentFrame(&ghostTransform);

		scene = GameData->GetGhostVehicleScene(VS_ELISE_BODY);
		scene->SetWorldMatrix(ghostTransform.CarMat);	
		scene->CalcMatrices();

		for(int i=0; i<4; i++)
		{
			scene = GameData->GetGhostVehicleScene(i);
			scene->SetWorldMatrix(ghostTransform.WheelMatrix[i]);	
			scene->CalcMatrices();
		}
	}
	//--
}

void GameLogic::Leave()
{
	RPMDemo* Game = RPMDemo::GetInstance();
	Game->RemoveScreen(m_hudOverlay);
	GameData->GetVehicleBody(VS_WHEEL_FL)->RemoveImpulseAppliedCallback(wheelImpulseCallback);
	GameData->GetVehicleBody(VS_WHEEL_FR)->RemoveImpulseAppliedCallback(wheelImpulseCallback);

	for(int i=0; i<4; i++)
		GameData->GetVehicleBody(i)->RemoveContactEvent(contactCallback);

	if(Game->Joystick)
		Game->Joystick->SetForceFeedback(0);

	GameData->EngineSound->Stop();
	for(int i=0; i<4; i++)
		GameData->TyreSkidSound[i]->Stop();

	delete m_hudOverlay;
}

void GameLogic::renderHud(SKRenderer::SKRenderDevice* renderDevice, void* user_data)
{
	GameLogic* self = reinterpret_cast<GameLogic*>(user_data);
	RPMDemo* Game = RPMDemo::GetInstance();

	//-- Draw FPS
	float scale = Game->VideoSettings.DisplayHeight / 720.f;
	int xPos, yPos;
	renderDevice->DrawString(Game->StandardFont, 0,0, 0, 0,0,0,255, "FPS: %i", Game->GetFPS());
	//--

	//-- Draw Tachometer
	renderDevice->EnableLighting(true);
	renderDevice->SetMode(EMD_TWOD, 1);
	renderDevice->SetShadeMode(RS_SHADE_SOLID, 0, NULL);
	
	yPos = Game->VideoSettings.DisplayHeight - (int)(260*scale + 0.5f);
	xPos = Game->VideoSettings.DisplayWidth - (int)(256*scale + 0.5f);
	SKMath::SKMatrix<float> transMat = SKMath::SKMatrix<float>::IdentityMat;
	transMat.Scale(scale, scale, 1);
	transMat.Translate((float)xPos, (float)yPos, 0);
	renderDevice->SetWorldTransform(&transMat);
	self->m_material->DiffuseTexture = self->m_tach;
	renderDevice->Render(VID_UU, self->m_tachQuad, 4, self->m_indices, 6, self->m_material);
	//--

	//-- Draw RPM Needle
	transMat.Identity();
	transMat.Translate(-6, -12, 0);
	
	float rotateFactor = (self->m_rpm - 1000) / 7000.f;
	float rotateAmount = ((PI*1.5f) * rotateFactor) + (PI*0.25f);
	SKMath::SKMatrix<float> rotMat;
	rotMat.Rotate(0, 0, rotateAmount);

	transMat *= rotMat;
	rotMat.Identity(); rotMat.Scale(scale, scale, 0);
	transMat *= rotMat;

	xPos = Game->VideoSettings.DisplayWidth - (int)(106*scale + 0.5f);
	yPos = Game->VideoSettings.DisplayHeight - (int)(155*scale + 0.5f);
	transMat.Translate((float)xPos,(float)yPos,0);
	renderDevice->SetWorldTransform(&transMat);
	self->m_material->DiffuseTexture = self->m_needle;
	renderDevice->Render(VID_UU, self->m_needleQuad, 4, self->m_indices, 6, self->m_material);
	//--

	//-- Draw speed and gear
	xPos = Game->VideoSettings.DisplayWidth - (int)(210*scale + 0.5f);
	yPos = Game->VideoSettings.DisplayHeight - (int)(49*scale + 0.5f);
	renderDevice->DrawString(Game->MonospaceFont, xPos, yPos, SK_TEXT_CENTER | SK_TEXT_VCENTER, 0,0,0,255, "%.0f", self->m_speed);

	xPos = Game->VideoSettings.DisplayWidth - (int)(106*scale + 0.5f);
	yPos = Game->VideoSettings.DisplayHeight - (int)(106*scale + 0.5f);
	if(self->m_rpm > 7500)
		renderDevice->DrawString(Game->BoldFont, xPos, yPos, SK_TEXT_CENTER | SK_TEXT_VCENTER, 255,0,0,255, self->m_gear);
	else
		renderDevice->DrawString(Game->BoldFont, xPos, yPos, SK_TEXT_CENTER | SK_TEXT_VCENTER, 0,0,0,255, self->m_gear);
	//--
	
	//-- Draw current lap time
	xPos = Game->VideoSettings.DisplayWidth / 2;
	yPos = (int)(12 * scale + 0.5f);
	if(self->GameData->CurrentLapTime > 0)
	{
		int iTime = (int)self->GameData->CurrentLapTime;
		int mins = iTime / 60;
		int secs = iTime % 60;
		int msecs = (int)((self->GameData->CurrentLapTime - iTime) * 1000);
		renderDevice->DrawString(Game->MonospaceFont, xPos, yPos, SK_TEXT_CENTER, 0,0,0,255, "%02d:%02d:%03d", mins, secs, msecs);
	}
	else
		renderDevice->DrawString(Game->MonospaceFont, xPos, yPos, SK_TEXT_CENTER, 0,0,0,255, "--:--:---");
	//--

	//-- Previous lap time
	yPos = (int)(16 * scale + 0.5f);
	xPos = Game->VideoSettings.DisplayWidth - yPos;
	if(self->GameData->PreviousLapTime > 0)
	{
		int iTime = (int)self->GameData->PreviousLapTime;
		int mins = iTime / 60;
		int secs = iTime % 60;
		int msecs = (int)((self->GameData->PreviousLapTime - iTime) * 1000);
		renderDevice->DrawString(Game->MonospaceSmallFont, xPos, yPos, SK_TEXT_RIGHT, 0,0,0,255, "%02d:%02d:%03d", mins, secs, msecs);
	}
	else
		renderDevice->DrawString(Game->MonospaceSmallFont, xPos, yPos, SK_TEXT_RIGHT, 0,0,0,255, "--:--:---");

	xPos = Game->VideoSettings.DisplayWidth - (int)(116*scale + 0.5f);
	renderDevice->DrawString(Game->StandardFont, xPos, yPos, SK_TEXT_RIGHT, 0,0,0,255, "Previous Lap");
	//--

	//-- Draw Fastest Lap
	yPos = (int)(40 * scale + 0.5f);
	xPos = Game->VideoSettings.DisplayWidth - (int)(16 * scale + 0.5f);
	if(self->GameData->FastestLapTime > 0)
	{
		int iTime = (int)self->GameData->FastestLapTime;
		int mins = iTime / 60;
		int secs = iTime % 60;
		int msecs = (int)((self->GameData->FastestLapTime - iTime) * 1000);
		renderDevice->DrawString(Game->MonospaceSmallFont, xPos, yPos, SK_TEXT_RIGHT, 0,128,0,255, "%02d:%02d:%03d", mins, secs, msecs);
	}
	else
		renderDevice->DrawString(Game->MonospaceSmallFont, xPos, yPos, SK_TEXT_RIGHT, 0,128,0,255, "--:--:---");

	xPos = Game->VideoSettings.DisplayWidth - (int)(116*scale + 0.5f);
	renderDevice->DrawString(Game->StandardFont, xPos, yPos, SK_TEXT_RIGHT, 0,128,0,255, "Fastest Lap");
	//--
}

void GameLogic::wheelImpulseCallback(LPVOID sender, SKRigidBody::ImpulseEventArgs* args, LPVOID user_data)
{
	GameLogic* self = reinterpret_cast<GameLogic*>(user_data);
	SKRigidBody* wheel = reinterpret_cast<SKRigidBody*>(sender);
	
	SKRigidBody* car = self->GameData->GetVehicleBody(VS_ELISE_BODY);
	SKMath::SKQuaternion<real> ori = ~car->GetOrientation();
	real forceDir = (real)(self->GameData->GetVehicleBody(VS_WHEEL_FL) == wheel ? -1 : 1);

	SKMath::SKVector<real> force = ori.Rotate(args->Force);
	self->m_ffb += force.z*(real)0.1*forceDir;

	force = ori.Rotate(args->Torque);
	self->m_ffb += force.y;
}

void GameLogic::contactCallback(LPVOID sender, SKRigidBody::ContactEventArgs* args, LPVOID user_data)
{
	GameLogic* self = reinterpret_cast<GameLogic*>(user_data);
	int wheelID = -1;
	for(int i=0; i<4; i++)
	{
		if(self->GameData->GetVehicleBody(i) == sender)
			wheelID = i;
	}

	if(wheelID != -1 && args->MaterialID == MAT_ROAD)
	{
		SKMath::SKVector<real> vel = args->ContactVelocity;
		vel.x = 0;
		self->m_wheelContactVel[wheelID] += vel.GetLength();
	}
}

void GameLogic::physicsUpdated(LPVOID sender, SKPhysicsWorld::UpdateEventArgs* e, LPVOID data)
{
	GameLogic* self = reinterpret_cast<GameLogic*>(data);
	RPMDemo* Game = RPMDemo::GetInstance();

	if(self->GameData->TimeLap)
		self->m_keyFrameTimer += e->TimeStep;

	if(self->m_keyFrameTimer > 0.03)
	{
		self->m_keyFrameTimer = 0;

		ReplayCaptureFrame curFrame;
		const SKRigidBody::State* state;
	
		//-- Get Wheel States
		for(int i=0; i<4; i++)
		{
			state = self->GameData->GetVehicleBody(i)->GetState();
			curFrame.WheelPos[i].Set((float)state->Position.x, (float)state->Position.y, (float)state->Position.z);
			curFrame.WheelRot[i].x = (float)state->Orientation.x;
			curFrame.WheelRot[i].y = (float)state->Orientation.y;
			curFrame.WheelRot[i].z = (float)state->Orientation.z;
			curFrame.WheelRot[i].w = (float)state->Orientation.w;
		}
		//-- Get Body State
		state = self->GameData->GetVehicleBody(VS_ELISE_BODY)->GetState();
		SKMath::SKVector<real> carPos = state->Position - self->GameData->GetVehicleBody(VS_ELISE_BODY)->GetCOM();
		curFrame.CarPos.Set((float)carPos.x, (float)carPos.y, (float)carPos.z);
		curFrame.CarRot.x = (float)state->Orientation.x;
		curFrame.CarRot.y = (float)state->Orientation.y;
		curFrame.CarRot.z = (float)state->Orientation.z;
		curFrame.CarRot.w = (float)state->Orientation.w;
		//--

		curFrame.Time = (e->PhysicsWorldTime - self->GameData->LapStartTime);
		self->GameData->ReplayBuffer.Add(curFrame);
	}

	//-- Calc average FFB
	if(Game->Joystick)
	{
		for(int i=0; i<23; i++)
			self->m_ffbAvgs[i] = self->m_ffbAvgs[i+1];
		self->m_ffbAvgs[23] = (float)self->m_ffb;

		float feedback = 0;
		for(int i=0; i<24; i++)
			feedback += self->m_ffbAvgs[i];
		feedback *= 0.04166667f;

		feedback *= 500; //Multiplyer controls strength of FFB
		feedback = max(-10000, min(10000, feedback));
		Game->Joystick->SetForceFeedback((int)(feedback));
		self->m_ffb = 0;
	}
	//--
}