#ifdef BACON
#include "StaticGame.h"

SKRenderer::VertexCacheManager*		Game::VertexManager = NULL;
SKRenderer::SKRenderDevice*			Game::Renderer = NULL;
SKPhysics::SKPhysicsWorld*			Game::PhysicsWorld = NULL;
SKInput::SKInputDevice*				Game::Keyboard = NULL;
SKInput::SKInputDevice*				Game::Joystick = NULL;
SKInput::SKInputDeviceMaster*		Game::InputDevices = NULL;
TopState*							Game::Top = NULL;
Font*								Game::TitleFont = NULL;
Font*								Game::StandardFont = NULL;
ModelManager*						Game::ModelMan = NULL;
DArray<ButtonMapData>				Game::StoredButtonMaps(6, 1);
SKRenderer::RenderSettings			Game::VideoSettings;
POINT								Game::BorderSize;
HWND								Game::Window = NULL;
HANDLE								Game::RendererLock = NULL;
HANDLE								Game::BufferLock = NULL;
volatile int						Game::NextRenderBufferID = 1;
volatile int						Game::RenderBufferID = 1;
volatile int						Game::WorkingBufferID = 0;
int									Game::NumRendererThreads = 0;
int									Game::NumPhysicsThreads = 0;
HMODULE								Game::DataLibDll = NULL;
DataLib::IDataManager*				Game::DataMan = NULL;
#endif