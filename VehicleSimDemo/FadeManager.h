#pragma once

#include <SKDefines.h>
#include <SKEngine.h>
#include <SKRenderer.h>

enum FadeState
{
	FADING_IN,
	FADE_OFF,
	FADING_OUT,
	FADE_ON
};

class FadeEventArgs
{
public:
	FadeEventArgs(FadeState state) : CurrentState(state) {}
	const FadeState CurrentState;
};

class FadeManager
{
public:
	~FadeManager();
	static FadeManager* Instance();
	static void DestroyInstance()
	{
		if(m_inst)
		{
			delete m_inst;
			m_inst = NULL;
		}
	}

	void	Update	(float timeStep);
	void	Draw	();
	bool	FadeOut	(SKColour fadeCol, float duration);
	bool	FadeIn	(float duration);

	void	ForceAlpha(SKColour col, float alpha);
	
	FadeState GetState() { return m_currentState; }

	void	AddStateChangedHandler( void (*func)(void* sender, FadeEventArgs* args, void* user_data), void* user_data = NULL);
	void	RebuildVerts();

private:
	FadeManager();
	void	setQuadColour();
	static FadeManager* m_inst;

	SKColour	m_fadeCol;
	float		m_fadeDuration;
	float		m_timer;
	float		m_fadeAlpha;
	FadeState	m_currentState;
	UINT		m_colour;
	
	SKEngine::SKEvent<FadeEventArgs>	m_stateChangedEvent;
};