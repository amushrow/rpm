#pragma once
#include <string>
#include <DArray.h>
#include <Windows.h>

typedef void (*MenuItemCallback)(UINT ItemID, DWORD Input, void* data);
class MenuItem
{
public:
	MenuItem(UINT ID, MenuItemCallback Callback, void* data = NULL)
	{
		m_id = ID;
		m_callback = Callback;
		m_data = data;
	}
	~MenuItem() {}

	void DoCallback(DWORD Input)
	{
		m_callback(m_id, Input, m_data);
	}

	UINT GetID() { return m_id; }

private:
	MenuItemCallback m_callback;
	void* m_data;
	UINT m_id;
};

class MenuController
{
public:
	MenuController(MenuController* parentMenu = NULL);
	~MenuController();

	void AddItem(UINT ID, MenuItemCallback Callback, void* data = NULL);

	void HandleInput();
	void BlockInput(bool block) { m_blockInput = block; }

	UINT			GetHotItem() { return m_menuItems[m_selectedItem]->GetID(); }
	MenuController*	GetParentMenu() { return m_parent; }

private:
	DArray<MenuItem*>	m_menuItems;
	UINT				m_selectedItem;
	MenuController*		m_parent;
	bool				m_blockInput;
};