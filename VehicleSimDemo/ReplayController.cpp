#include "ReplayController.h"

ReplayController::ReplayController()
{
	m_time = 0;
	m_lastFrame = 1;
}

void ReplayController::SetFrames(const DArray<ReplayCaptureFrame> &inFrames)
{
	m_lastFrame = 1;
	m_time = 0;
	m_frames = inFrames;
}

void ReplayController::Update(double timeStep)
{
	m_time += timeStep;

	unsigned int currentFrame = 0;
	unsigned int maxFrame = m_frames.Length();
	if(maxFrame > 0)
	{
		bool foundFrame = false;
		for(unsigned int i=m_lastFrame; i<maxFrame; i++)
		{
			if(m_frames[i].Time >= m_time)
			{
				foundFrame = true;
				currentFrame = i;

				//This way by the time we get to frame 60789 we don't need to go through the first 60788 frames to get here
				m_lastFrame = i; 

				break;
			}
		}

		//-- Interpolate to get the current positions
		if(foundFrame)
		{
			double span = m_frames[currentFrame].Time - m_frames[currentFrame-1].Time;
			double curSpan = m_time - m_frames[currentFrame-1].Time;
		
			float weight = (float)(curSpan / span);
			m_currentState.CarPos.Interpolate( m_frames[currentFrame-1].CarPos, m_frames[currentFrame].CarPos, weight );
			m_currentState.CarRot.Interpolate( m_frames[currentFrame-1].CarRot, m_frames[currentFrame].CarRot, weight );
			for(int i=0; i<4; i++)
			{
				m_currentState.WheelPos[i].Interpolate( m_frames[currentFrame-1].WheelPos[i], m_frames[currentFrame].WheelPos[i], weight );
				m_currentState.WheelRot[i].Interpolate( m_frames[currentFrame-1].WheelRot[i], m_frames[currentFrame].WheelRot[i], weight );
			}
		}
		//-- Or just use the last frame
		else
		{
			m_lastFrame = maxFrame - 1;
			currentFrame = maxFrame - 1;
			m_currentState.CarPos = m_frames[currentFrame].CarPos;
			m_currentState.CarRot = m_frames[currentFrame].CarRot;
			for(int i=0; i<4; i++)
			{
				m_currentState.WheelPos[i] = m_frames[currentFrame].WheelPos[i];
				m_currentState.WheelRot[i] = m_frames[currentFrame].WheelRot[i];
			}
		}
		//--
	}
}

void ReplayController::GetCurrentFrame(ReplayOutputFrame* out_frame)
{
	if(out_frame != NULL)
	{
		m_currentState.CarRot.GetMatrix(&out_frame->CarMat);
		out_frame->CarMat._row4 = m_currentState.CarPos;
		for(int i=0; i<4; i++)
		{
			m_currentState.WheelRot[i].GetMatrix(&out_frame->WheelMatrix[i]);
			out_frame->WheelMatrix[i]._row4 = m_currentState.WheelPos[i];
		}
	}
}