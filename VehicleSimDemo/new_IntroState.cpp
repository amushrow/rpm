#include "new_States.h"
#include "new_RPMDemo.h"
using namespace TopStates;

IntroState::IntroState()
{
	m_material = NULL;
	m_logoTexture = NULL;
}

IntroState::~IntroState()
{

}

void IntroState::Enter()
{
	m_resourcesLoaded = false;
	m_screen = new Screen(IntroState::render, 0, this);
	m_subState = IS_FADE_IN;
	m_material = new SKRenderer::Material("LogoMaterial");
	m_material->Ambient = 1.f;
	m_material->Diffuse = 1.f;
	m_material->Specular = 1.f;
	m_material->Emissive = 0.f;
	m_material->SpecularExponent = 1.f;

	RPMDemo* Game = RPMDemo::GetInstance();
	Game->Invoke(loadResources, this);

	POINT Res;
	Game->Renderer->GetResolution(&Res);
	m_material->DiffuseTexture = m_logoTexture;
	
	float height = (float)Res.y;
	float scale = height / 720.f;
	float width = 608.f * scale;
	height = 380.f * scale;
	float y = (Res.y - height) * 0.5f;
	float x = (Res.x - width) * 0.5f;

	m_verts[0].x = x + 0.5f;			m_verts[0].y = y + 0.5f;			m_verts[0].z = 0;	m_verts[0].tu = 0;	m_verts[0].tv = 0;
	m_verts[1].x = x + width + 0.5f;	m_verts[1].y = y + 0.5f;			m_verts[1].z = 0;	m_verts[1].tu = 1;	m_verts[1].tv = 0;
	m_verts[2].x = x + width + 0.5f;	m_verts[2].y = y + height + 0.5f;	m_verts[2].z = 0;	m_verts[2].tu = 1;	m_verts[2].tv = 0.625f;
	m_verts[3].x = x + 0.5f;			m_verts[3].y = y + height + 0.5f;	m_verts[3].z = 0;	m_verts[3].tu = 0;	m_verts[3].tv = 0.625f;
	for(int i=0; i<4; i++)
		m_verts[i].Colour = 0xFF000000;

	m_indices[0] = 0; m_indices[1] = 1; m_indices[2] = 2;
	m_indices[3] = 2; m_indices[4] = 3; m_indices[5] = 0;

	m_timer = 0.f;
}

//-- Warning: Runs In Renderer Thread
void IntroState::loadResources(LPVOID Args)
{
	IntroState* self = reinterpret_cast<IntroState*>(Args);
	RPMDemo* Game = RPMDemo::GetInstance();
	if(Game && Game->IsInitialised())
	{
		Game->Renderer->CreateTexture("Logo.tga", &self->m_logoTexture);
		Game->AddScreen(self->m_screen);
	}
	self->m_resourcesLoaded = true;
}

void IntroState::unloadResources(LPVOID Args)
{
	RPMDemo* Game = RPMDemo::GetInstance();
	Texture* tex = reinterpret_cast<Texture*>(Args);
	if(tex)
		Game->Renderer->DestroyTexture(&tex);
}
//--

void IntroState::Leave()
{
	RPMDemo* Game = RPMDemo::GetInstance();
	Game->RemoveScreen(m_screen);
	Game->Invoke(unloadResources, m_logoTexture);
	m_logoTexture = NULL;
	if(m_material)
	{
		delete m_material;
		m_material = NULL;
	}
}

void IntroState::Run()
{
	RPMDemo* Game = RPMDemo::GetInstance();
	if(m_resourcesLoaded)
		m_timer += Game->TimeStep;

	switch(m_subState)
	{
	case IS_FADE_IN:
		{
			//1.5 second fade in
			float brightness = (float)(m_timer / 1.5);
			brightness = min(1.f, brightness);
			int col = (int)(255*brightness);
			int vertCol = 0xFF000000;
			vertCol |= col; col = col << 8;
			vertCol |= col; col = col << 8;
			vertCol |= col;

			for(int i=0; i<4; i++)
				m_verts[i].Colour = vertCol;
	
			if(m_timer > 1.5f)
			{
				m_subState = IS_WAIT;
				m_timer = 0.f;
			}
		}
		break;

	case IS_WAIT:
		//2 second wait
		if(m_timer > 2.f)
		{
			m_subState = IS_FADE_OUT;
			m_timer = 0.f;
		}
		break;

	case IS_FADE_OUT:
		{
			//1.5 second fade out
			float brightness = (float)(1.f - (m_timer / 1.5f));
			brightness = max(0.f, brightness);
			
			int col = (int)(255*brightness);
			int vertCol = 0xFF000000;
			vertCol |= col; col = col << 8;
			vertCol |= col; col = col << 8;
			vertCol |= col;

			for(int i=0; i<4; i++)
				m_verts[i].Colour = vertCol;
		
			if(m_timer > 1.5f)
			{
				m_subState = IS_LEAVE;
				m_timer = 0.f;
			}
		}
		break;

	case IS_LEAVE:
		//Small pause
		if(m_timer > 0.75f)
			this->GetParent()->FireEvent("IntroCompleted");
		break;
	}
}

void IntroState::render(SKRenderDevice* renderDevice, void* user_data)
{
	//Render the logo
	IntroState* self = static_cast<IntroState*>(user_data);
	renderDevice->EnableLighting(false);
	renderDevice->SetMode(EMD_TWOD, 1);
	renderDevice->Render(VID_UL, &self->m_verts[0], 4, &self->m_indices[0], 6, self->m_material);
}