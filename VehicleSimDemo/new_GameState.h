#pragma once
#include "SKRenderDevice.h"
#include "SKPhysics.h"
#include "CustomDataLib.h"
#include "EngineSoundController.h"
#include "ReplayController.h"
#include "new_FiniteStateMachine.h"

class Screen;
class Camera;
class CamTarget;

enum VehicleScenes
{
	VS_WHEEL_FL = 0,
	VS_WHEEL_FR,
	VS_WHEEL_RL,
	VS_WHEEL_RR,
	VS_ELISE_BODY,
	VS_ELISE_SHADOW,
	VS_ELISE_SHELL,
	VS_END
};

enum PhysicsMaterials
{
	MAT_DEFAULT = 0,
	MAT_ROAD = 1,
	MAT_GRASS = 2,
	MAT_TYRE = 3
};

struct TyreData
{
	const SKMath::CubicSpline<real>* LonAccCurve;
	const SKMath::CubicSpline<real>* LonDecCurve;
	const SKMath::CubicSpline<real>* LoadSensitivity;
	const SKMath::CubicSpline<real>* SlipVelocity;
	const SKMath::CubicSpline<real>* SlipAngle;
	real WheelRadius;
	real MaxFriction;
};

namespace TopStates
{
	//Parent Game State
	class GameState : public FiniteStateMachine::Machine
	{
	public:
		GameState();
		~GameState() {}

		int						GetControllerIndex()	{ return m_controllerIndex; }
		SKPhysics::SKVehicle*	GetVehicle()			{ return m_vehicle; }
		SKRenderer::SceneList	GetTrackScenes()		{ return m_culledTrack; }
		SKMath::SKPlane<real>	GetFinishPlane()		{ return m_finishLine; }
		SKMath::SKVector<real>	GetFinishPos()			{ return m_finishPos; }
		SKMath::SKVector<real>	GetLapDir()				{ return m_lapDir; }
		SKRenderer::SKScene*	GetVehicleScene(int Part)		{ return m_vehicleScene[Part]; }
		SKRenderer::SKScene*	GetGhostVehicleScene(int Part)	{ return m_ghostVehicleScene[Part]; }
		SKPhysics::SKRigidBody*	GetVehicleBody(int Part)		{ return m_vehicleRigidBody[Part]; }

		void					FreezeFrame(bool freeze, DWORD pp = 0);
		void					SetCameraPos(int index);
		void					ClearGraphicsResources();
		void					InitialiseGraphicsResources();
		void					Restart();
		void					RenderScene(bool val);

		SKAudioClipInstance*			TyreSkidSound[4];
		EngineSounds*					EngineSound;
		SKRenderer::SKScene*			SkyBox;
		Camera*							GameCamera;
		CamTarget*						GameCamTarget;

		SKRenderer::Effect*				DepthRender;
		SKRenderer::Effect*				EdgeDetect;
		SKRenderer::Effect*				ToonBlinnPhong;
		SKRenderer::Effect*				PerlinNoise;
		SKRenderer::Effect*				ShadowMap;
		SKRenderer::Effect*				GaussianBlur;
		SKRenderer::Effect*				HighPassFilter;
		SKRenderer::Effect*				BloomCombine;
		SKRenderer::Effect*				Desaturate;
		SKRenderer::EffectUserParams	DepthRenderParams;
		SKRenderer::EffectUserParams	EdgeDetectrParams;
		SKRenderer::EffectUserParams	ToonPhongParams;
		SKRenderer::EffectUserParams	PerlinParams;
		SKRenderer::EffectUserParams	ShadowParams;
		SKRenderer::EffectUserParams	BlurParams;
		SKRenderer::EffectUserParams	HighPassParams;
		SKRenderer::EffectUserParams	BloomParams;
		SKRenderer::EffectUserParams	DesaturateParams;

		SKRenderer::RenderTarget*		NormalRenderTarget;
		SKRenderer::RenderTarget*		ShadowRenderTarget;
		SKRenderer::DepthStencil*		ShadowDepthBuffer;
		SKRenderer::DepthStencil*		DepthBuffer;
			
		SKRenderer::Texture*			LineDrawingRenderTarget;
		SKRenderer::Texture*			Ping;
		SKRenderer::Texture*			Pong;
		SKRenderer::Texture*			ShadowMapHi;
		SKRenderer::Texture*			ShadowMapMed;
		SKRenderer::Texture*			ShadowMapLo;
		SKRenderer::Texture*			NormalTex;

		//-- Used by GameLogic
		double					CurrentLapTime;
		double					LapStartTime;
		int						PosClassify;
		int						LapCounter;
		double					FastestLapTime;
		double					PreviousLapTime;
		bool					TimeLap;

		DArray<ReplayCaptureFrame>		ReplayBuffer;
		DArray<ReplayCaptureFrame>		FastestReplay;
		ReplayController				GhostCarController;
		//--

	protected:
		virtual void Enter();
		virtual void Run();
		virtual void Leave();

	private:
		static void renderScene(SKRenderer::SKRenderDevice* renderDevice, void* user_data);
		static void loadResources(LPVOID Args);
		static void unloadResources(LPVOID Args);
		static real tyreFriction(SKPhysics::FrictionFuncData& data, void* user_data);
		void loadPhysics();
		void loadGraphics();
		void loadModel(LPCSTR FileName, LPCSTR InternalName, SKRenderer::SKScene** out_scene);
		void setupWheel(VehicleScenes WheelID, const DataLibExporter::WheelData& WheelData, const SKMath::SKVector<real>& bodyCOM, SKPhysics::WheelFlags Flags);
		void renderScene(SKRenderer::SKScene* scene, SKRenderer::SKRenderDevice* device);


		DArray<TyreData>				m_tyreData;
		Vertex							m_fullscreenQuad[4];
		static WORD						m_fullscreenIndices[6];
		SKMath::SKVector<float>			m_camOffsets[3];
		SKMath::SKVector<float>			m_camLookOffsets[2];
		SKMath::SKPlane<real>			m_finishLine;
		SKMath::SKVector<real>			m_finishPos;
		SKMath::SKVector<real>			m_lapDir;
		SKMath::SKVector<real>			m_vehStartPos;
		SKMath::SKQuaternion<real>		m_vehStartDir;
		DArray<SKPhysics::SKRigidBody*> m_trackMesh;
		SKRenderer::SceneList			m_culledTrack;
		double							m_clippingTimer;
		SKRenderer::Material*			m_material;	
		SKRenderer::SKScene*			m_vehicleScene[VS_END];
		SKRenderer::SKScene*			m_ghostVehicleScene[VS_END];
		SKRenderer::SKScene*			m_trackScene;
		SKPhysics::SKRigidBody*			m_vehicleRigidBody[5];
		SKRenderer::SKSceneGraph*		m_sceneGraph;
		Screen*							m_scene;
		SKPhysics::SKVehicle*			m_vehicle;
		SKAudioClip*					m_tyreSkid;
		LPVOID							m_globalAlpha;
		LPVOID							m_lgtCamPos;
		LPVOID							m_lgtLightViewProjHi;
		LPVOID							m_lgtLightViewProjMed;
		LPVOID							m_lgtLightViewProjLo;
		LPVOID							m_lgtTexShadowHi;
		LPVOID							m_lgtTexShadowMed;
		LPVOID							m_lgtTexShadowLo;
		LPVOID							m_edgScreenResolution;
		LPVOID							m_edgNormalsTex;
		DWORD							m_postProcessFlags;
		int								m_controllerIndex;
		bool							m_doLowShadow;
		bool							m_doMedShadow;
		bool							m_usePrerendered;
		bool							m_createPrerendered;
		bool							m_initialised;
		bool							m_active;
		bool							m_renderLines;
		bool							m_renderCar;
		bool							m_enterSubState;
		bool							m_renderScene;
	};
}