#pragma once
#include <SKMath.h>
#include <DArray.h>

struct ReplayCaptureFrame
{
	double						Time;
	SKMath::SKVector<float>		CarPos;
	SKMath::SKQuaternion<float>	CarRot;
	SKMath::SKVector<float>		WheelPos[4];
	SKMath::SKQuaternion<float>	WheelRot[4];
};

struct ReplayOutputFrame
{
	SKMath::SKMatrix<float> CarMat;
	SKMath::SKMatrix<float> WheelMatrix[4];
};

class ReplayController
{
public:
	ReplayController();

	void	SetFrames(const DArray<ReplayCaptureFrame> &inFrames);
	void	SetTime(double time) {  m_time = 0; m_lastFrame = 1; }
	void	Update(double timeStep);

	void	GetCurrentFrame(ReplayOutputFrame* out_frame);

private:
	DArray<ReplayCaptureFrame>	m_frames;
	ReplayCaptureFrame			m_currentState;
	double						m_time;
	unsigned int				m_lastFrame;
};