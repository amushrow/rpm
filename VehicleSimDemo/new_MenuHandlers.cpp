#include "GameButtons.h"
#include "new_Menu.h"
#include "new_RPMDemo.h"
#include "new_MenuHandlers.h"
#include "new_States.h"
#include "new_GameState.h"

using namespace TopStates;
using namespace GameStates;
using namespace GameStates::GameSubStates;

static char* mainMenuText[]		= { "Resume", "Restart", "Display", "Keyboard", "Joypad", "Quit" };
static char* displayMenuText[]	= { "Resolution", "Windowed", "VSync", "Shadow Resolution", "Filtering Quality", "Anti-Aliasing", "Back", "Apply" };
static char* shadowRes[]		= { "Low", "Medium", "High"};
static char* filterQ[]			= { "None", "Low", "High"};
static char* MSList[]			= { "None", "MSAA 2X", "MSAA 4X", "MSAA 8X", "MSAA 16X", "CSAA 8X", "CSAA 8XQ", "CSAA 16X", "CSAA 16XQ" };
static char* keybdText[]		= { "Accelerate", "Brake", "Turn Left", "Turn Right", "Gear Up", "Gear Down", "Back", "Action", "Pause", "Camera", "Back" };
static char* joyText[]			= { "Throttle Sensitivity", "Brake Sensitivity", "Steering Sensitivity" };
static char* notSet				= "Not Set";
static char* pressKey			= "--Press Key--";
static DWORD mbl[]				= {GB_THROTTLE, GB_BRAKE, GB_STEER_LEFT, GB_STEER_RIGHT, GB_GEARUP, GB_GEARDOWN, GB_BACK, GB_ACTION, GB_PAUSE, GB_CAM};


void GamePause::mainMenuCallback(UINT ID, DWORD Input, void* data)
{
	GamePause* self = reinterpret_cast<GamePause*>(data);
	if(Input == GB_PAUSE || Input == GB_BACK)
	{
		self->GetParent()->FireEvent("ResumeGame");
	}
	else if(Input == GB_ACTION)
	{
		switch(ID)
		{
		case MI_RESUME:
			self->GetParent()->FireEvent("ResumeGame");
			break;

		case MI_RESTART:
			self->GameData->Restart();
			self->GetParent()->FireEvent("ResumeGame");
			break;

		case MI_DISPLAY:
			self->m_activeMenu = self->m_displayMenu;
			break;

		case MI_KEYBOARD:
			self->m_activeMenu = self->m_keyboardMenu;
			break;

		case MI_JOY:
			self->m_activeMenu = self->m_joyMenu;
			break;

		case MI_QUIT:
			RPMDemo::GetInstance()->StopGame();
			break;
		}
	}
}

void GamePause::drawMainMenu(SKRenderDevice* renderDevice)
{
	RPMDemo* Game = RPMDemo::GetInstance();
	float scale = Game->VideoSettings.DisplayHeight / 720.f;
	int itemHeight = (int)(38 * scale + 0.5f);
	int yPos = (Game->VideoSettings.DisplayHeight/2) - (itemHeight*3);
	int xPos = Game->VideoSettings.DisplayWidth/2;
	UINT hotItem = m_activeMenu->GetHotItem();

	for(UINT i=0; i<6; i++)
	{
		if(i == MI_JOY && m_controllers.Length() == 0)
			continue;

		if(hotItem == i)
			renderDevice->DrawString(Game->BoldFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_CENTER, 255,255,255,255, mainMenuText[i]);
		else
			renderDevice->DrawString(Game->TitleFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_CENTER, 200,200,200,255, mainMenuText[i]);

		yPos += itemHeight;
	}
}

void GamePause::displayMenuCallback(UINT ID, DWORD Input, void* data)
{
	RPMDemo* Game = RPMDemo::GetInstance();
	GamePause* self = reinterpret_cast<GamePause*>(data);
	if(Input == GB_BACK)
	{
		self->m_activeMenu = self->m_mainMenu;
	}
	else
	{
		switch(ID)
		{
		case MI_RESOLUTION:
			if(Input == GB_MENU_LEFT)
			{
				self->m_selectedResolution--;
				if(self->m_selectedResolution < 0)
					self->m_selectedResolution = self->m_resolutions.Length()-1;
			}
			else if(Input == GB_MENU_RIGHT)
			{
				self->m_selectedResolution++;
				if(self->m_selectedResolution >= (int)self->m_resolutions.Length())
					self->m_selectedResolution = 0;
			}
			break;

		case MI_WINDOWED:
			if(Input == GB_MENU_LEFT || Input == GB_MENU_RIGHT || Input == GB_ACTION)
			{
				self->m_windowed = !self->m_windowed;
			}
			break;

		case MI_VSYNC:
			if(Input == GB_MENU_LEFT || Input == GB_MENU_RIGHT || Input == GB_ACTION)
			{
				self->m_vsync = !self->m_vsync;
			}
			break;

		case MI_TEXRES:
			if(Input == GB_MENU_LEFT)
			{
				if(self->m_shadowLevel > 0)
					self->m_shadowLevel--;
			}
			else if(Input == GB_MENU_RIGHT)
			{
				if(self->m_shadowLevel < 2)
					self->m_shadowLevel++;
			}
			break;

		case MI_FILTERING:
			if(Input == GB_MENU_LEFT)
			{
				if(self->m_filterQuality > 0)
					self->m_filterQuality--;
			}
			else if(Input == GB_MENU_RIGHT)
			{
				if(self->m_filterQuality < 2)
					self->m_filterQuality++;
			}
			break;

		case MI_AA:
			if(Input == GB_MENU_LEFT)
			{
				if(self->m_msType > MS_NONE)
				{
					int len = self->m_availableAA.Length();
					for(int i=0; i<len; i++)
					{
						if(self->m_availableAA[i] == self->m_msType)
						{
							self->m_msType = self->m_availableAA[i-1];
							break;
						}
					}
				}
			}
			else if(Input == GB_MENU_RIGHT)
			{
				int len = self->m_availableAA.Length();
				if(self->m_msType < self->m_availableAA[len-1])
				{
					for(int i=0; i<len; i++)
					{
						if(self->m_availableAA[i] == self->m_msType)
						{
							self->m_msType = self->m_availableAA[i+1];
							break;
						}
					}
				}
			}
			break;

		case MI_BACK:
			self->m_activeMenu = self->m_mainMenu;
			break;

		case MI_APPLY:
			{
				Resolution& res = self->m_resolutions[self->m_selectedResolution];
				Game->VideoSettings.DisplayHeight = res.Height;
				Game->VideoSettings.DisplayWidth = res.Width;
				Game->VideoSettings.RefreshRate = res.Frequency;
				Game->VideoSettings.FSAAQuality = self->m_msType;
				Game->VideoSettings.EnableVSync = self->m_vsync;
				Game->VideoSettings.Windowed = self->m_windowed;
				Game->ShadowResolutionLevel = self->m_shadowLevel;
				Game->FilteringQuality = self->m_filterQuality;
				Game->DisplaySettingsChanged();
			}
			break;
		}
	}
}

void GamePause::drawDisplayMenu(SKRenderDevice* renderDevice)
{
	RPMDemo* Game = RPMDemo::GetInstance();
	float scale = Game->VideoSettings.DisplayHeight / 720.f;
	int itemHeight = (int)(38 * scale + 0.5f);
	int yPos = (Game->VideoSettings.DisplayHeight/2) - (itemHeight*3);
	int xPos = (Game->VideoSettings.DisplayWidth/2) - (int)(15*scale + 0.5f);
	UINT hotItem = m_activeMenu->GetHotItem();

	for(UINT i=0; i<6; i++)
	{
		renderDevice->DrawString(Game->TitleFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_RIGHT, 200,200,200,255, displayMenuText[i]);
		yPos += itemHeight;
	}

	xPos += (int)(30*scale + 0.5f);
	yPos = (Game->VideoSettings.DisplayHeight/2) - (itemHeight*3);

	Resolution& res = m_resolutions[m_selectedResolution];
	if(hotItem == MI_RESOLUTION)
		renderDevice->DrawString(Game->BoldFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_LEFT, 255,255,255,255, "%dx%d", res.Width, res.Height);
	else
		renderDevice->DrawString(Game->TitleFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_LEFT, 200,200,200,255, "%dx%d", res.Width, res.Height);

	yPos += itemHeight;
	if(hotItem == MI_WINDOWED)
		renderDevice->DrawString(Game->BoldFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_LEFT, 255,255,255,255, m_windowed ? "Yes" : "No");
	else
		renderDevice->DrawString(Game->TitleFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_LEFT, 200,200,200,255, m_windowed ? "Yes" : "No");

	yPos += itemHeight;
	if(hotItem == MI_VSYNC)
		renderDevice->DrawString(Game->BoldFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_LEFT, 255,255,255,255, m_vsync ? "Enabled" : "Disabled");
	else
		renderDevice->DrawString(Game->TitleFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_LEFT, 200,200,200,255, m_vsync ? "Enabled" : "Disabled");

	yPos += itemHeight;
	if(hotItem == MI_TEXRES)
		renderDevice->DrawString(Game->BoldFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_LEFT, 255,255,255,255, shadowRes[m_shadowLevel]);
	else
		renderDevice->DrawString(Game->TitleFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_LEFT, 200,200,200,255, shadowRes[m_shadowLevel]);

	yPos += itemHeight;
	if(hotItem == MI_FILTERING)
		renderDevice->DrawString(Game->BoldFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_LEFT, 255,255,255,255, filterQ[m_filterQuality]);
	else
		renderDevice->DrawString(Game->TitleFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_LEFT, 200,200,200,255, filterQ[m_filterQuality]);

	yPos += itemHeight;
	if(hotItem == MI_AA)
		renderDevice->DrawString(Game->BoldFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_LEFT, 255,255,255,255, MSList[m_msType]);
	else
		renderDevice->DrawString(Game->TitleFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_LEFT, 200,200,200,255, MSList[m_msType]);

	yPos += 2*itemHeight;
	xPos = Game->VideoSettings.DisplayWidth/2;
	if(hotItem == MI_APPLY)
		renderDevice->DrawString(Game->BoldFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_CENTER, 255,255,255,255, displayMenuText[7]);
	else
		renderDevice->DrawString(Game->TitleFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_CENTER, 200,200,200,255, displayMenuText[7]);

	yPos += itemHeight;
	if(hotItem == MI_BACK)
		renderDevice->DrawString(Game->BoldFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_CENTER, 255,255,255,255, displayMenuText[6]);
	else
		renderDevice->DrawString(Game->TitleFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_CENTER, 200,200,200,255, displayMenuText[6]);
}

void GamePause::keyboardMenuCallback(UINT ID, DWORD Input, void* data)
{
	GamePause* self = reinterpret_cast<GamePause*>(data);
	RPMDemo* Game = RPMDemo::GetInstance();
	if(Input == GB_BACK)
	{
		self->m_activeMenu = self->m_mainMenu;
	}
	else if(Input == GB_ACTION)
	{
		switch(ID)
		{
		case MI_BACK:
			self->m_activeMenu = self->m_mainMenu;
			break;

		case MI_BTN_ACC:
		case MI_BTN_BRAKE:
		case MI_BTN_TLEFT:
		case MI_BTN_TRIGHT:
		case MI_BTN_GUP:
		case MI_BTN_GDOWN:
		case MI_BTN_BACK:
		case MI_BTN_ACTION:
		case MI_BTN_PAUSE:
		case MI_BTN_CAM:
			if(self->m_editButton == 0xFFFFFFFF)
			{
				self->m_activeMenu->BlockInput(true);
				self->m_editButton = mbl[ID-MI_BTN_ACC];
				Game->Keyboard->UnassignKey(Game->Keyboard->GetKeyAssignments(self->m_editButton)[0], self->m_editButton);
				Game->Keyboard->AddKeyPressedEvent(keyDown, self);
			}
			break;
		}
	}
}

void GamePause::drawkeyboardMenu(SKRenderDevice* renderDevice)
{
	RPMDemo* Game = RPMDemo::GetInstance();
	float scale = Game->VideoSettings.DisplayHeight / 720.f;
	int itemHeight = (int)(38 * scale + 0.5f);
	int yPos = (Game->VideoSettings.DisplayHeight/2) - (itemHeight*5);
	int xPos = (Game->VideoSettings.DisplayWidth/2) - (int)(15*scale + 0.5f);
	UINT hotItem = m_activeMenu->GetHotItem();

	int valXpos = xPos + (int)(30*scale + 0.5f);
	DArray<SKInput::Key> keys;
	char* keyVal;
	
	for(int i=0; i<10; i++)
	{
		keys = Game->Keyboard->GetKeyAssignments(mbl[i]);
		if(m_editButton == mbl[i]) {
			keyVal = pressKey;
		} else if(keys.Length() > 0) {
			keyVal = SKInput::GetButtonName((SKInput::Button)keys[0]);
		} else {
			keyVal = notSet;
		}

		if(hotItem == MI_BTN_ACC+i) {
			renderDevice->DrawString(Game->BoldFont, valXpos, yPos, SK_TEXT_VCENTER, 255,255,255,255, keyVal);
		} else {
			renderDevice->DrawString(Game->TitleFont, valXpos, yPos, SK_TEXT_VCENTER, 200,200,200,255, keyVal);
		}

		renderDevice->DrawString(Game->TitleFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_RIGHT, 200,200,200,255, keybdText[i]);
		yPos += itemHeight;
	}


	yPos += itemHeight;
	xPos = Game->VideoSettings.DisplayWidth/2;
	if(hotItem == MI_BACK)
		renderDevice->DrawString(Game->BoldFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_CENTER, 255,255,255,255, keybdText[10]);
	else
		renderDevice->DrawString(Game->TitleFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_CENTER, 200,200,200,255, keybdText[10]);
}

void GamePause::joyMenuCallback(UINT ID, DWORD Input, void* data)
{
	GamePause* self = reinterpret_cast<GamePause*>(data);
	RPMDemo* Game = RPMDemo::GetInstance();
	if(Input == GB_BACK)
	{
		self->m_activeMenu = self->m_mainMenu;
	}
	else 
	{
		switch(ID)
		{
		case MI_BACK:
			if(Input == GB_ACTION)
				self->m_activeMenu = self->m_mainMenu;
			break;

		case MI_BTN_ACC:
		case MI_BTN_BRAKE:
		case MI_BTN_TLEFT:
		case MI_BTN_TRIGHT:
		case MI_BTN_GUP:
		case MI_BTN_GDOWN:
		case MI_BTN_BACK:
		case MI_BTN_ACTION:
		case MI_BTN_PAUSE:
		case MI_BTN_CAM:
			if(Input == GB_ACTION && self->m_editButton == 0xFFFFFFFF)
			{
				for(int i=0; i<8; i++)
					self->m_analogueMove[i] = 0.f;
				self->m_activeMenu->BlockInput(true);
				self->m_editButton = mbl[ID-MI_BTN_ACC];
				DArray<Key> keys = Game->Joystick->GetKeyAssignments(self->m_editButton);
				if(keys.Length() > 0)
					Game->Joystick->UnassignKey(keys[0], self->m_editButton);
				Game->Joystick->AddKeyPressedEvent(joyDown, self);
			}
			break;

		case MI_CONTROLLER:
			{
				int len = self->m_controllers.Length();
				if(len > 1)
				{
					int index = 0;
					for(int i=0; i<len; i++)
					{
						if(self->m_controllers[i].ID == Game->Joystick->GetDeviceID())
						{
							index = i;
							break;
						}
					}
					if(Input == GB_MENU_LEFT)
					{
						if(index > 0)
							index--;
						else
							index = len-1;
					}
					else if(Input == GB_MENU_RIGHT)
					{
						if(index < len-1)
							index++;
						else
							index = 0;
					}

					delete Game->Joystick;
					Game->Joystick = Game->InputDevices->CreateJoystick(self->m_controllers[index]);
					Game->Joystick->Acquire();
					Game->LoadJoypadConfig();
				}
			}
			break;

		case MI_BRAKESENS:
			if(Input == GB_MENU_RIGHT)
			{
				float val = Game->StoredButtonMaps[Game->JoypadMapIndex].BrakeSens;
				val += 0.025f;
				if(val > 1) val = 1.f;
				Game->StoredButtonMaps[Game->JoypadMapIndex].BrakeSens = val;
				self->GameData->GetVehicle()->SetBrakeSensitivity(val);
			}
			else if(Input == GB_MENU_LEFT)
			{
				float val = Game->StoredButtonMaps[Game->JoypadMapIndex].BrakeSens;
				val -= 0.025f;
				if(val < 0) val = 0.f;
				Game->StoredButtonMaps[Game->JoypadMapIndex].BrakeSens = val;
				self->GameData->GetVehicle()->SetBrakeSensitivity(val);
			}
			break;

		case MI_THROTTLESENS:
			if(Input == GB_MENU_RIGHT)
			{
				float val = Game->StoredButtonMaps[Game->JoypadMapIndex].ThrottleSens;
				val += 0.025f;
				if(val > 1) val = 1.f;
				Game->StoredButtonMaps[Game->JoypadMapIndex].ThrottleSens = val;
				self->GameData->GetVehicle()->SetThrottleSensitivity(val);
			}
			else if(Input == GB_MENU_LEFT)
			{
				float val = Game->StoredButtonMaps[Game->JoypadMapIndex].ThrottleSens;
				val -= 0.025f;
				if(val < 0) val = 0.f;
				Game->StoredButtonMaps[Game->JoypadMapIndex].ThrottleSens = val;
				self->GameData->GetVehicle()->SetThrottleSensitivity(val);
			}
			break;

		case MI_STEERSENS:
			if(Input == GB_MENU_RIGHT)
			{
				float val = Game->StoredButtonMaps[Game->JoypadMapIndex].SteerSens;
				val += 0.025f;
				if(val > 1) val = 1.f;
				Game->StoredButtonMaps[Game->JoypadMapIndex].SteerSens = val;
				self->GameData->GetVehicle()->SetSteerSensitivity(val);
			}
			else if(Input == GB_MENU_LEFT)
			{
				float val = Game->StoredButtonMaps[Game->JoypadMapIndex].SteerSens;
				val -= 0.025f;
				if(val < 0) val = 0.f;
				Game->StoredButtonMaps[Game->JoypadMapIndex].SteerSens = val;
				self->GameData->GetVehicle()->SetSteerSensitivity(val);
			}
			break;
		}
	}

}

void GamePause::drawJoyMenu(SKRenderDevice* renderDevice)
{
	RPMDemo* Game = RPMDemo::GetInstance();
	float scale = Game->VideoSettings.DisplayHeight / 720.f;
	int itemHeight = (int)(38 * scale + 0.5f);
	int yPos = (Game->VideoSettings.DisplayHeight/2) - (itemHeight*7);
	int xPos = (Game->VideoSettings.DisplayWidth/2) - (int)(15*scale + 0.5f);
	UINT hotItem = m_activeMenu->GetHotItem();

	int valXpos = xPos + (int)(30*scale + 0.5f);

	renderDevice->DrawString(Game->TitleFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_RIGHT, 200,200,200,255, "Input Device");
	if(hotItem == MI_CONTROLLER) {
		renderDevice->DrawString(Game->BoldFont, valXpos, yPos, SK_TEXT_VCENTER, 255,255,255,255, Game->Joystick->GetDeviceName());
	} else {
		renderDevice->DrawString(Game->TitleFont, valXpos, yPos, SK_TEXT_VCENTER, 200,200,200,255, Game->Joystick->GetDeviceName());
	}
	yPos += itemHeight;

	DArray<SKInput::Key> keys;
	ButtonMapData& data = Game->StoredButtonMaps[Game->JoypadMapIndex];
	char* keyVal;

	//-- Throttle
	if(m_editButton == GB_THROTTLE) {
		keyVal = pressKey;
	} else if(data.Throttle != 0xFFFFFFFF) {
		keyVal = SKInput::GetButtonName((SKInput::Button)data.Throttle);
	} else if(data.CombinedThrottleBrake != 0xFFFFFFFF) {
		keyVal = SKInput::GetButtonName((SKInput::Button)data.CombinedThrottleBrake);
	} else
		keyVal = notSet;

	if(hotItem == MI_BTN_ACC) {
		renderDevice->DrawString(Game->BoldFont, valXpos, yPos, SK_TEXT_VCENTER, 255,255,255,255, keyVal);
	} else {
		renderDevice->DrawString(Game->TitleFont, valXpos, yPos, SK_TEXT_VCENTER, 200,200,200,255, keyVal);
	}

	renderDevice->DrawString(Game->TitleFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_RIGHT, 200,200,200,255, keybdText[0]);
	yPos += itemHeight;
	//--

	//-- Brake
	if(m_editButton == GB_BRAKE) {
		keyVal = pressKey;
	} else if(data.Brake != 0xFFFFFFFF) {
		keyVal = SKInput::GetButtonName((SKInput::Button)data.Brake);
	} else if(data.CombinedThrottleBrake != 0xFFFFFFFF) {
		keyVal = SKInput::GetButtonName((SKInput::Button)data.CombinedThrottleBrake);
	} else
		keyVal = notSet;

	if(hotItem == MI_BTN_BRAKE) {
		renderDevice->DrawString(Game->BoldFont, valXpos, yPos, SK_TEXT_VCENTER, 255,255,255,255, keyVal);
	} else {
		renderDevice->DrawString(Game->TitleFont, valXpos, yPos, SK_TEXT_VCENTER, 200,200,200,255, keyVal);
	}

	renderDevice->DrawString(Game->TitleFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_RIGHT, 200,200,200,255, keybdText[1]);
	yPos += itemHeight;
	//--

	//-- Left
	if(m_editButton == GB_STEER_LEFT) {
		keyVal = pressKey;
	} else if(data.SteerLeft != 0xFFFFFFFF) {
		keyVal = SKInput::GetButtonName((SKInput::Button)data.SteerLeft);
	} else if(data.Steering != 0xFFFFFFFF) {
		keyVal = SKInput::GetButtonName((SKInput::Button)data.Steering);
	} else
		keyVal = notSet;

	if(hotItem == MI_BTN_TLEFT) {
		renderDevice->DrawString(Game->BoldFont, valXpos, yPos, SK_TEXT_VCENTER, 255,255,255,255, keyVal);
	} else {
		renderDevice->DrawString(Game->TitleFont, valXpos, yPos, SK_TEXT_VCENTER, 200,200,200,255, keyVal);
	}

	renderDevice->DrawString(Game->TitleFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_RIGHT, 200,200,200,255, keybdText[2]);
	yPos += itemHeight;
	//--

	//-- Right
	if(m_editButton == GB_STEER_RIGHT) {
		keyVal = pressKey;
	} else if(data.SteerRight != 0xFFFFFFFF) {
		keyVal = SKInput::GetButtonName((SKInput::Button)data.SteerRight);
	} else if(data.Steering != 0xFFFFFFFF) {
		keyVal = SKInput::GetButtonName((SKInput::Button)data.Steering);
	} else
		keyVal = notSet;

	if(hotItem == MI_BTN_TRIGHT) {
		renderDevice->DrawString(Game->BoldFont, valXpos, yPos, SK_TEXT_VCENTER, 255,255,255,255, keyVal);
	} else {
		renderDevice->DrawString(Game->TitleFont, valXpos, yPos, SK_TEXT_VCENTER, 200,200,200,255, keyVal);
	}

	renderDevice->DrawString(Game->TitleFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_RIGHT, 200,200,200,255, keybdText[3]);
	yPos += itemHeight;
	//--

	for(int i=4; i<10; i++)
	{
		keys = Game->Joystick->GetKeyAssignments(mbl[i]);
		if(m_editButton == mbl[i]) {
			keyVal = pressKey;
		} else if(keys.Length() > 0) {
			keyVal = SKInput::GetButtonName((SKInput::Button)keys[0]);
		} else {
			keyVal = notSet;
		}

		if(hotItem == MI_BTN_ACC+i) {
			renderDevice->DrawString(Game->BoldFont, valXpos, yPos, SK_TEXT_VCENTER, 255,255,255,255, keyVal);
		} else {
			renderDevice->DrawString(Game->TitleFont, valXpos, yPos, SK_TEXT_VCENTER, 200,200,200,255, keyVal);
		}

		renderDevice->DrawString(Game->TitleFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_RIGHT, 200,200,200,255, keybdText[i]);
		yPos += itemHeight;
	}

	if(hotItem == MI_THROTTLESENS) {
		renderDevice->DrawString(Game->BoldFont, valXpos, yPos, SK_TEXT_VCENTER, 255,255,255,255, "%.4f", data.ThrottleSens);
	} else {
		renderDevice->DrawString(Game->TitleFont, valXpos, yPos, SK_TEXT_VCENTER, 200,200,200,255, "%.4f", data.ThrottleSens);
	}
	renderDevice->DrawString(Game->TitleFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_RIGHT, 200,200,200,255, joyText[0]);
	yPos += itemHeight;

	if(hotItem == MI_BRAKESENS) {
		renderDevice->DrawString(Game->BoldFont, valXpos, yPos, SK_TEXT_VCENTER, 255,255,255,255, "%.4f", data.BrakeSens);
	} else {
		renderDevice->DrawString(Game->TitleFont, valXpos, yPos, SK_TEXT_VCENTER, 200,200,200,255, "%.4f", data.BrakeSens);
	}
	renderDevice->DrawString(Game->TitleFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_RIGHT, 200,200,200,255, joyText[1]);
	yPos += itemHeight;

	if(hotItem == MI_STEERSENS) {
		renderDevice->DrawString(Game->BoldFont, valXpos, yPos, SK_TEXT_VCENTER, 255,255,255,255, "%.4f", data.SteerSens);
	} else {
		renderDevice->DrawString(Game->TitleFont, valXpos, yPos, SK_TEXT_VCENTER, 200,200,200,255, "%.4f", data.SteerSens);
	}
	renderDevice->DrawString(Game->TitleFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_RIGHT, 200,200,200,255, joyText[2]);
	yPos += itemHeight;


	yPos += itemHeight;
	xPos = Game->VideoSettings.DisplayWidth/2;
	if(hotItem == MI_BACK)
		renderDevice->DrawString(Game->BoldFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_CENTER, 255,255,255,255, keybdText[10]);
	else
		renderDevice->DrawString(Game->TitleFont, xPos, yPos, SK_TEXT_VCENTER | SK_TEXT_CENTER, 200,200,200,255, keybdText[10]);
};
