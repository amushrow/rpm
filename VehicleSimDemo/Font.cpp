#include <iostream>
#include <fstream>
#include "new_RPMDemo.h"
#include "Font.h"

Font::Font(SKRenderer::Texture* tex, SKRenderer::Material* mat, const char* data) : m_verts(64, 64), m_indices(128, 64)
{
	m_fontTexture = tex;
	m_material = mat;

	m_fullSize = (float)m_fontTexture->GetWidth();
	m_charSize = m_fullSize / 16.f;

	for(int i=0; i<256; i++)
	{
		short* sval = (short*)&data[i*2];
		float fval = (float)*sval;
		m_charWidth[i] = fval;
		m_uvWidth[i] = fval / m_fullSize;
	}
}

HRESULT Font::CreateFont(std::string name, Font** out_font)
{
	*out_font = NULL;

	std::string fileName = name;
	fileName.append(".dds");
	std::string datName = name;
	datName.append(".dat");

	RPMDemo* game = RPMDemo::GetInstance();
	if(!game || !game->IsInitialised())
		return SK_FAIL;

	Texture* fontTexture;
	Material* material;
	if(FAILED(game->Renderer->CreateTexture(fileName, &fontTexture)))
		return SK_FAIL;
		
	material = new Material("FontMaterial");
	material->UserData = NULL;
	material->Ambient.Set(1,1,1);
	material->Diffuse.Set(1,1,1);
	material->Specular.Set(0,0,0);
	material->Emissive.Set(0,0,0);
	material->SpecularExponent = 0.f;
	material->DiffuseTexture = fontTexture;

	std::ifstream charData;
	charData.open(datName.c_str(), std::ios::in | std::ios::binary);
	char buff[512];
	if(charData.is_open())
	{
		charData.read(buff, 512);
		charData.close();
	}
	else if(DataLib::IFile* file = game->DataMan->GetFiles().Find(datName))
	{
		DataLib::IStream* stream = game->DataMan->GetFileStream(file->GetUID());
		int len = (int)file->GetOriginalLength();
		stream->Read((BYTE*)&buff[0], 0, len);
		stream->Release();
	}
	else
	{
		game->Renderer->DestroyTexture(&fontTexture);
		delete material;

		return SK_FAIL;
	}

	*out_font = new Font(fontTexture, material, buff);

	return SK_OK;
}

Font::~Font()
{
	RPMDemo* game = RPMDemo::GetInstance();
	if(game && game->IsInitialised())
		game->Renderer->DestroyTexture(&m_fontTexture);
	
	delete m_material;
}

void Font::Draw(SKRenderer::SKRenderDevice* device, const char* text, float size, float x, float y, TextAlign alignment, SKColour col)
{
	Draw(device, text, size, x, y, alignment, col, col);
}

void Font::Draw(SKRenderer::SKRenderDevice* device, const char* text, float size, float x, float y, TextAlign alignment, SKColour col1, SKColour col2)
{
	if(size < EPSILON)
		return;

	UINT topCol = col1.GetUintCol();
	UINT bottomCol = col2.GetUintCol();

	int vertIndex = 0;
	int indIndex = 0;;
	int str_length = strlen(text);
	float scale = size / m_charSize;
	bool texelAdjust = false;

	float tmpScale = scale;
	float mipWidth = m_fullSize;
	float invScale = 1.f/scale;
	float int_invScale;
	
	if(modf(invScale, &int_invScale) != 0 || ((int)int_invScale & ((int)int_invScale-1)) != 0)
	{
		while(tmpScale < 1.f)
		{
			mipWidth *= 0.5f;
			tmpScale *= 2.f;
		}
		mipWidth *= 1.5f;
		texelAdjust = true;
	}
	
	int numVerts = str_length * 4;
	int numIndices = str_length * 6;
	m_verts.Reserve(numVerts);
	m_indices.Reserve(numIndices);
	
	//size = floor(size + 0.5f);
	x = floor(x + 0.5f);
	y = floor(y + 0.5f);

	x -= 0.5f;
	y -= 0.5f;
	float xPos = x;
	float yPos = y;
	for(int i=0; i<str_length; i++)
	{
		BYTE c = (BYTE)text[i];

		if(c == 10)
		{
			m_lineWidths.Add(xPos - x);
			m_lineEndVert.Add( m_verts.Length() );
			xPos = x;
			yPos += size;//floor(size + 0.5f);
		}
		if(c < 32)
			continue;

		float texel = 0.f;
		float pixel = 0.f;
		if(texelAdjust)
		{
			texel = 1.f / mipWidth;
			pixel = size / mipWidth;
		}
		float uvSize = 1.f / 16.f;
		float u = (float)(c % 16) * uvSize;
		float v = (float)(c / 16) * uvSize;

		float width = (m_charWidth[c] * scale);
		//width = ceil( width );

		unsigned int currentIndex = vertIndex;
		LVertex& vert = m_verts[vertIndex];
		vert.x = xPos;
		vert.y = yPos + pixel;
		vert.z = 0.f;
		vert.tu = u;
		vert.tv = v + texel;
		vert.Colour = topCol;

		LVertex& vert2 = m_verts[vertIndex+1];
		vert2.x = xPos + width;
		vert2.y = yPos + pixel;
		vert2.z = 0.f;
		vert2.tu = u + m_uvWidth[c];
		vert2.tv = v + texel;
		vert2.Colour = topCol;

		LVertex& vert3 = m_verts[vertIndex+2];
		vert3.x = xPos + width;
		vert3.y = yPos + size -pixel;
		vert3.z = 0.f;
		vert3.tu = u + m_uvWidth[c];
		vert3.tv = v + uvSize - texel;
		vert3.Colour = bottomCol;
		
		LVertex& vert4 = m_verts[vertIndex+3];
		vert4.x = xPos;
		vert4.y = yPos + size - pixel;	
		vert4.z = 0.f;
		vert4.tu = u;
		vert4.tv = v + uvSize - texel;
		vert4.Colour = bottomCol;
		
		m_indices[indIndex] = currentIndex;
		m_indices[indIndex+1] = currentIndex+1;
		m_indices[indIndex+2] = currentIndex+2;
		m_indices[indIndex+3] = currentIndex+2;
		m_indices[indIndex+4] = currentIndex+3;
		m_indices[indIndex+5] = currentIndex;

		vertIndex += 4;
		indIndex += 6;

		m_verts.SetLength(vertIndex);
		m_indices.SetLength(indIndex);

		xPos += width;
	}

	m_lineWidths.Add(xPos - x);
	m_lineEndVert.Add( m_verts.Length() );

	unsigned numLines = m_lineEndVert.Length();
	int curVertIndex = 0;
	if(alignment == TOP_MIDDLE)
	{
		for(unsigned i=0; i<numLines; i++)
		{
			int endVert = m_lineEndVert[i];
			float shift = m_lineWidths[i] * 0.5f;
			for(; curVertIndex < endVert; curVertIndex++)
			{
				m_verts[curVertIndex].x -= shift;
			}
		}
	}
	else if(alignment == TOP_RIGHT)
	{
		for(unsigned i=0; i<numLines; i++)
		{
			int endVert = m_lineEndVert[i];
			float shift = m_lineWidths[i];
			for(; curVertIndex < endVert; curVertIndex++)
			{
				m_verts[curVertIndex].x -= shift;
			}
		}
	}
	else if(alignment == MIDDLE)
	{
		float y_shift = (size * numLines) * 0.5f;
		for(unsigned i=0; i<numLines; i++)
		{
			int endVert = m_lineEndVert[i];
			float x_shift = m_lineWidths[i] * 0.5f;
			for(; curVertIndex < endVert; curVertIndex++)
			{
				m_verts[curVertIndex].x -= x_shift;
				m_verts[curVertIndex].y -= y_shift;
			}
		}
	}

	device->GetVertexCacheManager()->Render(VID_UL, &m_verts[0], m_verts.Length(), &m_indices[0], m_indices.Length(), m_material);

	m_verts.Clear();
	m_indices.Clear();
	m_lineEndVert.Clear();
	m_lineWidths.Clear();
}