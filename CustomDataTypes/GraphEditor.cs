﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CustomDataTypes
{
	public partial class GraphEditor : Form
	{
		private readonly string help_message = 
@"Cubic Polynomial Spline Editor

Controls:
  Ctrl+Click - Place anchor point
  Middle-Click - Hide anchor points

  To Delete and anchor point either right-click it, or select it and then press 'Delete'
  To Move an anchor point either drag it, or select it and then change it's co-ordinated via the edit boxes

**A minimum of 4 points are required to generate the spline**";

		private readonly int MarginSize = 24;
		private readonly int GridSize = 75;
		private CubicSpline m_spline;
		private bool hide = false;
		private Padding m_graphPadding;
		private List<PointF> m_points;
		private float m_minX = 0;
		private float m_maxX = 1;
		private float m_minY = 0;
		private float m_maxY = 1;
		private bool m_draggedPoint = false;
		private Point m_initialMousePos;
		private PointF m_initialGraphPos;
		private int m_hotIndex = -1;
		private int m_selectedIndex = -1;
		private delegate void VoidCall();
		VoidCall RepaintGraph;

		public CubicSpline Spline
		{
			get { return m_spline; }
		}

		public GraphEditor()
		{
			InitializeComponent();
			m_points = new List<PointF>();
			m_spline = new CubicSpline(m_points);
			this.graphPanel.Renderer = new RenderMethod(this.graphPanel_Paint);
			m_graphPadding.Left = MarginSize;
			m_graphPadding.Right = 0;
			m_graphPadding.Top = 0;
			m_graphPadding.Bottom = MarginSize;
			this.graphPanel.Padding = m_graphPadding;
			RepaintGraph = new VoidCall(this.repaintGraph);
		}

		public GraphEditor(CubicSpline data)
		{
			InitializeComponent();
			m_points = new List<PointF>(data.GetPoints());
			m_spline = new CubicSpline(data.GetPoints());
			this.graphPanel.Renderer = new RenderMethod(this.graphPanel_Paint);

			foreach(PointF point in m_points)
			{
				if(point.X  < m_minX)
					m_minX = point.X;
				else if(point.X > m_maxX)
					m_maxX = point.X;

				if(point.Y  < m_minY)
					m_minY = point.Y;
				else if(point.Y > m_maxY)
					m_maxY = point.Y;
			}
			scaleChanged();
			RepaintGraph = new VoidCall(this.repaintGraph);
		}

		private void repaintGraph()
		{
			this.graphPanel.Invalidate();
		}

		private void graphPanel_Paint(PaintEventArgs e)
		{
			e.Graphics.Clear(Color.White);
			PointF origin = new PointF(0, 0);
			origin = fullToClient(origin);
			int o_X = (int)Math.Round(origin.X);
			int o_Y = (int)Math.Round(origin.Y);
			int height = this.graphPanel.ClientRectangle.Height-(m_graphPadding.Size.Height);
			int width = this.graphPanel.ClientRectangle.Width - (m_graphPadding.Size.Width);

			int fullheight = this.graphPanel.ClientRectangle.Height;
			int fullwidth = this.graphPanel.ClientRectangle.Width;

			//Draw helpfull guide lines
			int curX = o_X - GridSize;
			int curY = o_Y - GridSize;
			while (curX > 0) {
				e.Graphics.DrawLine(Pens.LightGray, curX, 0, curX, fullheight);
				curX -= GridSize;
			}
			while (curY > 0)
			{
				e.Graphics.DrawLine(Pens.LightGray, 0, curY, fullwidth, curY);
				curY -= GridSize;
			}
			curX = o_X + GridSize;
			curY = o_Y + GridSize;
			while (curX < fullwidth) {
				e.Graphics.DrawLine(Pens.LightGray, curX, 0, curX, fullheight);
				curX += GridSize;
			}
			while (curY < fullheight) {
				e.Graphics.DrawLine(Pens.LightGray, 0, curY, fullwidth, curY);
				curY += GridSize;
			}

			//Draw Graph
			if (m_points.Count >= 4)
			{
				e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
				const float step = 2;
				float xScale = width / (m_maxX - m_minX);
				float scaledStep = step / xScale;
				float X = m_minX - ((MarginSize+step) / xScale);
				PointF posA = new PointF();
				PointF posB = new PointF();
				PointF posC = new PointF();

				posC.X = X;
				posC.Y = m_spline.Evaluate(X);
				posC = fullToClient(posC);
				posB = posC;

				Pen thickerPen = new Pen(Brushes.Tomato, 2);
				float endPos = m_maxX + ((MarginSize + step) / xScale);
				while (X < endPos)
				{
					posA = posB;
					posB = posC;
					X += scaledStep;
					posC.X = X;
					posC.Y = m_spline.Evaluate(X);
					posC = fullToClient(posC);

					bool a_out = posA.Y < 0 || posA.Y > this.graphPanel.Height;
					bool c_out = posC.Y < 0 || posC.Y > this.graphPanel.Height;
					bool ludicrous = Math.Abs(posA.Y - posC.Y) > Int32.MaxValue;
					bool unnatural = Single.IsNaN(posA.Y) || Single.IsNaN(posC.Y);
					if ((a_out && c_out) || ludicrous || unnatural)
						continue;

					e.Graphics.DrawLine(thickerPen, posA, posC);
				}
				e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.Default;
			}

			//Draw axis
			Pen blackPen = new Pen(Brushes.Gray, 2);
			e.Graphics.DrawLine(blackPen, o_X, 0, o_X, fullheight);
			e.Graphics.DrawLine(blackPen, 0, o_Y, fullwidth, o_Y);

			if (!hide)
			{
				foreach (PointF point in m_points)
				{
					PointF client = fullToClient(point);
					int y = (int)Math.Round(client.Y);
					int x = (int)Math.Round(client.X);
					Rectangle rect = new Rectangle(x - 4, y - 4, 8, 8);
					e.Graphics.FillRectangle(Brushes.RoyalBlue, rect);
					rect.X += 1; rect.Width -= 2;
					rect.Y += 1; rect.Height -= 2;
					e.Graphics.FillRectangle(Brushes.LightSkyBlue, rect);
				}

				if (m_selectedIndex != -1)
				{
					PointF client = fullToClient(m_points[m_selectedIndex]);
					int y = (int)Math.Round(client.Y);
					int x = (int)Math.Round(client.X);
					Rectangle rect = new Rectangle(x - 4, y - 4, 8, 8);
					e.Graphics.FillRectangle(Brushes.ForestGreen, rect);
					rect.X += 1; rect.Width -= 2;
					rect.Y += 1; rect.Height -= 2;
					e.Graphics.FillRectangle(Brushes.LawnGreen, rect);
				}

				if (m_hotIndex != -1)
				{
					Pen colouredPen = Pens.DarkGray;
					PointF client = fullToClient(m_points[m_hotIndex]);
					int y = (int)Math.Round(client.Y);
					int x = (int)Math.Round(client.X);
					Rectangle rect = new Rectangle(x - 5, y - 5, 9, 9);
					e.Graphics.DrawRectangle(colouredPen, rect);
				}
			}
		}

		private PointF fullToClient(PointF fullPoint)
		{
			PointF newPoint = new PointF();
			float width = this.graphPanel.ClientRectangle.Width - (m_graphPadding.Size.Width);
			float height = this.graphPanel.ClientRectangle.Height - (m_graphPadding.Size.Height);
			float realwidth = m_maxX - m_minX;
			float realheight = m_maxY - m_minY;
			float x = fullPoint.X - m_minX;
			float y = fullPoint.Y - m_minY;

			float scaleX = (width / realwidth);
			float scaleY = (height / realheight);

			newPoint.X = (x * scaleX) + m_graphPadding.Left;
			newPoint.Y = this.graphPanel.ClientRectangle.Height - ((y * scaleY) + m_graphPadding.Bottom);

			return newPoint;
		}

		private SizeF pixelToGraph(Size pixels)
		{
			SizeF newSize = new SizeF();
			float width = this.graphPanel.ClientRectangle.Width - (m_graphPadding.Size.Width);
			float height = this.graphPanel.ClientRectangle.Height - (m_graphPadding.Size.Height);
			float realwidth = m_maxX - m_minX;
			float realheight = m_maxY - m_minY;

			float x = pixels.Width;
			float y = pixels.Height;

			float scaleX = (width / realwidth);
			float scaleY = (height / -realheight);

			newSize.Width = (x / scaleX);
			newSize.Height = (y / scaleY);

			return newSize;
		}

		private PointF clientToFull(PointF client)
		{
			return clientToFull(client.X, client.Y);
		}

		private PointF clientToFull(float cX, float cY)
		{
			PointF newPoint = new PointF();
			float width = this.graphPanel.ClientRectangle.Width - (m_graphPadding.Size.Width);
			float height = this.graphPanel.ClientRectangle.Height - (m_graphPadding.Size.Height);
			float realwidth = m_maxX - m_minX;
			float realheight = m_maxY - m_minY;

			float x = cX - m_graphPadding.Left;
			float y = (this.graphPanel.ClientRectangle.Height - cY) - m_graphPadding.Bottom;

			float scaleX = (width / realwidth);
			float scaleY = (height / realheight);

			newPoint.X = (x / scaleX) + m_minX;
			newPoint.Y = (y / scaleY) + m_minY;

			return newPoint;
		}

		private void graphPanel_MouseMove(object sender, MouseEventArgs e)
		{
			if (this.graphPanel.Capture && m_selectedIndex != -1)
			{
				bool copy = false;
				if(!e.Location.Equals(m_initialMousePos))
				{
					PointF allowedPoint = m_initialGraphPos;
					PointF graphPoint = m_initialGraphPos;
					Size pixelDiff = new Size();
					SizeF diff = new SizeF();
					pixelDiff.Width = e.Location.X - m_initialMousePos.X;
					pixelDiff.Height = e.Location.Y - m_initialMousePos.Y;
					graphPoint = m_initialGraphPos + pixelToGraph(pixelDiff);

					//-- Make sure we don't put the point in the same position
					//   as another, it's not healthy
					do
					{
						diff = pixelToGraph(pixelDiff);
						copy = false;
						allowedPoint = m_initialGraphPos + diff;
						for(int i=0; i<m_points.Count; i++)
						{
							if (i == m_selectedIndex)
								continue;

							if (m_points[i].X == allowedPoint.X)
							{
								copy = true;
								pixelDiff.Width++;
							}
						}
					} while (copy);
					//--

					//--Set allowed point
					m_points[m_selectedIndex] = allowedPoint;
					m_spline.SetPoints(m_points);
					//--

					//--Set actual point
					m_points[m_selectedIndex] = graphPoint;
					//--

					this.BeginInvoke(this.RepaintGraph);
					m_draggedPoint = true;
				}
				else
				{
					m_draggedPoint = false;
					m_points[m_selectedIndex] = m_initialGraphPos;
					m_spline.SetPoints(m_points);
					this.BeginInvoke(this.RepaintGraph);
				}

				this.txtXVal.Text = m_points[m_selectedIndex].X.ToString();
				this.txtYVal.Text = m_points[m_selectedIndex].Y.ToString();
			}
			else
			{
				int preindex = m_hotIndex;
				m_hotIndex = -1;
				for (int i = m_points.Count - 1; i >= 0; i--)
				{
					PointF client = fullToClient(m_points[i]);
					RectangleF rect = new RectangleF(client.X - 3, client.Y - 3, 6, 6);
					if (rect.Contains(e.Location))
					{
						m_hotIndex = i;
						break;
					}
				}

				if (preindex != m_hotIndex)
					this.BeginInvoke(this.RepaintGraph);
			}
		}

		private void graphPanel_Click(object sender, MouseEventArgs e)
		{
			if (e.Button == System.Windows.Forms.MouseButtons.Middle)
			{
				hide = !hide;
				this.BeginInvoke(this.RepaintGraph);
			}

			if ((ModifierKeys & Keys.Control) == Keys.Control)
			{
				bool copy = false;
				int xPos = e.Location.X;
				PointF newPoint;

				//-- Make sure we don't put the point in the same position
				//   as another, it's not healthy
				do
				{
					copy = false;
					newPoint = clientToFull(xPos, e.Location.Y);
					for (int i = 0; i < m_points.Count; i++)
					{
						if (i == m_selectedIndex)
							continue;

						if (m_points[i].X == newPoint.X)
						{
							copy = true;
							xPos++;
						}
					}
				} while (copy);

				//--
				m_points.Add(newPoint);
				m_spline.AddPoint(newPoint);
				m_selectedIndex = m_points.Count-1;
				m_initialMousePos = e.Location;
				m_initialGraphPos = newPoint;
				this.txtXVal.Enabled = true;
				this.txtYVal.Enabled = true;
				this.txtXVal.Text = m_points[m_selectedIndex].X.ToString();
				this.txtYVal.Text = m_points[m_selectedIndex].Y.ToString();
				this.BeginInvoke(this.RepaintGraph);
			}
			else if (e.Button == System.Windows.Forms.MouseButtons.Right)
			{
				if (m_hotIndex != -1)
				{
					m_points.RemoveAt(m_hotIndex);
					m_spline.SetPoints(m_points);
					m_selectedIndex = -1;
					this.txtXVal.Enabled = false;
					this.txtYVal.Enabled = false;
					m_hotIndex = -1;
					this.BeginInvoke(this.RepaintGraph);
				}
			}
			else //Left Click
			{
				if (m_selectedIndex != m_hotIndex)
				{
					m_selectedIndex = m_hotIndex;
					if(m_selectedIndex == -1)
					{
						this.txtXVal.Enabled = false;
						this.txtYVal.Enabled = false;
					}
					else
					{
						this.txtXVal.Enabled = true;
						this.txtYVal.Enabled = true;
						this.txtXVal.Text = m_points[m_selectedIndex].X.ToString();
						this.txtYVal.Text = m_points[m_selectedIndex].Y.ToString();
						m_initialGraphPos = m_points[m_selectedIndex];
					}
					this.BeginInvoke(this.RepaintGraph);
				}

				m_initialMousePos = e.Location;
			}
		}

		private void graphPanel_MouseUp(object sender, MouseEventArgs e)
		{
			if (e.Button == System.Windows.Forms.MouseButtons.Left)
			{
				if (m_draggedPoint)
				{
					m_spline.SetPoints(m_points);
					m_draggedPoint = false;
					m_initialGraphPos = m_points[m_selectedIndex];
					this.BeginInvoke(this.RepaintGraph);
				}
			}
		}

		private void GraphEditor_HelpRequested(object sender, HelpEventArgs hlpevent)
		{
			MessageBox.Show(help_message, "Help");
		}

		private void GraphEditor_HelpButtonClicked(object sender, CancelEventArgs e)
		{
			e.Cancel = true;
			MessageBox.Show(help_message, "Help");
		}

		private void validateNumeric(object sender, KeyEventArgs e)
		{
			//--Only allow numbers and a single decimal point in this text box
			TextBox txt = sender as TextBox;
			if (txt != null)
			{
				//Check for illegal keys
				if ((e.KeyCode >= Keys.A && e.KeyCode <= Keys.Z) ||
					(e.KeyCode >= Keys.Multiply && e.KeyCode <= Keys.Divide) ||
					(e.KeyCode >= Keys.Oem1 && e.KeyCode <= Keys.OemBackslash) ||
					e.KeyCode == Keys.Space)
				{
					e.SuppressKeyPress = true;
				}

				//Allow a decimal point though if we don't have one already
				bool hasDecimal = txt.Text.Contains('.');
				if (e.KeyCode == Keys.OemPeriod || e.KeyCode == Keys.Decimal)
					e.SuppressKeyPress = hasDecimal;

				//Allow negative sign if we havn't got ont already, and we are at position zero
				bool hasNeg = txt.Text.Contains('-');
				if ((e.KeyCode == Keys.OemMinus || e.KeyCode == Keys.Subtract) && txt.SelectionStart == 0)
					e.SuppressKeyPress = hasNeg;

				//If the enter key is pressed, then we will shift focus to cause final validation
				if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
				{
					this.label1.Focus();
					e.SuppressKeyPress = true;
				}
			}
			//--
		}

		private void scaleChanged()
		{
			PointF client = new PointF(0, 0);
			client = fullToClient(client);
			m_graphPadding.All = 0;

			if (client.X <= MarginSize)
				m_graphPadding.Left = MarginSize;
			if(client.X >= this.graphPanel.ClientRectangle.Width-MarginSize)
				m_graphPadding.Right = MarginSize;
			if (client.Y <= MarginSize)
				m_graphPadding.Top = MarginSize;
			if (client.Y >= this.graphPanel.ClientRectangle.Height - MarginSize)
				m_graphPadding.Bottom = MarginSize;

			this.graphPanel.Padding = m_graphPadding;

			this.txtMaxX.Text = m_maxX.ToString();
			this.txtMaxY.Text = m_maxY.ToString();
			this.txtMinX.Text = m_minX.ToString();
			this.txtMinY.Text = m_minY.ToString();
		}

		private enum ViewValue
		{
			MinX, 
			MaxX,
			MinY, 
			MaxY
		}

		private bool validateTextBox(ViewValue thingy, TextBox box)
		{
			bool result = true;
			float val;
			if (Single.TryParse(box.Text, out val))
			{
				switch(thingy)
				{
					case ViewValue.MinX:
						if(val > m_maxX)
						{
							m_minX = m_maxX;
							m_maxX = val;
						}
						else if(val < m_maxX)
							m_minX = val;
						else
							result = false;
						break;

					case ViewValue.MaxX:
						if(val < m_minX)
						{
							m_maxX = m_minX;
							m_minX = val;
						}
						else if(val > m_minX)
							m_maxX = val;
						else
							result = false;
						break;

					case ViewValue.MinY:
						if(val > m_maxY)
						{
							m_minY = m_maxY;
							m_maxY = val;
						}
						else if(val < m_maxY)
							m_minY = val;
						else
							result = false;
						break;

					case ViewValue.MaxY:
						if(val < m_minY)
						{
							m_maxY = m_minY;
							m_minY = val;
						}
						else if(val > m_minY)
							m_maxY = val;
						else
							result = false;
						break;
				}
				scaleChanged();
				this.BeginInvoke(this.RepaintGraph);
			}
			else
			{
				MessageBox.Show("Value is not a number", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				result = false;
			}

			return result;
		}

		private void txtMaxX_Validating(object sender, CancelEventArgs e)
		{
			if (!validateTextBox(ViewValue.MaxX, this.txtMaxX))
				e.Cancel = true;
		}

		private void txtMaxY_Validating(object sender, CancelEventArgs e)
		{
			if (!validateTextBox(ViewValue.MaxY, this.txtMaxY))
				e.Cancel = true;
		}

		private void txtMinX_Validating(object sender, CancelEventArgs e)
		{
			if (!validateTextBox(ViewValue.MinX, this.txtMinX))
				e.Cancel = true;
		}

		private void txtMinY_Validating(object sender, CancelEventArgs e)
		{
			if (!validateTextBox(ViewValue.MinY, this.txtMinY))
				e.Cancel = true;
		}

		private void txtXVal_Validating(object sender, CancelEventArgs e)
		{
			if(m_selectedIndex != -1)
			{
				float val;
				if(Single.TryParse(this.txtXVal.Text, out val))
				{
					for(int i=0; i<m_points.Count; i++)
					{
						if(i == m_selectedIndex)
							continue;

						if(m_points[i].X == val)
						{
							e.Cancel = true;
							MessageBox.Show("Point already occupies that position", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
							break;
						}
					}

					if(!e.Cancel)
					{
						PointF argh = m_points[m_selectedIndex];
						argh.X = val;
						m_points[m_selectedIndex] = argh;
						m_spline.SetPoints(m_points);
						this.BeginInvoke(this.RepaintGraph);
					}
				}
				else
				{
					MessageBox.Show("Value is not a number", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					e.Cancel = true;
				}
			}
		}

		private void txtYVal_Validating(object sender, CancelEventArgs e)
		{
			if(m_selectedIndex != -1)
			{
				float val;
				if(Single.TryParse(this.txtYVal.Text, out val))
				{
					PointF argh = m_points[m_selectedIndex];
					argh.Y = val;
					m_points[m_selectedIndex] = argh;
					m_spline.SetPoints(m_points);
					this.BeginInvoke(this.RepaintGraph);
				}
				else
				{
					MessageBox.Show("Value is not a number", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
					e.Cancel = true;
				}
			}
		}

		private void graphPanel_KeyDown(object sender, KeyEventArgs e)
		{
			if(e.KeyCode == Keys.Delete && m_selectedIndex != -1)
			{
				 m_points.RemoveAt(m_selectedIndex);
				 m_spline.SetPoints(m_points);
				 m_selectedIndex = -1;
				 this.txtXVal.Enabled = false;
				 this.txtYVal.Enabled = false;
				 this.BeginInvoke(this.RepaintGraph);
			}
		}
	}
}
