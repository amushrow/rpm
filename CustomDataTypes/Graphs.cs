﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using DataLibInterfaces;

namespace CustomDataTypes
{
    [DataType("Cubic Spline", "Piecewise cubic spline")]
    [Guid("8B702777-808F-4038-A2BE-8EF715F4E9F3")]
    public class Graph : IDataType
    {
        public Graph()
        {
            m_interalPropDesc = new EditablePropertyDescription(this);
            m_interalPropDesc.Name = "Cubic Spline";
            m_interalPropDesc.EditDialog = true;
            m_interalPropDesc.TextEdit = false;

            m_data = new CubicSpline();
        }

        public int ID { get; set; }
        
        public string Name
        {
            get { return m_interalPropDesc.Name; }
            set { m_interalPropDesc.Name = value; }
        }

        public PropertyDescription Property
        {
            get { return new PropertyDescription(m_interalPropDesc); }
        }

        public string StringValue
        {
            get { return "[Cubic Spline]"; }
        }

        public Guid TypeGUID
        {
            get { return this.GetType().GUID; }
        }

        public string TypeName
        {
            get { return "Cubic Spline"; }
        }

        public IEnumerable<PropertyDescription> GetSubProperties()
        {
            List<PropertyDescription> empty = new List<PropertyDescription>();
            return empty;
        }

        public void ReadStream(Stream s)
        {
            byte[] buffer = new byte[4];
            s.Read(buffer, 0, 4);
            int count = BitConverter.ToInt32(buffer, 0);

            List<PointF> dataPoints = new List<PointF>(count);
            for (int i = 0; i < count; i++)
            {
                s.Read(buffer, 0, 4);
                float x = BitConverter.ToSingle(buffer, 0);
                s.Read(buffer, 0, 4);
                float y = BitConverter.ToSingle(buffer, 0);

                PointF point = new PointF(x, y);
                dataPoints.Add(point);
            }

            m_data.SetPoints(dataPoints);
        }

        public bool SetValue(string val)
        {
            return false;
        }

        public void ShowEditForm()
        {
            GraphEditor gedit = new GraphEditor(m_data);
            System.Windows.Forms.DialogResult dr = gedit.ShowDialog();
            if(dr == System.Windows.Forms.DialogResult.OK)
            {
                m_data.SetPoints( gedit.Spline.GetPoints() );
            }
        }

        public void WriteStream(Stream s)
        {
            List<PointF> dataPoints = m_data.GetPoints();
            byte[] buffer;

            buffer = BitConverter.GetBytes(dataPoints.Count);
            s.Write(buffer, 0, 4);

            foreach (PointF point in dataPoints)
            {
                buffer = BitConverter.GetBytes(point.X);
                s.Write(buffer, 0, 4);

                buffer = BitConverter.GetBytes(point.Y);
                s.Write(buffer, 0, 4);
            }
        }

        private EditablePropertyDescription m_interalPropDesc;
        private CubicSpline m_data;
    }


    public class CubicSpline
    {
		 public CubicSpline()
        {
            m_dataPoints = new List<PointF>();

            m_coefA = null;
            m_coefB = null;
            m_coefC = null;
            m_coefD = null;
        }

        public CubicSpline(IEnumerable<PointF> points)
        {
            m_dataPoints = new List<PointF>(points);

			if(m_dataPoints.Count > 0)
			{
				m_coefA = new float[m_dataPoints.Count-1];
				m_coefB = new float[m_dataPoints.Count-1];
				m_coefC = new float[m_dataPoints.Count-1];
				m_coefD = new float[m_dataPoints.Count-1];
			}

            constructSpline();
        }

        public List<PointF> GetPoints()
        {
            return m_dataPoints;
        }
        
        public void SetPoints(IEnumerable<PointF> points)
        {
            m_dataPoints.Clear();
            m_dataPoints.AddRange(points);

            m_coefA = new float[m_dataPoints.Count];
            m_coefB = new float[m_dataPoints.Count];
            m_coefC = new float[m_dataPoints.Count];
            m_coefD = new float[m_dataPoints.Count];

            constructSpline();
        }

        public void AddPoints(IEnumerable<PointF> points)
        {
            m_dataPoints.AddRange(points);

            m_coefA = new float[m_dataPoints.Count];
            m_coefB = new float[m_dataPoints.Count];
            m_coefC = new float[m_dataPoints.Count];
            m_coefD = new float[m_dataPoints.Count];

            constructSpline();
        }

        public void AddPoint(PointF point)
        {
            m_dataPoints.Add(point);

            m_coefA = new float[m_dataPoints.Count-1];
            m_coefB = new float[m_dataPoints.Count-1];
            m_coefC = new float[m_dataPoints.Count-1];
            m_coefD = new float[m_dataPoints.Count-1];

            constructSpline();
        }

        public float Evaluate(float X)
        {
            if (m_dataPoints.Count < 4)
                return 0.0f;

            int piece = 0;
            float resultY = 0.0f;

            //--Find spline section
            for (int i = m_dataPoints.Count - 2; i >= 0; i--)
            {
                if (X > m_dataPoints[i].X)
                {
                    piece = i;
                    break;
                }
            }
            //--

            //--Find the result
            float tmp = (X - m_dataPoints[piece].X);
            float sq = tmp * tmp;
            float cb = sq * tmp;

            resultY = m_coefA[piece] + (m_coefB[piece] * tmp) + (m_coefC[piece] * sq) + (m_coefD[piece] * cb);
            //--

            return resultY;
        }

        public float Evaluate(float X, int piece)
        {
            if (m_dataPoints.Count < 4)
                return 0.0f;

            float resultY = 0.0f;
            //--Find the result
            float tmp = (X - m_dataPoints[piece].X);
            float sq = tmp * tmp;
            float cb = sq * tmp;

            resultY = m_coefA[piece] + (m_coefB[piece] * tmp) + (m_coefC[piece] * sq) + (m_coefD[piece] * cb);
            //--

            return resultY;
        }

        private void constructSpline()
        {
            if (m_dataPoints.Count < 4)
                return;

            //--Sort data points
            m_dataPoints.Sort(XValSort);
            //--

            int N = m_dataPoints.Count - 1;
            float[] arrayH = new float[N];
            float[] arrayU = new float[N];
            float[] arrayV = new float[N];
            float[] arrayZ = new float[N+1];
            float[] arrayB = new float[N];

            for (int i = 0; i < N; i++)
            {
                arrayH[i] = m_dataPoints[i + 1].X - m_dataPoints[i].X;
                arrayB[i] = (m_dataPoints[i + 1].Y - m_dataPoints[i].Y) / arrayH[i];
            }

            arrayU[1] = 2 * (arrayH[0] + arrayH[1]);
            arrayV[1] = 6 * (arrayB[1] - arrayB[0]);

            for (int i = 2; i < N; i++)
            {
                float hsq = arrayH[i-1] * arrayH[i-1];
                arrayU[i] = (2 * (arrayH[i - 1] + arrayH[i])) - (hsq / arrayU[i - 1]);
                arrayV[i] = (6 * (arrayB[i] - arrayB[i - 1])) - ((arrayH[i - 1] * arrayV[i - 1]) / arrayU[i - 1]);
            }

            arrayZ[N] = 0;
            for (int i = N - 1; i > 0; i--)
            {
                arrayZ[i] = (arrayV[i] - (arrayH[i] * arrayZ[i + 1])) / arrayU[i];
            }
            arrayZ[0] = 0;

            //--Calculate Coeffs
            for (int i = 0; i < N; i++)
            {
                m_coefA[i] = m_dataPoints[i].Y;
                m_coefB[i] = ((m_dataPoints[i + 1].Y - m_dataPoints[i].Y) / arrayH[i]) - ((arrayH[i] / 6) * arrayZ[i + 1]) - ((arrayH[i] / 3) * arrayZ[i]);
                m_coefC[i] = arrayZ[i] / 2;
                m_coefD[i] = (arrayZ[i + 1] - arrayZ[i]) / (arrayH[i] * 6);
            }
            //--

        }

        private List<PointF> m_dataPoints;
        private float[] m_coefA;
        private float[] m_coefB;
        private float[] m_coefC;
        private float[] m_coefD;
        private static Comparison<PointF> XValSort = delegate(PointF A, PointF B)
        {
            return A.X.CompareTo(B.X);
        };

    }
}
