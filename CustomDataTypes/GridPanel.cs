﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace CustomDataTypes
{
    public delegate void RenderMethod(PaintEventArgs e);
    class CustomRenderPanel : UserControl
    {
        public RenderMethod Renderer
        {
            get;
            set;
        }

        public CustomRenderPanel()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.ResizeRedraw | ControlStyles.UserPaint | ControlStyles.OptimizedDoubleBuffer, true);
            this.UpdateStyles();
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // CustomRenderPanel
            // 
            this.DoubleBuffered = true;
            this.Name = "CustomRenderPanel";
            this.ResumeLayout(false);

        }

        protected override void OnPaint(PaintEventArgs e)
        {
            if (Renderer != null)
                Renderer(e);
            else
                base.OnPaint(e);
        }

        protected override void WndProc(ref Message m)
        {
            const int WM_MOUSEMOVE = 0x0200;

            //Prevent mouse from leaving client area
            if (this.Capture && m.Msg == WM_MOUSEMOVE)
            {
                int pos = m.LParam.ToInt32();
                short x = (short)(pos & 0xFFFF);
                short y = (short)(pos >> 16);
                if (x < this.Padding.Left)
                    x = (short)this.Padding.Left;
                if (x > (this.ClientRectangle.Width-this.Padding.Right))
                    x = (short)(this.ClientRectangle.Width - this.Padding.Right);

                if (y < this.Padding.Top)
                    y = (short)this.Padding.Top;
                if (y > (this.ClientRectangle.Height-this.Padding.Bottom))
                    y = (short)(this.ClientRectangle.Height - this.Padding.Bottom);

                pos = y << 16;
                pos |= (ushort)x;

                m.LParam = new IntPtr(pos);
            }
            base.WndProc(ref m);
        }
    }
}
