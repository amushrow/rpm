struct ShadowVertex
{
	float4 Position	: POSITION;
	float4 Position2D : TEXCOORD0;
};

float4x4 LightWorldViewProj : WORLDVIEWPROJECTION;

ShadowVertex Shadow_VS(float4 inPos : POSITION)
{
	ShadowVertex Output;
	Output.Position = mul(inPos, LightWorldViewProj);
	Output.Position2D = Output.Position;

	return Output;
}

float4 Shadow_PS(ShadowVertex PSIn) : COLOR0
{
	float depth = PSIn.Position2D.z / PSIn.Position2D.w;
	float moment1 = depth;
	float moment2 = depth*depth;

	float dx = ddx(depth);
	float dy = ddy(depth);
	moment2 += 0.25*(dx*dx+dy*dy);

	return float4(moment1, moment2, 0, 1);
}

technique ShadowMap
{
	pass CreateShadowMap
	{
		VertexShader = compile vs_3_0 Shadow_VS();
		PixelShader = compile ps_3_0 Shadow_PS();
	}
}