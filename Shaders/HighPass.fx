struct VS_INPUT
{
	float4 position : POSITION;
	float2 texCoord : TEXCOORD;
};
struct VS_OUTPUT
{
	float4 position : POSITION;
	float2 texCoord : TEXCOORD;	
};

float4x4 WorldViewProj	: WORLDVIEWPROJECTION;
Texture texSource : DIFFUSEMAP;
sampler sampSource = sampler_state {
	Texture = <texSource>;
};

static const float3 luminance = { 0.2989f, 0.5866f, 0.1145f };
float threshold = 0.3f;

VS_OUTPUT VS(const VS_INPUT IN)
{
	VS_OUTPUT OUT;
	
	OUT.position = mul(IN.position, WorldViewProj);
	OUT.texCoord = IN.texCoord;

	return OUT;
}

float4 PS(const VS_OUTPUT IN) : COLOR
{
	float normalizationFactor = 1 / (1 - threshold) * 0.4f;

	float3 sample = tex2D(sampSource, IN.texCoord).rgb;
	float greyLevel = saturate(mul(sample, luminance));
	float3 desaturated = lerp(sample, greyLevel.rrr, threshold);
			  
	return float4((desaturated - threshold) * normalizationFactor, 1);
}

technique HighPass
{
	pass Pass0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
}