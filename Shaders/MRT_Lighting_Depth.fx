//-- Combined Lighting and NormalDepth Shaders --//
struct VSInput
{
	float4 Position	: POSITION;
	float3 Normal	: NORMAL;
	float2 TexCoord	: TEXCOORD0;
};

struct VSOutput
{
	float4 Position			: POSITION;
	float2 TexCoord			: TEXCOORD0;
	
	float3 Normal			: TEXCOORD1;
	float3 Eye				: TEXCOORD2;
	float3 Half				: TEXCOORD3;
	float2 Depth			: TEXCOORD4;
	float4 WorldPosition	: TEXCOORD5;
};

struct PSOutput
{
	float4	Colour	: COLOR0;
	float4	NormalD	: COLOR1;
};

//-- Engine Controlled Variables
float4x4 World			: WORLD;
float4x4 WorldViewProj	: WORLDVIEWPROJECTION;
float4 MatAmbient 		: AMBIENT;
float4 MatDiffuse		: DIFFUSE;
float4 MatSpecular		: SPECULAR;
float MatPower			: SPECULARPOWER;
Texture Tex0			: DIFFUSEMAP;
bool UseTexture			: USEDIFFUSEMAP;

sampler ColoredTextureSampler =
sampler_state
{
	texture = <Tex0>;
	AddressU  = WRAP;        
	AddressV  = WRAP;
	MAGFILTER = LINEAR;
	MINFILTER = ANISOTROPIC;
	MIPFILTER = LINEAR;
};
//--

//-- Lighting
float4 CamPos;
float3 LightDir = float3(-0.44721, -0.89442, 0);
float4 LightAmbient = float4(0.3,0.3,0.3,1);
float4 LightColour = float4(0.7, 0.68, 0.63, 1);
float Spec = 0.75;
//--

//--Shadows
Texture TexShadow;
float4x4 LightViewProj;

sampler ShadowTextureSampler =
sampler_state
{
	texture = <TexShadow>;
	AddressU  = CLAMP;        
	AddressV  = CLAMP;
	MAGFILTER = LINEAR;
	MINFILTER = ANISOTROPIC;
	MIPFILTER = LINEAR;
};
//--

//-- Misc
float Thresholds[2] = { 0.8, 0.4 };
float BrightnessLevels[3] = { 1.1,0.8,0.5 };
float BlendFactor = 0.05;
float RenderDepth;
//--

float chebyshevUpperBound(float2 shadowCoord, float depth)
{
	float2 moments = tex2D(ShadowTextureSampler, shadowCoord).rg;
		
	if (depth <= moments.x)
		return 1.0 ;
	
	float variance = moments.y - (moments.x*moments.x);
	variance = max(variance,0.00002);
	
	float d = depth - moments.x;
	float p_max = variance / (variance + d*d);
	
	return p_max;
}	

VSOutput BlinnPhong_VS(VSInput input)
{
	VSOutput output = (VSOutput)0;
	output.Position = mul(input.Position, WorldViewProj);
	output.TexCoord = input.TexCoord;

	output.Normal = normalize( mul(input.Normal, (float3x3)World) );
	float4 WorldPos = mul(input.Position, World);
	output.Eye = normalize(CamPos - WorldPos).xyz;
	output.Half = normalize( -LightDir + output.Eye );
	output.WorldPosition = WorldPos;
	output.Depth = output.Position.zw;

	return output;
}

PSOutput BlinnPhong_PS(VSOutput input)
{
	PSOutput Output;
	input.Normal = normalize(input.Normal);
	input.Half = normalize(input.Half);
	input.Eye = normalize(input.Eye);

	float pixelDepth = input.Depth.x/input.Depth.y;
	Output.NormalD = float4(input.Normal.xy, pixelDepth, 1);


	//Square pixel depth for non-linear blend at the end of the clipping
	// region
	pixelDepth = pixelDepth * pixelDepth;
	if(pixelDepth > RenderDepth)
	{
		Output.Colour = float4(0,0,0,0);
		return Output;
	}

	float lightIntensity = 1;
	float4 shadowPos = mul(input.WorldPosition, LightViewProj);
	shadowPos =  shadowPos / shadowPos.w;
	float shadowDepth = shadowPos.z - 0.001;
	if(shadowPos.x*shadowPos.x < 1 && shadowPos.y*shadowPos.y < 1)
	{
		shadowPos.x = shadowPos.x*0.5 + 0.5;
		shadowPos.y = -shadowPos.y*0.5 + 0.5;

		lightIntensity = chebyshevUpperBound(shadowPos.xy, shadowDepth);
	}

	float4 Diffuse = MatDiffuse;
	if(UseTexture)
	{
		Diffuse = tex2D(ColoredTextureSampler, input.TexCoord);
		Diffuse.a += MatDiffuse.a;
	}
	
	float4 Ia = MatAmbient * Diffuse * LightAmbient;
	Output.Colour = Ia;
	Output.Colour.a = Diffuse.a;

	if(lightIntensity > 0)
	{
		float4 Id = Diffuse * saturate(dot(input.Normal, -LightDir));
		float4 Is = MatSpecular * pow( saturate(dot(input.Normal, input.Half)), MatPower) * Spec;

		float4 Combined = Id+Is;
		float lightLevel = 1;
		float len = Combined.r * 0.3 + Combined.g * 0.59 + Combined.b * 0.11;
		if(len > Thresholds[0])
		{
			lightLevel = BrightnessLevels[0];
		}
		else if(len > Thresholds[1])
		{
			float offset = Thresholds[0]-(Thresholds[0]*BlendFactor);
			float diff = saturate((len-offset) / (Thresholds[0]-offset));
			lightLevel = lerp(BrightnessLevels[1], BrightnessLevels[0], diff);
		}
		else
		{
			float offset = Thresholds[1]-(Thresholds[1]*BlendFactor);
			float diff = saturate((len-offset) / (Thresholds[1]-offset));
			lightLevel = lerp(BrightnessLevels[2], BrightnessLevels[1], diff);
		}
	
		Output.Colour += Combined*lightLevel*LightColour*lightIntensity;
		Output.Colour.a = saturate(Diffuse.a + (Is.a * (Diffuse.a-MatDiffuse.a)));
	}

	float dropOff = 0.025 / RenderDepth;
	float distanceFade = lerp(0, 1, saturate((RenderDepth-pixelDepth)/dropOff));
	Output.Colour.a *= distanceFade;

	return Output;
}

technique RPMLighting
{
	pass Pass0
	{
		VertexShader = compile vs_3_0 BlinnPhong_VS();
		PixelShader = compile ps_3_0 BlinnPhong_PS();
	}
}