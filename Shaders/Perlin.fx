struct VSInput
{
	float4 Position	: POSITION0;
	float2 TexCoord	: TEXCOORD0;
};

struct VSOutput
{
	float4 Position	: POSITION0;
	float2 TexCoord	: TEXCOORD0;
};

float4x4 WorldViewProj	: WORLDVIEWPROJECTION;
float4x4 ViewProjIT;
texture permTexture;
texture gradTexture4d;
float time = 0;
float OverBright = 0;

sampler gradSampler4d = sampler_state 
{
	texture = <gradTexture4d>;
	AddressU  = Wrap;        
	AddressV  = Clamp;
	MAGFILTER = POINT;
	MINFILTER = POINT;
	MIPFILTER = NONE;
};

sampler permSampler = sampler_state 
{
	texture = <permTexture>;
	AddressU  = Wrap;        
	AddressV  = Clamp;
	MAGFILTER = POINT;
	MINFILTER = POINT;
	MIPFILTER = NONE;   
};

float4 fade(float4 t)
{
	return t * t * t * (t * (t * 6 - 15) + 10);
}

float grad(float x, float4 p)
{
	return dot(tex1D(gradSampler4d, x), p);
}

float perm(float x)
{
	return tex1D(permSampler, x).a;
}

float inoise(float4 p)
{
	float4 P = fmod(floor(p), 256.0);	// FIND UNIT HYPERCUBE THAT CONTAINS POINT
	p -= floor(p);                      // FIND RELATIVE X,Y,Z OF POINT IN CUBE.
	float4 f = fade(p);                 // COMPUTE FADE CURVES FOR EACH OF X,Y,Z, W
	P = P / 256.0;
	const float one = 1.0 / 256.0;
	
	// HASH COORDINATES OF THE 16 CORNERS OF THE HYPERCUBE
	float A = perm(P.x) + P.y;
	float AA = perm(A) + P.z;
	float AB = perm(A + one) + P.z;
	float B =  perm(P.x + one) + P.y;
	float BA = perm(B) + P.z;
	float BB = perm(B + one) + P.z;

	float AAA = perm(AA)+P.w, AAB = perm(AA+one)+P.w;
	float ABA = perm(AB)+P.w, ABB = perm(AB+one)+P.w;
	float BAA = perm(BA)+P.w, BAB = perm(BA+one)+P.w;
	float BBA = perm(BB)+P.w, BBB = perm(BB+one)+P.w;

	// INTERPOLATE DOWN
	return lerp(
				lerp( lerp( lerp( grad(perm(AAA), p ),  
								  grad(perm(BAA), p + float4(-1, 0, 0, 0) ), f.x),
							lerp( grad(perm(ABA), p + float4(0, -1, 0, 0) ),
								  grad(perm(BBA), p + float4(-1, -1, 0, 0) ), f.x), f.y),
								  
					  lerp( lerp( grad(perm(AAB), p + float4(0, 0, -1, 0) ),
								  grad(perm(BAB), p + float4(-1, 0, -1, 0) ), f.x),
							lerp( grad(perm(ABB), p + float4(0, -1, -1, 0) ),
								  grad(perm(BBB), p + float4(-1, -1, -1, 0) ), f.x), f.y), f.z),
							
				 lerp( lerp( lerp( grad(perm(AAA+one), p + float4(0, 0, 0, -1)),
								   grad(perm(BAA+one), p + float4(-1, 0, 0, -1) ), f.x),
							 lerp( grad(perm(ABA+one), p + float4(0, -1, 0, -1) ),
								   grad(perm(BBA+one), p + float4(-1, -1, 0, -1) ), f.x), f.y),
								   
					   lerp( lerp( grad(perm(AAB+one), p + float4(0, 0, -1, -1) ),
								   grad(perm(BAB+one), p + float4(-1, 0, -1, -1) ), f.x),
							 lerp( grad(perm(ABB+one), p + float4(0, -1, -1, -1) ),
								   grad(perm(BBB+one), p + float4(-1, -1, -1, -1) ), f.x), f.y), f.z), f.w);
}

float turbulence(float4 p, int octaves, float lacunarity = 2.0, float gain = 0.5)
{
	float sum = 0;
	float freq = 1.0, amp = 0.75;
	for(int i=0; i<octaves; i++) {
		sum += abs(inoise(p*freq))*amp;
		freq *= lacunarity;
		amp *= gain;
	}
	return sum;
}

VSOutput Perlin_VS(VSInput input)
{
	VSOutput output = (VSOutput)0;

	output.Position = mul(input.Position, WorldViewProj);
	output.TexCoord = input.TexCoord;

	return output;
}

static const float PI = 3.14159265;
float4 CamPos = float4(0,0,0,0);
float2 ScreenRes = float2(1280, 720);

float4 Perlin_PS(float2 TexCoord : TEXCOORD0) : COLOR0
{
	TexCoord.x = TexCoord.x*2 -1;
	TexCoord.y = TexCoord.y*2 -1;
	float4 screenPos = float4(TexCoord.x, TexCoord.y, 0, 1);
	screenPos = mul(screenPos, ViewProjIT);
	screenPos /= screenPos.w;
	screenPos -= CamPos;
	screenPos = normalize(screenPos);

	float x = (screenPos.x + 1) * 0.5;
	float z = (screenPos.z + 1) * 0.5;
	float y = (screenPos.y + 1) * 0.5;

	float4 pos = float4(x*10, y*10, z*10, time);
	float4 col = 1;
	float perlin = turbulence(pos, 3, 2, 0.6)*0.5+0.5;
	col.rg = perlin;
		//col = col*(1-OverBright) + OverBright;

	return col;
}

technique PerlinNoise
{
	pass Pass0
	{
		AlphaBlendEnable = false;
		VertexShader = compile vs_3_0 Perlin_VS();
		PixelShader = compile ps_3_0 Perlin_PS();
	}
}