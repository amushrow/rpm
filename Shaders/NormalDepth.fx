//-- Structures
struct VSInput
{
	float4 Position	: POSITION0;
	float3 Normal	: NORMAL0;
	float2 TexCoord	: TEXCOORD0;
};

struct VSOutput
{
	float4 Position	: POSITION0;
	float3 Normal	: TEXCOORD0;
	float2 Depth	: TEXCOORD1;
};

struct PSOutput
{
	float4	Normal	: COLOR0;
};
//--

float4x4 WorldViewProj	:	WORLDVIEWPROJECTION;
float4x4 World			:	WORLD;

VSOutput NormalDepthVertex(VSInput input)
{
	VSOutput output = (VSOutput)0;

	output.Position = mul(input.Position, WorldViewProj);
	
	float3 worldNormal = normalize(mul(input.Normal, (float3x3)World));
	output.Normal = (worldNormal + 1) / 2;
	output.Depth = output.Position.zw;
	
	return output;
}

PSOutput NormalDepthPixel(VSOutput input)
{
	PSOutput output;
	float pixelDepth = input.Depth.x/input.Depth.y;
	output.Normal = float4(input.Normal.xyz, pixelDepth);
	return output;
}

technique NormalDepthRender
{
	pass Pass0
	{
		AlphaBlendEnable = false;
		VertexShader = compile vs_2_0 NormalDepthVertex();
		PixelShader = compile ps_2_0 NormalDepthPixel();
	}
}