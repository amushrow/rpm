struct VSInput
{
	float4 Position	: POSITION;
	float3 Normal	: NORMAL;
	float2 TexCoord	: TEXCOORD0;
};

struct VSOutput
{
	float4 Position			: POSITION;
	float2 TexCoord			: TEXCOORD0;
	
	float3 Normal			: TEXCOORD1;
	float3 Eye				: TEXCOORD2;
	float3 Half				: TEXCOORD3;
	float2 Depth			: TEXCOORD4;
	float4 WorldPosition	: TEXCOORD5;
};

//-- Engine Controlled Variables
float4x4 World			: WORLD;
float4x4 WorldViewProj	: WORLDVIEWPROJECTION;
float4 MatAmbient 		: AMBIENT;
float4 MatDiffuse		: DIFFUSE;
float4 MatSpecular		: SPECULAR;
float MatPower			: SPECULARPOWER;
Texture Tex0			: DIFFUSEMAP;
bool UseTexture			: USEDIFFUSEMAP;

sampler ColoredTextureSampler =
sampler_state
{
	texture = <Tex0>;
	AddressU  = WRAP;        
	AddressV  = WRAP;
	MAGFILTER = LINEAR;
	MINFILTER = ANISOTROPIC;
	MIPFILTER = LINEAR;
};
//--

//-- Lighting
float4 CamPos;
float3 LightDir = float3(-0.22808579f, -0.76028591f, 0.60822874f);
float4 LightAmbient = float4(0.35,0.35,0.35,1);
float4 LightColour = float4(0.7, 0.68, 0.63, 1);
float Spec = 1.0;
//--

//--Shadows
Texture TexShadowHi;
Texture TexShadowMed;
Texture TexShadowLo;
float4x4 LightViewProjHi;
float4x4 LightViewProjMed;
float4x4 LightViewProjLo;

sampler ShadowTextureSamplerHi =
sampler_state
{
	texture = <TexShadowHi>;
	AddressU  = CLAMP;        
	AddressV  = CLAMP;
	MAGFILTER = LINEAR;
	MINFILTER = ANISOTROPIC;
	MIPFILTER = LINEAR;
};
sampler ShadowTextureSamplerMed =
sampler_state
{
	texture = <TexShadowMed>;
	AddressU  = CLAMP;        
	AddressV  = CLAMP;
	MAGFILTER = LINEAR;
	MINFILTER = ANISOTROPIC;
	MIPFILTER = LINEAR;
};
sampler ShadowTextureSamplerLo =
sampler_state
{
	texture = <TexShadowLo>;
	AddressU  = CLAMP;        
	AddressV  = CLAMP;
	MAGFILTER = LINEAR;
	MINFILTER = ANISOTROPIC;
	MIPFILTER = LINEAR;
};
//--

//-- Misc
float Thresholds[2] = { 0.7, 0.4 };
float BrightnessLevels[3] = { 1.0,0.8,0.5 };
float BlendFactor = 0.05;
float RenderDepth;
float GlobalAlpha = 1;
//--

float chebyshevUpperBound(float2 shadowCoord, float depth, int level)
{
	float2 moments;
	float varianceMax = 0;
	if(level == 1) {
		moments = tex2D(ShadowTextureSamplerHi, shadowCoord).rg;
		varianceMax = 0.0001;
	}
	else if(level == 2) {
		moments = tex2D(ShadowTextureSamplerMed, shadowCoord).rg;
		varianceMax = 0.0001;
	}
	else {
		moments = tex2D(ShadowTextureSamplerLo, shadowCoord).rg;
		varianceMax = 0.00002;
	}
		
	if (depth <= moments.x)
		return 1.0 ;
	
	float variance = moments.y - (moments.x*moments.x);
	variance = max(variance,varianceMax);
	
	float d = depth - moments.x;
	float p_max = min(variance / (variance + d*d), 1);
	
	return p_max;
}

float LightBleedingReduction(float p_max, float Reduction)
{
	return clamp((p_max-Reduction) / (1-Reduction), 0, 1);
}
	

VSOutput BlinnPhong_VS(VSInput input)
{
	VSOutput output = (VSOutput)0;
	output.Position = mul(input.Position, WorldViewProj);
	output.TexCoord = input.TexCoord;

	output.Normal = normalize( mul(input.Normal, (float3x3)World) );
	float4 WorldPos = mul(input.Position, World);
	output.Eye = normalize(CamPos - WorldPos).xyz;
	output.Half = normalize( -LightDir + output.Eye );
	output.WorldPosition = WorldPos;
	output.Depth = output.Position.zw;

	return output;
}

float4 BlinnPhong_PS(VSOutput input) : COLOR0
{
	input.Normal = normalize(input.Normal);
	input.Half = normalize(input.Half);
	input.Eye = normalize(input.Eye);

	float pixelDepth = input.Depth.x/input.Depth.y;
	if(pixelDepth > RenderDepth)
	{
		return float4(0,0,0,0);
	}

	float lightIntensity = 1;
	float4 shadowPos = mul(input.WorldPosition, LightViewProjHi);
	shadowPos /= shadowPos.w;
	int level = 1;
	if(shadowPos.x*shadowPos.x > 1 || shadowPos.y*shadowPos.y > 1)
	{
		level = 2;
		shadowPos = mul(input.WorldPosition, LightViewProjMed);
		shadowPos /= shadowPos.w;
		if(shadowPos.x*shadowPos.x > 1 || shadowPos.y*shadowPos.y > 1)
		{
			level = 3;
			shadowPos = mul(input.WorldPosition, LightViewProjLo);
			shadowPos /= shadowPos.w;
			if(shadowPos.x*shadowPos.x > 1 || shadowPos.y*shadowPos.y > 1)
				level = 0;
		}
	}
	
	if(level > 0)
	{
		float shadowDepth = shadowPos.z - 0.0001;
		shadowPos.x = shadowPos.x*0.5 + 0.5;
		shadowPos.y = -shadowPos.y*0.5 + 0.5;
		lightIntensity = chebyshevUpperBound(shadowPos.xy, shadowDepth, level);
		//lightIntensity = LightBleedingReduction(lightIntensity, 0.00);
	}

	float4 Diffuse = MatDiffuse;
	if(UseTexture)
	{
		Diffuse = tex2D(ColoredTextureSampler, input.TexCoord);
		Diffuse.a += MatDiffuse.a;
	}

	
	float4 Ia = MatAmbient * Diffuse * LightAmbient;
	float4 FinalColour = Ia;
	FinalColour.a = Diffuse.a;

	if(lightIntensity > 0)
	{
		float4 Id = LightColour * saturate(dot(input.Normal, -LightDir));
		float4 Is = MatSpecular * LightColour * pow( saturate(dot(input.Normal, input.Half)), MatPower) * Spec;

		const float4 luminance = float4(0.3, 0.59, 0.11, 0);
		float len = dot(Id+Is, luminance);
		if(len > Thresholds[0])
		{
			lightIntensity *= BrightnessLevels[0];
		}
		else if(len > Thresholds[1])
		{
			float offset = Thresholds[0]-(Thresholds[0]*BlendFactor);
			float diff = saturate((len-offset) / (Thresholds[0]-offset));
			lightIntensity *= lerp(BrightnessLevels[1], BrightnessLevels[0], diff);
		}
		else
		{
			float offset = Thresholds[1]-(Thresholds[1]*BlendFactor);
			float diff = saturate((len-offset) / (Thresholds[1]-offset));
			lightIntensity *= lerp(BrightnessLevels[2], BrightnessLevels[1], diff);
		}
	
		FinalColour += (Diffuse*Id+Is)*lightIntensity;
		FinalColour.a = saturate(Diffuse.a + (Is.a * (Diffuse.a-MatDiffuse.a)));
	}

	float dropOff = ((1-RenderDepth) + 0.004) / RenderDepth;
	float distanceFade = lerp(0, 1, saturate((RenderDepth-pixelDepth)/dropOff));
	FinalColour.a *= distanceFade * GlobalAlpha;

	return FinalColour;
}

technique RPMLighting
{
	pass Pass0
	{
		VertexShader = compile vs_3_0 BlinnPhong_VS();
		PixelShader = compile ps_3_0 BlinnPhong_PS();
	}
}