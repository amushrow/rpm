struct VS_INPUT
{
	float4 position : POSITION;
	float2 texCoord : TEXCOORD0;
};

struct VS_OUTPUT_5
{
	float4 position : POSITION;
	float2 centerTap : TEXCOORD0;	
	float2 taps[4] : TEXCOORD1;
};

struct VS_OUTPUT_7
{
	float4 position : POSITION;
	float2 centerTap : TEXCOORD0;	
	float2 positiveTaps[3] : TEXCOORD1;
	float2 negativeTaps[3] : TEXCOORD4;		
};

struct VS_OUTPUT_9
{
	float4 position : POSITION;
	float2 centerTap : TEXCOORD0;	
	float4 positiveTaps[2] : TEXCOORD1;
	float4 negativeTaps[2] : TEXCOORD3;
};

float4x4 WorldViewProj	: WORLDVIEWPROJECTION;

Texture texTexture : DIFFUSEMAP;
sampler sampTexture = sampler_state {
	Texture = <texTexture>;
	AddressU = Clamp;
	AddressV = Clamp;
	MAGFILTER = LINEAR;
	MINFILTER = ANISOTROPIC;
	MIPFILTER = LINEAR;
};


static const float2 texelSize = float2(1.0/1280.0, 1.0/720.0);


static const float centerTapWeight_5 = 0.40262f;
static const float tapOffsets_5[4] = {-2.5f, -1.25f, 1.25f, 2.5f};
static const float4 tapWeights_5 = float4(0.05448868f, 0.2442013f, 0.2442013f, 0.05448868f);

static const float centerTapWeight_7 = 0.2386398f;
static const float tapOffsets_7[3] = {1.25f, 2.5f, 3.75f};
static const float3 tapWeights_7 = float3(0.2025027f, 0.1237355f, 0.05444194f);

static const float centerTapWeight_9 = 0.1630297f;
static const float tapOffsets_9[4] = {1.25f, 2.5f, 3.75f, 5};
static const float4 tapWeights_9 = float4(0.1522228f, 0.1239135f, 0.08793943f, 0.05440949f);

// -------------------------------------------------------------
// Vertex Shader
// -------------------------------------------------------------
VS_OUTPUT_5 Gaussian5_VS(const VS_INPUT IN, uniform float2 DIRECTION)
{
	VS_OUTPUT_5 OUT;
	OUT.position = mul(IN.position, WorldViewProj);
	OUT.centerTap = IN.texCoord;	

	for(int i=0; i<4; i++)
		OUT.taps[i] = IN.texCoord + tapOffsets_5[i] * DIRECTION * texelSize;
	
	return OUT;
}

VS_OUTPUT_7 Gaussian7_VS(const VS_INPUT IN, uniform float2 DIRECTION)
{
	VS_OUTPUT_7 OUT;
	OUT.position = mul(IN.position, WorldViewProj);
	OUT.centerTap = IN.texCoord;	

	for(int i=0; i<3; i++)
	{
		OUT.positiveTaps[i] = IN.texCoord + tapOffsets_7[i] * DIRECTION * texelSize;
		OUT.negativeTaps[i] = IN.texCoord - tapOffsets_7[i] * DIRECTION * texelSize;
	}		
	
	return OUT;
}

VS_OUTPUT_9 Gaussian9_VS(const VS_INPUT IN, uniform float2 DIRECTION)
{
	VS_OUTPUT_9 OUT;
	OUT.position = mul(IN.position, WorldViewProj);
	OUT.centerTap = IN.texCoord;	

	for(int i=0; i<2; i++)
	{
		OUT.positiveTaps[i].xy = IN.texCoord + tapOffsets_9[i*2] * DIRECTION * texelSize;
		OUT.negativeTaps[i].xy = IN.texCoord - tapOffsets_9[i*2] * DIRECTION * texelSize;
		OUT.positiveTaps[i].zw = IN.texCoord + tapOffsets_9[i*2+1] * DIRECTION * texelSize;
		OUT.negativeTaps[i].zw = IN.texCoord - tapOffsets_9[i*2+1] * DIRECTION * texelSize;			
	}		
	
	return OUT;
}

float4 Gaussian5_PS(VS_OUTPUT_5 IN) : COLOR
{
	float4x4 samples;

	float4 color = tex2D(sampTexture, IN.centerTap) * centerTapWeight_5;
	for(int i=0; i<4; i++)
		samples[i] = tex2D(sampTexture, IN.taps[i]);
	
	color += mul(tapWeights_5, samples);
	return color;
}

float4 Gaussian7_PS(VS_OUTPUT_7 IN) : COLOR
{
	float3x4 positiveSamples;
	float3x4 negativeSamples;

	float4 color = tex2D(sampTexture, IN.centerTap) * centerTapWeight_7;
	for(int i=0; i<3; i++)
	{
		positiveSamples[i] = tex2D(sampTexture, IN.positiveTaps[i]);
		negativeSamples[i] = tex2D(sampTexture, IN.negativeTaps[i]);
	}

	color += mul(tapWeights_7, positiveSamples) + mul(tapWeights_7, negativeSamples);	
	return color;
}

float4 Gaussian9_PS(VS_OUTPUT_9 IN) : COLOR
{
	float4x4 positiveSamples;
	float4x4 negativeSamples;

	float4 color = tex2D(sampTexture, IN.centerTap) * centerTapWeight_9;
	for(int i=0; i<2; i++)
	{
		positiveSamples[i*2] = tex2D(sampTexture, IN.positiveTaps[i].xy);
		negativeSamples[i*2] = tex2D(sampTexture, IN.negativeTaps[i].xy);
		positiveSamples[i*2+1] = tex2D(sampTexture, IN.positiveTaps[i].zw);
		negativeSamples[i*2+1] = tex2D(sampTexture, IN.negativeTaps[i].zw);		
	}
	
	color += mul(tapWeights_9, positiveSamples) + mul(tapWeights_9, negativeSamples);	
	return color;
}

technique Gaussian5
{
	pass HBlur
	{
		VertexShader = compile vs_3_0 Gaussian5_VS(float2(1, 0));
		PixelShader = compile ps_3_0 Gaussian5_PS();
	}
	pass VBlur
	{
		VertexShader = compile vs_3_0 Gaussian5_VS(float2(0, 1));
		PixelShader = compile ps_3_0 Gaussian5_PS();
	}		
}

technique Gaussian7
{
	pass HBlur
	{
		VertexShader = compile vs_3_0 Gaussian7_VS(float2(1, 0));
		PixelShader = compile ps_3_0 Gaussian7_PS();
	}
	pass VBlur
	{
		VertexShader = compile vs_3_0 Gaussian7_VS(float2(0, 1));
		PixelShader = compile ps_3_0 Gaussian7_PS();
	}		
}

technique Gaussian9
{
	pass HBlur
	{
		VertexShader = compile vs_3_0 Gaussian9_VS(float2(1, 0));
		PixelShader = compile ps_3_0 Gaussian9_PS();
	}
	pass VBlur
	{
		VertexShader = compile vs_3_0 Gaussian9_VS(float2(0, 1));
		PixelShader = compile ps_3_0 Gaussian9_PS();
	}		
}