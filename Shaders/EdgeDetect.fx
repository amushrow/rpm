//-- Structures
struct VSInput
{
	float4 Position	: POSITION0;
	float3 Normal	: NORMAL0;
	float2 TexCoord	: TEXCOORD0;
};

struct VSOutput
{
	float4 Position	: POSITION0;
	float4 Colour	: COLOR0;
	float2 TexCoord	: TEXCOORD0;
	float3 Eye		: TEXCOORD2;
};
//--

float2 screenResolution;
float4x4 WorldViewProj		: WORLDVIEWPROJECTION;

Texture normalTexture;
sampler NormalSampler = sampler_state
{
	Texture = <normalTexture>;
	MAGFILTER = LINEAR;
	MINFILTER = LINEAR;
	MIPFILTER = LINEAR;
};

VSOutput EdgeDetectVertex(VSInput input)
{
	VSOutput output = (VSOutput)0;

	output.Position = mul(input.Position, WorldViewProj);
	output.TexCoord = input.TexCoord - float2(0.5 / screenResolution.x, 0.5 / screenResolution.y);

	return output;

}

VSOutput EdgeDetectVertex_NoShift(VSInput input)
{
	VSOutput output = (VSOutput)0;

	output.Position = mul(input.Position, WorldViewProj);
	output.TexCoord = input.TexCoord;

	return output;

}

float4 EdgeDetectPixel(float2 texCoord : TEXCOORD0) : COLOR0
{
	const float edgeWidth = 1;
	const float edgeIntensity = 1;
	const float normalSensitivity = 0.4;
	const float depthSensitivity = 0.6;
	const float normalThreshold = 0.4;
	const float depthThreshold = 0.025;

	float2 edgeOffset = edgeWidth / screenResolution;
	
	//-- 9 Samples
	float4 diagonalDelta = (float4)0;
	float4 sample =  tex2D(NormalSampler, texCoord);
	float3 original_norm = sample.xyz;
	float original_depth = sample.w;
	for(int i=-1; i<2; i++)	{
		for(int j=-1; j<2; j++) {
			sample = tex2D(NormalSampler, texCoord + float2(i, j)*edgeOffset);
			diagonalDelta.xyz += abs(original_norm - sample.xyz);
			diagonalDelta.w += abs(original_depth - sample.w);
		}
	}
	//--

	float normalDelta = dot(diagonalDelta.xyz, 1);
	float depthDelta = diagonalDelta.w;

	
	normalDelta = saturate((normalDelta - normalThreshold) * normalSensitivity);
	depthDelta = saturate((depthDelta - depthThreshold) * depthSensitivity);

	float edgeAmount = saturate( (normalDelta + depthDelta) * edgeIntensity );
	return float4(0,0,0,edgeAmount);
}

float4 EdgeDetectPixel_Opt(float2 texCoord : TEXCOORD0) : COLOR0
{
	const float edgeWidth = 1.25;
	const float edgeIntensity = 1;
	const float normalSensitivity = 0.25;
	const float depthSensitivity = 0.6;
	const float normalThreshold = 0.4;
	const float depthThreshold = 0.025;

	float2 edgeOffset = edgeWidth / screenResolution;
	
	//-- 9 Samples
	float4 diagonalDelta = (float4)0;
	float4 sample =  tex2D(NormalSampler, texCoord);
	float3 original_norm = sample.xyz;
	float original_depth = sample.w;
	for(int i=-1; i<2; i++)	{
		for(int j=-1; j<2; j++) {
			sample = tex2D(NormalSampler, texCoord + float2(i, j)*edgeOffset);
			diagonalDelta.xyz += abs(original_norm - sample.xyz);
			diagonalDelta.w += abs(original_depth - sample.w);
		}
	}
	//--

	float normalDelta = dot(diagonalDelta.xyz, 1);
	float depthDelta = diagonalDelta.w;
	
	normalDelta = saturate((normalDelta - normalThreshold) * normalSensitivity);
	depthDelta = saturate((depthDelta - depthThreshold) * depthSensitivity);

	float edgeAmount = saturate( (normalDelta + depthDelta) * edgeIntensity );
	return float4(0,0,0,edgeAmount);
}



technique EdgeDetect
{
	pass Pass0
	{
		AlphaBlendEnable = true;
		VertexShader = compile vs_3_0 EdgeDetectVertex();
		PixelShader = compile ps_3_0 EdgeDetectPixel();
	}
}

technique EdgeDetect_NoFiltering
{
	pass Pass0
	{
		AlphaBlendEnable = true;
		VertexShader = compile vs_3_0 EdgeDetectVertex_NoShift();
		PixelShader = compile ps_3_0 EdgeDetectPixel_Opt();
	}
}