struct VS_INPUT
{
	float4 position : POSITION;
	float2 texCoord : TEXCOORD;
};
struct VS_OUTPUT
{
	float4 position : POSITION;
	float2 texCoord : TEXCOORD;	
};

float4x4 WorldViewProj	: WORLDVIEWPROJECTION;

texture texBloom;
sampler sampBloom = sampler_state
{
	Texture = (texBloom);
	AddressU = CLAMP;
	AddressV = CLAMP;	
};

texture texScene;
sampler sampFrameBuffer = sampler_state {
	Texture = texScene;
	AddressU = CLAMP;
	AddressV = CLAMP;
};

float sceneIntensity = 1;

VS_OUTPUT VS(const VS_INPUT IN)
{
	VS_OUTPUT OUT;
	
	OUT.position = IN.position;
	OUT.texCoord = IN.texCoord;

	return OUT;
}

float4 PS(VS_OUTPUT IN) : COLOR
{
	float3 bloomSample = tex2D(sampBloom, IN.texCoord).rgb;
	float3 frameBufferSample = tex2D(sampFrameBuffer, IN.texCoord).rgb;
	return float4(lerp(bloomSample, bloomSample + frameBufferSample, sceneIntensity), 1);
}

technique CombineBloom
{
	pass Pass0
	{
		VertexShader = compile vs_3_0 VS();
		PixelShader = compile ps_3_0 PS();
	}
}