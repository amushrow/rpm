#include "CubicSpline.h"
#include <ObjBase.h>

StaticGuid::StaticGuid(WCHAR* guid)
{
	CLSIDFromString(guid, &Guid);
}

DTCubicSpline::DTCubicSpline()
{
	m_propDesc = new PropDesc(this);
	m_propDesc->Name = "Cubic Spline";
	m_propDesc->TypeName = "Cubic Spline";
	m_propDesc->EditDialog = false;
	m_propDesc->TextEdit = false;
}

DTCubicSpline::~DTCubicSpline()
{
	delete m_propDesc;
}

std::string DTCubicSpline::GetTypeName()
{
	return m_propDesc->TypeName;
}

GUID DTCubicSpline::GetTypeGUID()
{
	return CubicSplineGUID.Guid;
}

std::string DTCubicSpline::GetName()
{
	return m_propDesc->Name;
}

void DTCubicSpline::SetName(std::string name)
{
	m_propDesc->Name = name;
}

std::string DTCubicSpline::GetStringValue()
{
	return "[Cubic Spline]";
}

bool DTCubicSpline::SetValue(std::string value)
{
	return false;
}

int DTCubicSpline::GetID()
{
	return m_id;
}

void DTCubicSpline::SetID(int id)
{
	m_id = id;
}

DataLib::LPPROPDESC DTCubicSpline::GetProperty()
{
	return m_propDesc;
}

int DTCubicSpline::GetSubProperties(DataLib::LPPROPDESC** Props)
{
	*Props = NULL;
	return 0;
}

void DTCubicSpline::ShowEditForm()
{

}

void DTCubicSpline::WriteStream(DataLib::IStream* stream)
{
	SKMath::Point<real>* points;
	int numPoints = m_spline.GetPoints(&points);
	stream->WriteInt(numPoints);
	if(numPoints > 0)
	{
		for(int i=0; i<numPoints; i++)
		{
			stream->WriteFloat((float)points[i].X);
			stream->WriteFloat((float)points[i].Y);
		}
		delete[] points;
	}
}

void DTCubicSpline::ReadStream(DataLib::IStream* stream)
{
	int numPoints = stream->ReadInt();
	if(numPoints > 0)
	{
		SKMath::Point<real>* points = new SKMath::Point<real>[numPoints];
		for(int i=0; i<numPoints; i++)
		{
			points[i].X = stream->ReadFloat();
			points[i].Y = stream->ReadFloat();
		}

		m_spline.SetPoints(points, numPoints);

		delete[] points;
	}
}

const void* DTCubicSpline::GetValuePtr()
{
	return &m_spline;
}

void DTCubicSpline::SetValue(const SKMath::CubicSpline<real>& spline)
{
	m_spline = spline;
}

SKMath::CubicSpline<real>& DTCubicSpline::GetValue()
{
	return m_spline;
}