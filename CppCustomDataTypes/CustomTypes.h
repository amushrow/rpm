#pragma once
#include "..\VehicleSimDemo\DataLib.h"

class StaticGuid
{
public:
	StaticGuid(WCHAR* guid);
	GUID Guid;
};

static StaticGuid VehicleDescriptionGuid(L"{EE99E3C4-EBCE-4749-A1B2-9F7839FFFDCA}");

enum VehicleDescription
{
	Mass = 1,
	Brake_Balance = 2,
	Brake_Force = 3,
	Engine_Efficiency = 4,
	Max_Steering_Angle = 5,
	Ackerman_Value = 6,
	Drag_Coefficient = 7,
	Centre_of_Mass = 8,
	Torque_Curve = 9,
	FrontLeft = 10,
	FrontRight = 11,
	RearLeft = 12,
	RearRight = 13,
	Gears = 14,
	Friction = 15
};

enum GameObjects
{
	Lotus_Elise_SC = 1
};