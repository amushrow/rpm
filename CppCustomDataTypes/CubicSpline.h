#pragma once
#include "CustomTypes.h"
#include <SKMath.h>

#ifdef USE_DOUBLE_PRECISION
typedef double real;
#else
typedef float real;
#endif

static StaticGuid CubicSplineGUID(L"{8B702777-808F-4038-A2BE-8EF715F4E9F3}");

class PropDesc : public DataLib::IPropertyDescriptor
{
public:
	PropDesc(DataLib::IDataType* data)
	{
		m_data = data;
	}

	virtual bool				CanTextEdit() { return TextEdit; }
	virtual bool				HasEditDialog() { return EditDialog; }
	virtual DataLib::IDataType*	GetIDataType() { return m_data; }
	virtual std::string			GetName() { return Name; }
	virtual std::string			GetTypeName() { return TypeName; }

	bool TextEdit;
	bool EditDialog;
	std::string Name;
	std::string TypeName;

private:
	DataLib::IDataType* m_data;
};

class DTCubicSpline : public DataLib::IDataType
{
public:
	DTCubicSpline();
	~DTCubicSpline();

	virtual std::string			GetTypeName();
	virtual GUID				GetTypeGUID();
	virtual std::string			GetName();
	virtual void				SetName(std::string name);
	virtual std::string			GetStringValue();
	virtual const void*			GetValuePtr();
	virtual bool				SetValue(std::string value);
	virtual int					GetID();
	virtual void				SetID(int id);
	virtual DataLib::LPPROPDESC	GetProperty();
	virtual int					GetSubProperties(DataLib::LPPROPDESC** Props);
	virtual void				ShowEditForm();
	virtual void				WriteStream(DataLib::IStream* stream);
	virtual void				ReadStream(DataLib::IStream* stream);

	SKMath::CubicSpline<real>&	GetValue();
	void						SetValue(const SKMath::CubicSpline<real>& spline);

private:
	int m_id;
	PropDesc* m_propDesc;
	SKMath::CubicSpline<real> m_spline;
};

class SplineContainer : public DataLib::ITypeContainer
{
	virtual std::string			GetName()
	{
		return "Cubic Spline";
	}
	virtual std::string			GetDescription()
	{
		return "Piecewise cubic spline";
	}
	virtual GUID				GetGUID()
	{
		return CubicSplineGUID.Guid;
	}
	virtual DataLib::IDataType*	CreateInstance()
	{
		return new DTCubicSpline();
	}
};